<?php
	// Enter your name or company name below
	$receiver_name = "Company";
	
	// Enter your message subject below
	$receiver_subject = "New contact message"; 
	
	// The main email address for receiving the form message
	//In case no department is selected, the email will be sent to this address 
	$receiver_email = 'ryan@steelpixel.co.za';
	
	// If you want to store all form data in a CSV file 
	// Change the generateCSV option from (false) to (true)
	$generateCSV = false;	
	
	// Name for generated CSV file 
	// Please don't change this name unless you have to
	$csvFileName = "formcsv.csv";
	
	// If you want to automatically reply to the sender 
	// Change the autoresponder option below from (false) to (true)
	$autoResponder = false;

	/* If you want to redirect to another page after sending the form
	 * Change the autoRedirectForm option below from (false) to (true)
	 * Then add your redirect page URL replace - http://example.com/thankyou.php
	----------------------------------------------------------------------------- */	
	$autoRedirectForm = false;
	$autoRedirectForm_URL = "http://example.com/thankyou.php";
?>