<?php 
  if (!isset($_SESSION)) session_start(); 
  if(!$_POST) exit;

  $sender_name = "CodeJika";
  $sender_email = 'contact@codejika.com';
  $receiver_name = "CodeJika";
  $receiver_email = 'sibusiso@codejika.com';
  //$reply_name = "CodeJika";
  //$reply_email = 'jika@code4change.co.za';
  $noreply_email = 'noreply@codejika.com';
  $generateCSV = true; 
  $csvFileName = "submit-lesson-url.csv";

  $user_ID = strip_tags(trim($_POST["user_ID"]));
  $date_submitted = date("d/m/Y");    
  $firstname = strip_tags(trim($_POST["first_name"]));  
  $lastname = strip_tags(trim($_POST["last_name"]));
  $DOB = strip_tags(trim($_POST["day"])) . "/" . strip_tags(trim($_POST["month"])) . "/" . strip_tags(trim($_POST["year"]));
  $email_address = $_POST["email_id"] ? strip_tags(trim($_POST["email_id"])) : $noreply_email ; // if user doesn't have email address then use noreply_email as sender
  $mobile = strip_tags(trim($_POST["mobile_number"]));
  $lesson_ID = strip_tags(trim($_POST["lesson_ID"]));
  $submitted_link = strip_tags(trim($_POST["submitted_link"]));
  $submitted_code = $_POST["submitted_code"];
  
  $receiver_subject = "Code submission for: " . strip_tags(trim($_POST["lesson_ID"]));
  $reply_name = $firstname;
  $reply_email = $email_address;  
/*  ----------------------------------------------------------------------
  : Prepare form field variables for CSV export
  ----------------------------------------------------------------------- */  
  if($generateCSV == true){
    $csvFile = $csvFileName;  
    $csvData = array(
      $user_ID,
      $date_submitted,
      $firstname,
      $lastname,
      $DOB,
      $email_address,
      $mobile,
      $lesson_ID,
      $submitted_link,
      htmlentities($submitted_code)
    );
  }


  $errors = array();
    
  /* output all errors as a list 
  -------------------------------------- */ 
  if ($errors) {
    $errortext = "";
    foreach ($errors as $error) {
      $errortext .= $error . "\n";
    }
    echo 'Errors: '. $errortext;
  
  } else{ 

      include dirname(__FILE__).'/phpmailer/PHPMailerAutoload.php';
      include dirname(__FILE__).'/templates/submit-lesson-url_template.php';      
        
      $mail = new PHPMailer();
      $mail->isSendmail();

      /*
      $mail->isSMTP();  // Set mailer to use SMTP
      $mail->Host = 'smtp.domain-name.com'; // Specify main and backup SMTP servers
      $mail->SMTPAuth = true; // Enable SMTP authentication
      $mail->Username = 'esername'; // SMTP username
      $mail->Password = 'password'; // SMTP password
      $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587; // TCP port to connect to
      */

      $mail->IsHTML(true);
      $mail->From = $sender_email;
      $mail->CharSet = "UTF-8";
      $mail->FromName = $sender_name;
      $mail->Encoding = "base64";
      $mail->Timeout = 200;
      $mail->ContentType = "text/html";
      $mail->AddReplyTo($reply_email, $reply_name);
      $mail->addAddress($receiver_email, $receiver_name);
      $mail->Subject = $receiver_subject; 
      $mail->Body = $email_message;
      $mail->AltBody = "Please use an HTML compatible email client";

      
    /*  -------------------------------------------------------------------
      : Prepare sending to multiple  addresses / recepients if true
      ------------------------------------------------------------------- */
      // If you want the form to be BCC emailed to other addresses
      // Change the extra_recipients option below from (false) to (true)
      // Then enter email addresses with corresponding names seperated by comas
      // For example "john@example.com" => "John", "jack@example.com" => "Jack", "jeny@example.com" => "Jeny"
      // echo $email_message;
      // echo $automessage;
      $recipients = true;
      if($recipients == true){
        $recipients = array(
            "codejika@steelpixel.co.za" => "Ryan"
            //"example@domain.com" => "Zapier"
        );
        
        foreach($recipients as $email => $name){
          $mail->AddBCC($email, $name);
        } 
      }     
      
      if($mail->Send()) {
      
      /*  -----------------------------------------------------------------
        : Generate the CSV file and post values if its true
        ----------------------------------------------------------------- */    
        if($generateCSV == true){ 
          if (file_exists($csvFile)) {
            $csvFileData = fopen($csvFile, 'a');
            fputcsv($csvFileData, $csvData );
          } else {
            $csvFileData = fopen($csvFile, 'a'); 
            $headerRowFields = array(
              'User ID',   
              'Date Submitted',
              'First Name',
              'Last Name',
              'Date of Birth',
              'Email',
              'Mobile',
              'Lesson ID',      
              'Submitted Link',     
              'Submitted Code'     
            );
            fputcsv($csvFileData,$headerRowFields);
            fputcsv($csvFileData, $csvData );
          }
          fclose($csvFileData);
        }
        
        echo 'Message has been sent successfully!';
      } 
      else {
          echo 'Message failed to send: ' . $mail->ErrorInfo;
      }
  }
?>

