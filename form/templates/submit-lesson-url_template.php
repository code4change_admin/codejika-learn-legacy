<?php  
$email_message = '
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CodeJika - Project Submitted</title>    
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    <center>
        <table style="padding:15px 15px;background:#F4F4F4;width:100%;font-family:arial" cellpadding="0" cellspacing="0">
                
                <tbody>
                    <tr>
                        <td>
                        
                            <table style="max-width:540px;min-width:320px" align="center" cellspacing="0">
                                <tbody>
                                
                                    <tr>
                                        <td style="background:#fff;border:1px solid #D8D8D8;padding:15px 30px" align="center">
                                        
                                            <table align="center">
                                                <tbody>
                                                
                                                    <tr>
                                                        <td style="border-bottom:1px solid #D8D8D8;color:#666;text-align:center;padding-bottom:15px">
                                                            
                                                            <table style="margin:auto" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="color:#005f84;font-size:22px;font-weight:bold;text-align:center;font-family:arial">
                                                                            PROJECT SUBMITTED
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                               <td style="color:#666;padding:15px; padding-bottom:0;font-size:14px;line-height:20px;font-family:arial;text-align:left">
                                    
                                                    <div style="font-style:normal;padding-bottom:15px;font-family:arial;line-height:20px;text-align:left">
                                                        <p><span style="font-weight:bold;font-size:16px">Date Submitted:</span> '.$date_submitted.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">User ID:</span> '.$user_ID.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Submitted on:</span> '.$user_ID.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">First Name:</span> '.$firstname.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Last Name:</span> '.$lastname.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Date of Birth:</span> '.$DOB.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Email:</span> '.$email_address.' </p>
                            														<p><span style="font-weight:bold;font-size:16px">Mobile:</span> '.$mobile.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Lesson ID:</span> '.$lesson_ID.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Submitted Link:</span> '.$submitted_link.' </p>
                                                        <p><span style="font-weight:bold;font-size:16px">Submitted Code:</span> <br><pre>'.htmlentities($submitted_code).' </pre></p>
                                                        
                                                      </div>
                                                            
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="background:#f9f9f9;border:1px solid #D8D8D8;border-top:none;padding:15px 10px" align="center">
                                            
                                            <table style="width:100%;max-width:650px" align="center">
                                                <tbody>
                                                
                                                    <tr>
                                                        <td style="font-size:14px;line-height:20px;text-align:center;max-width:650px">
        
                                                            <a href="#" style="text-decoration:none;color:#69696c" target="_blank">
                                                                <span style="color:#3B99D0;font-weight:bold;max-width:180px">POWERED BY:</span> 
                                                                CODEJIKA
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                            
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <table style="max-width:650px" align="center">
                                
                                <tbody>
                                    <tr>
                                        <td style="color:#b4b4b4;font-size:10px;padding-top:10px;line-height:15px;font-family:arial; 
	letter-spacing:1px;">
                                            <span> &copy; CODEJIKA ' . date("Y") . ' - CodeJika.com </span>
                                        </td>
            
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
            </tbody>
        </table>
    </center>
</body>
</html>';
?>