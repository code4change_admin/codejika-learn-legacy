//Set console feedback level
//console.debug(true);


// START OF CodeMirror
(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})

(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "html", function(text) {
    var found = [],
      message;
    if (!window.HTMLHint) return found;
    var messages = HTMLHint.verify(text, ruleSets);
    for (var i = 0; i < messages.length; i++) {
      message = messages[i];
      var startLine = message.line - 1,
        endLine = message.line - 1,
        startCol = message.col - 1,
        endCol = message.col;
      found.push({
        from: CodeMirror.Pos(startLine, startCol),
        to: CodeMirror.Pos(endLine, endCol),
        message: message.message,
        severity: message.type
      });
    }
    return found;
  });
});
// ruleSets for HTMLLint

var ruleSets = {
  "tagname-lowercase": true,
  "attr-lowercase": true,
  "attr-value-double-quotes": true,
  "doctype-first": false,
  "tag-pair": true,
  "spec-char-escape": true,
  "id-unique": true,
  "src-not-empty": true,
  "attr-no-duplication": true
};

// console.log(document.getElementById('myeditor').value);
// Initialize CodeMirror editor
var editor = CodeMirror.fromTextArea(document.getElementById('myeditor'), {
  mode: "htmlmixed",
  tabMode: "indent",
  theme: 'none',
  styleActiveLine: true,
  lineNumbers: true,
  lineWrapping: true,
  autoCloseTags: false,
  foldGutter: true,
  dragDrop: true,
  lint: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter", "CodeMirror-lint-markers"]
});
// console.log(editor);

// Live preview
editor.on('change', function() {
  clearTimeout(delay);
  delay = setTimeout(updatePreview, 300);
});

// END OF CodeMirror


setTimeout(updatePreview, 300);


// With HTML5 history API, we can easily prevent scrolling!
$('.nav-tabs a').on('shown.bs.tab', function(e) {
  if (history.pushState) {
    history.pushState(null, null, e.target.hash);
  } else {
    window.location.hash = e.target.hash; //Polyfill for old browsers
  }
})

function updateProgressBar(e, step) {
  console.log("active_slide: "+active_slide);
  var percent = (parseInt(active_slide) / total_slides) * 100;


  $('.progress-bar').css({
    width: percent + '%'
  });
  $('.lessonProgressText').text("Slide " + active_slide + " of " + total_slides);

  if (active_slide === 1) {
    $(".pagination .text-right .next").html('Start Slideshow').removeClass('disabled');
    $(".pagination .prev").addClass('disabled');
    // $(".pagination .prev").click(function(e) {
    //	e.preventDefault();
    // });
  } else if (active_slide === parseInt(total_slides)) {
    //$("#slide"+(active_slide)).append("<a class='btn btn-primary' href='GitHub/codejika/learn/nomzamos-website-01.php?lesson=P01-T001-D/nomzamos-website-01.php?lesson=P01-T001-D' style='top:65%;'>Start next training →</a></div>");

    $(".pagination .next").addClass('disabled');
    $(".pagination .prev").removeClass('disabled');
    // $(".pagination .next").click(function(e) {
    //	e.preventDefault();
    // });
  } else {
    $(".pagination .next").removeClass('disabled');
    $(".pagination .text-right .next").html('Next >').removeClass('disabled');
    $(".pagination .prev").removeClass('disabled');
  }
  //e.relatedTarget // previous tab

  //validateCheckpoint();
  //validatorLessonChallenges();
  //localStorage.setItem(lessson_url + '_' + auth_id + '_active_slide', active_slide);
 
}


function onStartDesktop() {
  //buildPage();
  //buildLessonSlides();
  //setInterval(saveUserData, 10000)

  swiperD = new Swiper('.swiper-container-d', {
    spaceBetween: 0,
    shortSwipes: true,
    mousewheel: true,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouXchMove: false,
    // Disable preloading of all images
    loop: false,
    updateOnWindowResize: true,
    preloadImages: false,
    // initialSlide: active_slide,
    // Enable lazy loading
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 5
    },
  //  observer: true,
   // observeParents: true,

    on: {
      init: function () { 
        if (typeof onlessonLoaded === 'function') {
            onlessonLoaded();
          }
        slide_data = lesson_data.slides[active_slide-1]
      },
      lazyImageReady: function () {
        //TODO not sure what this does but triggers JS error onload
        /*
        if (slide_data.lazy_function !== undefined) {
          var func = new Function(slide_data.lazy_function)
          func()
        }
        */
        /// /console.log('lazy_function triggered: ' + slide_data.lazy_function);
      },
      slideChange: function () {
        var slider = this
        // var mindex = slider.activeIndex+1;
        active_slide = slider.activeIndex + 1
        console.log("active_slide(slider.activeIndex): " + active_slide);
        localStorage.setItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide', active_slide)
        slide_data = lesson_data.slides[active_slide-1]
      //  console.log("dfs: "+ JSON.stringify(slide_data));
        var unlocked = $('#slide' + (active_slide)).hasClass('cp-unlocked')
        if (!unlocked && slide_data.reg !== undefined) {
          slider.allowSlideNext = false
         $('.next').addClass('d-none');
          console.log('lock' + slider.allowSlideNext + active_slide);
        } else {
          slider.allowSlideNext = true
          $('.next').removeClass('d-none');
          console.log('unlock' + active_slide);
        }
        // console.log("mindex: "+mindex);
        /// /console.log("active_slide: " + active_slide);

        $('.swiper-pagination-d span:nth-child(' + (active_slide + 1) + ')')
          .prevAll()
          .addClass('bullet-green')
        $('.swiper-pagination-d span:nth-child(' + (active_slide + 1) + ')')
          .next()
          .removeClass('bullet-green')
        validateCheckpoint()
        updateProgressBar("lessonProgressBar", active_slide);
        saveUserData()
        // ga('send', 'pageview', 'CJ Lesson', 'swipe', lessson_url, active_slide + 1);
        ga('send', {
          hitType: 'event',
          eventCategory: lessson_url,
          eventAction: 'Slide swipe',
          eventLabel: active_slide + 1
        })
        console.log('Slide swipe');
      }
    }
  })

  swiperLesson = document.querySelector('.swiper-container-d').swiper

  swiperD.allowTouchMove = false


  $('.swiper-prev, .prev').click(function () {
    swiperLesson.slidePrev()
    // swiperLesson.allowSlideNext = false;
    console.log("slidePrev");
  })

  $('.swiper-next, .next').click(function () {
    swiperLesson.allowSlideNext = true
    swiperLesson.slideNext()
    // swiperLesson.allowSlideNext = false;
    console.log("slideNext: " + swiperLesson.allowSlideNext);
  })

  initColResize();
  loadUserData();

  console.log("slideTo active_slide: " + active_slide);
  swiperLesson.slideTo(active_slide - 1)
  // active_slide = 0;

  mapButtons();

}


$(document).ready(function() {
  
  /* 
  initFirebase();

  var getGallery = getGalleryData('', '')
  var getLessonProgress = getLessonProgressPromise();
  var getPrevSubmitCode = getPrevSubmitCodePromise();
  Promise.all([getLessonProgress, getGallery, getPrevSubmitCode]).then(function(results) {
    lesson_progress = results[0];
    get_prev_submit_code = results[2];

    //if no lesson data then set default values to DB
    if (lesson_progress === null) {
      console.log("no lesson progress found:" + JSON.stringify(lesson_progress));
      lesson_progress = {
        "last_checkpoint": "0",
        //"user_code": "",
        "progress_completed": "0"
      }
     //saveUserData(true);
      console.log("default lesson_progress inserted");
    }

  console.log("get_prev_submit_code: "+JSON.stringify(get_prev_submit_code));
    if (get_prev_submit_code !== null) {
      console.log("get_prev_submit_code found");
      prev_submit_code = get_prev_submit_code.submit_code;
    } else {
      prev_submit_code = null;
    }

    //str = JSON.stringify(lesson_progress, null, 4);
    //console.log("default lesson_progress:\n"+str);

    console.log("promise all is true, now running onStart");

    onStart();
    checkUserKey();
  });
  */
  
  $(window).resize(function() {
    slideScale();
  });


 


      
  
  $("#lesson_tab .next").click(function(e) {
    var $active = $('#lesson_tab .nav-tabs li>a.active');
    $active.parent().next().removeClass('disabled');
    
    ga('send', {
      hitType: 'event',
      eventCategory: lessson_url,
      eventAction: 'Slide swipe',
      eventLabel: current_page
    })
    console.log('Slide swipe (Next)');
  });

  $("#lesson_tab .prev").click(function(e) {
    var $active = $('#lesson_tab .nav-tabs li>a.active');
    
    ga('send', {
      hitType: 'event',
      eventCategory: lessson_url,
      eventAction: 'Slide swipe',
      eventLabel: current_page
    })
    console.log('Slide swipe (Previous)');
  });

 
  
});

function homePage() {
  // console.log('home')
  $("#main_page").show();
  $("#gallery_section").hide();
  $("#menu_page").hide();
}

function galleryPage() {
  // console.log('galery')
  $("#main_page").hide();
  $("#menu_page").hide();
  $("#gallery_section").show();
  $("#menu_page").hide();
  // addToGallery();
}

function menuPage() {
  $("#gallery_section").hide();
  $("#main_page").hide();
  $("#menu_page").show();
}




// document.getElementById('pageNumber2').innerHTML = current_page + 1




// **************** popup modal **********************


// *************** pagination code ***********
function hideButton() {
  if (current_page == 0) {
    document.getElementById('previous').style.visibility = 'hidden'
  }
}

// START OF DESKTOP ONLY 
var slide_ratio = ".5625";
var default_col_width = 50;
var default_font_style_width = 500;
var min_slides_width = 400;
var max_slides_width = 800;
var restore_sizes = localStorage.getItem('split-sizes')
var lesson_code_width;
var slides_height;


function slideScale() {
  lesson_code_width = $(".col-lesson-code").width();
  slides_height = lesson_code_width * slide_ratio;

  swiperLesson.update();

  font_scale = lesson_code_width * 100 / default_font_style_width;
  $(".swiper-container-d .swiper-slide").css("font-size", font_scale + '%');
  $(".previous-overlay-btn-container, .next-overlay-btn-container, .swiper-container-d .swiper-slide, .swiper-container-d .swiper-wrapper").css("height", slides_height + 'px');
  $(".is-desktop .code-editor").css("height", 'calc(100% - 30px - ' + slides_height + 'px)');

  //console.log("font_scale eq: "+ lesson_code_width +" * 100 / " + default_font_style_width);
  //console.log("font_scale: " + font_scale)
  //console.log("lesson_code_width: " + lesson_code_width);
  //console.log("slides_height: " + slides_height);
}

function initColResize() {

  if (restore_sizes) {
    restore_sizes = JSON.parse(restore_sizes)
    //console.log("restore_sizes local: " +restore_sizes)
  } else {
    restore_sizes = [default_col_width, 100 - default_col_width] // default sizes
    console.log("restore_sizes default: " +restore_sizes)
  }

  var split = Split(['.col-lesson-code', '.col-preview'], {
    snapOffset: 0, //turn off snap
    sizes: restore_sizes,
    dragInterval: 20,

    minSize: [min_slides_width],
    // expanedToMin: true,
    onDrag: function() {
        slideScale();
    },
    onDragEnd: function(sizes) {
      localStorage.setItem('split-sizes', JSON.stringify(sizes))
    },
    elementStyle: function(dimension, size, gutterSize) {
      return {
        'flex-basis': 'calc(' + size + '% - ' + gutterSize + 'px)',
        //'height': 'calc(' + size + '% - ' + gutterSize + 'px /)',
        /*'width': 'calc(' + size + '% - ' + gutterSize + 'px)',
        'min-width': 'calc(' + size + '% - ' + gutterSize + 'px)',
        'max-width': 'calc(' + size + '% - ' + gutterSize + 'px)'*/
      }
    },
    gutterStyle: function(dimension, gutterSize) {
      return {
        'flex-basis': gutterSize + 'px'
      }
    }
  });

  slideScale();

  $(".toggle").click(function() {
    $(".resizer-parent").toggleClass("open");
    $(".col-lesson-code").removeAttr("style");
    $(".col-preview").removeAttr("style");
  });
}

// END OF DESKTOP ONLY 
