console.debug(true);
// MOBILE ONLY VARIABLES
var subModal= false; // TODO rewite code?
var screenHeight = screen.height + "px";
var screenHeight_plus23 = screen.height + "px";
var initialX = null;
var initialY = null;
var is_keyboard = false;
var initial_screen_size = window.innerHeight;



/// START OF moved from lesson-m.php      
    window.addEventListener("resize", function() {
      is_keyboard = (window.innerHeight < initial_screen_size);

      console.log(is_keyboard, 'Here');
      if (!is_keyboard) {
        $('textarea').blur();
      }
    }, false);


    function btclicked(symb) {
      insertAtCaret(document.getElementById('editor'), symb);
      console.log("Hello");
    }

    function insertAtCaret(element, text) {
      if (document.selection) {
        console.log('Hello');
        element.focus();
        var sel = document.selection.createRange();
        sel.text = text;
        element.focus();
      } else if (element.selectionStart || element.selectionStart === 0) {
        var startPos = element.selectionStart;
        var endPos = element.selectionEnd;
        var scrollTop = element.scrollTop;
        element.value =
          element.value.substring(0, startPos) +
          text +
          element.value.substring(endPos, element.value.length);
        element.focus();
        element.selectionStart = startPos + text.length;
        element.selectionEnd = startPos + text.length;
        element.scrollTop = scrollTop;
      } else {
        element.value += text;
        element.focus();
      }
    }

    $('textarea').on('keydown', function(e) {
      var keyCode = e.keyCode || e.which;

      if (keyCode === 9) {
        e.preventDefault();
        var start = this.selectionStart;
        var end = this.selectionEnd;
        var val = this.value;
        var selected = val.substring(start, end);
        var re = /^/gm;
        var count = selected.match(re).length;


        this.value = val.substring(0, start) + selected.replace(re, '\t') + val.substring(end);
        this.selectionStart = start;
        this.selectionEnd = end + count;
      }
    });

    var myElement = document.getElementById('editor');

    myElement.addEventListener("touchstart", startTouch, false);
    myElement.addEventListener("touchmove", moveTouch, false);

    // Swipe Up / Down / Left / Right


    function startTouch(e) {
      initialX = e.touches[0].clientX;
      initialY = e.touches[0].clientY;
    };

    function moveTouch(e) {
      if (initialX === null) {
        return;
      }

      if (initialY === null) {
        return;
      }

      var currentX = e.touches[0].clientX;
      var currentY = e.touches[0].clientY;

      var diffX = initialX - currentX;
      var diffY = initialY - currentY;

      if (Math.abs(diffX) > Math.abs(diffY)) {
        // sliding horizontally
        if (diffX > 0) {
          // swiped left
          $('textarea').blur();
        } else {
          // swiped right
          // alert('Swipe Right');
          $('textarea').blur();
        }
      } else {
        // sliding vertically
        if (diffY > 0) {
          // swiped up
          console.log("swiped up");
        } else {
          // swiped down
          console.log("swiped down");
        }
      }

      initialX = null;
      initialY = null;

      e.preventDefault();
    };

    // TLN line number code below
    const TLN = {
      eventList: {},
      update_line_numbers: function(ta, el) {
        let lines = ta.value.split("\n").length;
        let child_count = el.children.length;
        let difference = lines - child_count;

        if (difference > 0) {
          let frag = document.createDocumentFragment();
          while (difference > 0) {
            let line_number = document.createElement("span");
            line_number.className = "tln-line";
            frag.appendChild(line_number);
            difference--;
          }
          el.appendChild(frag);
        }
        while (difference < 0) {
          el.removeChild(el.firstChild);
          difference++;
        }
      },
      append_line_numbers: function(id) {
        let ta = document.getElementById(id);
        if (ta == null) {
          return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
        }
        if (ta.className.indexOf("tln-active") != -1) {
          return console.warn("[tln.js] textarea of id '" + id + "' is already numbered");
        }
        ta.classList.add("tln-active");
        ta.style = {};

        let el = document.createElement("div");
        ta.parentNode.insertBefore(el, ta);
        el.className = "tln-wrapper";
        TLN.update_line_numbers(ta, el);
        TLN.eventList[id] = [];

        const __change_evts = [
          "propertychange", "input", "keydown", "keyup", 'focus'
        ];
        const __change_hdlr = function(ta, el) {
          return function(e) {
            if ((+ta.scrollLeft == 10 && (e.keyCode == 37 || e.which == 37 ||
                e.code == "ArrowLeft" || e.key == "ArrowLeft")) ||
              e.keyCode == 36 || e.which == 36 || e.code == "Home" || e.key == "Home" ||
              e.keyCode == 13 || e.which == 13 || e.code == "Enter" || e.key == "Enter" ||
              e.code == "NumpadEnter")
              ta.scrollLeft = 0;
            TLN.update_line_numbers(ta, el);
          }
        }(ta, el);
        for (let i = __change_evts.length - 1; i >= 0; i--) {
          ta.addEventListener(__change_evts[i], __change_hdlr);
          TLN.eventList[id].push({
            evt: __change_evts[i],
            hdlr: __change_hdlr
          });
        }

        const __scroll_evts = ["change", "mousewheel", "scroll", "focus"];
        const __scroll_hdlr = function(ta, el) {
          return function() {
            el.scrollTop = ta.scrollTop;
          }
        }(ta, el);
        for (let i = __scroll_evts.length - 1; i >= 0; i--) {
          ta.addEventListener(__scroll_evts[i], __scroll_hdlr);
          TLN.eventList[id].push({
            evt: __scroll_evts[i],
            hdlr: __scroll_hdlr
          });
        }
      },
      remove_line_numbers: function(id) {
        let ta = document.getElementById(id);
        if (ta == null) {
          return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
        }
        if (ta.className.indexOf("tln-active") == -1) {
          return console.warn("[tln.js] textarea of id '" + id + "' isn't numbered");
        }
        ta.classList.remove("tln-active");

        ta.previousSibling.remove();

        if (!TLN.eventList[id]) return;
        for (let i = TLN.eventList[id].length - 1; i >= 0; i--) {
          const evt = TLN.eventList[id][i];
          ta.removeEventListener(evt.evt, evt.hdlr);
        }
        delete TLN.eventList[id];
      }
    }

    // TLN line No. code ends here
    
    
/// END OF moved from lesson-m.php    


buildPage();



// MOBILE ONLY FUNCTIONS

$('.create-GalleryPage').click(function () {
  dialogInMenu = true;
  userCodeHtml2 = ''

  createGalleryPage();
  // hideButton()
      document.getElementById('previous').style.visibility = 'hidden'
    document.getElementById('previous2').style.visibility = 'hidden'
  loading = document.getElementById('loadingIcon')
  document.getElementById('pageNumber').innerHTML = current_page + 1
  document.getElementById('pageNumber2').innerHTML = current_page + 1

  getGalleryData();
})

function createGalleryPage() {
  let GalleryInit = document.getElementById('#galleryContainer')

  $.galleryDialog({
    modalName: 'createGalleryPage',
    htmlContent:
      `
      <div class="container" style="height:${screenHeight_plus23}; padding: 23px;overflow-y: scroll;" id="gallery_page">
      <h2 class="">Projects</h2>
      <div class="search-continer">
        <div class="search-list">
          <div class="">
            <h6>Search By:</h6>
          </div>
          <ul>
            <li>
              <select name="" id="selectOptions" onchange="FilterData(this.event)">
                <option value="Projects" id="projects">All Projects</option>
                <option value="Recent" id="recent"> Recent</option>
                <option value="MostLiked" id="likes">Most Liked</option>
              </select>
            </li>
          </ul>
        </div>
      </div>
      <div class="pagination-section">
        <ul class="pagination">
          <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div>
      <div class="loading" id="loadingIcon">
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
      </div>
      <div class="gallery-container" id="galleryContainer">

      </div>
      <div class="gallery-container" id="galleryLikes">

      </div>
      <div class="gallery-container" id="recentProjects">
      </div>
      <div class="pagination-section" id="bottom-pagination">
        <ul class="pagination">
          <li class="page-item previous" id="previous2"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber2"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div>
    </div>
    </div>`
  })
  console.log('Test');
}

// ************ profile Page
$('.create-profilePage').click(function () {
  dialogInMenu = true
  checkLoggedInAndGetUserDetails().then(function(user_details){ createProfilePage(user_details)}).catch(function(err){
    console.log(err, "Here is the error");
     UnRegisteredModal()})
})

function createProfilePage(user_details) {

  console.log("Inside createProfilepage", user_details);
  let ProfileFName;
  let ProfileLName;
  $.galleryDialog({
    modalName: 'createProfilePage',
    htmlContent: `
    <div  id="user_profile" style="height:${screenHeight}; padding: 23px;overflow-y: scroll;">
      <div class="container">
          <i class="icon-account_circle fs6" style="padding: 30px 0px 10px 0; display: block;"></i>
          <div>
            <input class="form-control" id="pf_name" placeholder="First name" name="fname" type="text" required="" style="width:45%;display:inline-block" value="${user_details.first_name ? user_details.first_name : ''}">
            <input class="form-control" id="pl_name" placeholder="Last name" name="fname" type="text" required="" style="width:45%;display:inline-block" value="${user_details.last_name ? user_details.last_name : ''}">
            <div class="text-left ml-4"><label style="margin: 0;">Aspiring:</label></div>
            <div class="mb-3"><textarea class="form-control m-auto" id="aspiringContent" placeholder="Dream Job" type="text" cols="4" style="width:90%" >${user_details.details ? user_details.details.aspiring ? user_details.details.aspiring : '' : ''}</textarea></div>
            <input class="form-control" id="Pgrade" placeholder="Grade" name="Grade" type="text" required="" style="width:45%;display:inline-block" value="${user_details.details ? user_details.details.grade ? user_details.details.grade : '' : ''}">
            <input class="form-control" id="PConuntry" placeholder="Country" name="country" type="text" required="" style="width:45%;display:inline-block" value="${user_details.details ? user_details.details.country ? user_details.details.country : '' : ''}">
            <button id="action_button" class="btn btn-primary light action ">Save</button>
          </div>
          <br><br>
          <div class="container">
            <h2 class="profile_name mt-4">Certificates:</h2>`
            +
            `<hr>`
            +`
            <div class="certificates-blog" id="certificateBlog">
              ${user_details.certificate ? prepareCertificates(user_details) :'<p>Please complete the Course to get the certificates</p>'}
            </div>`+
            `<hr>`+
          `
          </div>
      </div>
    </div>
    `
  })
  // let hideSaveBtn = document.getElementById('pf_name').value
  $('.action').click(function (){
    let ProfileFName = document.getElementById('pf_name').value;
    let ProfileLName = document.getElementById('pl_name').value;
    let Aspiring = document.getElementById('aspiringContent').value;
    let grade = document.getElementById('Pgrade').value;
    let p_country = document.getElementById('PConuntry').value

    // store to users
    var ref = firebase.database().ref('/users/' + auth_id)

    ref.update({
      first_name : ProfileFName,
      last_name: ProfileLName,
      'details/country' : p_country,
      'details/aspiring': Aspiring,
      'details/grade': grade
    })

    document.getElementById('action_button').style.display = 'none';
    successProfileDetails('Profile Updated', 'Thank you for updating your profile')
  })
  console.log("Completed task");

}
// *************  profile page end

function checkpointCompleted(completed_id) {
 /* firebase
    .database()
    .ref(
      'user_profile/' +
      auth_id +
      '/lesson_progress/' +
      save_lesson_id +
      '/user_checkpoint/' +
       checkpoint_id
    )
    .update({
      completed: true,
      user_code: editor.value,
      completed_at: timeNow()
    })*/
  checkpoint_completed++

  console.log("checkpoint completed", (checkpoint_completed/checkpoint_count));
  if(checkpoint_completed/checkpoint_count > 0.8){
    console.log("80% checkpoint completed");
    firebase
      .database()
      .ref(
        'users/' +
        auth_id +
        '/certificate/'+
        save_lesson_id)
      .update({
        completed: true,
        user_code: editor.value,
        completed_at: timeNow()
      })
  }

  if (checkpoint_completed === checkpoint_count) {
    lesson_progress.progress_completed = '100'
    lesson_status = 'completed'
    /// /console.log("checkpointCompleted: " + completed_id + " " + timeNow() + " " + checkpoint_completed + " " + checkpoint_count);
  } else {
    lesson_progress.progress_completed = Math.round(
      (checkpoint_id * 100) / (checkpoint_count + 1)
    )
    /// console.log(checkpoint_id + " * 100 / " + checkpoint_count + " = " + lesson_progress.progress_completed);
  }
  ga('send', {
    hitType: 'event',
    eventCategory: lessson_url,
    eventAction: 'Challenge Completed',
    eventLabel: active_slide + 1
  })
  // console.log(active_slide + 1)
  saveUserData();
}

function codeUpdated() {
  save_pending = true
  validateCheckpoint()
   console.log('Save Pending and Code Validated')
}

function successAnimation() {
  if (active_tab > 0) {
    editor.blur()
    $('.success-animation').removeClass('hide')

    setTimeout(() => {
      $('.success-animation').addClass('hide')
      // console.log('animate2')
      // editor.focus()
    }, 1100)
  }
  // console.log('animate1' + active_tab)
}

function loadSkills() {
  if (user_skills) {
    $.each(user_skills, function (index) {
      unlockSkills(user_skills[index])
    })
  }
}

function unlockSkills(skills) {
  // console.log("skills: "+skills);
  $('#' + skills + '-skill').addClass('has-skill-true')
  $('#' + skills + '-skill').removeClass('has-skill-false')
  // console.log('Unlocked skill '+skills);
  // console.log("#"+slide_id+"-skill");

  $('#' + skills + '-skill')
    .attr(
      'data-original-title',
      $('#' + skills + '-skill').attr('data-title-text')
    )
    .tooltip('show')
    .tooltip('hide')
}

function inactivityTime() {
  var t
  window.onload = resetTimer
  document.onload = resetTimer
  document.onmousemove = resetTimer
  document.onmousedown = resetTimer // touchscreen presses
  document.ontouchstart = resetTimer
  document.onclick = resetTimer // touchpad clicks
  document.onscroll = resetTimer // scrolling with arrow keys
  document.onkeypress = resetTimer

  /// /console.log("inactivityTime");
  resetTimer()

  var resumeCoding = [
    'You snooze, you loose.',
    'Well begun is only half done',
    'You are on your way to becoming a coding genius',
    'Did you know when you complete the lesson you will get a certificate?'
  ]
  var rindex = Math.floor(Math.random() * resumeCoding.length)

  function timedOut() {
    /*
         if ( !$(".modal:visible").length ) {
    $('.modal').modal('hide');

    //console.log("timed out");
    $.createDialog({
      modalName: 'timed-out',
             popupStyle: 'speech-bubble '+current_avatar+' surprised',
            htmlContent:
            '<div id=\"confirm_dialog\" style=\"display: block;\">'+
            '\t<h2 id=\"confirm_title\">'+resumeCoding[rindex]+'</h2>'+
            '\t<div id=\"confirm_actions\">'+
            '\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s continue coding</button>'+
            '\t</div>'+
            '</div>'
    });
  } */
  }

  function resetTimer() {
    // console.log("r timer: "+$(".modal").length);
    clearTimeout(t)
    t = setTimeout(timedOut, 60000)

    // ////console.log("didn't reset timer"+$(".modal").attr('style').display == 'block' ));
  }
}

function editorChange() {
  clearTimeout(delay)
  // console.log('preview');
  delay = setTimeout(updatePreview, 100)
  setTimeout(codeUpdated, 200)
}

function closeModal(options) {
  /// /console.log("close modal: " + options);
  $(options).modal('hide')
}

function drawArrow(start_id, end_id, dynamicAnchors, acolor = 'red-line') {
  jsPlumb.ready(function () {
    jsPlumb.setContainer('container')
    var link = jsPlumb.getInstance()

    link.connect({
      cssClass: acolor,
      source: start_id,
      target: end_id,
      anchors: dynamicAnchors,
      endpoint: 'Blank',
      overlays: [
        [
          'Arrow',
          {
            location: 1,
            width: 10,
            length: 10
          }
        ]
      ],
      paintstyle: {
        lineWidth: 40,
        strokeStyle: 'red'
        // dashstyle:" 0 1"
      },
      connector: [
        'Bezier',
        {
          curviness: 80
        }
      ]
    })
  })
}


function greetingsCode(){
    // console.log("reg");
    localStorage.setItem('greetcode', true)
    refreshValues();
    $('.modal ').remove()
    $.createDialog({
      modalName: 'greetings',
      popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">GUESS WHAT?!? :)</h2>' +
        
        '\t<p>This is the Code Editor.'+'<br>'+' Where the magic happens.</p><br>Swipe back to keep learning. ' +'<br>'+'<br>'+'<br>'+
        '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Got it!">' +
        '\t</div>'
        
    })
    $('#greetings').click(function () {
      $('#greetings').css("display","none");
      $('.current').css("display","none");
    })
  }
  
  function greetingsView(){
    // console.log("reg");
    localStorage.setItem('greetview', true)
    refreshValues();
    $('.modal ').remove()
    $.createDialog({
      modalName: 'greetings',
      popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">CHECK YOUR WORK.</h2>' +
        
        '\t<p> This is the Web Viewer.<br> Where you get to check on what you'+"'"+'re building. </p>' +'Swipe back twice to keep learning.'+'<br>'+'<br>'+'<br>'+
        '\t\t<input id="action_button" class="btn btn-encouraging light action" type="submit" value="Nice!">' +
        '\t</div>'
        
    })
    $('#greetings').click(function () {
      $('#greetings').css("display","none");
      $('.current').css("display","none");
      $('')

    })
  }
// END OF MOBILE ONLY FUNCTIONS




// MOBILE SPECIFIC 

function buildSlides() {
  var slide_checkpoint_id = 1
  var slide_indexes = []
  var unlocked_class = ''

  

  slide_data = lesson_data.slides[active_slide]
  all_slides = lesson_data.slides
  
 // $("#resources_tab").html(hints_data);


   console.log("active_slide:\n" + active_slide);
  // str = JSON.stringify(slide_data, null, 4);
  // console.log("slide_data:\n" + str);
  // str = JSON.stringify(all_slides, null, 4);
  // console.log("all_slides:\n" + str);

  $.each(all_slides, function (index) {
    /// /console.log(all_slides[index]);

    if (all_slides[index].css_class) {
      custom_class = all_slides[index].css_class
    } else {
      custom_class = ''
    }
        console.log("checkpoint: "+index);
    if (all_slides[index].checkpoint === true) {
      // cp_class = " checkpoint";
        checkpoint_completed++
      if (
        lesson_progress.user_checkpoint !== undefined &&
        lesson_progress.user_checkpoint[slide_checkpoint_id] !== undefined
      ) {
        unlocked_class = ' cp-unlocked'
      console.log("checkpoint completed: "+slide_checkpoint_id);

      } else {
         console.log("checkpoint not completed " + slide_checkpoint_id);
      }
      checkpoint_count++
      slide_indexes.push(index)
    } else {
      // cp_class = "";
      unlocked_class = ''
    }

    // $("#lesson-page .swiper-wrapper").append("<div class=\"swiper-slide chal" + slide_checkpoint_id + " " + unlocked_class + " \" id=\"slide" + (index + 1) + "\">\n" + all_slides[index].html_content + "\n</div>");
    $('#lesson-page .swiper-wrapper').append(
      '<div class="swiper-slide ' +
      custom_class +
      unlocked_class +
      ' " id="slide' +
      (index + 1) +
      '">\n' +
      all_slides[index].html_content +
      '\n</div>'
    )

    if (all_slides[index].onload_function !== undefined) {
      /// /console.log('onload_function triggered: ' + all_slides[index].onload_function);
      var func = new Function(all_slides[index].onload_function)
      func()
    }

    if (all_slides[index].checkpoint === true) {
      $(
        '#lesson-page .swiper-wrapper .swiper-slide:nth-child(' +
        (index + 2) + // FIX: use "+2" because there is extra loading div
        ')'
      ).attr('data-checkpoint_id', slide_checkpoint_id++)
      // console.log("cp_index: "+slide_checkpoint_id);
    }
  })

  active_slide = setActiveSlide();

  // console.log("checkpoint_count: "+checkpoint_count);
  // $( "#lesson-page .swiper-wrapper" )
  // $('#lesson-page .swiper-wrapper .swiper-slide:nth-child(10)').addClass('active');
}

function validateCheckpoint() {
  // slide_data = lesson_data.slides[active_slide-1];
  // active_slide = swiperV.activeIndex+1;
  // console.log('Trigger validation' + slide_data.reg)
  str = JSON.stringify(slide_data, null, 4)
  // console.log("slide_data:\n" + str);

  slide_id = active_slide + 1

 
 
  
  if (slide_data.action === true) {
    // ////console.log('action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
    // console.log(
    //   'action set for slide | slide_data action: ' +
    //     slide_data.action +
    //     ' | slide_id: ' +
    //     slide_id
    // )
    if (slide_data.reg !== undefined) {
      var cp_unlocked = false
      // do something
      // console.log('Run validation on ' + active_slide)

      $.each(slide_data.reg, function (index, re) {
        // console.log('#slide' + slide_id + ' .task-' + (index + 1))
         var reResults = new RegExp(re, 'i').test(editor.value)
        console.log('Run validation results ' + reResults,re)
        //  $(".check-info").removeClass('d-none');
        if (reResults) {
          current_checkpoint_id = getCurrentCheckpointId();
            setCheckpointProgress();
            console.log('inside reResults and getCurrentCheckpointId(): ' + getCurrentCheckpointId());
          cp_unlocked = true
          if (
            !$('#slide' + slide_id + ' .task-' + (index + 1)).hasClass('correct')
          ) {
            $('#slide' + slide_id + ' .task-' + (index + 1)).addClass('correct')
            saveUserData(true)
          }
          // console.log(index)
          if (
            !$('#slide' + slide_id + ' .task-' + (index + 1) + ' i').hasClass(
              'icon-beenhere'
            )
          ) {
            $('.success-animation').addClass('hide')
            successAnimation()

            // $("#slide" + slide_id + " li:eq( " + index + " ) ").css( "color", "red" );
            /// /console.log("added correct class " + "#slide" + slide_id + " .task-" + (index + 1) + " i");
          }
          // console.log(
          //   '#slide' + slide_id + ' li:eq( ' + index + ' ) .check-icon'
          // )
          $(
            '#slide' + slide_id + ' .task-' + (index + 1) + ' .check-icon'
          ).html('<i class="icon-beenhere"></i>')
          // $("#slide" + slide_id + " .actions li").addClass('correct');
          // $(".check-info").addClass('correct');
          // else { //else used so don't save data twice
          // }

          // $(".check-info .check-icon").html('<i class="icon-close1"></i>');

          // $(".pagination .next").addClass('disabled');
          // $(".pagination .next").removeClass('disabled');
          console.log('Run validation using: ' + re);
          // $(".success-animation").show();

          // console.log('checkpoint_id: '+checkpoint_id);
           
        } else {
          $(
            '#slide' + slide_id + ' .task-' + (index + 1) + ' .check-icon'
          ).html('<i class="icon-close1"></i>')
          $('#slide' + slide_id + ' .task-' + (index + 1)).removeClass(
            'correct'
          )
          // $(".check-info .check-icon").html('<i class="icon-close1"></i>');
          // $(".check-info").removeClass('correct');
          // $(".pagination .next").addClass('disabled');
        }
      })

      if (cp_unlocked) {
        /// /console.log("#slide" + slide_id + " cp_unlocked");
      } else {
        /// /console.log("#slide" + slide_id + " cp_locked");
      }

      if (!$('#slide' + slide_id).hasClass('cp-unlocked') && cp_unlocked) {
        checkpoint_id = $('#slide' + slide_id).data('checkpoint_id')
        $('#slide' + slide_id).addClass('cp-unlocked')
        checkpoint_id = slide_data.checkpoint_id;
        checkpointCompleted(checkpoint_id)

        if (slide_data.unlock_skills !== undefined) {
          unlockSkills(slide_data.unlock_skills)

          if (user_skills !== null) {
            user_skills.push(slide_data.unlock_skills)
            var unique_user_skills = []
            $.each(user_skills, function (i, el) {
              if ($.inArray(el, unique_user_skills) === -1) {
                unique_user_skills.push(el)
              }
            })
            /// /console.log("set unique unlockSkills: " + unique_user_skills);
            firebase
              .database()
              .ref('/user_profile/' + auth_id + '/skills')
              .set(unique_user_skills)
          } else {
            user_skills = {
              '0': slide_data.unlock_skills
            }
            /// /console.log("set unlockSkills: " + user_skills);
            firebase
              .database()
              .ref('/user_profile/' + auth_id + '/skills')
              .set(user_skills)
          }
        }
      } /*  */
    }
  } else {
    /// /console.log('no action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
  }

  if (slide_data.js_function !== null) {
    /// /console.log('js_function trigger: ' + slide_data.js_function);
    // var func = new Function("console.log('i am a new function')");
    var func = new Function(slide_data.js_function)
    func()
  }
}

function onStart() {


  loadUserData();
  buildSlides()
  /// /console.log("start" + active_slide);
  // saveUserData(true);
  // Initialize tooltips
  $('.nav-tabs > li a[title]').tooltip()

  // Wizard
  $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    var $target = $(e.target)

    if ($target.parent().hasClass('disabled')) {
      return false
    }
  })


  setInterval(saveUserData, 30000)

  // Live preview

  // editor.on('change', function () {
  //   clearTimeout(delay);
  //   console.log('preview');
  //   delay = setTimeout(codeUpdated, 500);
  // });
  var lessonLoaded = false

  // initFullPage();

  $('#fp-nav ul li:nth-child(2) a span').addClass('cp-green')
  $('#fp-nav ul li:nth-child(12) a span').addClass('cp-red')

  swiperH = new Swiper('.swiper-container-h', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowToXuchMove: false,
    pagiXnation: {
      el: '.swiper-pagination-h',
      clickable: true
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () { },
      slideChange: function () {
        var slider = this
        active_tab = slider.activeIndex
        $('#lesson-navbar li.active').removeClass('active')
        var mindex = slider.activeIndex + 1

        $('#lesson-navbar li:nth-child(' + mindex + ')').addClass('active')
        // console.log('#lesson-navbar:nth-child('+mindex+')');
        if (slider.activeIndex <= 0) {
          $('.success-animation').addClass('')
        }
        if (mindex === 2) {
          if (greetcode === false) {
            greetingsCode().delay(1000);
          }
          //$('.bottom_keys').removeClass('hide')
          
        } else {
         // $('.bottom_keys').addClass('hide')
        }
        if (mindex === 3) {
          slider.allowSlideNext = true
          if (greetview === false) {
            greetingsView().delay(1000);
          }
        }
        if (slider.activeIndex <= 3) {
         
        } else {
          slider.allowSlideNext = false
        }
        // console.log("tab: "+active_tab);
      },
      transitionEnd: function () {
        var slider = this
        if (slider.activeIndex === 2) {
          updatePreview()
        }
        if (slider.activeIndex === 1) {
          $('.CodeMirror-code').focus()
          editor.focus()
          // $('.ui-keyboard').show();
          // console.log('show')
        } else {
          $('.CodeMirror-code').blur()
          // $('.ui-keyboard').hide();
          // console.log('hide')
        }
      }
    }
  })

  swiperV = new Swiper('.swiper-container-v', {
    direction: 'vertical',
    spaceBetween: 50,
    shortSwipes: true,
    mousewheel: true,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouXchMove: false,
    // Disable preloading of all images
    preloadImages: false,
    // initialSlide: active_slide,
    // Enable lazy loading
    lazy: {
      loadPrevNext: true
    },
    pagination: {
      el: '.swiper-pagination-v',
      clickable: false
    },
    observer: true,
    observeParents: true,

    on: {
      init: function () { },
      lazyImageReady: function () {
        if (slide_data.lazy_function !== undefined) {
          var func = new Function(slide_data.lazy_function)
          func()
        }
        /// /console.log('lazy_function triggered: ' + slide_data.lazy_function);
      },
      slideChange: function () {
        var slider = this
        // var mindex = slider.activeIndex+1;
        active_slide = slider.activeIndex
        localStorage.setItem(
          lessson_url + '_' + auth_id + '_active_slide',
          active_slide
        )
        slide_data = lesson_data.slides[active_slide]
        // console.log(slide_data)
        var unlocked = $('#slide' + (active_slide + 1)).hasClass('cp-unlocked')
        if (!unlocked && slide_data.reg !== undefined) {
          slider.allowSlideNext = false
          /// /console.log('lock' + slider.allowSlideNext + active_slide);
        } else {
          slider.allowSlideNext = true
          /// /console.log('unlock' + active_slide);
        }
        // console.log("mindex: "+mindex);
        /// /console.log("active_slide: " + active_slide);

        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')')
          .prevAll()
          .addClass('bullet-green')
        $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')')
          .next()
          .removeClass('bullet-green')
        validateCheckpoint()
        saveUserData()
        // ga('send', 'pageview', 'CJ Lesson', 'swipe', lessson_url, active_slide + 1);
        ga('send', {
          hitType: 'event',
          eventCategory: lessson_url,
          eventAction: 'Slide swipe',
          eventLabel: active_slide + 1
        })
        console.log('Slide swipe');
      }
    }
  })

  swiperTabs = document.querySelector('.swiper-container-h').swiper
  swiperLesson = document.querySelector('.swiper-container-v').swiper

  swiperH.allowTouchMove = false
  swiperV.allowTouchMove = false

  $('#lesson-navbar li').click(function () {
    if ($(this).index() === 6) {
      /// /console.log(swiperH.activeIndex);
      // ////console.log($(this).index());
      swiperH.allowSlideNext = true
      swiperH.slideTo($(this).index())
      swiperH.allowSlideNext = false
    } else {
      // console.log("dfd"+swiperH.activeIndex);
      // ////console.log("dfd"+$(this).index());
      swiperH.slideTo($(this).index())
      $('#lesson-navbar li.active').removeClass('active')
      $(this).addClass('active')
    }
  })

  $('.swiper-pagination-switch').click(function () {
    swiperH.slideTo($(this).index() + 1)
    $('.swiper-pagination-switch').removeClass('active')
    $(this).addClass('active')
  })

  $('.swiper-next').click(function () {
    swiperLesson.allowSlideNext = true
    swiperLesson.slideNext()
    // swiperLesson.allowSlideNext = false;
    /// /console.log("skip");
  })

  $('.swiper-editor').click(function () {
    swiperTabs.allowSlideNext = true
    swiperTabs.slideNext()
    // swiperTabs.allowSlideNext = false;
    /// /console.log("skip2");
  })

  swiperLesson.slideTo(active_slide)
  // active_slide = 0;


   var twitterShare = document.querySelector('[data-js="twitter-share"]')

  twitterShare.onclick = function (e) {
    e.preventDefault()
    var twitterWindow = window.open(
      'https://twitter.com/share?url=' +
      'https://www.codejika.com/learn/' +
      save_lesson_id +
      '/' +
      auth_id,
      'twitter-popup',
      'height=350,width=600'
    )
    if (twitterWindow.focus) {
      twitterWindow.focus()
    }
    return false
  }

  var facebookShare = document.querySelector('[data-js="facebook-share"]')

  facebookShare.onclick = function (e) {
    e.preventDefault()
    var facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' +
      'https://www.codejika.com/learn/' +
      save_lesson_id +
      '/' +
      auth_id,
      'facebook-popup',
      'height=350,width=600'
    )
    if (facebookWindow.focus) {
      facebookWindow.focus()
    }
    return false
  }

  $('#whatsapp,#whatsapp2').attr(
    'href',
    'whatsapp://send?text=Check out the website I coded on CodeJIKA 😎‏%0A‎👉 ' +
    'https://www.codejika.com/preview/' +
    save_lesson_id +
    '?' +
    auth_id
  )

  $('#gallery').click(function () {
    // console.log('clicked gallery')
    // loginModal();
    addToGallery()
  })

  $('.loading').hide()
  swiperH.allowTouchMove = true
  swiperV.allowTouchMove = true
  inactivityTime()
}

// END OF MOBILE SPECIFIC 

/*
// CHECK why is this for only 5 min website???
function getLessonProgressPromise(default_code) {
  return firebase
    .database()
    .ref('/gallery/' + auth_id + '/5-min-website/')
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
      },
      function (error) {
        console.log(error);
      }
    )
  /// /console.log("getLessonProgressPromise");
}
// END OF CHECK */





function go_next() {
  // console.log(page_id_array, 'here is the array')
  current_page += 1
  loading.style.display = 'block'
  document.getElementById('galleryContainer').innerHTML = loading
  $('#bottom-pagination').css('visibility', 'hidden')
  $('#previous2').css('visibility', 'hidden')
  userCodeHtml2 = ''
  getGalleryData(page_id_array[current_page], '')
  document.getElementById('pageNumber').innerHTML = 1 + current_page
  document.getElementById('pageNumber2').innerHTML = 1 + current_page

  if (current_page != 0) {
    document.getElementById('previous').style.visibility = 'visible'
    document.getElementById('previous2').style.visibility = 'visible'
  }
}

function go_previous() {
  if (current_page != 0) {
    current_page -= 1
    loading.style.display = 'block'
    document.getElementById('galleryContainer').innerHTML = loading
    $('#bottom-pagination').css('visibility', 'hidden')
    userCodeHtml2 = ''
    getGalleryData(page_id_array[current_page], '')
    document.getElementById('pageNumber').innerHTML = 1 + current_page
    document.getElementById('pageNumber2').innerHTML = 1 + current_page
  }

  // Hide Previous button
  if (current_page == 0) {
    $('#previous').css('visibility', 'hidden')
    $('#previous2').css('visibility', 'hidden')
  }
}


$(document).ready(function () {
  successAnimation()
  // ================= jquery Modal
  $(function () {
    $('#viewHTML').click(function () {
      // takes the ID of appropriate dialogue
      var id = $(this).data('#sub-modal')
      // open dialogue
      $(id).dialog('open')
    })
  })
  initFirebase()
  var getUserProfile = getUserProfilePromise()
  // var getUserSkills = getUserSkillsPromise();  P0 disabled
  var getLessonProgress = getLessonProgressPromise()
  // var getGallery = getGalleryData('', '')
  var getPrevSubmitCode = getPrevSubmitCodePromise();
  // Promise.all([getLessonData, getUserProfile, getUserSkills, getLessonProgress]).then(function (results) {  P0 disabled
  Promise.all([
    getUserProfile,
    getLessonProgress,
    getPrevSubmitCode,
    // getGallery
  ]).then(function (results) {

    user_profile = results[0]
    // str = JSON.stringify(user_profile, null, 4);
    // console.log("user_profile:\n"+str);

    // user_skills = results[2]; P0 disabled
    // str = JSON.stringify(user_skills, null, 4);
    // console.log("user_skills:\n"+str);

    lesson_progress = results[1]
    prev_submit_code = results[2].submit_code;
    // if no lesson data save default values to DB
    if (lesson_progress === null) {
      /// /console.log("no lesson progress found");
      lesson_progress = {
        last_checkpoint: '0',
        //user_code: lesson_data.defaultCode,
        progress_completed: '0'
      }
      // saveUserData(true);
      /// /console.log("default lesson_progress inserted");
    }

    // str = JSON.stringify(lesson_progress, null, 4);
    // console.log("default lesson_progress:\n"+str);

    /// /console.log("promise all is true, now running onStart");
    onStart()
  })

  $('#disallow-interact,#allow-interact').click(function () {
    $('#disallow-interact.icon-locked').toggleClass('hide')
    $('#allow-interact.icon-unlocked').toggleClass('hide')
    $('.swipe-overlay').toggleClass('pointer-none')
  })

  $('.swipe-overlay').click(function () {
    // $( "#disallow-interact.icon-locked" ).toggleClass( "hide" )
    // $( "#allow-interact.icon-unlocked" ).toggleClass( "hide" )
    // $( ".swipe-overlay" ).toggleClass( "pointer-none" )
    // console.log("test");
  })

  // TLN
  TLN.append_line_numbers('editor')

  hideButton()  // REMOVE does nothing
  $('#close_button').click(function () {
    $('.blocker .jquery-modal').css('background', 'none')
  })

// function to display register modal
  // setTimeout(checkUserKey, 5000);
  checkUserKey()
})
//  ============ end of document.ready firebaseinit()




// REMOVE does nothing
function hideButton() {
  if (current_page == 0) {
    // document.getElementById('previous').style.visibility = 'hidden'
    // document.getElementById('previous2').style.visibility = 'hidden'
  }
}
// END OF REMOVE does nothing


$('.projectTitle').click(function () {
  titleInput()
})
function titleInput() {
  var title = document.getElementById('project_name').value
  var projectNotes = document.getElementById('project_notes').value
  if (title == '') {
    document.querySelector('#project_name').classList.add('error-border')
  } else {
    // update DB gallery
    var user_firstName
    var user_lasttName

    var getDetails = firebase.database()
    getDetails
      .ref('/users/' + auth_id)
      .once('value')
      .then(function (snapshot) {
        user = snapshot.val()
        // console.log(user, 'firstname to gal')
        var toupdate = {
          first_name: user.first_name,
          last_name: user.last_name,
          likes: 0,
          user_code: editor.value,
          title: title,
          notes: projectNotes,
          date: new Date()
        }
        var ref = firebase.database().ref('/gallery/5-min-website')
        ref.update({
          [auth_id]: toupdate
        })
      })
    savedToGallery()

    userCodeHtml2 = ''
    setTimeout(getGalleryData('', ''), 300)

    document.querySelector('#project_name').classList.remove('error-border')
    $('.modal , .modal-backdrop').remove()
    $('.blocker').css('display', 'none')
    // console.log(title, projectNotes)
  }
}

// ============= thumbnail Model =========
function viewThumbnail(clicked_key, user_html_code, user_code_output) {
  // $('.modal ').remove()
  subModal = true;
  $.nestedModal1({
    modalName: 'viewThumbnail-modal',
    htmlContent: `<div >
        <div class="popup-header">
          <a id="viewHTML" class="view-code">VIEW</a>
        </div>
        <iframe src="data:text/html;charset=utf-8, ${user_code_output}" style="width:100%; height: 100vh;overflow:hidden; border: none;" scrolling="yes" id="my_iframe_${clicked_key}">

        </iframe>
      </div>
      `,
      closeExisting:false
  })
  $('.view-code').click(function () {
    outputHTML1(clicked_key, user_html_code, user_code_output);
    // $('#viewThumbnail').modal({
    //   closeExisting:true
    // })
  })
}
// ============== thumbnail Model end =========


//REWRITE can remove this function once fix modal
function openthumbnail(clicked_key, user_html_code, user_code_output) {
  // console.log(clicked_key, user_html_code, user_code_output, 'Here is my key')
  viewThumbnail(clicked_key, user_html_code, user_code_output)
  //subModal = true;
}

function p1t1Output() {
  $.galleryDialog({
    modalName: 'P1T1-output',
    htmlContent: `
    <header class="p1t1-header">
		<div class="p1t1-div">
			<h1 class="p1t1-h1">Jeff  Molefe</h1>
			<h3 class="p1t1-h3"> Launching soon... </h3>
			<br><p>18th June, 2022</p>
		</div>
	</header>
	<section class="p1t1-section">
	<div class="p1t1-div">
		<h3 class="p1t1-h3">
				MOTIVATION:
				<i class="p1t1-i"><br><br>
				I want to learn how to code so that I can...
				[ ...build cool websites, learn more about tech...]
				</i>
		</h3>
	</div>
	</section>
 <footer class="p1t1-footer" style="padding-bottom:40px;">
  <div class="p1t1-div" >
  <p>&copy 2021 Jeff Molefe</p>
  </div>
 </footer>
    `
  })
}

// ///////////////  backButton functionality
var timeout;
history.pushState(null, null, location.href);
window.onpopstate = function () {
  history.go(1);

  clearTimeout(timeout);
  timeout = setTimeout(function () {
    if (swiperTabs.activeIndex == 0) {
      swiperLesson.slideTo(swiperLesson.activeIndex - 1);
      console.log("on lesson tab" + swiperLesson.activeIndex);
    }
    if (swiperTabs.activeIndex == 3 && dialogInMenu) {
      // alert('clicked Back button')
      $('.blocker').trigger('click')
      dialogInMenu = false
      console.log("on lesson tab" + swiperLesson.activeIndex);
      return
    }
    if (swiperTabs.activeIndex == 3 && subModal){
       $('.blocker').trigger('click')
       subModal = false
      // return
    } else {
      return swiperTabs.slideTo(swiperTabs.activeIndex - 1);
      // console.log("on other tab" + swiperTabs.activeIndex);
    }

  }, 50);
};

//Check if user is loggedin or not
function checkLoggedInAndGetUserDetails(){
  let promise = new Promise(function(resolve, reject){
    firebase.database()
      .ref('/users/' + auth_id)
      .once('value')
      .then(function (snapshot) {
        console.log("Database searched",auth_id );
        user = snapshot.val()
        if(user){
          console.log("Database searched resolve");
          resolve(user)
        }else{
          console.log("Database searched reject");
          reject()
        }
      })

  })
  return promise;
}
