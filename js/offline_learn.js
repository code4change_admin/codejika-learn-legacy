/* submit offline code to the firebase
======================================*/
function validateOfllineCode() {
  var project_name = $(".project-name :selected").val();
  var project_code = $("textarea#offline-code").val();

  if (project_name === "") {
    $("select.project-name").css('border', 'solid 1px red');
    alert("Please select the project");
  } else if (project_code === "") {
    $("textarea#offline-code").css('border', 'solid 1px red');
    alert("Please paste your code in textarea");
  } else {
    selectproject_name();
  }
}

function selectproject_name() {
  var project_name = $(".project-name :selected").val();

  if (project_name === "project1") {
    validateProject1();
  } else if (project_name === "project2") {
    validateProject2();
  } else if (project_name === "project3") {
    validateProject3();
  } else {
    validateProject4();
  }
}

function validateProject2() {
  alert("Project 2 is not yet ready");
}

function validateProject3() {
  alert("Project 3 is not yet ready");
}

function validateProject4() {
  alert("Project 4 is not yet ready");
}


/*Declare html tags as checkpoint, validate and save to DB
==========================================================*/
function validateProject1() {
  /*Check points for P1-training 1
  ================================*/
  var check_points_for_training1 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*Launching Soon...((.|\n)*)\s*<\/h3>',
    '(<p>|<p [^>]*>)((.|\n)*)\s*<\/p>'
  ];
  var order_of_HTML_tags_training1 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>',
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/header>',
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020)((.|\n)*)\s*<\/p>((.|\n)*)\s*<\/header>'
  ];
  /*End Checkpoints for P1 training 1
  ==================================== */

  /*Check points for P1-training 2
  ================================*/
  var check_points_for_training2 = [
    '(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>',
    'h1((.|\n)*)\s*{((.|\n)*)\s*}',
    'h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*75px;((.|\n)*)\s*}',
    '(Motivation|motivation|MOTIVATION)',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/h3>',
    '(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>',
    'i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}'
  ];
  var order_of_HTML_tags_training2 = [
    '(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/body>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION)((.|\n)*)\s*<\/h3>',
    '(<h3>|<h3 [^>]*>)((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>'
  ];
  /*End Checkpoints for P1 training 2
  =================================== */

  /*Check points for P1-training 3
  ================================*/
  var check_points_for_training3 = [
    '(<header>|<header [^>]*>)((.|\n)*)\s*<\/header>',
    'header((.|\n)*)\s*{((.|\n)*)\s*}',
    'header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}',
    'header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient[(]110deg((.|\n)*)\s*,((.|\n)*)\s*%((.|\n)*)\s*%((.|\n)*)\s*[)]((.|\n)*)\s*}',
    '(<section>|<section [^>]*>)((.|\n)*)\s*<\/section>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|motivation|MOTIVATION):((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>',
    'section((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightgrey((.|\n)*)\s*}',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>',
    'footer((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*}'
  ];
  // 'header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient[(]110geg,((.|\n)*)\s*yellow((.|\n)*)\s*40%,((.|\n)*)\s*pink((.|\n)*)\s*40%\s*[)]((.|\n)*)\s*}',  
  var order_of_HTML_tags_training3 = [
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/header>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*section((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<body>|<body [^>]*>)((.|\n)*)\s*(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>((.|\n)*)\s*<\/body>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy((.|\n)*)\s*<\/footer>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*footer((.|\n)*)\s*}((.|\n)*)\s*<\/style>'
  ];
  /*End Checkpoints for P1 training 3
  =================================== */

  /*Check points for P1-training 4
    ================================*/
  var check_points_for_training4 = [
    '(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/div>',
    '(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>',
    'div((.|\n)*)\s*{((.|\n)*)\s*}',
    'div((.|\n)*)\s*{((.|\n)*)\s*text-align((.|\n)*)\s*:((.|\n)*)\s*center((.|\n)*)\s*}',
    'div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*((white((.|\n)*)\s*solid((.|\n)*)\s*2px)|(solid((.|\n)*)\s*white((.|\n)*)\s*2px)|(solid((.|\n)*)\s*2px((.|\n)*)\s*white)|(2px((.|\n)*)\s*solid((.|\n)*)\s*white)|(2px((.|\n)*)\s*white((.|\n)*)\s*solid))((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*45px((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*15px((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*:((.|\n)*)\s*auto((.|\n)*)\s*}',
    'h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*:((.|\n)*)\s*400px((.|\n)*)\s*}',
    'h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*:((.|\n)*)\s*(white|#FFF|#fff)((.|\n)*)\s*}'
  ];
  var order_of_HTML_tags_training4 = [
    '(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>',
    '(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>',
    '(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>',
    '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>'
  ];
  /*End Checkpoints for P1 training 4
  =================================== */

  var marks_for_training1 = validateHtmlElement(check_points_for_training1, order_of_HTML_tags_training1);
  var marks_for_training2 = validateHtmlElement(check_points_for_training2, order_of_HTML_tags_training2);
  var marks_for_training3 = validateHtmlElement(check_points_for_training3, order_of_HTML_tags_training3);
  var marks_for_training4 = validateHtmlElement(check_points_for_training4, order_of_HTML_tags_training4);

  marks_for_training1.project_name = "P1 Training 1";
  marks_for_training2.project_name = "P1 Training 2";
  marks_for_training3.project_name = "P1 Training 3";
  marks_for_training4.project_name = "P1 Training 4";

  var overall_marks = {
    p1_training1: marks_for_training1,
    p1_training2: marks_for_training2,
    p1_training3: marks_for_training3,
    p1_training4: marks_for_training4
  }

  /*Save overall marks to DB
  ==========================*/
  saveProject(overall_marks);
}

/*Take array of html tags as parameter and validate input code
==============================================================*/
function validateHtmlElement(check_points, wrapping_tags) {
  if (!$.isArray(check_points) && !$.isArray(wrapping_tags)) {
    throw "validateHtmlElement() needs a array parameters";
  }
  var total_marks = 0;
  var input_code = $("#offline-code").val();
  var validation_pass = false;
  var invalid_tags = [];

  /*get total marks
  =================*/
  for (var i = 0; i < check_points.length; i++) {
    var challenge_number = i + 1;
    var results_for_check_point = new RegExp(check_points[i], "i").test(input_code);
    if (results_for_check_point) {
      var results_for_wrapping_tag = new RegExp(wrapping_tags[i], "i").test(input_code);
      if (results_for_wrapping_tag) {
        total_marks++;
      } else {
        var openning_tag = check_points[i].split('|')[0];
        var is_css_selector = openning_tag.includes("((.");


        if (is_css_selector) {
          console.log("=> " + openning_tag);

          var css_selector = openning_tag.replace('((.', '');

          var wrapping_tag = wrapping_tags[i].split('|')[0];
          wrapping_tag = wrapping_tag.replace('(', '');
          invalid_tags.push("C" + challenge_number + ". " + css_selector + " css selector must be inside " + wrapping_tag + " tag.<br>");
        } else {
          if (openning_tag.includes('<') && openning_tag.includes('>')) {
            openning_tag = openning_tag.replace('(', '');
            openning_tag = openning_tag.replace('<', '&lt;');
            openning_tag = openning_tag.replace('>', '&gt;');

            var results = new RegExp(openning_tag).test(input_code);
            if (results) {
              invalid_tags.push("C" + challenge_number + ". There seems to be no " + openning_tag + " closing tag.<br>");
            } else {
              invalid_tags.push("C" + challenge_number + ". There seems to be an error with " + openning_tag + " tag or is missing.<br>");
            }
          } else {
            // total_marks++;
            invalid_tags.push("C" + challenge_number + ". did not include: &ldquo;" + openning_tag + "&rquo;.<br>");
          }
        }
      }
    }
  }


  /*get tags that have errors or are invalid
  ==========================================*/
  for (var i = 0; i < check_points.length; i++) {
    var challenge_number = i + 1;
    var reResults = new RegExp(check_points[i], "i").test(input_code);
    if (reResults) {
      if ((i + 1) == check_points.length) {
        invalid_tags.push("This project correct.<br>");
      }
    } else {
      var openning_tag = check_points[i].split('|')[0];
      var is_css_selector = openning_tag.includes("((.");
      if (is_css_selector) {
        var css_selector = openning_tag.replace('((.', '');
        invalid_tags.push("C" + challenge_number + ". " + css_selector + " ..... css selector is incorrect or it has errors.<br>");
      } else {
        if (openning_tag.includes('<') && openning_tag.includes('>')) {
          openning_tag = openning_tag.replace('(', '');

          var results = new RegExp(openning_tag).test(input_code);
          if (results) {
            openning_tag = openning_tag.replace('<', '&lt;');
            openning_tag = openning_tag.replace('>', '&gt;');
            invalid_tags.push("C" + challenge_number + ". .....There seems to be no " + openning_tag + " closing tag.<br>");
          } else {
            openning_tag = openning_tag.replace('<', '&lt;');
            openning_tag = openning_tag.replace('>', '&gt;');
            invalid_tags.push("C" + challenge_number + "..... There seems to be an error with " + openning_tag + " tag or is missing.<br>");
          }
        } else {
          total_marks++;
        }
      }
    }
  }

  var total_marks_in_percent = (total_marks / check_points.length) * 100;
  total_marks_in_percent = Math.round(total_marks_in_percent * 10) / 10;

  var score_for_trainning = total_marks_in_percent;
  var comments_on_score = "(" + total_marks + " of " + check_points.length + " key validators triggered.)<br>"; // Correct (3 of 5 key validators triggered.)

  return {
    comments_on_invalid_tags: invalid_tags,
    comments_on_score: comments_on_score,
    total_marks: total_marks,
    total_marks_in_percent: total_marks_in_percent
  }
}

/*save to firebase database
===========================*/
function saveProject(results) {
  var project_name = $(".project-name :selected").val();
  var project_code = $(".project-code").val();
  if (project_name !== "" && project_code !== "") {

    initFirebase();

    firebase.database().ref('/offline_version_submitted_code/' + auth_id).update({
      project: results
    });

    $(".offline-submition-page").fadeOut();
    $(".thank-you-page").fadeIn();
  }
}

function reSubmitCode() {
  location.reload();
}

/*Retrieve learner's marks from DB
==================================*/
function getOfflineProjectRecord() {
  initFirebase();

  var ref = firebase.database().ref('/offline_version_submitted_code/' + auth_id + '/project');
  ref.on("value", function(snapshot) {
    if (snapshot.exists()) {

      var data = snapshot.val();

      /*Construct text line for the better display
      ============================================*/
      var project1_marks = "";
      project1_marks += data.p1_training1.project_name + ": " + data.p1_training1.total_marks_in_percent + "%";
      project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>" + data.p1_training1.comments_on_score + "</span><br>";
      project1_marks += data.p1_training2.project_name + ": " + data.p1_training2.total_marks_in_percent + "%";
      project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>" + data.p1_training2.comments_on_score + "</span><br>";
      project1_marks += data.p1_training3.project_name + ": " + data.p1_training3.total_marks_in_percent + "%";
      project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>" + data.p1_training3.comments_on_score + "</span><br>";
      project1_marks += data.p1_training4.project_name + ": " + data.p1_training4.total_marks_in_percent + "%";
      project1_marks += " Correct <span class='clickable-link' onclick='showComments()'>" + data.p1_training4.comments_on_score + "</span><br>";

      $("#project01_marks").html(project1_marks);

      /*Filter project-training: if have errors or not
      ================================================*/
      if (data.p1_training1.comments_on_invalid_tags === undefined) {
        $("#project01_comments-per-trainging").html("You passed all challenges.");
      } else {
        $("#project01_comments-per-trainging").html(data.p1_training1.comments_on_invalid_tags)
      }
      if (data.p1_training2.comments_on_invalid_tags === undefined) {
        $("#project02_comments-per-trainging").html("You passed all challenges.");
      } else {
        $("#project02_comments-per-trainging").html(data.p1_training2.comments_on_invalid_tags);
      }
      if (data.p1_training3.comments_on_invalid_tags === undefined) {
        $("#project03_comments-per-trainging").html("You passed all challenges.");
      } else {
        $("#project03_comments-per-trainging").html(data.p1_training3.comments_on_invalid_tags);
      }
      if (data.p1_training4.comments_on_invalid_tags === undefined) {
        $("#project04_comments-per-trainging").html("You passed all challenges.");
      } else {
        $("#project04_comments-per-trainging").html(data.p1_training4.comments_on_invalid_tags);
      }

      var total_marks = (parseInt(data.p1_training1.total_marks_in_percent) / 100) + (parseInt(data.p1_training2.total_marks_in_percent) / 100) + (parseInt(data.p1_training3.total_marks_in_percent) / 100) + (parseInt(data.p1_training4.total_marks_in_percent) / 100);
      total_marks = (total_marks / 4) * 100;
      total_marks = Math.round(total_marks * 10) / 10;
      $("#total-marks").html("Total score: " + total_marks + "% Correct");
    }
  }, function(error) {
    console.log("Error: " + error.code);
  });
}