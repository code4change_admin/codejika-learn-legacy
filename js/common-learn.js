 var showRegisterModal
  if(window.location.href.indexOf("#register") > -1) {
     showRegisterModal = true;
  } 

if (set_user_id === undefined || set_user_id === '') {
  var auth_id = localStorage.getItem('userid')
  if (auth_id == null) {
    auth_id = Math.random()
      .toString(20)
      .replace('0.', '')
    localStorage.setItem('userid', auth_id)
  }
} else {
  auth_id = set_user_id
  localStorage.setItem('userid', auth_id)
}
  console.log('auth_id: ' + auth_id)
if (history.replaceState) {
  var newurl =
    window.location.protocol +
    '//' +
    window.location.host +
    window.location.pathname +
    '?' +
    auth_id
  // '&lesson_id=' +
  // lesson_id    
  console.log('newurl: ' + newurl)
  window.history.replaceState({ path: newurl }, '', newurl)
}


var cjcoza  = location.hostname == "codejika.co.za" ? true : false;
var cjcom  = location.hostname == "codejika.com" ? true : false;
var cjtest = location.hostname == "codejika.test" ? true : false;
var zeroRated = false;

// use proxy 
if(cjcoza || cjtest){
  zeroRated = true
} 

// use alt gtag for staging domains or testing
if(!cjcom && !cjcoza){
  var staging_analytics = true
} 

var DEBUG = true
var delay
var lesson_progress = null
var prev_submit_code = null
var unlockSkills = { };
var slide_data = null
var user_skills = null
var active_slide = 1
var active_tab
var save_pending = false
var first_cp_reached = false
var checkpoint_id = 0
var current_checkpoint_id = 0
var checkpoint_count = 0
//var checkpoint_completed = 0
var progressPercentage
var current_avatar = 'robot'
var swiperTabs
var swiperLesson
var swiperMenu
var first_name
var last_name
var nick_name 
var user_details
var total_slides = 0;
var loading = null
var current_page = 0
var page_id_array = ['']
var user_HTML
var current_key1
var user_code1
var userCodeHtml2 = ''
var pagesize = 6
var current_page_size = 0
var dialogInMenu = false;
var current_year = new Date().getFullYear()
var screenHeight = screen.height + "px";
var screenHeight_plus23 = screen.height + "px";
var student_fullname = ""
var reload_projects;
var historySet = false;
var timeout;
var user_status;
var checkpoints_completed = [];

var greetcode = Boolean(localStorage.getItem('greetcode')) || false;
var greetview = Boolean(localStorage.getItem('greetview')) || false;

if(page_template == "lessons") {
  if (lesson_data['save_lesson_id']  !== "undefined") {     
    save_lesson_id = lesson_data['save_lesson_id']    
  }
}

//
// Override console.log output and set custom feedback level
//

if (!DEBUG) {
  if (!window.console) window.console = {}
  var methods = ['log', 'debug', 'warn', 'info']
  for (var i = 0; i < methods.length; i++) {
    console[methods[i]] = function () { }
  }
}

/* disabled as this method cause all console logs to show  all as coming from same line number in js file
window.console = (function (origConsole) {

    if (!window.console || !origConsole) {
        origConsole = {};
    }

    var isDebug = false, isSaveLog = false,
        logArray = {
            logs: [],
            errors: [],
            warns: [],
            infos: []
        };

    return {
        log: function () {
            this.addLog(arguments, "logs");
            isDebug && origConsole.log && origConsole.log.apply(origConsole, arguments);
        },
        warn: function () {
            this.addLog(arguments, "warns");
            isDebug && origConsole.warn && origConsole.warn.apply(origConsole, arguments);
        },
        error: function () {
            this.addLog(arguments, "errors");
            isDebug && origConsole.error && origConsole.error.apply(origConsole, arguments);
        },
        info: function (v) {
            this.addLog(arguments, "infos");
            isDebug && origConsole.info && origConsole.info.apply(origConsole, arguments);
        },
        debug: function (bool) {
            isDebug = bool;
        },
        saveLog: function (bool) {
            isSaveLog = bool;
        },
        addLog: function (arguments, array) {
            if (!isSaveLog) {
                return;
            }
            logArray[array || "logs"].push(arguments);
        },
        logArray: function () {
            return logArray;
        }
    };
}(window.console));
*/

function removeLocationHash(){
  console.log('removeLocationHash')
 // var noHashURL = window.location.href.replace(/#.*$/, '');
 // window.history.replaceState('', document.title, noHashURL) 
}

function timeNow() {
  return new Date().toISOString()
}

function findIndexOfKeys(keys, value) {
  var index = 1
  for (var key in keys) {
    if (value == key) {
      return index
    } else {
      index++
    }
  }
}

function titleCase(str) {
  str = str.toLowerCase().split(' ');
  for (var i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return str.join(' ');
}


/// TRIGGERS IF CLICK LINK TO LEAVE PAGE
function goodbye(e) { //, bypass) {
  //  if (!bypass) {
  if (!e) e = window.event;
  //e.cancelBubble is supported by IE - this will kill the bubbling process.
  e.cancelBubble = true;
  e.returnValue = 'You sure you want to leave?'; //This is displayed on the dialog

  //e.stopPropagation works in Firefox.
  if (e.stopPropagation) {
    e.stopPropagation();
    e.preventDefault();
  }
  // }
}
//window.onbeforeunload = goodbye;

function refreshValues() {
  greetcode = localStorage.getItem('greetcode') || false;
  greetview = localStorage.getItem('greetview') || false;
}

function buildPage() {

  if (page_template == "lesson") {
    if (lesson_data['pageTitle']  !== "undefined") { 
      $('title').text(lesson_data['pageTitle']);
    }
    if (lesson_data['pageDesc']  !== "undefined") {     
      $('meta[name=description]').attr('content', lesson_data['pageDesc']);
    }
    if (lesson_data['pageKeywords']  !== "undefined") {     
      $('meta[name=keywords]').attr('content', lesson_data['pageKeywords']);
    }
  }



}

//////////////////////////////////////////////
// GOOGLE ANALYTICS                         //
//////////////////////////////////////////////

  <!-- Global Site Tag (gtag.js) - Google Analytics -->

  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
    (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
    m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', window.location.protocol +
    '//' + window.location.host + '/libraries/analytics/analytics-custom.js', 'ga');

  if (staging_analytics) {
    ga('create', 'UA-63106610-4', 'auto');
  } else {
    ga('create', 'UA-63106610-3', 'auto');   
  }
  ga('send', 'pageview');
  

//////////////////////////////////////////////
// USER PROFILE                             //
//////////////////////////////////////////////


// ***********  save user login details to local storage
function saveLogin() {
  // console.log('logindetails//////')
  var first_name = document.getElementById('f_name').value
  var last_name = document.getElementById('l_name').value
  var nick_name = document.getElementById('n_name').value
  var day = document.getElementById('B-day').value
  var month = document.getElementById('B-month').value
  var year = document.getElementById('B-year').value
  // console.log(first_name, last_name, nick_name, day, month, year)
}

function resetUserData() {
  firebase
    .database()
    .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
    .remove()
}

function resetCurrentProject() {
  resetUserData()
  localStorage.setItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide', 1)
  window.location.reload()
}

function checkUserKey() {
/*  let userName = document.querySelector('.profile_name')
  var validateAuthId = firebase.database()
  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function(snapshot) {
      data = snapshot.val()
      var keys = Object.keys(data)
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
      if (findKey != auth_id) {
        // registerModal()
        console.log("no user found with auth_id): " + auth_id);
      } else {
        validateAuthId.ref('/users/' + auth_id).once('value').then((snapshot) => {          res = snapshot.val()
          // var uKeys = Object.keys(res);
          user_details = res;
*/
        if (user_details !== undefined && user_details !== null) { 
          if (user_details.hasOwnProperty('first_name')) { 
            $("h2.profile_name").html('Hello, ' + user_details.first_name);          }
          
          $(".login-button, .register-button").addClass("d-none");
          $(".logout-button, .profile-button").removeClass("d-none");
          user_status = "userRegistered"
        }

        if (user_status == "userSession") {
          $('body').addClass("is-userSession");        
        } else if (user_status == "userRegistered"){
          $('body').addClass("is-userRegistered"); 
        } else {
          $('body').addClass("is-userGuest"); 
        }
     /*   })
      }
    })
    */   
}

function changeAvatar(avatar) {
  $('.modal-content')
    .removeClass(current_avatar)
    .addClass(avatar)
  $('.select_robot, .select_lego').toggleClass(
    'option_selected option_unselected'
  )
  current_avatar = avatar
  //  successAnimation()
}

function logoutProfile() {
  saveUserData(true);  
  localStorage.removeItem("userid");
  Cookies.remove('auth_id');
  var newurl =
    window.location.protocol +
    '//' +
    window.location.host +
    window.location.pathname
  console.log('newurl: ' + newurl)
  window.history.replaceState({
    path: newurl
  }, '', newurl)
  window.location.reload()
}


//////////////////////////////////////////////
// SAVE/LOAD/UPDATE LESSON DATA             //
//////////////////////////////////////////////

function setCheckpointProgress() {
 // console.log("checkpoints_completed before: " + checkpoints_completed + " " + $.inArray(current_checkpoint_id, checkpoints_completed) );

  if($.inArray(current_checkpoint_id, checkpoints_completed) < 0) {
    checkpoints_completed.push(current_checkpoint_id)
    checkpoints_completed.sort(function(a, b){return a-b})
    console.log("checkpoints_completed after: " + checkpoints_completed );
}
   
   console.log("progress_completed: " + checkpoints_completed.length * 100 / checkpoint_count );
   console.log("getCurrentCheckpointId: " + getCurrentCheckpointId() );
  firebase
  .database()
  .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
  .update({
      last_checkpoint: current_checkpoint_id,  // log which was current checkpoint so can return on reload
      progress_completed: (checkpoints_completed.length * 100 / checkpoint_count) < 100 ? checkpoints_completed.length * 100 / checkpoint_count : 100,
      checkpoints_completed: checkpoints_completed
    });

}

function getCheckpointProgress() {
  firebase
    .database()
    .ref('/user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
    .once('value')
    .then(function(snapshot) {
      data = snapshot.val()
      console.log("progress_completed: "+ data.progress_completed);   
      return data.progress_completed;
    })        
}

function saveSubmitData() {
  //console.log("saveSubmitData " + active_slide + '  ' + getCheckpointSlideId(checkpoint_count));

 /* if (device_type == "desktop") { */
    var check_slide = active_slide
 /*} else {
    var check_slide = active_slide + 1;
  }*/

  if (checkpoints_completed.length == checkpoint_count) {
      firebase
        .database()
        .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
        .update({
          submit_code: getEditorValue(),
          last_updated: timeNow()
        })      
      console.log("Saved submit code at checkpoint #" + active_slide + " " + timeNow());
  } else {
      console.log("Nothing to submit: haven't completed the last CP");
  }      
}

function loadUserData() {

 var loaded_user_code 
 if (lesson_progress !== undefined && lesson_progress.user_code !== undefined) {
  loaded_user_code = lesson_progress.user_code
  }
  //console.log("lesson_data: "+JSON.stringify(lesson_data));   
  //console.log("lesson_progress: "+JSON.stringify(lesson_progress));   

  console.log("getMostRecentCode(): "+ loaded_user_code)
  if (loaded_user_code == undefined || isBlank(loaded_user_code)) {
    //prev_submit_code = getPrevSubmitCode();  
    console.log("Prev submit_code: "+ prev_submit_code); 
    //        console.log("val2 true" + timeNow()+prev_submit_code)
    //console.log("prev_submit_code: "+JSON.stringify(getPrevSubmitCode()))  
    if (prev_submit_code != null) { // && !isBlank(prev_submit_code)) {
      loaded_user_code = prev_submit_code;  

    } else {
        console.log("lesson_data.defaultCode: "+JSON.stringify(lesson_data.defaultCode));
      loaded_user_code = lesson_data.defaultCode;        
    }
  }
   
  console.log("loaded_user_code: "+JSON.stringify(loaded_user_code));
  
  setEditorValue(loaded_user_code);
}

function buildCerts(cert_data) {

 /* if (progress_data.hasOwnProperty('certificate')) {
    return prepareCertificates(progress_data);
  } else {
    return '<p>Please complete the Course to get the certificates</p>';
  }

*/
    var sectionCertificate = ''
  var certificates = Object.keys(user_details.certificate)
  certificates.map(element => {
    sectionCertificate +=`<h6 class="mt-3">${element}</h6>
        <div class="certificates-container">
          <a class="certificate-card Card_pdf_${element}" onclick="exportPdfCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}',showCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}'))">
            <div class="certificate"> certificate PDF </div>
            <strong style="display-block">PDF</strong>
          </a>
          <a class="certificate-card Card_html_${element}" onclick="openHtmlCertificate(showCertificate('${user_details.first_name}', '${user_details.last_name}', '${element}','${user_details.certificate[element].completed_at}'))">
            <div class="certificate"> certificate html </div>
            <strong style="display:block">HTML</strong>
          </a>
        </div>`
  });
 // $(".dfsd").appendChild(node: Node)


}

var progress_count_5MW = 0;
var progress_count_P1 = 0;
var progress_count_P2 = 0;
var progress_count_P3 = 0;
var next_lesson = "";
var unlock_next_lesson_P1 = "";
var unlock_next_lesson_P2 = "";
var unlock_next_lesson_P3 = "";
var certs_awarded = [];

function setProjectProgress() {
  if (project_progress !== null && project_progress.lesson_progress !== undefined) {
    var keys = Object.keys(project_progress.lesson_progress)
    //  console.log("keys: "+JSON.stringify(project_progress.lesson_progress));
   //   console.log("keys: "+JSON.stringify(keys));
   
  Object.keys(project_progress.lesson_progress).forEach(function(index) {
     
       var progress_data = project_progress.lesson_progress[index];
       console.log("keys: "+index);     
     //console.log("keys: "+JSON.stringify(progress_data));
     
      if (progress_data.hasOwnProperty('certificate')) { 
        next_lesson = index;
        //return lesson_progress.user_checkpoint[index].user_code
console.log("next_lesson: "+$(".menu .row."+index+" .icon-list ").hasClass("bg-green") ); 
          
        certs_awarded.push(index);

        
        $(".menu .row."+index).removeClass("locked").addClass("unlocked");
        $(".menu .row."+index+" .icon-list ").html("<i class='fas fa-check'></i>").removeClass("bg-silver").addClass("bg-green");
        if (!$(".menu .row."+index+"  ").next().find(".icon-list").hasClass("bg-green") ) {
          $(".menu .row."+index+"  ").next().removeClass("locked").addClass("unlocked").find(".icon-list").html("<i class='fas fa-play'></i>").removeClass("bg-silver").addClass("bg-orange");
        }
        

       // $(".menu .row."+index).append("<a class='divLink' href='../learn/"+index+"'></a>");        
        $(".menu .row."+index+" .lesson-progress-bar div").css({"background-size":"100% 100%","background-image":"linear-gradient(to right, #8bc34a , #8bc34a )"});
console.log("progress_count_P1a: "+JSON.stringify(index).substring(1,4));
        if (JSON.stringify(index).substring(1,4) =="5-m") {
          progress_count_5MW++;    
        } else if (JSON.stringify(index).substring(1,4) =="P1T") {
          progress_count_P1++;    
          if (next_lesson !== unlock_next_lesson_P1) {
           unlock_next_lesson_P1 = index;
           console.log("unlock_next_lesson: "+unlock_next_lesson_P1); 
          }
        } else if (JSON.stringify(index).substring(1,4) == "P2T") {
          progress_count_P2++;    
          if (next_lesson !== unlock_next_lesson_P2) {
           unlock_next_lesson_P2 = index;
           console.log("unlock_next_lesson: "+unlock_next_lesson_P2); 
          }
        }else if (JSON.stringify(index).substring(1,4) == "P3T") {
          progress_count_P3++;       
          if (next_lesson !== unlock_next_lesson_P3) {
           unlock_next_lesson_P3 = index;
           console.log("unlock_next_lesson: "+unlock_next_lesson_P3); 
          }
        }
     //   console.log("certficate.completed is true for: "+JSON.stringify(index));
      } else if (progress_data.hasOwnProperty('progress_completed')) {
     //  console.log("progress_completed: "+JSON.stringify(progress_data.progress_completed));
       if ($(".menu .row."+index+" .icon-list ").hasClass("bg-silver") ) {
         $(".menu .row."+index).removeClass("locked").addClass("unlocked");
         $(".menu .row."+index+" .icon-list ").html("<i class='fas fa-play icon-play'></i>").removeClass("bg-silver").addClass("bg-orange");
        }
       //$(".menu .row."+index).append("<a class='divLink' href='../learn/"+index+"'></a>");
           $(".menu .row."+index+" .lesson-progress-bar div").css({"background-size": progress_data.progress_completed+"% 100%","background-repeat-x" : "no-repeat","background-image":"linear-gradient(to right, #8bc34a, #8bc34a)"});
        


      } else  {
       console.log("certificate.completed is null");
      } 
 /*
      */ 
    }); 

    console.log("progress_count_5MW: "+progress_count_5MW);
    console.log("progress_count_P1: "+progress_count_P1);
    console.log("progress_count_P2: "+progress_count_P2);

console.log("unlock_next_lesson_final: "+unlock_next_lesson_P1); 
console.log("unlock_next_lesson_final: "+unlock_next_lesson_P2); 
console.log("unlock_next_lesson_final: "+unlock_next_lesson_P3); 

    if(progress_count_5MW == 1) {
     $(".5MW-intro .project-start").html("View Training <i class='fas fa-fw  fa-play ml-1' aria-hidden='true'></i>"); 
     //$(".5MW-intro .project-start").removeClass("bg-green").addClass("bg-orange");
     $(".5MW-completed").html("<i class='fas fa-check-circle green mr-1'></i>Intro Completed<i class='far fa-smile ml-1'></i> <i class='far fa-thumbs-up'></i>");
    } else {
     $(".5MW-intro .project-view").html("Start Training <i class='fas fa-fw  fa-play ml-1' aria-hidden='true'></i>"); 
     $(".5MW-completed").html("Course Progress: 00 of 01");
    }

    if(progress_count_P1 == 4) {
     $(".P1T-intro .project-view").html("View Project  <i class='fas fa-smile-beam ml-1'></i>"); 
     //$(".P1T-intro .project-view").removeClass("bg-green").addClass("bg-orange");
     $(".P1T-completed").html("All Trainings Completed<i class='far fa-smile ml-1'></i> <i class='far fa-thumbs-up'></i>");
    } else if(progress_count_P1 >= 1) {
     $(".P1T-intro .project-view").html("Continue Project <i class='fas  fa-fw fa-play ml-1'></i>"); 
     $(".P1T-intro .project-view").removeClass("bg-green").addClass("bg-orange");
     $(".P1T-completed").html("Course Progress: 0" + progress_count_P1 + " of 04");
    // console.log($(".menu .row.P1Training2").next().html());
    // $(".menu .row."+unlock_next_lesson_P1).next().children("div").html("<i class='fas fa-play'></i>").removeClass("bg-silver").addClass("bg-orange");     
     //$(".P1T-intro .menu .row."+index+" .icon-list ").html("<i class='fas fa-play'></i>").removeClass("bg-silver").addClass("bg-orange");

   /*  $("#project-1.lesson-details .menu > .row ").each(function( index, element ) {
        
        if($("#project-1 .menu > .row:nth-child(" + (index +1 ) + ") .icon-list .fa-play")) {
          $("#project-1 .menu > .row:nth-child(" + (index +2 ) + ") .icon-list").removeClass("bg-silver").addClass("bg-orange").html("<i class='fas fa-play'></i>")
          console.log("#project-1 .menu > .row:nth-child(" + (index +1 ) + ") ");
          return false;
        }
      // $(".menu .row."+unlock_next_lesson_P1).next().children("div").html("<i class='fas fa-play'></i>").removeClass("bg-silver").addClass("bg-orange"
     })*/
    }
    
    if(progress_count_P2 == 6) {
     $(".P2T-intro .project-view").html("View Project <i class='fa fa-smile-beam ml-1'></i>"); 
     //$(".P2T-intro .project-view").removeClass("bg-green").addClass("bg-orange");
     $(".P2T-completed").html("All Trainings Completed<i class='far fa-smile ml-1'></i> <i class='far fa-thumbs-up'></i>");
    } else if(progress_count_P2 >= 1) {
     $(".P2T-intro .project-view").html("Continue Project <i class='fas fa-fw  fa-play ml-1' aria-hidden='true'></i>"); 
     $(".P2T-intro .project-view").removeClass("bg-green").addClass("bg-orange");
     $(".P2T-completed").html("Course Progress: 0" + progress_count_P2 + " of 06");
    }
    
    if(progress_count_P3 == 6) {
     $(".P3T-intro .project-view").html("View Project <i class='fa fa-smile-beam ml-1'></i>"); 
     //$(".P2T-intro .project-view").removeClass("bg-green").addClass("bg-orange");
     $(".P3T-completed").html("All Trainings Completed<i class='far fa-smile ml-1'></i> <i class='far fa-thumbs-up'></i>");
    } else if(progress_count_P3 >= 1) {
     $(".P3T-intro .project-view").html("Continue Project <i class='fas fa-fw  fa-play ml-1' aria-hidden='true'></i>"); 
     $(".P3T-intro .project-view").removeClass("bg-green").addClass("bg-orange");
     $(".P3T-completed").html("Course Progress: 0" + progress_count_P3 + " of 06");
    }

  }
}

function getMostRecentCode() {
  /*if (lesson_progress.user_checkpoint) {
    var keys = Object.keys(lesson_progress.user_checkpoint)
      console.log("keys: "+JSON.stringify(keys));
    var k = keys.length
      console.log("k: "+k);
    if (lesson_progress.user_checkpoint[k].user_code) {
      return lesson_progress.user_checkpoint[k].user_code
    } else {
      return "";
    } 
  }*/
  return lesson_progress.user_code;
}

/*function getPrevSubmitCode() {
  if (lesson_data.prevLessonID != undefined) {
    console.log("lesson_data.prevLessonID is: " + lesson_data.prevLessonID);   
    firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_data.prevLessonID).once('value').then(function(snapshot) {
      data = snapshot.val();
      console.log("Prev submit_code: "+ JSON.stringify(data));   
      return data.submit_code;
    })        
  }   else {
    return "";
  }    
}
*/

function getPrevSubmitCodePromise() {
  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + lesson_data.prevLessonID).once('value').then(function(snapshot) {
    return snapshot.val();
  }, function(error) {
    console.log(error);
  });
}

/*
function getPrevSubmitCode() {
  if (lesson_data.prevLessonID != undefined) {
    get_prev_submit_code = getPrevSubmitCodePromise();
    Promise.all([get_prev_submit_code]).then(function(results) {
      console.log("val results[0]" + results[0].submit_code)
      data = results[0];
        console.log("val true" + timeNow()+data.submit_code)
        return data.submit_code;
      });
  }   else {
    return "f";
  }    
}
*/

function getCurrentCheckpointId() {
  let checkpoint_id = $("#slide"+ active_slide).data('checkpoint_id')
  if (checkpoint_id) {
  console.log("getCurrentCheckpointId: " + checkpoint_id);
    return checkpoint_id; 
  } else {
      console.log("getCurrentCheckpointId: null" );
    return null;
  }
} 

function getCheckpointSlideId(check_checkpoint_id) {
  console.log("check_checkpoint_id " + check_checkpoint_id);
  return $("div[data-checkpoint_id="+check_checkpoint_id+"]").attr("id").replace("slide", "");
} 

function setActiveSlide() {
  var slide_num = 1;

  if (localStorage.getItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide') !== null) {
    console.log("localStorage.getItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide' " + slide_num);
    slide_num = eval(localStorage.getItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide'))
  } else {
    //TODO temp disabled as issue with mobile/desktop not having same amount of CP count in lesson
    //if (lesson_progress.last_checkpoint > 0) {
    //  slide_num = getCheckpointSlideId(lesson_progress.last_checkpoint)
   //console.log("lesson_progress.last_checkpoint > 0 " + slide_num);
   // } else {
      slide_num = 1
      console.log(" lesson_progress else " + slide_num);
   // }
   // END TODO
   //TESTING: 24-04-20 removed as prob no need to set as already happens on slide swipe
    //localStorage.setItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide', slide_num)
    // END TESTING
  }     
   console.log("return " + slide_num);

   // TODO temp fix as both versions are not in sync forwhat is the active_slide index
  /* if (device_type == "mobile") {
      return slide_num -1 ;
   } else {*/
    return slide_num;           
   //}

} 

function saveUserData(save_code) {
  console.log("saveUserData > checkpoint_id: " + checkpoint_id + ", active_slide: " + active_slide + ", save_pending: " + save_pending  + ", save_code: " +  save_code  + ", first_cp_reached: " +  first_cp_reached);
  //TODO getCP is not working accurately enough, so just save after 5 slides
  if (first_cp_reached == false && checkpoint_id >= 1) { 
    first_cp_reached = true
  }
  if (first_cp_reached == true) {   
    if (save_pending || save_code) {
      //console.log("lesson_progress.progress_completed: " + lesson_progress.progress_completed);
      firebase        
        .database()
        .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
        .update({
          user_code: getEditorValue(),
          last_updated: timeNow()
        });

      console.log("Saved code at checkpoint #" + active_slide + " " + timeNow());
      save_pending = false;
    } else {
      console.log("save_pending || save_code: "+save_pending + " || " + save_code);
      console.log("Nothing to save: "+timeNow());
    }
  } else {
      console.log("Nothing to save: haven't reached 1st challenge");
  }      
}

function updatePreview() {
  var previewFrame = document.getElementById(device_type + '-preview');  
  var preview = previewFrame.contentDocument || previewFrame.contentWindow.document;
  preview.open();
  console.log('in preview: ' +getEditorValue());
  //var checkisBlank_if_blank = getEditorValue().replace(/(\r\n\t|\n|\r\t)/gm, '').trim() 
  if (isBlank(getEditorValue())) {
    preview.write(
      '<html><head></head><body style="Xheight:100%;Xwidth:100%;padding-top: 30%;"><h2 class="no_preview" style="width:100%;color:#bbb;font-size:16px;font-family:\'Rajdhani\',sans-serif;text-transform:uppercase;font-weight:bold;text-align:center;margin:auto;padding:0;">No preview available<br><br>Time to get coding</h2></body></html>'
      
    )
    console.log('no preview');
  } else {
    console.log('show  preview');
  preview.write(getEditorValue());
  }
  preview.close();
  save_pending = true;

  if (device_type == "desktop") {
    validateCheckpoint(); // mobile will validate instead on tab change 
  }
}

function progressUpdate() {
  console.log("progressUpdate" + check_string)
  var progressPercentage = checkpoint_count * 100 / getCurrentCheckpointId();
  firebase
    .database()
    .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id)
    .set({
      progressCompleted: progressPercentage
    });
}

function isBlank(check_string) {
  //console.log("check_string: " + check_string)
  if (check_string.replace(/(\r\n\t|\n|\r\t)/gm, '').trim() == "") {
    return true;
  }
  else {
    return false;
  }
}

function validateCheckpoint() {

 // console.log("slides:\n" + JSON.stringify(lesson_data.slides));
  console.log("slide_data:\n" + JSON.stringify(slide_data));

  let slide_id = active_slide; // + 1
  //     checkpoint_id = $('#slide' + slide_id).data('checkpoint_id')  
  console.log('#slide_id: ' + slide_id)


  if (slide_data !== null && slide_data !== undefined && slide_data.checkpoint === true) {
   console.log('#checkpoint_id: ' + checkpoint_id)   
    if (slide_data.checkpoint === true) {
         checkpoint_id = $('#slide' + slide_id).data('checkpoint_id')  
    console.log('#checkpoint_id: ' + checkpoint_id)
       }

    if (slide_data !== null && slide_data.action === true) {


      if (slide_data.reg !== undefined) {
        var cp_unlocked = false
        // do something

        console.log('Trigger validation' + slide_data.reg)      
        console.log('Run validation on ' + active_slide)

        $.each(slide_data.reg, function (index, re) {
          // console.log('#slide' + slide_id + ' .task-' + (index + 1))
           var reResults = new RegExp(re, 'i').test(getEditorValue())
          console.log('Run validation results ' + reResults,re)
          //  $(".check-info").removeClass('d-none');
          if (reResults) {
            current_checkpoint_id = getCurrentCheckpointId();
            setCheckpointProgress();
            saveSubmitData();
            console.log('inside reResults and getCurrentCheckpointId(): ' + current_checkpoint_id);

            if(device_type == "desktop")  {
              $("#slide" + active_slide + " .actions li").addClass('correct');
            }

            cp_unlocked = true
            if (
              !$('#slide' + slide_id + ' .task-' + (index + 1)).hasClass('correct')
            ) {
              $('#slide' + slide_id + ' .task-' + (index + 1)).addClass('correct')
              saveUserData(true)
            }
            // console.log(index)
            if (
              !$('#slide' + slide_id + ' .task-' + (index + 1) + ' i').hasClass(
                'icon-beenhere'
              )
            ) {
              if (device_type == "mobile") {
                $('.success-animation').addClass('hide')
                successAnimation()
              }
            }
            $(
              '#slide' + slide_id + ' .task-' + (index + 1) + ' .check-icon'
            ).html('<i class="icon-beenhere"></i>')

            console.log('Run validation using: ' + re);
            // $(".success-animation").show();


           /* if (device_type == "mobile")  {
              $('#slide' + slide_id).addClass('validated')
            } else {*/
              $('#slide' + (slide_id)).addClass('validated')     
           // }

             
          } else {
              $(
                '#slide' + slide_id + ' .task-' + (index + 1) + ' .check-icon'
              ).html('<i class="icon-close1"></i>')
              $('#slide' + slide_id + ' .task-' + (index + 1)).removeClass(
                'correct'
              )
            if(device_type == "desktop")  {
              if (!$(".hint-popup-checked").hasClass("checked" + active_slide)) {
                $(".hint-popup-checked").addClass("checked" + active_slide);
                if (!$("#slide" + active_slide + " .check").hasClass('passed')) {
                  if (hintsForCheckPonts[active_slide] !== undefined) {
                    htmlTagToString = hintsForCheckPonts[active_slide];
                    $(".hint-text").html(htmlTagToString);
                    $(".hint-popup").delay(5000).fadeIn().delay(3000).fadeOut(5000);
                    $(".close-hint-popup").click(function() {
                      $(".hint-popup").clearQueue();
                      $(".hint-popup").fadeOut();
                    });
                    $(".hint-popup").click(function() {
                      $(".hint-popup").clearQueue();
                      $(".hint-popup").fadeOut();
                    });
                  }
                }
              }
            }
           /*   if (device_type == "mobile")  {
                $('#slide' + slide_id).removeClass('validated')
              } else {*/
                $('#slide' + (slide_id)).removeClass('validated')
              //}

             // $("#slide" + active_slide + " .check-icon").html('\\');
             // $("#slide" + active_slide + " .actions li").removeClass('correct');
             // $("#slide" + active_slide + " .check").html('Skip this →').removeClass('passed');

          }
        });



        if (!$('#slide' + slide_id).hasClass('cp-unlocked') && cp_unlocked) {
   

         /* if (device_type == "mobile")  {
            $('#slide' + slide_id).addClass('cp-unlocked')
          } else {*/
            $('#slide' + (slide_id)).addClass('cp-unlocked')     
         // }

          //checkpoint_id = slide_data.checkpoint_id;
          
          // TEST disable as bug in adding info to profile that wasn't stored prev in '/user/''
          checkpointCompleted(checkpoint_id)

          if (slide_data.unlock_skills !== undefined) {
            unlockSkills(slide_data.unlock_skills)

            if (user_skills !== null) {
              user_skills.push(slide_data.unlock_skills)
              var unique_user_skills = []
              $.each(user_skills, function (i, el) {
                if ($.inArray(el, unique_user_skills) === -1) {
                  unique_user_skills.push(el)
                }
              })
              /// /console.log("set unique unlockSkills: " + unique_user_skills);
              firebase
                .database()
                .ref('/user_profile/' + auth_id + '/skills')
                .set(unique_user_skills)
            } else {
              user_skills = {
                '0': slide_data.unlock_skills
              }
              /// /console.log("set unlockSkills: " + user_skills);
              firebase
                .database()
                .ref('/user_profile/' + auth_id + '/skills')
                .set(user_skills)
            }
          }
        }


      }

    } else {
      /// /console.log('no action set for slide | slide_data action: ' + slide_data.action + ' | slide_id: ' + slide_id);
    }

    if (slide_data !== null && slide_data.js_function !== undefined) {
      console.log('js_function trigger: ' + slide_data.js_function);
      // var func = new Function("console.log('i am a new function')");
      var func = new Function(slide_data.js_function)
      func()
    }

    if (slide_data !== null && slide_data.reg !== undefined && device_type == "desktop")  {

      if ($('#slide' + active_slide).hasClass('cp-unlocked')) {
        $(".pagination .next").removeClass('d-none');
        console.log('remove d-none cp');
      } else {
        $(".pagination .next").addClass('d-none');
        console.log('add d-none cp');
      }
    }
  }

}

function checkpointCompleted(completed_id) {
  //checkpoint_completed++

  console.log("checkpoints_completed: " + checkpoints_completed);
  //console.log("checkpoint_completed: " + checkpoint_completed);
  console.log("checkpoint_count: " +  checkpoint_count);

  if (checkpoints_completed.length === checkpoint_count) {
    lesson_progress.progress_completed = '100'
    lesson_status = 'completed'
    console.log("checkpointCompleted: " + completed_id + " " + timeNow() + " " + checkpoints_completed + " " + checkpoint_count);

    firebase
      .database()
      .ref('user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id + '/certificate')
      .update({
        completed: true,
        user_code: getEditorValue(),
        completed_at: timeNow()
      })

    checkCertAwarded();

  } else {
    lesson_progress.progress_completed = Math.round(
      (checkpoint_id * 100) / (checkpoint_count + 0)
    )
    /// console.log(checkpoint_id + " * 100 / " + checkpoint_count + " = " + lesson_progress.progress_completed);
  }
  ga('send', {
    hitType: 'event',
    eventCategory: lessson_url,
    eventAction: 'Challenge Completed',
    eventLabel: active_slide + 1
  })
  // console.log(active_slide + 1)
  saveUserData();

}


function buildLessonSlides() {
  var slide_checkpoint_id = 1
  var slide_indexes = []
  var unlocked_class = ''

    active_slide = setActiveSlide();


  slide_data = lesson_data.slides[active_slide]
  all_slides = lesson_data.slides
  
  // $("#resources_tab").html(hints_data);


   console.log("active_slide:" + active_slide);
  // str = JSON.stringify(slide_data, null, 4);
  // console.log("slide_data:\n" + str);
  // str = JSON.stringify(all_slides, null, 4);
  // console.log("all_slides:\n" + str);

  $.each(all_slides, function (index) {
    /// /console.log(all_slides[index]);

    total_slides++; //used to count how many slides in total
    console.log("total_slides: " + total_slides);
    if (all_slides[index].css_class) {
      custom_class = all_slides[index].css_class
    } else {
      custom_class = ''
    }
     //   console.log("checkpoint: "+index);
    /*if (all_slides[index].checkpoint === true) {
      // cp_class = " checkpoint";
        checkpoint_completed++
      if (
        lesson_progress.user_checkpoint !== undefined &&
        lesson_progress.user_checkpoint[slide_checkpoint_id] !== undefined
      ) {
        unlocked_class = ' cp-unlocked'
      console.log("checkpoint completed: "+slide_checkpoint_id);

      } else {
         console.log("checkpoint not completed " + slide_checkpoint_id);
      }
      
      slide_indexes.push(index)
    } else {
      // cp_class = "";
      unlocked_class = ''
    }*/

    $('.' + device_type + '-only #lesson-page .swiper-wrapper').append(
      '<div class="swiper-slide ' +
      custom_class + unlocked_class +
      ' " id="slide' +
      (index + 1) +
      '">\n' +
      all_slides[index].html_content +
      '\n</div>'
    )
   //console.log("append");
    if (all_slides[index].onload_function !== undefined) {
      /// /console.log('onload_function triggered: ' + all_slides[index].onload_function);
      var func = new Function(all_slides[index].onload_function)
      func()
    }

    if (all_slides[index].checkpoint === true) {
      checkpoint_count++
      // FIX: use "+2" because there is extra loading div
     /* if (device_type == "mobile")  {
         var nth_child = (index + 2)  
      } else { */
        var nth_child = (index + 1)          
      //}

      $('#lesson-page .swiper-wrapper .swiper-slide:nth-child(' + nth_child + ')').attr('data-checkpoint_id', slide_checkpoint_id++)

      console.log("cp_index: "+slide_checkpoint_id);
    }

     
      if (lesson_data.cert_awarded_at) {
        checkpoint_count = lesson_data.cert_awarded_at;
      }
      
  })



  if (device_type == "desktop") {
    $("#lessonProgressBar").attr("data-valuemax", total_slides);
    updateProgressBar("lessonProgressBar", active_slide);
  }
   console.log("checkpoint_count: "+checkpoint_count);
  // $( "#lesson-page .swiper-wrapper" )
  // $('#lesson-page .swiper-wrapper .swiper-slide:nth-child(10)').addClass('active');
}

//////////////////////////////////////////////
// MODALS                                   //
//////////////////////////////////////////////

$.createDialog = function(options) {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()

  if (!options.modalType) { 
    options.modalType = 'avatar-modal'
  }
  //options.showClose = false;
  let modalContents = "";
  if(options.htmlHeader != undefined){
    modalContents += '<div class="modal-header">\n' 
    modalContents += options.htmlHeader 
    modalContents += "</div>\n" 
  } 
  modalContents += '<div class="modal-body">\n' 
  modalContents += options.htmlContent 
  modalContents += "</div>\n" 
  if(options.htmlFooter != undefined){
    modalContents += '<div class="modal-footer">\n' 
    modalContents += options.htmlFooter 
    modalContents += "</div>\n" 
  } 



  console.log("clicked " + options.modalName);
  $(".modalPlaceholder").after(
    '<div class="modal ' +
      options.modalType +
      '" id="' +
      options.modalName +
      '">\n' +
     '<div class="modal-dialog ' +
      options.popupStyle +
      '">\n' +
       '<div class="modal-content">\n' +
      "<a rel=\"modal:close\" class=\"menu-button cross\" data-dismiss=\"modal\"><div class=\"bar\"></div><div class=\"bar\"></div><div class=\"bar\"></div></a>\n" +
      modalContents +
      "</div>\n" +
      "</div>\n" +
      "</div>\n"
  );

  $("#action_button").bind("click", options.actionButton);
  /* removed as now using  data-dismiss="modal"*/
  //$(" #close_button, .modal-backdrop, .jquery-modal > .blocker, .modal-backdrop").bind("click", function(e) {
   $(" #close_button, .modal-backdrop, .jquery-modal > .blocker, .modal-backdrop").bind("click", function(e) {
    event.preventDefault();
   // $(this).find('.menu-button').trigger('click');
    $.modal.close()
   // $("#" + options.modalName).modal("hide");
   //$(".jquery-modal:has(#"+ options.modalName + ")").remove()
    //$(".blocker").css("display",'none')
    //$(this).removeClass('blocker')
    //inactivityTime();
    console.log("closed " + options.modalName);
  });
  $("#" + options.modalName).modal("show");
  $("#" + options.modalName).modal({
    closeExisting: true,
    showClose: false
  })
  $('.jquery-modal > .blocker, .avatar-modal ').click(function() {
   $(this).find('.close-modal').trigger('click');
  })
};


// thumbnail modal
(function($) {
  $.galleryDialog = function(options) {
    //$(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class="modal" id="' + options.modalName + '">\n' +
      '<div class="' + options.popupStyle + '">\n' +
      '<div class="modal-content">\n' +
      options.htmlContent +
      "</div>\n" +
      "</div>\n" +
      "</div>\n"
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop, .jquery-modal > .blocker").bind("click", function(e) {
        
        $("#" + options.modalName).modal("hide");
        $('.modal').remove()
        $(".blocker").css("display",'none')
        $(this).removeClass('blocker')
        //inactivityTime();
        console.log("closed " + options.modalName);
      });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);

// nested modal
(function($) {
  $.nestedModal1 = function(options) {
    //$(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class="modal" id="' + options.modalName + '">\n' +
      '<div class="' + options.popupStyle + '">\n' +
      '<div class="modal-content">\n' +
      '<div class="">\n' +
      options.htmlContent +
      "</div>\n" +
      "</div>\n" +
      "</div>\n" +
      "</div>\n"
    );
    $("#action_button").bind("click", options.actionButton);

    $("#" + options.modalName).modal("show");
  };
})(jQuery);

function successProfileDetails(title, message, set_user_id) {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'successProfileDetails',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-xs',
    // actionButton: loginModal,
    htmlContent: 
    '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">' + title + '</h2>' +
      '<label>' + message + '</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })


  if (title != "Profile Updated") {
   $('.action').click(function() {
      event.preventDefault();

    if (set_user_id != undefined) {      
      console.log("Logged user in");
      localStorage.setItem('userid', set_user_id)
     }

      var newurl =
        window.location.protocol +
        '//' +
        window.location.host +
        window.location.pathname
      console.log('newurl: ' + newurl)
      window.history.replaceState({path: newurl}, '', newurl)
      // TODO not working to reload page info so for now just reload
      //loadUserData(); 
      //checkUserKey()
      window.location.reload()
    });
  }
}

function savedToGallery() {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'projectSaved-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-xs',
    // actionButton: loginModal,
    htmlContent: 
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Yeahhhh!</h2>' +
      '<label>Your Project is Saved...</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light action " data-dismiss="modal">Ok</button>' +
      '\t</div>' +
      '</div>'
  })
}

function loginModal() {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'login-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-sm',
    // actionButton: saveLogin,
    htmlContent: `
    <h2 id="confirm_title">Please enter your details to login</h2>
      <form class="login-form needs-validation wXas-validated" novalidate>
          <div class="form-row">
            <div class="input-group password-group mb-4">
              <div class="col-6 col-md-6 mb-1">
                <label for="pf_name">First name</label>
                <input type="text" class="form-control" id="f_name" placeholder="First name" required>
                <div class="invalid-feedback ">
                  Please enter your first name.
                </div> 
              </div>
              <div class="col-6 col-md-6 mb-1">
                <label for="pl_name">Last name</label>
                <input type="text" class="form-control" id="l_name" placeholder="Last name"  required>
                <div class="invalid-feedback ">
                  Please enter your last name.
                </div> 
              </div>
              <div class="col-12 col-md-8 offset-md-2 mb-1">
                <label for="">Date of Birth</label>
                <div class="form-group row DOB_select mb-2 ml-1 mr-1">
                  <div class="col-4 pr-1 pl-0 ">
                    ${createDOBForm()}
                  </div>
                  <div class="invalid-feedback pr-1 pl-3">
                    Please enter your full date of birth.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Cancel</button>
          <button id="action_button" class="btn btn-primary light action" >Login</button>
        </form>
        <p class="sign-up register-button pointer w-50 mx-auto" data-dismiss="modal">Don\'t have an account? <span class="a-link">Sign Up</span></p>
        `
  })

   $('.register-button').click(function() {
    registerModal()
  })

  var $submitForm = $('.action');

  $(".login-form")
    .change(function () {
      validateDOB() 
    });

  function validateDOB() {    
    console.log("validating")
    if ( $('#B-day').val() != null && $('#B-month').val() != null  && $('#B-year').val() != null ) {
        $('.DOB_select .invalid-feedback').hide();
           console.log("valid date")
    }else{
        $('.DOB_select .invalid-feedback').show();
           console.log("invalid date")
    }
  }

  //validateDOB() 


  $submitForm.click(function(event){
  event.preventDefault();
  validateDOB() 
    if ($('.login-form')[0].checkValidity() != true) {
      $('.login-form').addClass('was-validated')
        console.log("checkValidity false");
    }else {
          $('.login-form').addClass('was-validated')
    console.log('Here is the click event');
    var first_name = $("#f_name").val();
    var last_name = $("#l_name").val();
    //first_name = titleCase(first_name);
    //last_name = titleCase(last_name);
    // var nick_name = document.getElementById('n_name').value
    var day = $("#B-day").val();
    var month = $("#B-month").val();
    var year = $("#B-year").val();

    console.log("form values: " + first_name, last_name, day, month, year)

      // store to users storage
      // var ref = firebase.database().ref('/users/' + auth_id)

      let full_name = first_name + "_" + last_name;
      full_name = full_name.replace(/\s+/g, '').toLowerCase();

      var ref = firebase.database().ref('/users/')
      ref.orderByChild("full_name").equalTo(full_name).once('value').then((snapshot) => {
        data = snapshot.val()
        console.log("data: " + data);
        var matched = false;
        if (data) {
          Object.keys(data).forEach(function(k) {
            var in_data = data[k];
            Fname = in_data.hasOwnProperty('first_name') ? in_data['first_name'] : '';
            Lname = in_data.hasOwnProperty('last_name') ? in_data['last_name'] : '';
            v_day = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('day') ? in_data.D_O_B['day'] : '' : '';
            v_month = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('month') ? in_data.D_O_B['month'] : '' : '';
            v_year = in_data.hasOwnProperty('D_O_B') ? in_data.D_O_B.hasOwnProperty('year') ? in_data.D_O_B['year'] : '' : '';

            console.log(Fname, Lname, v_day, v_month, v_year, "From firebase");
            console.log(first_name, last_name, day, month, year, "Inputs");
            //console.log("fname input: " + first_name);

            //if (Fname == first_name && Lname == last_name && v_day == day && v_month == month && v_year == year) {
            if ( v_day == day && v_month == month && v_year == year) {
              $('#login-modal').modal('hide')
              $('.avatar-modal, .modal-backdrop, .blocker').remove()
              $('.blocker').css('display', 'none');
              successProfileDetails('Login Successfull', 'Welcome back ' + Fname, k)
              matched = true;
              // break;
            }
          });
        }
        if (!matched) {
          alert('Invalid credentials')
        }
      })
    }
  })   
}

function registerModal() {

  removeLocationHash();

  // console.log("reg");
  //$('.modal ').remove()
  $('.avatar-modal, .modal-backdrop, .blocker').remove()

  $.createDialog({
    modalName: 'register-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-sm ',
    //actionButton: registerNextModal,
    htmlContent: 
    `
    <div id="confirm_dialog" style="display: block;">
      <h2 id="confirm_title">Registration is soooo easy. We just need a few details to get started</h2>
      <form class="register-form needs-validation waXs-validated" novalidate>
        <div class="form-row">
          <div class="input-group password-group mb-4">
            <div class="col-6 col-md-6 mb-1">
              <label for="pf_name">First name</label>
              <input type="text" class="form-control" id="f_name" placeholder="First name" required>
                <div class="invalid-feedback ">
                  Please enter your first name.
                </div>              
            </div>
            <div class="col-6 col-md-6 mb-1">
              <label for="pl_name">Last name</label>
              <input type="text" class="form-control" id="l_name" placeholder="Last name" required>
                <div class="invalid-feedback ">
                  Please enter your last name.
                </div>              
            </div>
            <div class="col-12 col-md-8 offset-md-2 mb-1">
              <label for="">Date of Birth</label>
              <div class="form-group row DOB_select mb-2 ml-1 mr-1">
                <div class="col-4 pr-1 pl-0 ">
                    ` + createDOBForm() + `
                </div>
                <div class="invalid-feedback pr-1 pl-3">
                  Please enter your full date of birth.
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group coppa-group ">
          <div class="form-check">
            <p class="mb-1">
              Since you are younger than 13 years old your parent must give their approval for you to register (as per the COPPA requirements to help protect minors).
            </p>
            <input class="form-check-input" type="checkbox" value="" id="coppaCheck" required>
            <label class="form-check-label" for="coppaCheck">
              <strong>I (the parent) give consent</strong>
            </label>
            <div class="invalid-feedback">
              Parent must agree before submitting.
            </div>

          </div>
        </div>
        <button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Maybe later</button>
        <button id="action_button" class="btn btn-primary light action">Sign up</button>
      </form>
      <p class="sign-up login-button pointer w-50 mx-auto" data-dismiss="modal">Already have an account? <span class="a-link">Sign In</span></p>
    </div>
    `
  })

  var $submitForm = $('.action');

  $(".register-form")
    .change(function () {
      validateDOB() 
    });

  let dt = new Date();
  dt.setFullYear((dt.getFullYear()-13));
  let minAgeDate = dt;
  //console.log("minAgeDate: ", minAgeDate);
   
  function validateDOB() {    
    //console.log("validating")
    if ( $('#B-day').val() != null && $('#B-month').val() != null  && $('#B-year').val() != null ) {
        $('.DOB_select .invalid-feedback').hide();
           console.log("valid date")
    }else{
        $('.DOB_select .invalid-feedback').show();
           console.log("invalid date")
    }

    dt = new Date();
    dt.setFullYear($('#B-year').val(), $('#B-month').val()-1, $('#B-day').val());
    let checkAgeDate = dt;
    //console.log("checkAgeDate: ", checkAgeDate); 

    if ( checkAgeDate > minAgeDate) {
        $('.coppa-group input').attr('required', true);
        $('.coppa-group').show();
    } else{
      $('.coppa-group input').removeAttr('required');
      $('.coppa-group').hide();
    }

    if ($('#coppaCheck').prop('checked')) {
      $('.coppa-group .invalid-feedback').hide();
      $('.coppa-group').addClass('parent-confirmed');
    } else{
      $('.coppa-group .invalid-feedback').show();
      $('.coppa-group').removeClass('parent-confirmed');      
    }
  }

  validateDOB() 


  $submitForm.click(function(event){
  event.preventDefault();
    if ($('.register-form')[0].checkValidity() != true) {
      $('.register-form').addClass('was-validated')
        //console.log("checkValidity false");
    }else {
          $('.register-form').addClass('was-validated')
      registerNextModal();
    }
  })

  $('.login-button').click(function() {
    $('.avatar-modal, .modal-backdrop, .blocker').remove()
    loginModal()
  })
}

function registerNextModal() {
  var first_name = document.getElementById('f_name').value
  var last_name = document.getElementById('l_name').value
  var day = document.getElementById('B-day').value
  var month = document.getElementById('B-month').value
  var year = document.getElementById('B-year').value   

  var full_name = first_name + "_" + last_name;
  full_name = full_name.replace(/\s+/g, '').toLowerCase();

    console.log(first_name, last_name, nick_name, full_name, day, month, year)

    // store to users storage
    // var ref = firebase.database().ref('/users')
    var ref = firebase.database().ref('/users/');
    firebase.database().ref('/users/'+auth_id).once('value').then(function (snapshot) {
        data = snapshot.val()
        console.log("data", data);
        var updateLoginDetails = data;
        if(data){
          var keys = Object.keys(data)
          updateLoginDetails['first_name'] = first_name
          updateLoginDetails['last_name'] = last_name
          updateLoginDetails['full_name'] = full_name
          updateLoginDetails['D_O_B'] = { day, month, year }
        }else{
          updateLoginDetails = {
            first_name: first_name,
            last_name: last_name,
            full_name: full_name,
            D_O_B: { day, month, year }
          }
        }
        console.log("updateLoginDetails", updateLoginDetails);
        ref.update({
          [auth_id]: updateLoginDetails
        })
        // return snapshot.val();
    })


    var register_addtional =
    `
      <div id="confirm_dialog" style="display: block;">
        <h2 id="confirm_title">Congrats you have successfullly registered</h2>
        <p style="margin-bottom: 8px;">Please could you add in few more details about yourself so we can stay in touch.</p>
        <form lpformnum="1" id="email-login">
          <div>
            <input class="form-control" id="nick_name" placeholder="Nick name" name="nick_name" type="text" style="width: 100%">
            <input class="form-control d-none" id="mobile_number" placeholder="Mobile number" name="mobile" type="text" style="width: 100%">
            <input class="form-control" id="email_id" placeholder="Email address" name="email" type="email" style="width: 100%">
            <input class="form-control d-none" id="id_number" placeholder="ID number" name="id_number" type="text" style="width: 100%">
            <label for="PCountry" class="d-none">Country</label>
            ` +  createCountryForm() +    `
            <input class="form-control school d-none" id="schoolName" placeholder="Name of School" name="school" type="text" style="display:inline-block;width: 50%;margin-right: 0!important;">
          </div>
          <div id="error"></div>
        </form>
        <div id="confirm_actions">
          <button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>
          <input id="action_button" class="btn btn-primary light action" type="submit" value="Save">
        </div>
      </div>
`

  $('#register-modal .modal-body').html(register_addtional)

  $('#close_button, .modal-backdrop').click(function() {
    $('#register-modal').modal('hide')
    $('.avatar-modal').remove()
    $('.blocker').css('display', 'none')
    // console.log('closed #register-modal')
        //$('#login-modal').modal('hide')
    $('.avatar-modal, .modal-backdrop, .blocker').remove()
    $('.blocker').css('display', 'none')
    successProfileDetails('Signup Successfull', 'Welcome aboard ' + first_name)
    checkUserKey()
  })
  $('previewIcon').click(function() {
    $('.avatar-modal').remove()
  })

  $('.action').click(function() {

    let nick_name = document.getElementById('nick_name').value
    let mobile_number = document.getElementById('mobile_number').value
    let email_id = document.getElementById('email_id').value
    let id_number = document.getElementById('id_number').value
    let country = document.getElementById('PCountry').value
    let school = document.getElementById('schoolName').value
    // console.log(first_name, last_name, nick_name, day, month, year)

      // store to users storage
    var ref = firebase.database().ref('/users/' + auth_id )

    var additionalDetails = {            
      mobile_number: mobile_number ? mobile_number : null,
      email_id: email_id ? email_id : null,
      id_number: id_number ? id_number : null,
      country: country ? country : null,
      school_name: school ? school : null,
    }
    ref.update({
      nick_name: nick_name ? nick_name : null,
      details: additionalDetails
    }).then(function (snapshot){
          //$('#login-modal').modal('hide')
    $('.avatar-modal, .modal-backdrop, .blocker').remove()
    $('.blocker').css('display', 'none')
      successProfileDetails('Signup Successfull', 'Welcome aboard ' + first_name)
      checkUserKey()
      //checkUserKey()  // TODO IS THIS NEEDED HERE?
    // console.log(additionalDetails, 'Details')
    })

    

    console.log('updated extra user info + closed #registerNext-modal')
  })
}

function lockedLesson(lessonLink) {
  // console.log("reg");
  //$('.modal ').remove()
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'locked-lesson', 
    popupStyle: 'modal-sm speech-bubble ' + current_avatar + ' scared modal-sm',
    actionButton: logoutProfile,
    htmlContent: '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">It is best to do the trainings in order.</h2>' +
      '\t<h2 id="confirm_title">Are you sure you want to start this training?</h2>' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light  action" data-dismiss="modal">Hmmm... you\'re right.</button>' +
      '\t\t<button id="action_button" class="btn btn-primary light cancel goto-lesson">Yes</button>' +
      '\t</div>' +
      '</div>'
  })
  $('.goto-lesson').click(function() {
     window.location.href = lessonLink
  })
}

function titleModal() {
  // console.log("reg");
  //$('.modal ').remove()
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'title-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-sm',
    actionButton: titleInput,
    htmlContent: 
     '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Awesome</h2>' +
      '<form titleForm="1" id="title-form"><div ><label>What do you want to call your project?<span class="required-mark">*</span></label><input class="form-control" id="project_name" placeholder="Super Code Stretch-man  v1" name="Pname" type="text" required=""><div><label style="  margin: 0;">Do you have any notes? (Optional)</label></div><textarea class="form-control" id="project_notes" placeholder="This project reminds me of the pet spider I had in 2nd grade, that.." name="nickname" type="text" cols="4"></textarea></div></form>\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-primary light action projectTitle" type="submit" value="Save">' +
      '\t</div>' +
      '</div>'
  })
  $('.register-button').click(function() {
    registerModal()
  })
}

function UnRegisteredModal() {
  // console.log("reg");
  //$('.modal ').remove()
                                     
  $.createDialog({
    modalName: 'unRegistered-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' sad modal-xs',
    actionButton: registerModal,
    htmlContent: 
      '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Oops... doesn\'t look like<br>you are registered.</h2>\n\n\n' +
      '\t<p class="h5">Don\'t worry it is a quick and easy process, and then you\'ll be able to share your code to gallery.</p></label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Skip</button>' +
      '\t\t<input id="action_button" class="btn btn-primary light action " type="submit" value="Register Now">' +
      '\t</div>' +
      '<p class="sign-up register-button" data-dismiss="modal">Already have an account? <span class="a-link login-button" data-dismiss="modal">Login</span></p>' +
      '</div>'
  })
  $('.register-button').click(function() {
    loginModal()
  })
}

$('.reset-lesson').click(function() {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  //$('.modal ').remove()
  // console.log('Came here to reset the lesson')
  $.createDialog({
    modalName: 'reset-lesson',
    popupStyle: 'modal-xs speech-bubble ' + current_avatar + ' scared',
    actionButton: resetCurrentProject,
    htmlContent: '<div id="confirm_dialog" style="display: block;">' +
                                                             
      '\t<h2 id="confirm_title">Are you sure you want to reset this lesson?</h2>' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Hmmm, maybe not</button>' +
      '\t\t<button id="action_button" class="btn btn-primary light action reset-profile">Yes</button>' +
      '\t</div>' +
      '</div>'
  })
})

$('.profile-button').click(function () {
  //dialogInMenu = true
// createProfilePage();
})

function createProfilePage() {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  console.log("Inside createProfilepage", user_details);
/*
  if (user_details.hasOwnProperty('certificate')) {
    cert_html = prepareCertificates(user_details);
  } else {
    cert_html = '<p>Please complete the Course to get the certificates</p>';
  }
  */
  let ProfileFName;
  let ProfileLName;
  $.createDialog({
    modalName: 'createProfilePage',
    modalType: '',
    popupStyle: 'modal-dialog-scrollable modal-sm',
    htmlContent: `
      <div class="container -fluid">
        <i class="icon-account_circle fs4" style="d-block mt-2"></i>
        <h3>Edit My Profile</h3>

        <form class="edit-profile-form needs-validation was-validated" novalidate>
          <div class="form-row">
            <div class="input-group password-group mb-4">
              <div class="col-6 col-md-6 mb-1">
                <label for="pf_name">First name</label>
                <input type="text" class="form-control" id="pf_name" placeholder="First name" value="${user_details.first_name ? user_details.first_name : ''}" required>
              </div>
              <div class="col-6 col-md-6 mb-1">
                <label for="pl_name">Last name</label>
                <input type="text" class="form-control" id="pl_name" placeholder="Last name" value="${user_details.last_name ? user_details.last_name : ''}" required>
              </div>
              <div class="col-12 col-md-8 offset-md-2 mb-1">
                <label for="">Date of Birth</label>
                <div class="form-group row DOB_select mb-2 ml-1 mr-1">
                  <div class="col-4 pr-1 pl-0">
                    ${createDOBForm()}
                  </div>
                  <div class="invalid-feedback pr-1 pl-3">
                    Please enter your full date of birth.
                  </div>
                </div>
              </div>
              <div class="col-12 mb-1">
                <p class="mb-0"><strong>Important:</strong> Don't forget that the above is also your login details</p>
              </div>
            </div>
            <div class="col-12 col-md-6 mb-3 offset-md-3">
              <label for="Username" class="d-none">Username</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend">@</span>
                </div>
                <input type="text" class="form-control" id="Username" placeholder="Username" value="${user_details.nick_name ? user_details.nick_name : ''}">
                <div class="invalid-feedback">
                  Please .
                </div>
              </div>
            </div>
            <div class="col-8 col-md-8 mb-3">
              <label for="PCountry" class="d-none">Country</label>
              ${createCountryForm()}

            </div>
            <div class="col-4 col-md-4 mb-3">
              <label for="Pgrade" class="d-none">Grade</label>
              <input type="text" class="form-control" id="Pgrade" name="Grade" placeholder="Grade" value="${user_details.details ? user_details.details.grade ? user_details.details.grade : '' : ''}">
            </div>
          </div>
          <div class="col-12 mb-3">
            <label for="aspiring" class="d-none">Aspiring</label>
            <textarea class="form-control" id="aspiring" placeholder="Dream Job" type="text" cols="4">${user_details.details ? user_details.details.aspiring ? user_details.details.aspiring : '' : ''}</textarea>
          </div>
          <div class="col-12 mb-3">
            <label for="aspiring" class="d-none">School Name</label>
            <textarea class="form-control" id="school_name" placeholder="School Name" type="text" cols="4">${user_details.details ? user_details.details.school_name ? user_details.details.school_name : '' : ''}</textarea>
          </div>
          <div class="col-12 mb-3">
            <label for="aspiring" class="d-none">Email</label>
            <textarea class="form-control" id="email_id" placeholder="Email" type="text" cols="4">${user_details.details ? user_details.details.email_id ? user_details.details.email_id : '' : ''}</textarea>
          </div>
          <div class="col-12 mb-3">
            <label for="aspiring" class="d-none">Mobile Number</label>
            <textarea class="form-control" id="mobile_number" placeholder="Mobile Number" type="text" cols="4">${user_details.details ? user_details.details.mobile_number ? user_details.details.mobile_number : '' : ''}</textarea>
          </div>
      </div>
      <button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Cancel</button>
      <button id="action_button" class="btn btn-primary light action">Save</button>
      </form>
      <br><br><br><br><br><br><br><br><br>
      </div>

    `
  })

var $contactForm = $('.action');

if (user_details.D_O_B.day != undefined && user_details.D_O_B.day != "") {
$(".DOB_select #B-day option[value=" + user_details.D_O_B.day + "]").attr("selected","selected");
}
if (user_details.D_O_B.month != undefined && user_details.D_O_B.month != "") {
$(".DOB_select #B-month option[value=" + user_details.D_O_B.month + "]").attr("selected","selected");
}
if (user_details.D_O_B.year != undefined && user_details.D_O_B.year != "") {
$(".DOB_select #B-year option[value=" + user_details.D_O_B.year + "]").attr("selected","selected");
}
if (user_details.details !== undefined && user_details.details.country !== undefined ) {
$("#PCountry option[value='"+user_details.details.country+"']").attr("selected","selected");
}

$(".edit-profile-form")
  .change(function () {
    validateDOB() 
  });

function validateDOB() {    
  if ( $('#B-day').val() != "" && $('#B-month').val() != ""  && $('#B-year').val() != "" ) {
      $('.DOB_select .invalid-feedback').hide();
         console.log("valid date")
  }else{
      $('.DOB_select .invalid-feedback').show();
         console.log("invalid date")
  }
}

validateDOB() 


$contactForm.click(function(event){
  event.preventDefault();
  form = document.getElementsByClassName('edit-profile-form');
  if ($('.edit-profile-form')[0].checkValidity() != true) {
    $('.edit-profile-form').addClass('was-validated')
      console.log("checkValidity false");
  }else {
    $('.edit-profile-form').addClass('was-validated')
      console.log("checkValidity true");

      var full_name = document.getElementById('pf_name').value + "_" + document.getElementById('pl_name').value;
      full_name = full_name.replace(/\s+/g, '').toLowerCase();

      // store to users
      var ref = firebase.database().ref('/users/' + auth_id)

      ref.update({
        first_name : document.getElementById('pf_name').value,
        last_name: document.getElementById('pl_name').value,
        nick_name: document.getElementById('Username').value,
        full_name: full_name,
        'D_O_B/day' :  document.getElementById('B-day').value,
        'D_O_B/month' : document.getElementById('B-month').value,
        'D_O_B/year' : document.getElementById('B-year').value,
        'details/country' : document.getElementById('PCountry').value ? document.getElementById('PCountry').value : null,
        'details/aspiring': document.getElementById('aspiring').value ? document.getElementById('aspiring').value : null,
        'details/school_name': document.getElementById('school_name').value ? document.getElementById('school_name').value : null,
        'details/mobile_number': document.getElementById('mobile_number').value ? document.getElementById('mobile_number').value : null,
        'details/email_id': document.getElementById('email_id').value ? document.getElementById('email_id').value : null,
        'details/grade': document.getElementById('Pgrade').value ? document.getElementById('Pgrade').value : null

      })

      getUser = getUserPromise();
        Promise.all([
          getUser
        ]).then(function (results) {
          user_details = results[0];
          if (user_details.hasOwnProperty('first_name')) { 
            $("h2.profile_name").html('Hello, ' + user_details.first_name);
          }
          if (document.getElementById('PCountry').value !== undefined ) {
            $("#PCountry option[value='"+document.getElementById('PCountry').value+"']").attr("selected","selected");
            $("#PCountry").val(document.getElementById('PCountry').value);
          }
          $.modal.close();
      successProfileDetails('Profile Updated', 'Thank you for updating your profile')
      });

      
  }

});

var inputs = document.querySelectorAll('.edit-profile-form input, .edit-profile-form textarea');
for(var i=0;i<inputs.length;i++) {
 inputs[i].addEventListener('blur', function(){
   if(!this.checkValidity()) {
     this.classList.add('has-error');  
   } else {
     this.classList.remove('has-error');
   }
 }); 
}

  console.log("Completed task");

}

function createCountryForm() {
  return country_form = `
    <select id="PCountry" name="country" class="form-control" >
    <option value="">Country</option>
    <option value="Afghanistan">Afghanistan</option>
    <option value="Åland Islands">Åland Islands</option>
    <option value="Albania">Albania</option>
    <option value="Algeria">Algeria</option>
    <option value="American Samoa">American Samoa</option>
    <option value="Andorra">Andorra</option>
    <option value="Angola">Angola</option>
    <option value="Anguilla">Anguilla</option>
    <option value="Antarctica">Antarctica</option>
    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
    <option value="Argentina">Argentina</option>
    <option value="Armenia">Armenia</option>
    <option value="Aruba">Aruba</option>
    <option value="Australia">Australia</option>
    <option value="Austria">Austria</option>
    <option value="Azerbaijan">Azerbaijan</option>
    <option value="Bahamas">Bahamas</option>
    <option value="Bahrain">Bahrain</option>
    <option value="Bangladesh">Bangladesh</option>
    <option value="Barbados">Barbados</option>
    <option value="Belarus">Belarus</option>
    <option value="Belgium">Belgium</option>
    <option value="Belize">Belize</option>
    <option value="Benin">Benin</option>
    <option value="Bermuda">Bermuda</option>
    <option value="Bhutan">Bhutan</option>
    <option value="Bolivia">Bolivia</option>
    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
    <option value="Botswana">Botswana</option>
    <option value="Bouvet Island">Bouvet Island</option>
    <option value="Brazil">Brazil</option>
    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
    <option value="Brunei Darussalam">Brunei Darussalam</option>
    <option value="Bulgaria">Bulgaria</option>
    <option value="Burkina Faso">Burkina Faso</option>
    <option value="Burundi">Burundi</option>
    <option value="Cambodia">Cambodia</option>
    <option value="Cameroon">Cameroon</option>
    <option value="Canada">Canada</option>
    <option value="Cape Verde">Cape Verde</option>
    <option value="Cayman Islands">Cayman Islands</option>
    <option value="Central African Republic">Central African Republic</option>
    <option value="Chad">Chad</option>
    <option value="Chile">Chile</option>
    <option value="China">China</option>
    <option value="Christmas Island">Christmas Island</option>
    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
    <option value="Colombia">Colombia</option>
    <option value="Comoros">Comoros</option>
    <option value="Congo">Congo</option>
    <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
    <option value="Cook Islands">Cook Islands</option>
    <option value="Costa Rica">Costa Rica</option>
    <option value="Cote D'ivoire">Cote D'ivoire</option>
    <option value="Croatia">Croatia</option>
    <option value="Cuba">Cuba</option>
    <option value="Cyprus">Cyprus</option>
    <option value="Czech Republic">Czech Republic</option>
    <option value="Denmark">Denmark</option>
    <option value="Djibouti">Djibouti</option>
    <option value="Dominica">Dominica</option>
    <option value="Dominican Republic">Dominican Republic</option>
    <option value="Ecuador">Ecuador</option>
    <option value="Egypt">Egypt</option>
    <option value="El Salvador">El Salvador</option>
    <option value="Equatorial Guinea">Equatorial Guinea</option>
    <option value="Eritrea">Eritrea</option>
    <option value="Estonia">Estonia</option>
    <option value="Ethiopia">Ethiopia</option>
    <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
    <option value="Faroe Islands">Faroe Islands</option>
    <option value="Fiji">Fiji</option>
    <option value="Finland">Finland</option>
    <option value="France">France</option>
    <option value="French Guiana">French Guiana</option>
    <option value="French Polynesia">French Polynesia</option>
    <option value="French Southern Territories">French Southern Territories</option>
    <option value="Gabon">Gabon</option>
    <option value="Gambia">Gambia</option>
    <option value="Georgia">Georgia</option>
    <option value="Germany">Germany</option>
    <option value="Ghana">Ghana</option>
    <option value="Gibraltar">Gibraltar</option>
    <option value="Greece">Greece</option>
    <option value="Greenland">Greenland</option>
    <option value="Grenada">Grenada</option>
    <option value="Guadeloupe">Guadeloupe</option>
    <option value="Guam">Guam</option>
    <option value="Guatemala">Guatemala</option>
    <option value="Guernsey">Guernsey</option>
    <option value="Guinea">Guinea</option>
    <option value="Guinea-bissau">Guinea-bissau</option>
    <option value="Guyana">Guyana</option>
    <option value="Haiti">Haiti</option>
    <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
    <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
    <option value="Honduras">Honduras</option>
    <option value="Hong Kong">Hong Kong</option>
    <option value="Hungary">Hungary</option>
    <option value="Iceland">Iceland</option>
    <option value="India">India</option>
    <option value="Indonesia">Indonesia</option>
    <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
    <option value="Iraq">Iraq</option>
    <option value="Ireland">Ireland</option>
    <option value="Isle of Man">Isle of Man</option>
    <option value="Israel">Israel</option>
    <option value="Italy">Italy</option>
    <option value="Jamaica">Jamaica</option>
    <option value="Japan">Japan</option>
    <option value="Jersey">Jersey</option>
    <option value="Jordan">Jordan</option>
    <option value="Kazakhstan">Kazakhstan</option>
    <option value="Kenya">Kenya</option>
    <option value="Kiribati">Kiribati</option>
    <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
    <option value="Korea, Republic of">Korea, Republic of</option>
    <option value="Kuwait">Kuwait</option>
    <option value="Kyrgyzstan">Kyrgyzstan</option>
    <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
    <option value="Latvia">Latvia</option>
    <option value="Lebanon">Lebanon</option>
    <option value="Lesotho">Lesotho</option>
    <option value="Liberia">Liberia</option>
    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
    <option value="Liechtenstein">Liechtenstein</option>
    <option value="Lithuania">Lithuania</option>
    <option value="Luxembourg">Luxembourg</option>
    <option value="Macao">Macao</option>
    <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
    <option value="Madagascar">Madagascar</option>
    <option value="Malawi">Malawi</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Maldives">Maldives</option>
    <option value="Mali">Mali</option>
    <option value="Malta">Malta</option>
    <option value="Marshall Islands">Marshall Islands</option>
    <option value="Martinique">Martinique</option>
    <option value="Mauritania">Mauritania</option>
    <option value="Mauritius">Mauritius</option>
    <option value="Mayotte">Mayotte</option>
    <option value="Mexico">Mexico</option>
    <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
    <option value="Moldova, Republic of">Moldova, Republic of</option>
    <option value="Monaco">Monaco</option>
    <option value="Mongolia">Mongolia</option>
    <option value="Montenegro">Montenegro</option>
    <option value="Montserrat">Montserrat</option>
    <option value="Morocco">Morocco</option>
    <option value="Mozambique">Mozambique</option>
    <option value="Myanmar">Myanmar</option>
    <option value="Namibia">Namibia</option>
    <option value="Nauru">Nauru</option>
    <option value="Nepal">Nepal</option>
    <option value="Netherlands">Netherlands</option>
    <option value="Netherlands Antilles">Netherlands Antilles</option>
    <option value="New Caledonia">New Caledonia</option>
    <option value="New Zealand">New Zealand</option>
    <option value="Nicaragua">Nicaragua</option>
    <option value="Niger">Niger</option>
    <option value="Nigeria">Nigeria</option>
    <option value="Niue">Niue</option>
    <option value="Norfolk Island">Norfolk Island</option>
    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
    <option value="Norway">Norway</option>
    <option value="Oman">Oman</option>
    <option value="Pakistan">Pakistan</option>
    <option value="Palau">Palau</option>
    <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
    <option value="Panama">Panama</option>
    <option value="Papua New Guinea">Papua New Guinea</option>
    <option value="Paraguay">Paraguay</option>
    <option value="Peru">Peru</option>
    <option value="Philippines">Philippines</option>
    <option value="Pitcairn">Pitcairn</option>
    <option value="Poland">Poland</option>
    <option value="Portugal">Portugal</option>
    <option value="Puerto Rico">Puerto Rico</option>
    <option value="Qatar">Qatar</option>
    <option value="Reunion">Reunion</option>
    <option value="Romania">Romania</option>
    <option value="Russian Federation">Russian Federation</option>
    <option value="Rwanda">Rwanda</option>
    <option value="Saint Helena">Saint Helena</option>
    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
    <option value="Saint Lucia">Saint Lucia</option>
    <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
    <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
    <option value="Samoa">Samoa</option>
    <option value="San Marino">San Marino</option>
    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
    <option value="Saudi Arabia">Saudi Arabia</option>
    <option value="Senegal">Senegal</option>
    <option value="Serbia">Serbia</option>
    <option value="Seychelles">Seychelles</option>
    <option value="Sierra Leone">Sierra Leone</option>
    <option value="Singapore">Singapore</option>
    <option value="Slovakia">Slovakia</option>
    <option value="Slovenia">Slovenia</option>
    <option value="Solomon Islands">Solomon Islands</option>
    <option value="Somalia">Somalia</option>
    <option value="South Africa">South Africa</option>
    <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
    <option value="Spain">Spain</option>
    <option value="Sri Lanka">Sri Lanka</option>
    <option value="Sudan">Sudan</option>
    <option value="Suriname">Suriname</option>
    <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
    <option value="Swaziland">Swaziland</option>
    <option value="Sweden">Sweden</option>
    <option value="Switzerland">Switzerland</option>
    <option value="Syrian Arab Republic">Syrian Arab Republic</option>
    <option value="Taiwan, Province of China">Taiwan, Province of China</option>
    <option value="Tajikistan">Tajikistan</option>
    <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
    <option value="Thailand">Thailand</option>
    <option value="Timor-leste">Timor-leste</option>
    <option value="Togo">Togo</option>
    <option value="Tokelau">Tokelau</option>
    <option value="Tonga">Tonga</option>
    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
    <option value="Tunisia">Tunisia</option>
    <option value="Turkey">Turkey</option>
    <option value="Turkmenistan">Turkmenistan</option>
    <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
    <option value="Tuvalu">Tuvalu</option>
    <option value="Uganda">Uganda</option>
    <option value="Ukraine">Ukraine</option>
    <option value="United Arab Emirates">United Arab Emirates</option>
    <option value="United Kingdom">United Kingdom</option>
    <option value="United States">United States</option>
    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
    <option value="Uruguay">Uruguay</option>
    <option value="Uzbekistan">Uzbekistan</option>
    <option value="Vanuatu">Vanuatu</option>
    <option value="Venezuela">Venezuela</option>
    <option value="Viet Nam">Viet Nam</option>
    <option value="Virgin Islands, British">Virgin Islands, British</option>
    <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
    <option value="Wallis and Futuna">Wallis and Futuna</option>
    <option value="Western Sahara">Western Sahara</option>
    <option value="Yemen">Yemen</option>
    <option value="Zambia">Zambia</option>
    <option value="Zimbabwe">Zimbabwe</option>
  </select> 
  `
}

function createDOBForm() {
  DOB_form =
    '<select name="day" id="B-day" class="custom-select mb-0" required>\n' +
    '<option value=""  disabled selected hidden >Day</option>\n'
  for (i = 1; i <= 31; i++) {
    DOB_form += '<option value="' + pad2(i) + '">' + pad2(i) + '</option>\n'
  }
  DOB_form +=
    '</select></div>\n' +
    '<div class="col-4 pr-1 pl-1"><select name="month" id="B-month" class="custom-select mb-0" required>\n' +
    '<option value=""  disabled selected hidden >Month</option>\n' +
    '<option value="01">Jan</option>\n' +
    '<option value="02">Feb</option>\n' +
    '<option value="03">Mar</option>\n' +
    '<option value="04">Apr</option>\n' +
    '<option value="05">May</option>\n' +
    '<option value="06">June</option>\n' +
    '<option value="07">July</option>\n' +
    '<option value="08">Aug</option>\n' +
    '<option value="09">Sept</option>\n' +
    '<option value="10">Oct</option>\n' +
    '<option value="11">Nov</option>\n' +
    '<option value="12">Dec</option>\n' +
    '</select></div>\n' +
    '<div class="col-4 pl-1 pr-0"><select name="year" id="B-year" class="custom-select mb-0" required>\n' +
    '<option value=""  disabled selected hidden >Year</option>\n'
  for (i = current_year; i >= current_year - 100; i--) {
    DOB_form += '<option value="' + i + '">' + i + '</option>\n'
  }
  DOB_form += '</select>\n'

  return DOB_form;

}

var form

function greetingsCode(){
    // console.log("reg");
    localStorage.setItem('greetcode', true)
    refreshValues();
    $('.modal ').remove()
    $.createDialog({
      modalName: 'greetings',
      popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-xs',
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">GUESS WHAT?!? :)</h2>' +
        
        '\t<p>This is the Code Editor.'+'<br>'+' Where the magic happens.</p><p>Swipe back to keep learning. ' +'</p>'+
        '\t\t<input id="action_button" class="btn btn-primary light action" type="submit" value="Got it!">' +
        '\t</div>'
        
    })
    $('#greetings').click(function () {
      $('#greetings').css("display","none");
      $('.current').css("display","none");
    })
  }
  
  function greetingsView(){
    // console.log("reg");
    localStorage.setItem('greetview', true)
    refreshValues();
    $('.avatar-modal, .modal-backdrop, .blocker').remove()
    $.createDialog({
      modalName: 'greetings',
      popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-xs',
      htmlContent:
        '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">CHECK YOUR WORK.</h2>' +
        
        '\t<p> This is the Web Viewer.<br> Where you get to check on what you'+"'"+'re building. </p>' +'Swipe back twice to keep learning.'+'<br>'+'<br>'+'<br>'+
        '\t\t<input id="action_button" class="btn btn-primary light action" type="submit" value="Nice!">' +
        '\t</div>'
        
    })
    $('#greetings').click(function () {
      $('#greetings').css("display","none");
      $('.current').css("display","none");
      $('')

    })
  }

//////////////////////////////////////////////
// MAP BUTTONS                              //
//////////////////////////////////////////////

function mapButtons() {

  $('.login-button').click(function() {
    loginModal();
  })

  $('.register-button').click(function() {
    registerModal();
  })

  $('.logout-button').click(function() {
    // console.log('Came here to logout')
    $.createDialog({
      modalName: 'logout-profile',
      popupStyle: 'modal-xs speech-bubble ' + current_avatar + ' scared',
      actionButton: logoutProfile,
      htmlContent: '<div id="confirm_dialog" style="display: block;">' +
        '\t<h2 id="confirm_title">Are you sure you want to sign out now?</h2>' +
        '\t<div id="confirm_actions">' +
        '\t\t<button id="close_button" class="btn btn-primary light cancel" data-dismiss="modal">Hmmm, maybe not</button>' +
        '\t\t<button id="action_button" class="btn btn-primary light action reset-profile">Yes</button>' +
        '\t</div>' +
        '</div>'
    })
  })

  $('.select_lego').click(function() {
    changeAvatar('lego')
  })
  $('.select_robot').click(function() {
    changeAvatar('robot')
  })

  $('.reset-profile').click(function() {
    resetCurrentProject()
  })

  $('#gallery').click(function () {
    // console.log('clicked gallery')
    // loginModal();
    addToGallery()
  })


  $('#whatsapp')
    .attr('href',
    'whatsapp://send?text=Check out the CodeJIKA website where you can learn to code for free 😎‏%0A‎👉 https://codejika.com/'
  )

  $('#switch-device').click(function() {
   //preventDefault()
    var cookie_url = "." + window.location.host;

    //document.cookie = "devicetype=desktop; domain=.<?php echo$_SERVER['SERVER_NAME']; ?>;path=/; secure";
    Cookies.set('devicetype', device_type == 'mobile' ? 'desktop' : 'mobile', { domain: cookie_url, secure: true, expires: 7, path: '/' });   
    window.location.reload()
    
  })   

}

function mapButtonsLessons() {

  $('#whatsapp-code-link')
    .attr('href',
    'whatsapp://send?text=Check out the website I coded on CodeJIKA 😎‏‏ %0A👉 ' +
    'https://codejika.com/preview/' +
    save_lesson_id +
    '?' +
    auth_id
  )

}

function mapButtonsProjects() {


  $('.certificate-save').click(function() {
     createPDFCertificate($(this).attr("data-cert-awarded"), "save")
  })
  $('.certificate-view').click(function() {
    viewPDFCertificate($(this).attr("data-cert-awarded"))
  })
  

}

//////////////////////////////////////////////
// CODE GALLERY                              //
//////////////////////////////////////////////

// likes function
function likes(user_id) {
  // console.log("I am inside likes");

  like_count_ui = document.getElementById('my_like_' + user_id)
  var new_likes = parseInt(like_count_ui.innerHTML) + 1
  var ref = firebase.database().ref('/gallery/5-min-website/' + user_id)

  ref.once('value').then(function(snapshot) {
    // console.log(snapshot.val(), "Here os the snapsho");

    if (snapshot.val() && snapshot.val().likes) {
      new_likes = snapshot.val().likes + 1
    }
    like_count_ui.innerHTML = new_likes
    ref.update({
      likes: new_likes
    })
  })
}
var selectedOption 
// filter data
function FilterData() {
  selectedOption = document.getElementById('selectOptions').value
   console.log(selectedOption, 'option')
  userCodeHtml2 = ''
  $('#loadingIcon').css('display', 'block')
  $('.pagination-section').css('visibility', 'visible')
  // console.log('i am in projects')
  //var previousPage = document.getElementById('galleryLikes')
  //previousPage.style.display = 'none'
  getGalleryData(page_id_array[1] )
  go_first();

}

function addToGallery() {
  var userCode, keyString
 
  if (isBlank(getEditorValue())) {
    alert('write some code')
    return false
  }

  var validateAuthId = firebase.database()

  validateAuthId
    .ref('/users/')
    .once('value')
    .then(function(snapshot) {
      // loading.style.display = "block";
      data = snapshot.val()
      var keys = Object.keys(data)
      var validateName = keys['first_name']
      var findKey = keys.filter(key => key == auth_id)
      keyString = findKey.toString()
      // console.log(keyString, 'userDetails')
      console.log(auth_id, 'auid')
      if (findKey == auth_id) {
        titleModal()
      } else if (findKey !== auth_id || keys == null) {
        UnRegisteredModal()
        // alert("Your code is added to the gallery");
      }
    }) // end of firebase response validAuth2
}

function outputHTML1(clicked_key, user_html_code, user_code_output) {
  user_html_code = unescape(user_html_code)
  $('#viewThumbnail-modal ').remove()
  $.galleryDialog({
    modalName: 'outputHTML1-modal',
    popupStyle: "speech-bubble top-align " + current_avatar + " happy",
    // actionButton: resetCurrentProject,
    htmlContent: `
      <div style="height:100vh;overflow:hidden;">
        <div class="popup-header">
          <a type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </a>
          <a class="view-output view1_button" data-dismiss="modal"  >OUTPUT</a>
        </div>
        <div>
          <xmp id=view_html_code_${clicked_key} class="add-background">${user_html_code}</xmp>
        </div>
      </div>
    `
  })
  $('.view1_button').click(function() {
    // viewThumbnail(clicked_key)
    viewThumbnail(clicked_key, user_html_code, user_code_output)
  })
}

var keys

function getGalleryData(start_with_id, filter) {
  firebase_object = firebase
    .database()
    .ref('/gallery/5-min-website')
    .orderByKey()
  if (start_with_id && start_with_id != '') {
    // console.log(start_with_id, 'Here is last fetched key')
    firebase_object.startAt(start_with_id)
  }
  var lastVisible = ''

  firebase_object
   // .limitToFirst(pagesize * (current_page + 1))
    .once('value')
    .then(
      function(snapshot) {
        // console.log(pagesize * (current_page + 1), 'hello')
        var data = snapshot.val()
        keys = Object.keys(data)
        console.log(keys.length + " > " + pagesize +" * " +current_page +1);
        if (keys.length > pagesize * (current_page +1)) {
          $("#next2").css("visibility", "visible");
        } else {
          $("#next2").css("visibility", "hidden");
        }

        // console.log(keys, 'Here are the keys ')
        var sorted_array = []
        for (i = 0; i < keys.length; i++) {
          var k = keys[i]
          // dataLikes = data[k].date
          data[k]['key'] = k
          sorted_array.push(data[k])
           //console.log("sorted_array: " + sorted_array)
        }

        if (filter == undefined || filter == '') {
          filter = selectedOption
        }

         console.log(sorted_array, 'before SortArray')
         console.log(filter, 'before filter')
        if (filter && filter != '') {
          sorted_array.sort(function(a, b) {
            if (filter == 'MostLiked') {
              return b.likes - a.likes
            } else if (filter == 'Recent') {
              return new Date(b.date) - new Date(a.date)
            }
            return 0
          })
        }
        console.log(sorted_array, 'After SortArray' )
        sorted_array = sorted_array.slice(pagesize * current_page, pagesize * (current_page + 1));
        console.log(sorted_array, 'After Slice' + (pagesize * current_page) + " " + (pagesize * (current_page + 1)))
        current_page_size = sorted_array.length
        for (i = 0; i < sorted_array.length; i++) {
          $("#loadingIcon").css("display", "none");
          var key = sorted_array[i]['key']
          $("#bottom-pagination").css("display", "none");

       

          var current_key1 = key
          // var userCodeURL =
          //   "https://www.codejika.com/preview/5-minute-website?" + current_key;
          var name =
            data[current_key1].first_name + ' ' + data[current_key1].last_name
          var likes = data[current_key1].likes
          user_HTML = escape(data[current_key1].user_code)
          var user_code1 = escape(data[current_key1].user_code)
          var title = data[current_key1].title
          var notes = data[current_key1].notes      
          // console.log(user_HTML,'user')
          userCodeHtml2 += `
              <div id = "userKey" class="col-12 col-md-6  col-lg-4 p-2 p-md-4">
                  <div class="card-blog" id="cardHTML">
                  <div  class ="priview_blog"><a onclick="viewThumbnail('${current_key1}', '${user_HTML}', '${user_code1}')">
                    <h3>${title}</h3>
                    <a onclick="viewThumbnail('${current_key1}', '${user_HTML}', '${user_code1}')" id="previewIcon"> View &nbsp;
                     <span><i class="fas fa-external-link-square-alt"></i></span>
                    </a>
                  </div>
                  <a  class="HTMLscreen" >
                  <iframe src="data:text/html;charset=utf-8, ${user_code1}"  scrolling="no" id="my_iframe_${current_key1}">
                  </iframe style="background:#fff">
                </a>

                  <div class="detail-blog">
                  <h4>${name}</h4>
                  <p id="book_${current_key1}" class="text-center mb-0">${notes}</p>  
                  <div class="d-flex view-section">
                    <div class="likes">
                        <button onclick="likes('${current_key1}')"><span><i class="far fa-thumbs-up"></i></button><small id='my_like_${current_key1}'>${likes}</small></span>
                    </div>
                    <div class="views">
                    </div>
                  </div>
                  </div>
                </div>

              </div>`
          // console.log(userCodeHtml2, "Here is the code");
          lastVisible = current_key1
        }
        $("#galleryContainer").html(userCodeHtml2);
        // document.getElementById('view_html_code').innerText = user_HTML;
        // galleryPageJS = userCodeHtml2
        $( "#book_"+ current_key1 ).hide();
        $("#clickme_"+current_key1).click(function() {
          // console.log('toggle')
          $( "#book_"+ current_key1 ).slideToggle();
        });                                                      
        page_id_array.push(lastVisible)
        return snapshot.val()
      },
      function(error) {
        console.log(error, 'galleryError')
      }
    )

  return firebase_object
}

function viewThumbnail(clicked_key, user_html_code, user_code_output) {
  user_html_code = unescape(user_code_output)
  $('#outputHTML1-modal').remove()
  $.galleryDialog({
    modalName: 'viewThumbnail-modal',
   popupStyle: 'modal-dialog-scrollable modal-lg',
    htmlContent: `<div >
        <div class="popup-header">
          <a id="viewHTML" class="view-code" >SWITCH TO VIEW CODE </a><a class="view-output view1_button" style="display:none" >SWITCH TO PREVIEW</a>
        </div>
        <iframe class="show-preview-iframe" src="data:text/html;charset=utf-8, ${user_code_output}" style="width:100%; height: 100vh;overflow:hidden; border: none;" scrolling="yes" id="my_iframe_${clicked_key}">
        </iframe>
        <div class="show-html-code" style="display:none"><xmp id=view_html_code_${clicked_key} class="add-background">${user_html_code}</xmp></div>
      `
  })
  $('.view-code, .view-output').click(function() {
    $('.show-preview-iframe, .show-html-code, .view-code, .view1_button').toggle();
  });
}



function go_first() {
   console.log('go_first')
  current_page = 0
    document.getElementById('previous').style.visibility = 'hidden'
    document.getElementById('next2').style.visibility = 'visible'
  document.getElementById('pageNumber').innerHTML = 1 + current_page
}

function go_next() {
  // console.log(page_id_array, 'here is the array')
  current_page += 1
  loading.style.display = 'block'
  document.getElementById('galleryContainer').innerHTML = loading
  userCodeHtml2 = ''
  getGalleryData(page_id_array[current_page], '')
  document.getElementById('pageNumber').innerHTML = 1 + current_page

  if (current_page != 0) {
    document.getElementById('previous').style.visibility = 'visible'
  }
}

function go_previous() {
  if (current_page != 0) {
    current_page -= 1
    loading.style.display = 'block'
    document.getElementById('galleryContainer').innerHTML = loading
    $('#bottom-pagination').css('visibility', 'hidden')
    userCodeHtml2 = ''
    getGalleryData(page_id_array[current_page], '')
    document.getElementById('pageNumber').innerHTML = 1 + current_page
  }

  // Hide Previous button
  if (current_page == 0) {
    $('#previous').css('visibility', 'hidden')
  }
}

//////////////////////////////////////////////
// FIREBASE                                 //
//////////////////////////////////////////////

function initFirebase() {
  // Initialize Firebase

  var config = {
    apiKey: "AIzaSyA2KjWwZOoBVEvuv2n4mn1ey6wSzYphJME",
    authDomain: "codejika-2cf17.firebaseapp.com",
    databaseURL: "https://codejika-2cf17.firebaseio.com",
    projectId: "codejika-2cf17",
    storageBucket: "codejika-2cf17.appspot.com",
    messagingSenderId: "405485160215"
  }
   /* 
  // gallery Firebase

 var config = {
    apiKey: 'AIzaSyAw269vPfE3QreRGZDuEisv3wSnfFmFFoY',
    authDomain: 'codejika-staging.firebaseapp.com',
    databaseURL: 'https://cj-staging.firebaseio.com/',
    projectId: 'codejika-staging',
    storageBucket: 'codejika-staging.appspot.com',
    messagingSenderId: '405485160215'
  } 

  
  var config = {
      apiKey: 'AIzaSyBJ7ufovyD17jVlakE64GWQ7yPFnmNDvmA',
      authDomain: 'cjcoza-staging.firebaseapp.com',
      databaseURL: 'https://cjcoza-staging.firebaseio.com/',
      projectId: 'cjcoza-staging',
      storageBucket: 'cjcoza-staging.appspot.com',
      messagingSenderId: '183136998250'
    } 
  */

  firebase.initializeApp(config);
}

function getProjectProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id).once('value').then(function(snapshot) {
    return snapshot.val();
  }, function(error) {
    console.log(error);
  });
  //console.log("getLessonProgressPromise");
}

function getLessonProgressPromise(default_code) {

  return firebase.database().ref('/user_profile/' + auth_id + '/lesson_progress/' + save_lesson_id).once('value').then(function(snapshot) {
    return snapshot.val();
  }, function(error) {
    console.log(error);
  });
  //console.log("getLessonProgressPromise");
}

function getUserPromise() {
  //console.log("getUserPromise: " + auth_id);
  return firebase
    .database()
    .ref('/users/' + auth_id)
    .once('value')
    .then(
      function (snapshot) {
        //console.log("getUserPromise: " + snapshot.val());
        return snapshot.val()

      },
      function (error) {
        console.log(error);
      }
    )
  /// /console.log("getUserProfilePromise");
}

function getUserProfilePromise(default_code) {
  return firebase
    .database()
    .ref('/user_profile/' + auth_id + '/profile')
    .once('value')
    .then(
      function (snapshot) {
      //  console.log("getUserProfilePromise: " + snapshot.val());
        return snapshot.val()

      },
      function (error) {
        console.log(error);
      }
    )
  /// /console.log("getUserProfilePromise");
}

function getCertificatesPromise(default_code) {

  return firebase.database().ref('/certificates/').once('value').then(function(snapshot) {
    //console.log("getCertificatesPromise:" + JSON.stringify(snapshot.val()));
    return snapshot.val();
  }, function(error) {
    console.log(error);
  });
  
}

function getAllUsersPromise(default_code) {
//console.log("getAllUsersPromise");
  return firebase.database()
  .ref('/')
  .once('value')
  .then(function(snapshot) {

    snapshot.child("users").forEach(function(childSnapshot) {
      // key will be "ada" the first time and "alan" the second time
      var key = childSnapshot.key;
      //bulkCertAwarded(key);
      //console.log("childSnapshot:" + JSON.stringify(childSnapshot));
      if (childSnapshot.child("certificate").exists()) {
        // childData will be the actual contents of the child
        var childDataKey = childSnapshot.key;
        //console.log("childData key:" + JSON.stringify(childData));
        childDataVal = childSnapshot.val();
        //console.log("childData val:" + JSON.stringify(childData));

        Object.keys(childDataVal.certificate).forEach(function(index) {
        console.log("childDataVal index: " + index);
        console.log("childDataVal completed: " + childDataVal.certificate[index].completed);
        console.log("childDataVal completed_at: " + childDataVal.certificate[index].completed_at);
        console.log("childDataVal user_code: " + childDataVal.certificate[index].user_code);

        firebase        
        .database()
        .ref('user_profile/' + childDataKey + '/lesson_progress/' + index + '/certificate/')
        .update({
           completed: childDataVal.certificate[index].completed,
           completed_at: childDataVal.certificate[index].completed_at,
           user_code: childDataVal.certificate[index].user_code
        });
     
        })

      }
    });

   // console.log("getAllUsersPromise:" + JSON.stringify(snapshot.val()));
    return snapshot.val();
  }, function(error) {
    console.log(error);
  });
  
}

function bulkCertAwarded(userprofile_id, users_id, user_index) {


  var cert_data_keys = Object.keys(certificates_data)

  Object.keys(certificates_data).forEach(function(index) {
    if (userprofile_id.lesson_progress !== undefined){
        console.log("checkCertAwarded to: " + user_index);
        var cert_earned
        var cert_date = timeNow();
        var cert_check

      //console.log("user_profile: " +userprofile_id);
      console.log("certificates_data index: " +index);
      cert_requirements = certificates_data[index].requirements
      console.log("requirements: " +cert_requirements);
      
      cert_requirements.forEach(function(index) {
        cert_earned = undefined;
        cert_check = undefined
        /// add userprofile_id.certificate_awarded
           console.log("cert_requirements [index] : " +index);
         //  console.log("user_profile: " +JSON.stringify(userprofile_id[index]));
        if (userprofile_id.lesson_progress[index] !== undefined) {
          cert_check = userprofile_id.lesson_progress[index]
          console.log("cert_check : " +JSON.stringify(cert_check));
        }

           
        if (cert_check !== undefined && cert_check.hasOwnProperty('certificate')) {
          console.log("certificate found: " +index);
          cert_date = cert_check.certificate.completed_at;
          console.log("certificate date: " +cert_date);
        } else {
          console.log("certificate not found: " +index);
          cert_earned = false;
        }
      })
console.log("cert_earned: " +cert_earned);
      if (cert_earned != false) {
        var all_users_users = Object.keys(all_users.users)
        console.log("cXertificate earned: " +index);
        console.log("cXertificate all_users[index]: " +JSON.stringify(users_id));
        console.log("cXertificate user_profile[index]: " +JSON.stringify(userprofile_id));


        if (users_id !== undefined && users_id.hasOwnProperty('first_name')) {
          student_fullname = users_id.first_name;
        }
        if (users_id !== undefined && users_id.hasOwnProperty('last_name')) {
          student_fullname =  student_fullname + " " + users_id.last_name;
        }
        console.log("student_fullname: " +student_fullname);
        console.log("database: " + 'user_profile/' + user_index + '/certificate_awarded/' + index);

        firebase        
        .database()
        .ref('user_profile/' + user_index + '/certificate_awarded/' + index)
        .update({
           fullname: student_fullname,
           completed_at: cert_date
        });
     /*   */

      } else {
        console.log("certificate not earned: " +index);
      }
        //userprofile_id.lesson_progress[index].certificate
      
    } else {
        console.log("checkCert already awarded");
    }

  })
}

var cert_data_keys

function moveUserData() {
  cert_data_keys = Object.keys(all_users.users);

  cert_data_keys.forEach(function(index) {
      console.log("user found: " + JSON.stringify(all_users.users[index], null, 4));  
 /*
   if (cert_data_keys[index].hasOwnProperty(certificate)){
      console.log("user with cert found: " +index);
    }
    oldRef.once('value', function(snap)  {
          newRef.set( snap.val(), function(error) {
               if( !error ) {  oldRef.remove(); }
               else if( typeof(console) !== 'undefined' && console.error ) {  console.error(error); }
          });
     });
*/
  });
}


// NOT IN USE
/*
function getUserSkillsPromise() {
  return firebase
    .database()
    .ref('/user_profile/' + auth_id + '/skills')
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
      },
      function (error) {
        /// /console.log(error);
      }
    )
  /// /console.log("getUserSkillsPromise");
}

*/
// NOT IN USE

//////////////////////////////////////////////
// ASSORTED (TO DO: TIDY UP )               //
//////////////////////////////////////////////

// getEditorValue is called because desktop/mobile have diff editors
function getEditorValue() {
  if (device_type == "desktop") {
    return editor.getValue();        
  } else {
    return editor.value;
  }
}

// setEditorValue is called because desktop/mobile have diff editors
function setEditorValue(action) {
  if (device_type == "desktop") {
    editor.setValue(action);
  } else {
    editor.value = action;
  }
}


function pad2(number) {
  return (number < 10 ? '0' : '') + number
}


  date_form =
    '<form lpformnum="1" id="email-login"><div ><input class="form-control" id="f_name" placeholder="First name" name="fname" type="text" required><input class="form-control" id="l_name" placeholder="Last name" name="lname" type="text" required=""><input class="form-control" id="n_name" placeholder="Nickname (Username)" name="nickname" type="text" required=""></div><div><label style="    margin: 0;">Date of birth</label></div>\n' +
    '<select name="day" id="B-day" class="custom-select" style="width: 28%">\n' +
    '<option value="" selected></option>\n'
  for (i = 1; i <= 31; i++) {
    date_form += '<option value="' + pad2(i) + '">' + pad2(i) + '</option>\n'
  }
  date_form +=
    '</select>\n' +
    '<select name="month" id="B-month" class="custom-select" style="width: 28%">\n' +
    '<option value="" selected></option>\n' +
    '<option value="01">Jan</option>\n' +
    '<option value="02">Feb</option>\n' +
    '<option value="03">Mar</option>\n' +
    '<option value="04">Apr</option>\n' +
    '<option value="05">May</option>\n' +
    '<option value="06">June</option>\n' +
    '<option value="07">July</option>\n' +
    '<option value="08">Aug</option>\n' +
    '<option value="09">Sept</option>\n' +
    '<option value="10">Oct</option>\n' +
    '<option value="11">Nov</option>\n' +
    '<option value="12">Dec</option>\n' +
    '</select>\n' +
    '<select name="year" id="B-year" class="custom-select" style="width: 38%">\n' +
    '<option value="" selected></option>\n'
  for (i = current_year; i >= 1950; i--) {
    date_form += '<option value="' + i + '">' + i + '</option>\n'
  }
  date_form += '</select>\n'
  date_form += '</div><div id="error"></div></form>\n'



var twitterShare = document.querySelector('[data-js="twitter-share"]')

  twitterShare.onclick = function (e) {
    e.preventDefault()
    var twitterWindow = window.open(
      'https://twitter.com/share?url=' +
      'https://www.codejika.com/learn/' +
      save_lesson_id +
      '/' +
      auth_id,
      'twitter-popup',
      'height=350,width=600'
    )
    if (twitterWindow.focus) {
      twitterWindow.focus()
    }
    return false
  }

  var facebookShare = document.querySelector('[data-js="facebook-share"]')

  facebookShare.onclick = function (e) {
    e.preventDefault()
    var facebookWindow = window.open(
      'https://www.facebook.com/sharer/sharer.php?u=' +
      'https://www.codejika.com/learn/' +
      save_lesson_id +
      '/' +
      auth_id,
      'facebook-popup',
      'height=350,width=600'
    )
    if (facebookWindow.focus) {
      facebookWindow.focus()
    }
    return false
  }

//////////////////////////////////////////////
// CERTIFCATES                              //
//////////////////////////////////////////////

function checkCertAwarded(completed_id) {
  var cert_data_keys = Object.keys(certificates_data)
  
  if (project_progress !== null  && project_progress !== undefined  && project_progress.lesson_progress !== undefined) {
    Object.keys(certificates_data).forEach(function(index) {
      if (!project_progress.hasOwnProperty('certificate_awarded') || !project_progress.certificate_awarded.hasOwnProperty(index) ){
        cert_earned = false;
        console.log("check if Cert Awarded for: " +index);
        cert_requirements = certificates_data[index].requirements
        console.log("requirements: " +cert_requirements);
        
        cert_requirements.forEach(function(index) {
          /// add project_progress.certificate_awarded
          cert_check = project_progress.lesson_progress[index]
             console.log("index : " +index);
             console.log("cert_check : " +JSON.stringify(cert_check));
          if (cert_check !== undefined && cert_check.hasOwnProperty('certificate')) {
            console.log("certificate found: " +index);
            cert_earned = true;
          } else {
            console.log("certificate not found: " +index);
            cert_earned = false;
          }
        })

        if (cert_earned == true){
          console.log("certificate earned: " +index);
          console.log("user_details : " +JSON.stringify(user_details));    
          if (user_details !== undefined && user_details.hasOwnProperty('first_name')) {
            student_fullname = user_details.first_name;
          }
          if (user_details !== undefined && user_details.hasOwnProperty('last_name')) {
            student_fullname =  student_fullname + " " + user_details.last_name;
          }

          firebase        
          .database()
          .ref('user_profile/' + auth_id + '/certificate_awarded/' + index)
          .update({
             fullname: titleCase(student_fullname),
             completed_at: timeNow()
          });

          if (reload_projects == undefined) {
            reload_projects = true;
          }

        } else {
          console.log("certificate not earned: " +index);
        }
          //project_progress.lesson_progress[index].certificate
        
      } else {
          console.log("checkCert already awarded");
      }

    }) 
  }

  if (reload_projects == true) {
        getProjectProgress = getProjectProgressPromise();
        Promise.all([
          getProjectProgress
        ]).then(function (results) {
          project_progress = results[0];
         loadCertificatesAwarded()
        });
  } else {
    //loadCertificatesAwarded()
  }
}

function loadCertificatesAwarded() {

   var cert_slides = "";
 
    if (project_progress !== null && project_progress.hasOwnProperty('certificate_awarded')) {
      //return lesson_progress.user_checkpoint[index].user_code

    var cert_keys = Object.keys(project_progress.certificate_awarded)
      //console.log("cert_keys: "+JSON.stringify(project_progress.certificate_awarded));
   //   console.log("keys: "+JSON.stringify(keys));

      Object.keys(project_progress.certificate_awarded).forEach(function(index) {
         var cert_index = project_progress.certificate_awarded[index];
         //console.log("cert_index: "+index);     
        //console.log("cert_data: "+JSON.stringify(progress_data));
    //console.log("." + index + "-intro .certificate-goto")
    $("." + index + "-intro .certificate-goto").removeClass("d-none");
    $("." + index + "-intro .project-start").addClass(" more-intro d-none");
        cert_slides =  cert_slides + `
                        <div class="swiXper-slide pl-4 pr-4" id="certificate-${index}">
                          <div class="container mx-auto">
                            <div class="row">
                              <div class="col-3 pb-3 p-0 p-sm-3">
                                <img src="/img/emoji/cert-icon.png" class="w-100 mx-auto" style="max-width: 150px;">
                              </div>
                              <div class="col-9">
                                <h3 class="">${certificates_data[index].cert_title}</h3>
                                <p class=" pbX-3">${certificates_data[index].cert_desc}</p>
                                <p class=""><strong>Date completed:</strong> ${$.format.date(project_progress.certificate_awarded[index].completed_at, 'dd MMM yyyy')}</p>
                                <a class="btn btn-primary certificate-view light bg-pink mb-2 mb-sm-0" style="" data-cert-awarded="${index}" style="">VIEW CERTIFICATE <i class="fas fa-award ml-1"></i></a>
                                <a class="btn btn-primary bg-blue certificate-save project-view light " style="" data-cert-awarded="${index}" style="">SAVE <i class="fas fa-file-pdf"></i></a>
                              </div>
                            </div>
                          </div>
                        </div>
          `
        }); 
    } else {
      console.log("no certificate completed");
      cert_slides =    `
                        <div class="swiXper-slide pl-2 pr-2" id="certificate-blank">
                          <div class="container mx-auto">
                            <div class="row">
                              <div class="col-5 pb-3" style="opacity:.2">
                                <img src="/img/emoji/cert-icon.png" class="w-75 mx-auto" style="max-width: 150px;">
                              </div>
                              <div class="col-7 my-auto" style="opacity:.2">
                                <h3 class="pb-5">Sorry there are no certificates yet to display here. Time to get coding!</h3>
                              </div>
                            </div>
                          </div>
                        </div>
          `      
      } 

      $("#certifcates_page .certs").html(cert_slides);
      mapButtonsProjects();
}

function createPDFCertificate(certificate_id, action) {
      //alert(certificate_id)
  console.log("create certificate with ID:" + certificate_id);
  console.log("project_progress.certificate_awarded[certificate_id]:" + JSON.stringify(project_progress.certificate_awarded));
var doc = new jsPDF({
    orientation: 'l',
     unit: 'mm',
     format: 'a4'
    });
var student_fullname ="";
var img = new Image();
img.onError = function() {
 alert('Cannot load image: "'+url+'"');
};
img.src = 
    'https://' +
    window.location.host +
    '/learn/certificates/'+certificates_data[certificate_id].background_image;
           
img.onload = function () {
   //alert("image is loaded");
}           
doc.addImage(img, "JPEG", 0, 0, 297, 210);
doc.setFont("rajdhani-medium");
doc.setFontSize(20);
doc.text("Issued to", 148, 70, 'center');
doc.text(certificates_data[certificate_id].cert_desc, 148, 110, 'center');
doc.text("On this day the " + $.format.date(project_progress.certificate_awarded[certificate_id].completed_at, 'D of MMMM yyyy') + ".", 148, 144, 'center');
doc.setFontSize(35);
if (project_progress.certificate_awarded[certificate_id].hasOwnProperty('fullname')) {
  student_fullname = project_progress.certificate_awarded[certificate_id].fullname;
}
/*
if (user_details.hasOwnProperty('last_name')) {
  student_fullname =  student_fullname + " " + user_details.last_name;
}
*/
doc.text(titleCase(student_fullname), 148, 85, 'center');
doc.text(certificates_data[certificate_id].cert_title, 148, 127, 'center');
doc.setFont("helvetica");
doc.setFontSize(10);
//doc.text("VERIFY HERE: CODEJIKA.COM/CERT/"+certificate_id+"?"+auth_id, 340, 150, 'center', 90);
if(action == "save"){
  doc.save(certificates_data[certificate_id].PDF_filename+".pdf");
} else {
  return doc.output('bloburl');
}


}

function viewPDFCertificate(certificate_id) {
// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = createPDFCertificate(certificate_id, "view");

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = "https://" +
    window.location.host + "/js/pdf.worker.js";

// Asynchronous download of PDF
var loadingTask = pdfjsLib.getDocument(url);
loadingTask.promise.then(function(pdf) {
  console.log('PDF loaded');
  

  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'viewPDFCertificate',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy',
    // actionButton: loginModal,
    htmlContent: 
    '<div id="confirm_dialog"style="display: block;">' +
      '\t<h2 id="confirm_title" class="mx-auto mb-4" style="max-width: 380px;">Here is a preview of the certificate you have been awarded:</h2>' +
      '<canvas id="the-canvas"></canvas>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light action mt-4 " data-dismiss="modal">Wow, looks Great!</button>' +
      '\t</div>' +
      '</div>'
  })



  // Fetch the first page
  var pageNumber = 1;
  pdf.getPage(pageNumber).then(function(page) {
    console.log('Page loaded');
    
    var scale = 1.5;
    var viewport = page.getViewport({scale: scale});

    // Prepare canvas using PDF page dimensions
    var canvas = document.getElementById('the-canvas');
    var context = canvas.getContext('2d');
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);
    renderTask.promise.then(function () {
      console.log('Page rendered');
    });
  });
}, function (reason) {
  // PDF loading error
  console.error(reason);
});

}




  