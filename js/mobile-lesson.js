//console.debug(true);
// MOBILE ONLY VARIABLES
var subModal= false; // TODO rewite code?
var initialX = null;
var initialY = null;
var is_keyboard = false;
var initial_screen_size = window.innerHeight;
var menuIndex = 3;
var swiperH
var buildTour = false;
var introShow = true;

/// START OF moved from lesson-m.php      
    window.addEventListener("resize", function() {
      is_keyboard = (window.innerHeight < initial_screen_size);

      console.log(is_keyboard, 'Here');
      if (!is_keyboard) {
        $('textarea').blur();
      }
    }, false);


    function btclicked(symb) {
      if (symb == 'single_quote') {
        insertAtCaret(document.getElementById('editor'), "'");
      } else if  (symb == 'double_quote'){
        insertAtCaret(document.getElementById('editor'), "\"");
      } else {
        insertAtCaret(document.getElementById('editor'), symb);
      }
    }

    function insertAtCaret(element, text) {
      if (document.selection) {
        console.log('Hello');
        element.focus();
        var sel = document.selection.createRange();
        sel.text = text;
        element.focus();
      } else if (element.selectionStart || element.selectionStart === 0) {
        var startPos = element.selectionStart;
        var endPos = element.selectionEnd;
        var scrollTop = element.scrollTop;
        element.value =
          element.value.substring(0, startPos) +
          text +
          element.value.substring(endPos, element.value.length);
        element.focus();
        element.selectionStart = startPos + text.length;
        element.selectionEnd = startPos + text.length;
        element.scrollTop = scrollTop;
      } else {
        element.value += text;
        element.focus();
      }
    }

    $('textarea').on('keydown', function(e) {
      var keyCode = e.keyCode || e.which;

      if (keyCode === 9) {
        e.preventDefault();
        var start = this.selectionStart;
        var end = this.selectionEnd;
        var val = this.value;
        var selected = val.substring(start, end);
        var re = /^/gm;
        var count = selected.match(re).length;


        this.value = val.substring(0, start) + selected.replace(re, '\t') + val.substring(end);
        this.selectionStart = start;
        this.selectionEnd = end + count;
      }
    });

    var myElement = document.getElementById('editor');


    // Swipe Up / Down / Left / Right


    function startTouch(e) {
      initialX = e.touches[0].clientX;
      initialY = e.touches[0].clientY;
    };

    function moveTouch(e) {
      if (initialX === null) {
        return;
      }

      if (initialY === null) {
        return;
      }

      var currentX = e.touches[0].clientX;
      var currentY = e.touches[0].clientY;

      var diffX = initialX - currentX;
      var diffY = initialY - currentY;

      if (Math.abs(diffX) > Math.abs(diffY)) {
        // sliding horizontally
        if (diffX > 0) {
          // swiped left
          $('textarea').blur();
        } else {
          // swiped right
          // alert('Swipe Right');
          $('textarea').blur();
        }
      } else {
        // sliding vertically
        if (diffY > 0) {
          // swiped up
          console.log("swiped up");
        } else {
          // swiped down
          console.log("swiped down");
        }
      }

      initialX = null;
      initialY = null;

      e.preventDefault();
    };

    // TLN line number code below
    const TLN = {
      eventList: {},
      update_line_numbers: function(ta, el) {
        let lines = ta.value.split("\n").length;
        let child_count = el.children.length;
        let difference = lines - child_count;
        let line_num = 1;

        if (difference > 0) {
          let frag = document.createDocumentFragment();
          while (difference > 0) {
            let line_number = document.createElement("span");
            line_number.className = "tln-line tln-line-" + line_num;
            frag.appendChild(line_number);
            difference--;
            line_num++;
          }
          el.appendChild(frag);
        }
        while (difference < 0) {
          el.removeChild(el.firstChild);
          difference++;
        }
      },
      append_line_numbers: function(id) {
        let ta = document.getElementById(id);
        if (ta == null) {
          return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
        }
        if (ta.className.indexOf("tln-active") != -1) {
          return console.warn("[tln.js] textarea of id '" + id + "' is already numbered");
        }
        ta.classList.add("tln-active");
        ta.style = {};

        let el = document.createElement("div");
        ta.parentNode.insertBefore(el, ta);
        el.className = "tln-wrapper";
        TLN.update_line_numbers(ta, el);
        TLN.eventList[id] = [];

        const __change_evts = [
          "propertychange", "input", "keydown", "keyup", 'focus'
        ];
        const __change_hdlr = function(ta, el) {
          return function(e) {
            if ((+ta.scrollLeft == 10 && (e.keyCode == 37 || e.which == 37 ||
                e.code == "ArrowLeft" || e.key == "ArrowLeft")) ||
              e.keyCode == 36 || e.which == 36 || e.code == "Home" || e.key == "Home" ||
              e.keyCode == 13 || e.which == 13 || e.code == "Enter" || e.key == "Enter" ||
              e.code == "NumpadEnter")
              ta.scrollLeft = 0;
            TLN.update_line_numbers(ta, el);
          }
        }(ta, el);
        for (let i = __change_evts.length - 1; i >= 0; i--) {
          ta.addEventListener(__change_evts[i], __change_hdlr);
          TLN.eventList[id].push({
            evt: __change_evts[i],
            hdlr: __change_hdlr
          });
        }

        const __scroll_evts = ["change", "mousewheel", "scroll", "focus"];
        const __scroll_hdlr = function(ta, el) {
          return function() {
            el.scrollTop = ta.scrollTop;
          }
        }(ta, el);
        for (let i = __scroll_evts.length - 1; i >= 0; i--) {
          ta.addEventListener(__scroll_evts[i], __scroll_hdlr);
          TLN.eventList[id].push({
            evt: __scroll_evts[i],
            hdlr: __scroll_hdlr
          });
        }
      },
      remove_line_numbers: function(id) {
        let ta = document.getElementById(id);
        if (ta == null) {
          return console.warn("[tln.js] Couldn't find textarea of id '" + id + "'");
        }
        if (ta.className.indexOf("tln-active") == -1) {
          return console.warn("[tln.js] textarea of id '" + id + "' isn't numbered");
        }
        ta.classList.remove("tln-active");

        ta.previousSibling.remove();

        if (!TLN.eventList[id]) return;
        for (let i = TLN.eventList[id].length - 1; i >= 0; i--) {
          const evt = TLN.eventList[id][i];
          ta.removeEventListener(evt.evt, evt.hdlr);
        }
        delete TLN.eventList[id];
      }
    }

    // TLN line No. code ends here
    
    
/// END OF moved from lesson-m.php    


// MOBILE ONLY FUNCTIONS

function slideHistory() {
  historySet = true;
  
  history.pushState(null, null, location.href);

  window.onpopstate = function () {

    history.go(1);

    clearTimeout(timeout);
    timeout = setTimeout(function () {
      if (swiperTabs.activeIndex == 0) {
        swiperLesson.slideTo(swiperLesson.activeIndex - 1);
        console.log("on lesson tab" + swiperLesson.activeIndex);
      }
      if (swiperTabs.activeIndex == 3 && dialogInMenu) {
        // alert('clicked Back button')
        $('.blocker').trigger('click')
        dialogInMenu = false
        console.log("on lesson tab" + swiperLesson.activeIndex);
        return
      }
      if (swiperTabs.activeIndex == 3 && subModal){
         $('.blocker').trigger('click')
         subModal = false
        // return
      } else {
        return swiperTabs.slideTo(swiperTabs.activeIndex - 1);
        // console.log("on other tab" + swiperTabs.activeIndex);
      }

    }, 50);

    
 
  };
}

$('.create-GalleryPage').click(function () {
  dialogInMenu = true;
  userCodeHtml2 = ''

  createGalleryPage();
  // hideButton()
      document.getElementById('previous').style.visibility = 'hidden'
  loading = document.getElementById('loadingIcon')
  document.getElementById('pageNumber').innerHTML = current_page + 1

  getGalleryData();
})

$('.create-profilePage').click(function () {
  createProfilePage();
})

function createGalleryPage() {
  //let GalleryInit = document.getElementById('#galleryContainer')

  $.createDialog({
    modalName: 'createGalleryPage',
    modalType: '',
    popupStyle: "modal-dialog-scrollable hide-scrollbar modal-xl",
    htmlHeader:
    `
      <h2 class="">Code Gallery</h2>
    `,
    htmlContent:
    `

    <div id="gallery_page">

      <div class="loading" id="loadingIcon">
        <div class="spinner">
          <div class="rect1"></div>
          <div class="rect2"></div>
          <div class="rect3"></div>
          <div class="rect4"></div>
          <div class="rect5"></div>
        </div>
      </div>

      <div class="gallery-container row" id="galleryContainer">

      </div>
    </div>
    `,
    htmlFooter:
    `
    <div class="row w-100 ">
      <div class="createGalleryPage text-center text-md-left col-12 col-md-6 pt-0 pb-0 pr-2 pl-2">
        <div class="search-list">
          <h6 class="d-inline-flex">Search By:</h6>
          <select name="" id="selectOptions" onchange="FilterData(this.event)">
            <option value="Recent" id="recent" selected> Recent</option>
            <option value="MostLiked" id="likes">Most Liked</option>
          </select>
        </div>
      </div>
      <div class="pagination-section text-center text-md-right col-12 col-md-6  pt-0 pb-0 pr-2 pl-2">
        <ul class="pagination">
          <li class="page-item previous" id="previous"><a onclick="go_previous()" class="page-link">Previous</a></li>
          <li class="page-item"><a class="page-link" id="pageNumber"></a></li>
          <li class="page-item next" id="next2"><a onclick="go_next()" class="page-link">Next</a></li>
        </ul>
      </div> 
    </div>      
    `,    
  })
  console.log('Test');
}

function codeUpdated() {
  save_pending = true
  validateCheckpoint()
   console.log('Save Pending and Code Validated')
}

function successAnimation() {
  if (active_tab > 0) {
    editor.blur()
    $('.success-animation').removeClass('hide')
 console.log('successAnimation')
    setTimeout(() => {
      $('.success-animation').addClass('hide')
      
      // editor.focus()
    }, 1100)
  }
  // console.log('animate1' + active_tab)
}

function loadSkills() {
  if (user_skills) {
    $.each(user_skills, function (index) {
      unlockSkills(user_skills[index])
    })
  }
}

function unlockSkills(skills) {
  // console.log("skills: "+skills);
  $('#' + skills + '-skill').addClass('has-skill-true')
  $('#' + skills + '-skill').removeClass('has-skill-false')
  // console.log('Unlocked skill '+skills);
  // console.log("#"+slide_id+"-skill");

  $('#' + skills + '-skill')
    .attr(
      'data-original-title',
      $('#' + skills + '-skill').attr('data-title-text')
    )
    .tooltip('show')
    .tooltip('hide')
}

function inactivityTime() {
  var t
  window.onload = resetTimer
  document.onload = resetTimer
  document.onmousemove = resetTimer
  document.onmousedown = resetTimer // touchscreen presses
  document.ontouchstart = resetTimer
  document.onclick = resetTimer // touchpad clicks
  document.onscroll = resetTimer // scrolling with arrow keys
  document.onkeypress = resetTimer

  /// /console.log("inactivityTime");
  resetTimer()

  var resumeCoding = [
    'You snooze, you loose.',
    'Well begun is only half done',
    'You are on your way to becoming a coding genius',
    'Did you know when you complete the lesson you will get a certificate?'
  ]
  var rindex = Math.floor(Math.random() * resumeCoding.length)

  function timedOut() {
    /*
         if ( !$(".modal:visible").length ) {
    $('.modal').modal('hide');

    //console.log("timed out");
    $.createDialog({
      modalName: 'timed-out',
             popupStyle: 'speech-bubble '+current_avatar+' surprised',
            htmlContent:
            '<div id=\"confirm_dialog\" style=\"display: block;\">'+
            '\t<h2 id=\"confirm_title\">'+resumeCoding[rindex]+'</h2>'+
            '\t<div id=\"confirm_actions\">'+
            '\t\t<button id=\"close_button\" class=\"btn btn-primary light action\">Let\'s continue coding</button>'+
            '\t</div>'+
            '</div>'
    });
  } */
  }

  function resetTimer() {
    // console.log("r timer: "+$(".modal").length);
    clearTimeout(t)
    t = setTimeout(timedOut, 60000)

    // ////console.log("didn't reset timer"+$(".modal").attr('style').display == 'block' ));
  }
}

function editorChange() {
  clearTimeout(delay)
  // console.log('preview');
  delay = setTimeout(updatePreview, 100)
  setTimeout(codeUpdated, 200)
}

function closeModal(options) {
  /// /console.log("close modal: " + options);
  $(options).modal('hide')
}

function drawArrow(start_id, end_id, dynamicAnchors, acolor = 'red-line') {
  jsPlumb.ready(function () {
    jsPlumb.setContainer('container')
    var link = jsPlumb.getInstance()

    link.connect({
      cssClass: acolor,
      source: start_id,
      target: end_id,
      anchors: dynamicAnchors,
      endpoint: 'Blank',
      overlays: [
        [
          'Arrow',
          {
            location: 1,
            width: 10,
            length: 10
          }
        ]
      ],
      paintstyle: {
        lineWidth: 40,
        strokeStyle: 'red'
        // dashstyle:" 0 1"
      },
      connector: [
        'Bezier',
        {
          curviness: 80
        }
      ]
    })
  })
}

function titleInput() {
  var title = document.getElementById('project_name').value
  var projectNotes = document.getElementById('project_notes').value
  if (title == '') {
    document.querySelector('#project_name').classList.add('error-border')
  } else {
    // update DB gallery
    var user_firstName
    var user_lasttName

    var getDetails = firebase.database()
    getDetails
      .ref('/users/' + auth_id)
      .once('value')
      .then(function (snapshot) {
        user = snapshot.val()
        // console.log(user, 'firstname to gal')
        var toupdate = {
          first_name: user.first_name,
          last_name: user.last_name,
          likes: 0,
          user_code: getEditorValue(),
          title: title,
          notes: projectNotes,
          date: new Date()
        }
        var ref = firebase.database().ref('/gallery/5-min-website')
        ref.update({
          [auth_id]: toupdate
        })
      })
    

    userCodeHtml2 = ''
    
        //document.querySelector('#project_name').classList.remove('error-border')
    $('.modal , .modal-backdrop').remove()
    $('.blocker').css('display', 'none')
    // console.log(title, projectNotes)

      console.log("savedToGallery");
    savedToGallery()
      getGalleryData();
      $("#gallery.btn-primary").html("SHARE TO GALLERY <i class='fas fa-check'></i>").addClass("bg-green")

      
  }
}

function openProject(project_id) {
  current_project_id = project_id;
  $("#"+project_id).removeClass('hide');
  //$(".swiper-container-v").addClass('hide');
  $(".list_projects").addClass('hide');
  $('.lesson-navbar li:nth-child(1) span').html('PROJECT '+(project_id.split("-").pop()));
  console.log("openProject: "+project_id);

}

function closeProject(project_id) {
  $("#"+current_project_id).addClass('hide');
  //$(".swiper-container-v").removeClass('hide'); 
  $(".list_projects").removeClass('hide');  
  $('.lesson-navbar li:nth-child(1) span').html('PROJECTS');
  current_project_id = null;  
}
  

// END OF MOBILE ONLY FUNCTIONS



// MOBILE SPECIFIC 

function onStartProjects() {

  buildPage();

   // check no lesson data save default values to DB
  if (lesson_progress === null) {
    /// /console.log("no lesson progress found");
    setProjectProgress();
  }

  swiperH = new Swiper('.swiper-container-h', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouchMove: true,
    pagiXnation: {
      el: '.swiper-pagination-h',
      clickable: true,
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () {
        //console.log("init: "+active_tab);
      },
      slideChange: function () {
        var slider = this;
        active_tab = slider.activeIndex;
        $('.lesson-navbar li.active').removeClass('active');
        var mindex = slider.activeIndex + 1;

        $('.lesson-navbar li:nth-child(' + mindex + ')').addClass('active');
        //console.log('.lesson-navbar:nth-child('+mindex+')');

       /* if (slider.activeIndex <= 0) {
          $(".success-animation").addClass('hide');
        }
        
        if (slider.activeIndex <= 2) {
          slider.allowSlideNext = true;
        } else {
          slider.allowSlideNext = false;
        }
        */
          //console.log("tab: "+active_tab);        
      },
    }

  });

  /*swiperC = new Swiper('.swiper-container-c', {
    spaceBetween: 0,
    preventClicks: false,
    preventClicksPropagation: false,
    allowTouchMove: true,
    pagination: {
      el: '.swiper-pagination-h',
      clickable: true,
    },
    shortSwipes: true,

    loop: false,
    on: {
      init: function () {
        console.log("initC: "+active_tab);
      },
      slideChange: function () {
     
      },
    }

  });*/

  //swiperCerts = document.querySelector('.swiper-container-c').swiper;
  swiperTabs = document.querySelector('.swiper-container-h').swiper;
  
   
  $('.lesson-navbar li').click(function () {
      swiperH.slideTo($(this).index());
  })

  $('.certificate-goto').click(function() {
    swiperH.slideTo(1);
  })

  $('.see-more-less').click(function() {
    console.log('.' +$(this).attr("data-project-id")+ '-intro .more-intro')
    $('.' +$(this).attr("data-project-id")+ '-intro .more-intro').toggleClass('d-none');
    $('.' +$(this).attr("data-project-id")+ '-intro .see-more').toggleClass('d-none');
    $('.' +$(this).attr("data-project-id")+ '-intro .see-less').toggleClass('d-none');
  })
  
  
  $('.swiper-pagination-switch').click(function () {
    swiperH.slideTo($(this).index() + 1);
    $('.swiper-pagination-switch').removeClass('active');
    $(this).addClass('active')
  })

  $('.locked .divLink').click(function (event) {
    event.preventDefault();
         console.log('Came here to logout: ' + $(this).attr('href'))
    lockedLesson($(this).attr('href'));
  })

    var menuButton = document.querySelector('.toggle-menu');
  function openMenu() {
    if($('.toggle-menu').hasClass('cross')) {
      swiperMenu.slideTo(0);
      $('.swiper-container-m').toggleClass('show-menu');         
      $('.toggle-menu').toggleClass('cross');
      //$('.swiper-container-h .swiper-wrapper').removeEventListener('click', openMenu(), true);

    } else {
      swiperMenu.slideTo(1);
      $('.swiper-container-m').toggleClass('show-menu');      
      $('.toggle-menu').toggleClass('cross');
      //$('.swiper-container-h .swiper-wrapper').addEventListener('click', openMenu(), true);
    }
  };

  swiperM = new Swiper('.swiper-container-m', {
    slidesPerView: 'auto',
    //initialSlide: 0,
    resistanceRatio: 0,
    slideToClickedSlide: true,
    allowTouchMove: false,
    on: {
      slideChange: function () {
        var slider = this;        
        console.log("slide menu2: " + slider.activeIndex)
         menuIndex = slider.activeIndex;       
      }
      , slideChangeTransitionStart: function () {
                console.log("slide menu")
        var slider = this;
        console.log("slide menu3: " + slider.activeIndex)        
        if (slider.activeIndex === 0) {

          // required because of slideToClickedSlide
          //menuButton
        } else {

        }
      }
      , slideChangeTransitionEnd: function () {
        var slider = this;
        if (slider.activeIndex === 1) {
          //menuButton.addEventListener('click', openMenu(), true);
        }
      },
    }
  });

   $('.toggle-menu').click(function () {
  openMenu()
  });
   $('.fade-background').click(function () {
  openMenu()
  });

  swiperMenu = document.querySelector('.swiper-container-m').swiper;


    $(".project-view").click(function(){openProject($(this).data("project-id"))});
  $(".project-return").click(function(){closeProject($(this).data("project-id"))});

  $('#close_button').click(function () {
    $('.blocker .jquery-modal').css('background', 'none')
  })

  mapButtons(); 

  // function to display register modal
  // setTimeout(checkUserKey, 5000);
  checkUserKey();
  //  buildCerts();
  checkCertAwarded(); //and 
  loadCertificatesAwarded();



}

function onStartLessons() {

   // if no lesson data save default values to DB
  if (lesson_progress === null) {
    /// /console.log("no lesson progress found");
    lesson_progress = {
      last_checkpoint: '0',
      progress_completed: '0'
    }
  }

  if (get_prev_submit_code !== null) {
    console.log("get_prev_submit_code found");
    prev_submit_code = get_prev_submit_code.submit_code;
  } else {
    prev_submit_code = null;
  }

  buildPage();
  loadUserData();
  buildLessonSlides()

  if (device_type == "desktop") {
      $('.loadingIcon').addClass('d-none');
onStartDesktop();
//document.getElementById('pageNumber').innerHTML = current_page + 1
  } else {
    console.log("onStartLessons: Mobile");
    // saveUserData(true);
    // Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip()

    // Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
      var $target = $(e.target)

      if ($target.parent().hasClass('disabled')) {
        return false
      }
    })

    myElement.addEventListener("touchstart", startTouch, false);
    myElement.addEventListener("touchmove", moveTouch, false);  

    setInterval(saveUserData, 30000)

    var lessonLoaded = false

    // initFullPage();

    $('#fp-nav ul li:nth-child(2) a span').addClass('cp-green')
    $('#fp-nav ul li:nth-child(12) a span').addClass('cp-red')


    swiperH = new Swiper('.swiper-container-h', {
      spaceBetween: 0,
      preventClicks: false,
      preventClicksPropagation: false,
      allowToXuchMove: false,
      pagiXnation: {
        el: '.swiper-pagination-h',
        clickable: true
      },
      shortSwipes: true,

      loop: false,
      on: {
        init: function () { },
        slideChange: function () {
          var slider = this
          active_tab = slider.activeIndex
          $('.lesson-navbar li.active').removeClass('active')
          var mindex = slider.activeIndex + 1

          $('.lesson-navbar li:nth-child(' + mindex + ')').addClass('active')
          // console.log('.lesson-navbar:nth-child('+mindex+')');
          if (slider.activeIndex <= 0) {
            $('.success-animation').addClass('')
          }
          if (mindex === 2) {
            if (lesson_data.show_greetcode && greetcode === false) {
              setTimeout(function(){greetingsCode()}, 2000);
            }

            if (lesson_data.show_intro && showIntro && isBlank(getEditorValue())) {
              //$('.bottom_keys').removeClass('hide')
              showIntro();
              showIntro = false;
            }
            
          } else {
           // $('.bottom_keys').addClass('hide')
          }
          if (mindex === 3) {
            slider.allowSlideNext = true
            if (lesson_data.show_greetcode && greetview === false) {
             setTimeout(function(){greetingsView()}, 2000);
            }
          }
          if (slider.activeIndex <= 3) {
           
          } else {
            slider.allowSlideNext = false
          }
          // console.log("tab: "+active_tab);
        },
        transitionEnd: function () {
          var slider = this
          if (slider.activeIndex === 2) {
            updatePreview()
          }
          if (slider.activeIndex === 1) {
            $('.CodeMirror-code').focus()
            editor.focus()
            // $('.ui-keyboard').show();
            // console.log('show')
          } else {
            $('.CodeMirror-code').blur()
            // $('.ui-keyboard').hide();
            // console.log('hide')
          }
        }
      }
    })

    swiperV = new Swiper('.swiper-container-v', {
      direction: 'vertical',
      spaceBetween: 50,
      shortSwipes: true,
      mousewheel: true,
      preventClicks: false,
      preventClicksPropagation: false,
      allowTouXchMove: false,
      // Disable preloading of all images
      preloadImages: false,
      // initialSlide: active_slide,
      // Enable lazy loading
      lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 1
      },
      scrollbar: {
        el: '.swiper-scrollbar-v',
        hide: true,
        dragSize: '60px',
        clickable: false
      },
      observer: true,
      observeParents: true,

      on: {
        init: function () { 
          if (typeof onlessonLoaded === 'function') {
            onlessonLoaded();
          }
          slide_data = lesson_data.slides[active_slide-1]
          validateCheckpoint()
        },
        lazyImageReady: function () {
          //TODO not sure what this does but triggers JS error onload
          /*
          if (slide_data.lazy_function !== undefined) {
            var func = new Function(slide_data.lazy_function)
            func()
          }
          */
          /// /console.log('lazy_function triggered: ' + slide_data.lazy_function);
        },
        slideChange: function () {
          var slider = this
          // var mindex = slider.activeIndex+1;
          active_slide = slider.activeIndex + 1;

          localStorage.setItem(lessson_url + '_' + auth_id + '_'+ device_type + '_active_slide', active_slide)
          slide_data = lesson_data.slides[active_slide-1]
          // console.log(slide_data)
          var unlocked = $('#slide' + (active_slide)).hasClass('cp-unlocked')
          if (!unlocked && slide_data.reg !== undefined) {
            slider.allowSlideNext = false
            /// /console.log('lock' + slider.allowSlideNext + active_slide);
          } else {
            slider.allowSlideNext = true
            /// /console.log('unlock' + active_slide);
          }
          // console.log("mindex: "+mindex);
          /// /console.log("active_slide: " + active_slide);

          $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')')
            .prevAll()
            .addClass('bullet-green')
          $('.swiper-pagination-v span:nth-child(' + (active_slide + 1) + ')')
            .next()
            .removeClass('bullet-green')
          validateCheckpoint()
          saveUserData()
          // ga('send', 'pageview', 'CJ Lesson', 'swipe', lessson_url, active_slide + 1);
          ga('send', {
            hitType: 'event',
            eventCategory: lessson_url,
            eventAction: 'Slide swipe',
            eventLabel: active_slide + 1
          })
          console.log('Slide swipe');
        }
      }
    })
    swiperTabs = document.querySelector('.swiper-container-h').swiper
    swiperLesson = document.querySelector('.swiper-container-v').swiper

    swiperH.allowTouchMove = true
    swiperV.allowTouchMove = true

    swiperLesson.slideTo(active_slide-1);

    swiperLesson.on('slideChange', function () {
       // set  backButton functionality
      console.log("historySet: " + historySet)
          if (!historySet) { slideHistory() }

    });

    $('.lesson-navbar li').click(function () {
      if ($(this).index() === 6) {
        /// /console.log(swiperH.activeIndex);
        // ////console.log($(this).index());
        swiperH.allowSlideNext = true
        swiperH.slideTo($(this).index())
        swiperH.allowSlideNext = false
      } else {
        // console.log("dfd"+swiperH.activeIndex);
        // ////console.log("dfd"+$(this).index());
        swiperH.slideTo($(this).index())
        $('.lesson-navbar li.active').removeClass('active')
        $(this).addClass('active')
      }
    })

    $('.swiper-pagination-switch').click(function () {
      swiperH.slideTo($(this).index() + 1)
      $('.swiper-pagination-switch').removeClass('active')
      $(this).addClass('active')
    })

    $('.swiper-next').click(function () {
      swiperLesson.allowSlideNext = true
      swiperLesson.slideNext()
      // swiperLesson.allowSlideNext = false;
      /// /console.log("skip");
    })

    $('.swiper-editor').click(function () {
      swiperTabs.allowSlideNext = true
      swiperTabs.slideNext()
      // swiperTabs.allowSlideNext = false;
      /// /console.log("skip2");
    })    

  }

  var menuButton = document.querySelector('.toggle-menu');
  function openMenu() {
    if($('.toggle-menu').hasClass('cross')) {
      swiperMenu.slideTo(0);
      $('.swiper-container-m').toggleClass('show-menu');         
      $('.toggle-menu').toggleClass('cross');
      //$('.swiper-container-h .swiper-wrapper').removeEventListener('click', openMenu(), true);

    } else {
      swiperMenu.slideTo(1);
      $('.swiper-container-m').toggleClass('show-menu');      
      $('.toggle-menu').toggleClass('cross');
      //$('.swiper-container-h .swiper-wrapper').addEventListener('click', openMenu(), true);
    }
  };

  swiperM = new Swiper('.swiper-container-m', {
    slidesPerView: 'auto',
    //initialSlide: 0,
    resistanceRatio: 0,
    slideToClickedSlide: true,
    allowTouchMove: false,
    on: {
      slideChange: function () {
        var slider = this;        
        console.log("slide menu2: " + slider.activeIndex)
         menuIndex = slider.activeIndex;       
      }
      , slideChangeTransitionStart: function () {
                console.log("slide menu")
        var slider = this;
        console.log("slide menu3: " + slider.activeIndex)        
        if (slider.activeIndex === 0) {

          // required because of slideToClickedSlide
          //menuButton
        } else {

        }
      }
      , slideChangeTransitionEnd: function () {
        var slider = this;
        if (slider.activeIndex === 1) {
          //menuButton.addEventListener('click', openMenu(), true);
        }
      },
    }
  });

  $('.toggle-menu').click(function () {
   openMenu()
  });
  $('.fade-background').click(function () {
    openMenu()
  });

  swiperMenu = document.querySelector('.swiper-container-m').swiper
  menuIndex = swiperMenu.activeIndex;    


  // active_slide = 0;
 function showIntro() {
    let tip1 = tippy(document.querySelector('.bottom_keys'), {
      content: `
      <div class="h6 dark-grey pt-2 bold">TIP 1 OF 3</div>
      <div class="h4 pt-2">To help you, we added the &lt; & &gt; keys here.</div>
      <div class="btn h4 mt-2 editor-intro-tip1 bg-pink white align-center">THANKS ></div>      
      `,
      trigger: 'manual',
      placement: 'top',
      offset: [0, 30],
      hideOnClick: false,
      hideOnClick: false,
      arrows: 'large-arrow',
      animation: 'shift-toward-extreme',
      maxWidth: 250,     
      interactive: true,
      allowHTML: true,
      delay: [500,0],
      theme: 'light green intro2'
    });

    let tip2 = tippy(document.querySelector('.intro1-place'), {
      content: `
      <div class="h6 dark-grey pt-2 bold">TIP 2 OF 3</div>
      <div class="h4 pt-2">This is where you will type &lt;h1&gt;</div>
      <div class="btn h4 mt-2 editor-intro-tip2 bg-pink white align-center">OK ></div>
      `,
      trigger: 'manual',
      placement: 'right-start',
      offset: [-20, 40],
      hideOnClick: false,
      arrows: 'large-arrow',
      animation: 'shift-toward-extreme',
      maxWidth: 200,
      interactive: true,
      allowHTML: true,
      delay: [500,0],
      theme: 'light green intro1'
    });

    let tip3 = tippy(document.querySelector('.embed-nav'), {
      content: `
      <div class="h6 dark-grey pt-2 bold">TIP 3 OF 3</div>
      <div class="h4 pt-2">When done, swipe right ➡️ or click the <i class="icon-sentiment_satisfied"></i> icon to return to the slides.</div>
      <div class="btn h4 mt-2 editor-intro-tip3 bg-pink white align-center">GOT IT.</div>
      `,
      trigger: 'manual',
      placement: 'bottom',
      offset: [0, 40],
      hideOnClick: false,
      hideOnClick: false,
      arrows: 'large-arrow',
      animation: 'shift-toward-extreme',
      maxWidth: 250,
      interactive: true,
      allowHTML: true,
      delay: [500,0],
      theme: 'light green intro3'
    });  

    $('#editor').prop('selectionStart', 0);
    tippy.hideAll()
    tip1.show();
    $('.editor-intro-tip1').click(function () {
      tip1.destroy();
      setTimeout(function(){ 
        tip2.show()
        $('.editor-intro-tip2').click(function () {
          tip2.destroy();
          setTimeout(function(){ 
            tip3.show()
            $('.editor-intro-tip3').click(function () {
                 tip3.destroy();
                 $('#editor').prop('selectionStart', 0);
            })
          },300);
        })
      },300);
    })
  }


  $('.preview-output').click(function () {
    console.log("dfs");
    previewLessonModal();
  })

  $('.take-tour').click(function () {
    introTour()
  })


  $('.loading').hide()


  if (device_type == "desktop") {
    swiperLesson.slideTo(active_slide - 1)
  } else {
    $('#editor').prop('selectionEnd', 0);

    $('#disallow-interact,#allow-interact').click(function () {
      $('#disallow-interact.icon-locked').toggleClass('hide')
      $('#allow-interact.icon-unlocked').toggleClass('hide')
      $('.swipe-overlay').toggleClass('pointer-none')
    })

    $('.swipe-overlay').click(function () {
      // $( "#disallow-interact.icon-locked" ).toggleClass( "hide" )
      // $( "#allow-interact.icon-unlocked" ).toggleClass( "hide" )
      // $( ".swipe-overlay" ).toggleClass( "pointer-none" )
      // console.log("test");
    })

    // TLN
    TLN.append_line_numbers('editor')
  }
 
  $('#close_button').click(function () {
    $('.blocker .jquery-modal').css('background', 'none')
  })

  mapButtons();
  mapButtonsLessons();
  inactivityTime() 
  // function to display register modal
  // setTimeout(checkUserKey, 5000);
  checkUserKey();
}


function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.focus();
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
}

function setCaretToPos (input, pos) {
   setSelectionRange(input, pos, pos);
}

// END OF MOBILE SPECIFIC 

/*
// CHECK why is this for only 5 min website???
function getLessonProgressPromise(default_code) {
  return firebase
    .database()
    .ref('/gallery/' + auth_id + '/5-min-website/')
    .once('value')
    .then(
      function (snapshot) {
        return snapshot.val()
      },
      function (error) {
        console.log(error);
      }
    )
  /// /console.log("getLessonProgressPromise");
}
// END OF CHECK */


$(document).ready(function () {
  $('body').addClass('is-' + device_type); 

  successAnimation()
  // ================= jquery Modal
  $(function () {
    $('#viewHTML').click(function () {
      // takes the ID of appropriate dialogue
      var id = $(this).data('#sub-modal')
      // open dialogue
      $(id).dialog('open')
    })
  })
  initFirebase()

  if(page_template == "lessons") {
    var getUser = getUserPromise()
    var getUserProfile = getUserProfilePromise()
    // var getUserSkills = getUserSkillsPromise();  P0 disabled
    var getProjectProgress = getProjectProgressPromise();     
    //var getLessonProgress = getLessonProgressPromise()
    // var getGallery = getGalleryData('', '')
    var getPrevSubmitCode = getPrevSubmitCodePromise();   
    var getCertificates = getCertificatesPromise();

    Promise.all([
      getUser,
      getUserProfile,
      getProjectProgress,      
      //getLessonProgress,
      getPrevSubmitCode,
      getCertificates
    ]).then(function (results) {

      user_details = results[0]
      user_profile = results[1]
      // str = JSON.stringify(user_profile, null, 4);
      // console.log("user_profile:\n"+str);

      // user_skills = results[2]; P0 disabled
      // str = JSON.stringify(user_skills, null, 4);
      // console.log("user_skills:\n"+str);

      project_progress = results[2];

      //lesson_progress = results[1]
      if (project_progress != null && project_progress != undefined && project_progress.lesson_progress != undefined && project_progress.lesson_progress[save_lesson_id] != undefined) {
        lesson_progress = project_progress.lesson_progress[save_lesson_id]
        if (lesson_progress != null && lesson_progress != undefined && lesson_progress.checkpoints_completed != null && lesson_progress.checkpoints_completed != undefined) {
         checkpoints_completed = Object.values(lesson_progress.checkpoints_completed)
        } else {
          checkpoints_completed = []
        }
      }

      

      get_prev_submit_code = results[3];

      certificates_data = results[4];
 
      onStartLessons()

      if (showRegisterModal && user_status != "userRegistered") {
        registerModal();
      }

     });
  } else if (page_template == "projects") {
    var getUser = getUserPromise()
    var getUserProfile = getUserProfilePromise();
    var getProjectProgress = getProjectProgressPromise();
    var getCertificates = getCertificatesPromise();
    // var getUserSkills = getUserSkillsPromise();  P0 disabled


    Promise.all([
      getUser,
      getUserProfile,
      getProjectProgress,
      getCertificates
      // getGallery
    ]).then(function (results) {

      user_details = results[0]
      user_profile = results[1];
      project_progress = results[2];
      certificates_data = results[3];
      


     // str = JSON.stringify(getAllUsers, null, 4);
     // console.log("getAllUsers:\n"+str);

      //moveUserData();
      onStartProjects();

      if (showRegisterModal && user_status != "userRegistered") {
        registerModal();
      }
/*
      // Custom code to award certificates to students who already had completed project

      var getAllUsers = getAllUsersPromise();

      Promise.all([
        getAllUsers
      ]).then(function (results) {
        all_users = results[0];

        Object.keys(all_users.user_profile).forEach(function(index) {
          console.log("all_users: " +JSON.stringify(index));
          bulkCertAwarded(all_users.user_profile[index],all_users.users[index], index);   
        })

      });
*/
/*
      // Custom code to award certificates to students who already had completed project

      var getAllUsers = getAllUsersPromise();

      Promise.all([
        getAllUsers
      ]).then(function (results) {
        all_users = results[0];

        Object.keys(all_users.user_profile).forEach(function(index) {
          console.log("all_users: " +JSON.stringify(index));
          bulkCertAwarded(all_users.user_profile[index],all_users.users[index], index);   
        })

      });




          // Custom code to award certificates to students who already had completed project

      var getAllUsers = getAllUsersPromise();

      Promise.all([
        getAllUsers
      ]).then(function (results) {
        all_users = results[0];

        Object.keys(all_users.users).forEach(function(index) {
          full_name = all_users.users[index].first_name + "_" + all_users.users[index].last_name;
          console.log("all_users: " +JSON.stringify(index));
          console.log("titleCase: " +full_name.replace(/\s+/g, '').toLowerCase());
        
        firebase
          .database()
          .ref('users/' + index )
          .update({
            full_name: full_name.replace(/\s+/g, '').toLowerCase()
          })

      });
    });

*/
    });

  

  }

  
})

var all_users

$('.projectTitle').click(function () {
  titleInput()
})

$('#help-icon').click(function () {
  introTour()
})






function introTour() {
  console.log("introTour ");
  if(!buildTour) {
    buildTour = true;
    $(".modalPlaceholder").after(
      `
      <div class="swiper-container swiper-container-tour">
      <a class="menu-button cross close-tour"><div class="bar"></div><div class="bar"></div><div class="bar"></div></a>
      <div class="swiper-wrapper">
        <div class="swiper-slide bg-aqua2" id="intro_tour01" >
          <div>
          </div>            
          <div>
            <p class="slide-header h2 mb-4  ">THE TOUR</p>            
            <p class="h1 mb-3 ">Hey there,...</p>
            <p class="h3 mb-4 ">So glad you’re here.</p>
            <p class="mb-4 w-75 text-center mx-auto"><img class="w-30 swiper-lazy" data-src="../img/emoji/72/grinning-face-with-smiling-eyes.png" alt=""></p>
            <p class="h3 mb-3  ">We have 3 main sections. I’ll explain them.</p>
            <p class="h3 ">Swipe up to start.</p>
          </div> 
          <div class="arrow">
            <img class="swiper-lazy swiper-lazy-loaded" alt="" src="../learn/img/down-scroll.png">
          </div>
        </div> 
        <div class="swiper-slide bg-aqua2" id="intro_tour02" >
          <div>
          </div>            
          <div>         
            <p class="h3 mb-3  ">The first section is,...</p>
            <p class="h1 mb-4  ">1. SLIDES <i class="icon-sentiment_satisfied" style="font-size: 1em; vertical-align: -.15em;"></i></p>
            <p class="mb-4 text-center mx-auto"><img class="w-50 swiper-lazy" data-src="../learn/img/Intro-Briefing.jpg" alt=""></p>
            <p class="h3 mb-3  ">This is where you learn new things & progress through the training.</p>
          </div> 
          <div class="arrow">
            <img class="swiper-lazy swiper-lazy-loaded" alt="" src="../learn/img/down-scroll.png">
          </div>
        </div>  
        <div class="swiper-slide bg-aqua2" id="intro_tour03" >
          <div>
          </div>            
          <div>         
            <p class="h3 mb-3 ">Some of the slides will be...</p>
            <p class="h1 mb-4  ">CHALLENGES</p>
            <p class="mb-4 text-center mx-auto"><img class="w-50 swiper-lazy" data-src="../learn/img/Intro-Challenge.jpg" alt=""></p>
            <p class="h3 mb-3 ">These tell you what code to type.</p>
            <p class="h5 mb-3 ">(It’s super fun & there are lots of tips.)</p>
          </div> 
          <div class="arrow">
            <img class="swiper-lazy swiper-lazy-loaded" alt="" src="../learn/img/down-scroll.png">
          </div>
        </div> 
        <div class="swiper-slide bg-aqua2" id="intro_tour04" >
          <div>
          </div>            
          <div>         
            <p class="h3 mb-3 ">The next section is the...</p>
            <p class="h1 mb-4 ">2. EDITOR <i class="icon-code1" style="font-size: 1em; vertical-align: -.15em;"></i></p>
            <p class="mb-4 text-center mx-auto"><img class="w-50 swiper-lazy" data-src="../learn/img/Intro-Editor.jpg" alt=""></p>
            <p class="h3 mb-3  ">Here is where you type all of your code – The coolest place in the world. 🙂</p>
          </div> 
          <div class="arrow">
            <img class="swiper-lazy swiper-lazy-loaded" alt="" src="../learn/img/down-scroll.png">
          </div>
        </div> 
        <div class="swiper-slide bg-aqua2" id="intro_tour05" >
          <div>
          </div>            
          <div>         
            <p class="h3 mb-3  ">The last section is the...</p>
            <p class="h1 mb-4  ">3. BROWSER <i class="icon-personal_video" style="font-size: 1em; vertical-align: -.15em;"></i></p>
            <p class="mb-4 text-center mx-auto"><img class="w-50 swiper-lazy" data-src="../learn/img/Intro-Preview.jpg" alt=""></p>
            <p class="h3 mb-3  ">This is exactly like an internet browser & will show the websites you are building, as you go along.</p>
          </div> 
          <div class="arrow">
            <img class="swiper-lazy swiper-lazy-loaded" alt="" src="../learn/img/down-scroll.png">
          </div>
        </div>
        <div class="swiper-slide bg-aqua2" id="intro_tour06" >
          <div>
          </div>            
          <div>
            <p class="h1 mb-3">Got it?</p>
            <p class="mb-4 w-75 text-center mx-auto"><img class="w-30 swiper-lazy" data-src="../img/emoji/72/thinking-face.png" alt=""></p>
            <p class="h4 mb-4 ">If you’re not sure, just scroll up & through these slides one more time.</p>
            <p class="h3  mb-4 ">Yabadabadoo!!!!</p>
            <a class="btn btn-primary close-tour bg-pink fs-x2" style="">LET’S GET CODING!</a>
          </div> 
          <div>
          </div>
        </div> 
      </div>
      </div>

      `
    );

    swiperTour = new Swiper('.swiper-container-tour', {
      direction: 'vertical',
      shortSwipes: true,
      mousewheel: true,
      preventClicks: false,
      preventClicksPropagation: false,
      preloadImages: false,
      // initialSlide: active_slide,
      // Enable lazy loading
      lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 1
      },
      allowTouchMove: true,
      loop: false,
      on: {
        init: function () {

        },
        slideChange: function () {
       
        },
      }

    });
  }  
  $(".swiper-container-tour").show();

  $(".close-tour").click(function() {
    $(".swiper-container-tour").hide();
    swiperTour.slideTo(0);
  });

  
}

function submitCodeChallengeModal(submitTemplate) {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  
  var submitted_code_chars = htmlspecialchars(getEditorValue())
  //console.log('submitted_code_chars: ' + submitted_code_chars)

  $.createDialog({
    modalName: 'submitCodeChallenge-modal',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-sm ',
    //actionButton: registerNextModal,
    htmlContent: 
    `
    <div id="confirm_dialog" style="display: block;">
      <h2 id="confirm_title">Please confirm your details so we can be in touch when we announce the competition winner.</h2>
      <form class="submitCodeChallenge-form needs-validation waXs-validated" novalidate>
        <div class="form-row">
            <div class="col-6 col-md-6 mb-1">
              <label for="pf_name">First name</label>
              <input type="text" class="form-control" name="first_name" placeholder="Required" required value="${user_details.first_name ? user_details.first_name : ''}">
                <div class="invalid-feedback ">
                  Please enter your first name.
                </div>              
            </div>
            <div class="col-6 col-md-6 mb-1">
              <label for="pl_name">Last name</label>
              <input type="text" class="form-control" name="last_name" placeholder="Required" required value="${user_details.last_name ? user_details.last_name : ''}">
                <div class="invalid-feedback ">
                  Please enter your last name.
                </div>              
            </div>
            <div class="col-6 col-md-6 mb-1">
              <label for="pl_name">Email Address</label>
              <input type="email" class="form-control" name="email_id" placeholder="Required" required value="${(user_details.details && user_details.details.email_id) ? user_details.details.email_id : ''}">
                <div class="invalid-feedback ">
                  Please enter your Email.
                </div>              
            </div>
            <div class="col-6 col-md-6 mb-1">
              <label for="pl_name">Mobile (optional)</label>
              <input type="text" class="form-control" name="mobile_number" placeholder=""  value="${(user_details.details && user_details.details.mobile_number) ? user_details.details.mobile_number : ''}">
                <div class="invalid-feedback ">
                  Please enter your last name.
                </div>              
            </div>
        </div>
        <input type="hidden" name="day" value="${user_details.D_O_B.day ? user_details.D_O_B.day : ''}">
        <input type="hidden" name="month" value="${user_details.D_O_B.month ? user_details.D_O_B.month : ''}">
        <input type="hidden" name="year" value="${user_details.D_O_B.year ? user_details.D_O_B.year : ''}">
        <input type="hidden" name="lesson_ID" value="${save_lesson_id}">
        <input type="hidden" name="user_ID" value="${auth_id}">
        <input type="hidden" id="submitted_link" name="submitted_link" value="${'https://codejika.com/preview/' + save_lesson_id + '?' + auth_id}">
        <input type="hidden" id="submitted_code" name="submitted_code" value="${submitted_code_chars}">
        <button id="close_button" class="btn btn-primary light cancel mt-2" data-dismiss="modal">Maybe later</button>
        <button id="action_button" class="btn btn-primary light action mt-2">Submit</button>
      </form>
    </div>
    `
  })


  var $submitForm = $('.action');

  $submitForm.click(function(event){
    event.preventDefault();
    if ($('.submitCodeChallenge-form')[0].checkValidity() != true) {
      $('.submitCodeChallenge-form').addClass('was-validated')
        //console.log("checkValidity false");
    } else {          
      var dataparam = $('.submitCodeChallenge-form').serialize();
      console.log('submitCodeChallenge: ' + dataparam);
      $.ajax({
          type: 'POST',
          async: true,
          url: 'https://codejika.com/form/'+submitTemplate+'.php',
          data: dataparam,
          datatype: 'json',
          cache: true,
          global: false,
          beforeSend: function() { 
              //$('#loader').show();
          },
          success: function(result) {
              if(result){
                  console.log('result: ' + result);
              } else {
                  console.log('no result returned');
              }

          },
          complete: function() { 
              console.log('submit process complete');
          }
      });
      confirmCodeChallengeModal();
    }
  })


}

function confirmCodeChallengeModal() {
  $('.avatar-modal, .modal-backdrop, .blocker').remove()
  $.createDialog({
    modalName: 'successsubmitCodeChallenge',
    popupStyle: 'speech-bubble top-align ' + current_avatar + ' happy modal-xs',
    // actionButton: loginModal,
    htmlContent: 
    '<div id="confirm_dialog" style="display: block;">' +
      '\t<h2 id="confirm_title">Your Project has been submitted</h2>' +
      '<label>Don\'t forget to like our <a href="https://www.facebook.com/codejika" target="_blank">Facebook page</a> so can see the results when we post them.</label>\n\n\n' +
      '\t<div id="confirm_actions">' +
      '\t\t<button id="close_button" class="btn btn-primary light action " data-dismiss="modal">So exiting</button>' +
      '\t</div>' +
      '</div>'
  })

}

function htmlspecialchars(str) {
  if (typeof(str) == "string") {
  str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
  str = str.replace(/"/g, "&quot;");
  str = str.replace(/'/g, "&#039;");
  str = str.replace(/</g, "&lt;");
  str = str.replace(/>/g, "&gt;");
  }
  return str;
 }






