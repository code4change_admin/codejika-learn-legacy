 /*propStartingWith = function(obj, prefix) {
     var res = [];
     for (var m in obj) {
         if (m.indexOf(prefix) === 0) {
             res.push(m);
         }
     }
     return res;
 };*/
 $(function() {
     // Creating the console.
     var header = "";
     var intro = '<div class=\"log\"><p>Hi there\! People write programs to make computers do things. To start, we can make your computer do some math for us \(so we don\'t have to do it ourselves\)\!</p>\n' +

         '<p>Let\'s get started by adding any two numbers. Try typing a command that looks like this: <strong>3 + 4</strong></p>\n' +

         '<p>Hit <strong>Enter</strong> after you type them in (make sure to do this from now on after you complete the instructions). If you get stuck (output shows \'...\'), then press <strong>Ctrl-Z</strong> to exit current command and try again.</p><div>';
     var checkpoint = 1;
     window.jqconsole = $('#console').jqconsole(header, '> ');
     jqconsole.Append(intro);
     // Abort prompt on Ctrl+Z.
     jqconsole.RegisterShortcut('Z', function() {
         jqconsole.AbortPrompt();
         handler();
     });

    // jqconsole.RegisterMatching('{', '}', 'brace');
    // jqconsole.RegisterMatching('(', ')', 'paran');
    // jqconsole.RegisterMatching('[', ']', 'bracket');
     // Handle a command.
	 
	 var checkpoint = 1;
	 var result;
	 var errorMsg = '<span style="color: crimson">Oops, try again.<br><span style="color: #333">It looks like you didn\'t add any numbers! Make sure you added two numbers as shown in the directions.</span></span>';
	 
     var handler = function(command) {
		 
         if (command === "exit") {
			  jqconsole.AbortPrompt();
         handler();
		 console.log(command); // true
		 }
		 if (command) {

             try {
                 jqconsole.Write('==> ' + window.eval(command) + '\n', 'output');
                 if (checkpoint === 1) {
                     var re = /^\s*\d*\s*\+\s*\d*\s*$/;

					 //console.log(command);
                     //console.log(re);
                     result = re.test(command);

                     //console.log(result); // true
                     if (result) {

                         jqconsole.Append($('<span class=\"log\"><p>Perfect! You can get all kinds of information from the computer. It knows a lot!</p>\n' +
                             '<p>Type <strong>Date()</strong> (make sure to use a capital "D" and include the parentheses!) to get the current date from the computer.</p></span>'));

                         checkpoint = 2;
						 errorMsg = '<span style="color: crimson">Oops, try again.<br><span style="color: #333">Did you type Date() ? You don\'t need quote for this one!</span></span>';
                     } else {
						jqconsole.Append($(errorMsg).css('color', 'crimson').addClass('log'));
					 }
                 } else if (checkpoint === 2) {
                     var re = /^\s*Date\s*\(\s*\)\s*$/;

                     //console.log(command);
                     //console.log(re);
                     result = re.test(command);
                     //console.log(result); // true
                     if (result) {

                         jqconsole.Append($('<span class=\"log\"><p>Excellent! We can also get information from the computer. For example, you can find how long a sentence is by typing .length after it.</p>\n' +
                             '<p>How long is your e-mail address? For me, I would need to type the command <strong>"example@codejika.com".length</strong></p></span>'));

                         checkpoint = 3;
						 errorMsg = '<span style="color: crimson">Oops, try again.<br><span style="color: #333">Did you type your e-mail address between quotes and follow it with .length?</span></span>';
                     } else {
						jqconsole.Append($(errorMsg).css('color', 'crimson').addClass('log'));
					 }
                 } else if (checkpoint === 3) {
                     var re = /^\s*\"([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})\"\.(length)\s*$/;

                     //console.log(command);
                     //console.log(re);
                     var result = re.test(command);
                     //console.log(result); // true
                     if (result) {

                        $('#console').css('min-height', '420px');

                         checkpoint = 4;
						 errorMsg = '';
                     } else {
						jqconsole.Append($(errorMsg).css('color', 'crimson').addClass('log'));
					 }
                 }				 
             } catch (e) {
                 jqconsole.Write('ERROR: ' + e.message + '\n', 'error');
                 jqconsole.Append($(errorMsg).css('color', 'crimson').addClass('log'));

             }
         }
         jqconsole.Prompt(true, handler, function(command) {
             // Continue line if can't compile the command.
             try {
                 Function(command);
             } catch (e) {
                 if (/[\[\{\(]$/.test(command)) {
                     return 1;
                 } else {
                     return 0;
                 }
             }
             return false;
         });
     };
   /*  jqconsole.SetKeyPressHandler(function(e) {
         var text = jqconsole.GetPromptText() + String.fromCharCode(e.which);
         // We'll only suggest things on the window object.
         if (text.match(/\./)) {
             return;
         }
         var props = propStartingWith(window, text);
         if (props.length) {
             if (!$('.suggest').length) {
                 $('<div/>').addClass('suggest').appendTo($('.jqconsole'));
             }
             $('.suggest').empty().show();
             props.forEach(function(prop) {
                 $('.suggest').append('<div>' + prop + '</div>');
             });
             var pos = $('.jqconsole-cursor').offset();
             pos.left += 20;
             $('.suggest').offset(pos);
         } else {
             $('.suggest').hide();
         }
     });
     jqconsole.SetControlKeyHandler(function(e) {
         $('.suggest').hide();
         if (e.which === 9 && $('.suggest div').length) {
             jqconsole.SetPromptText($('.suggest div').first().text());
             return false;
         }
     });*/
     // Initiate the first prompt.
     handler();
 });