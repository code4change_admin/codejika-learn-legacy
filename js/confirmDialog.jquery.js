(function($) {
  $.createDialog = function(options) {
    $(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class="modal fade" id="' +
        options.modalName +
        '">\n' +
        '<div class="modal-dialog ' +
        options.popupStyle +
        '">\n' +
        '<div class="modal-content">\n' +
        '<div class="modal-body">\n' +
        options.htmlContent +
        "</div>\n" +
        "</div>\n" +
        "</div>\n" +
        "</div>\n"
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function(e) {
      $("#" + options.modalName).modal("hide");
      // $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display",'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
    $("#" + options.modalName).modal({
      closeExisting: true
    })
  };
})(jQuery);

// thumnail modal
(function($) {
  $.galleryDialog = function(options) {
    $(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class=" fade" id="' +
        options.modalName +
        '">\n' +
        '<div class="' +
        options.popupStyle +
        '">\n' +
        '<div class="modal-content">\n' +
        '<div class="">\n' +
        options.htmlContent 
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function(e) {
      $("#" + options.modalName).modal("hide");
      // $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display",'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);

// nested  modal 1
(function($) {
  $.nestedModal1 = function(options) {
    // $(".modal , .modal-backdrop").remove();
    console.log("clicked " + options.modalName);
    $(".modalPlaceholder").after(
      '<div class=" fade" id="' +
        options.modalName +
        '">\n' +
        '<div class="' +
        options.popupStyle +
        '">\n' +
        '<div class="modal-content">\n' +
        '<div class="">\n' +
        options.htmlContent 
    );
    $("#action_button").bind("click", options.actionButton);
    $("#close_button, .modal-backdrop").bind("click", function(e) {
      $("#" + options.modalName).modal("hide");
      // $(".modal , .modal-backdrop").remove();
      $(".blocker").css("display",'none')
      $(this).removeClass('blocker')
      //inactivityTime();
      console.log("closed " + options.modalName);
    });
    $("#" + options.modalName).modal("show");
  };
})(jQuery);
