<?php
$lang_name = 'Zambia'; // Country Name
$lang_where = 'in Zambia'; // location description
$lang_meta_title ='CodeJIKA.com - Coding Clubs in Schools in South Africa - Have fun, make friends & build websites.';
$lang_meta_description = 'CodeJIKA - eco-systems of vibrant student-run coding clubs in secondary schools in Zambia';

$lang_h1_seo ='<div style="font-size: 70%;padding-bottom: 30px;line-height: 86%;">HELLO ZAMBIA</div> Let\'s learn coding!';

?>