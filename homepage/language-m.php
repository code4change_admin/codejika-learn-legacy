<?php
//$lang_path = $_SERVER['DOCUMENT_ROOT'].'/_custom-homepage/'.$get_lang;

$lang_home_url = 'https://'.$_SERVER['SERVER_NAME']; 
$lang_name = 'World'; // Country Name
$lang_where = 'around the World'; // location description
$lang_name_1 = 'Coder';  // singular e.g 'Zambian'
$lang_name_2 = 'Coders'; // plural e.g 'Zambians'
$lang_meta_title ='CodeJIKA.com | Coding for Teens –Online, Offline, In-school, out-of-school - Just do it!';
$lang_meta_description = 'Vocational Coding in High Schools –Radical, offline, fun and student-driven. Join us.';
$lang_meta_keywords = 'coding, schools, Africa, South America, innovation, social impact, education, code';
$lang_h1_seo ='<div style="font-size: 70%;padding-bottom: 30px;">HELLO WORLD</div> Let\'s learn coding!';
$lang_h2_seo = 'Build websites,<br/>apps and games';
$lang_testimonial_photo01 = '/img/testimonal-themba.jpg';
$lang_testimonial_photo02 = '/homepage/dev/testimonal-banji2.jpg';  //custom image
$lang_logos = array(
  '/homepage/dev/logo-intercontinental-lusaka.jpg', //custom logo
  '/img/logo-datatec.jpg',
  '/img/logo-tcs.jpg',
  '/img/logo-dell.jpg',
);
/*
$lang_faqs = array(
  ['custom-faqs01', 'My custom FAQ title 01', 'Here is custom FAQ content 01'],
  ['custom-faqs02', 'My custom FAQ title 02', 'Here is custom FAQ content 02'],
);
*/
?>