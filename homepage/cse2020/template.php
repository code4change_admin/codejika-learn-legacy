<?php
  //$home_url = 'https://'.$_SERVER['SERVER_NAME'];
  /*  Header('Vary: User-Agent');
   $con = mysqli_connect("localhost","jika_admin","PvkQRaTtDvht","jika_wp");
   $result = mysqli_query($con,"SELECT option_value FROM `wp_options` WHERE `option_name` = 'widget_custom_html'");
   while($row = mysqli_fetch_assoc($result)) {
      $x = $row['option_value'];
   }
   $extracted= unserialize($x);
   $dp  = "";
   $curriculum  = "";
   if($extracted){
      foreach ($extracted as $value) {
         if($value['title'] == "custom html"){
            $dp  = $value['content'];
         }
         if($value['title'] == "curriculum"){
            $curriculum  = $value['content'];
         }
      }
   }*/
   if (!function_exists('pll_home_url')) {
   function pll_home_url() {
     return $home_link;
     
   }
   }
   
   $dp = " 
   <div class=\"box-menu-wraper\">
     <div class=\"container\">
       <div class=\"bxmenu-wrap\">
         <h3>Menu</h3>
         <div class=\"row\">
           <div class=\"col-lg-12 col-md-12\">
             <a href=\"/schools\">
               <div class=\"menu-col\">
                 <h4>Schools</h4>
                 <p>Join the network of forward-thinking schools building an eco-system of fun, student-run coding clubs.</p>
               </div>
             </a>
           </div>
           <div class=\"col-lg-12 col-md-12\">
             <a href=\"/business\">
               <div class=\"menu-col\">
                 <h4>Business</h4>
                 <p>Coding in schools allows youth to create and build tools to empower SMEs and the economy.</p>

               </div>
             </a>
           </div>
           <div class=\"col-lg-12 col-md-12\">
             <a href=\"/coding-resources\">
               <div class=\"menu-col\">
                 <h4>CodeJIKA Resources</h4>
                 <p>Find the tools to teach, inspire, advertise and \"Rock this\". :)</p>

               </div>
             </a>
           </div>
           <div class=\"col-lg-12 col-md-12\">
             <a href=\"#curriculum_section\">
               <div class=\"menu-col\">
                 <h4>Curriculum</h4>
                 <p>YAAASSSS! This is the fun part. </p>
                 <p>Download our offline tools or link up to rad mobile lessons.</p>
               </div>
             </a>
           </div>

         </div>
       </div>
     </div>
   </div>";

   $curriculum = " 
   <div class=\"box-menu-wraper x-box\">
     <div class=\"container\" id=\"curriculum_section\">
       <div class=\"bxmenu-wrap\">
         <h3>Curriculum</h3>
         <div class=\"main-box\">
           <div class=\"sub-headings\">
             <h4>Online</h4>
           </div>
           <div class=\"row\">
             <div class=\"col-lg-12 col-md-12 \">
               <div class=\"inside-col\">
                 <h4>5-Minute-Website</h4>
                 <div class=\"table-content\">
                   It breaks all the rules, but it’s the best way to start.
                 </div>
                 <div class=\"foot-links\">
                   <a href=\"/learn/5-minute-website\" class=\"link btn-green\">Start Now</a>
                 </div>
               </div>
             </div>
             <div class=\"col-lg-12 col-md-12\">
               <div class=\"inside-col\">
                 <h4>Curric: Projects 1 -3</h4>
                 <div class=\"table-content\">
                   Get started on mobile and keep coding on Desktop. The most fun and flexible platform in the world.
                 </div>
                 <div class=\"foot-links\">
                   <a href=\"/learn/\" class=\"link btn-green\">Start Now</a>
                 </div>
               </div>
             </div>
           </div>
           <div class=\"sub-headings\">
             <h4>Offline</h4>
           </div>
           <div class=\"row\">
             <div class=\"col-lg-12 col-md-12\">
               <div class=\"inside-col\">
                 <h4>5-Minute-Website</h4>
                 <h6>Lovin it! Do not ask me, just do it!</h6>
                 <div class=\"table-content\">
                   <div class=\"fst\">
                     <p>Awesome</p>
                     <p>Level: Beginner</p>
                   </div>
                   <div class=\"scnd\">
                     <p>Level: Beginner</p>
                     <p>Time: 20 Mins</p>
                   </div>
                 </div>
                 <div class=\"foot-links\">
                   <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                   <a href=\"/wp-content/uploads/2019/08/2019-CodeJIKA-5MW-Instr-How-to-teach-the-5-Minute-Website-v1.pdf\" download class=\"link color_green\">How To PDF</a>
                   <a href=\"/wp-content/uploads/2019/08/201906-5-Minute-Website-CodeJIKA.com-Workshop-v6.pdf\" class=\"link color_red\" download>Download PDF</a>
                 </div>
               </div>
             </div>
             <div class=\"col-lg-12 col-md-12\">
               <div class=\"inside-col\">
                 <h4>Project 1</h4>
                 <h6>Instead of watching a movie...</h6>
                 <div class=\"table-content\">
                   <div class=\"fst\">
                     <p>HTML & CSS Basics</p>
                     <p>4 Trainings</p>
                   </div>
                   <div class=\"scnd\">
                     <p>Level: Beginner</p>
                     <p>Time: 90 Mins</p>
                   </div>
                 </div>
                 <div class=\"foot-links\">
                   <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                   <a href=\"/wp-content/uploads/2019/09/201908-Intro-Guide-CodeJIKA.com-v2.pdf\" class=\"link color_green\" download>Intro Guide</a>
                   <a href=\"/downloads/201908 PROJECT 1 - CodeJIKA.com DT v2.pdf\" class=\"link color_red \" download>Download PDF</a>
                 </div>
               </div>
             </div>
           </div>
           <div class=\"row\">
             <div class=\"col-lg-12 col-md-12\">
               <div class=\"inside-col\">
                 <h4>Project 2</h4>
                 <h6>Mind blowing</h6>
                 <div class=\"table-content\">
                   <div class=\"fst\">
                     <p>Responsive & Forms</p>
                     <p>5 Trainings</p>
                   </div>
                   <div class=\"scnd\">
                     <p>Level: Beginner</p>
                     <p>Time: 150 Mins</p>
                   </div>
                 </div>
                 <div class=\"foot-links\">
                   <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                   <a href=\"/wp-content/uploads/2019/08/201903-PROJECT-2-CodeJIKA.com-DT-v2.pdf\" class=\"link color_red separate-it\" download>Download PDF</a>
                 </div>
               </div>
             </div>
             <div class=\"col-lg-12 col-md-12\">
               <div class=\"inside-col\">
                 <h4>Project 3</h4>
                 <h6>You would not believe it.</h6>
                 <div class=\"table-content\">
                   <div class=\"fst\">
                     <p>Responsive & Forms</p>
                     <p>6 Trainings</p>
                   </div>
                   <div class=\"scnd\">
                     <p>Level: Beginner</p>
                     <p>Time: 210 Mins</p>
                   </div>
                 </div>
                 <div class=\"foot-links\">
                   <a href=\"/curriculum\" class=\"link btn-gray\">Learn More</a>
                   <a href=\"/wp-content/uploads/2019/07/201903-PROJECT-3-CodeJIKA.com-DT-v1.pdf\" class=\"link color_red separate-it\" download>Download PDF</a>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   ";

   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
      <meta name="google-site-verification" content="SxN62DDQ5qWO9qlDiyiqQBj72i1hGxOukG8sqIDTZVo" />
      <meta name="google-site-verification" content="Cv8eoG1XMqtBWl5jJasnizpoNzQgThpzSBwynXUId0c" />
      <link rel="icon" type="image/ico" href="<?php echo $home_url;?>/assets/img/favicon.ico">
      <!--link rel="canonical" href="/" /-->
      <!--link rel="alternate" media="handheld" href="/?mobile" /-->
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <title>CodeJIKA.com | Coding for Teens Online, Offline, In-school, out-of-school - Just do it!</title>
      <meta name="title" content="Learn how to code in just 5 minutes a website. Join us for Computer Science Education Week.">
      <meta name="description" content="Vocational Coding in High Schools –Radical, offline, fun and student-driven. Join us">
      <meta name="keywords" content="coding, schools, Africa, SouthAmerica, innovation, social impact, education, code, Computer Science Education, Computer Science Education Week, CSE 2020">
      <meta property="og:title" content="Learn how to code in just 5 minutes a website. Join us for Computer Science Education Week.">
      <meta property="og:site_name" content="codejika">
      <meta property="og:url" content="/">
      <meta property="og:description" content="code on your phone, offline or in-class, but always have fun">
      <meta property="og:type" content="website">
      <meta property="og:image" content="/img/201908_cj_OG_girl_fb.jpg">
      <meta property="og:image:url" content="/img/201908_cj_OG_girl_fb.jpg" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:site" content="@Codejika" />
      <meta name="twitter:creator" content="@Codejika" />
      <meta name="twitter:title" content="Learn how to code in just 5 minutes a website. Join us for Computer Science Education Week." />
      <meta name="twitter:description" content="Code on your phone, offline or in-class, but always have fun. Frontend Web Development for awesome teens in Africa, South America or Anywhere." />
      <meta name="twitter:url" content="https://codejika.com" />
      <meta name="twitter:image" content="/img/201908_cj_OG_girl_tw.jpg" />
      <meta name="twitter:image:alt" content="Codejika" />
      <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
      <!--     Fonts and icons     -->
      <link href="../libraries/fontawesome/css/fontawesome.min.css" rel="stylesheet">
      <!--link rel="stylesheet" type="text/css" media="screen, print, projection" href="/css/css-compress.php" /-->
      <link rel="stylesheet" type="text/css" media="screen, print, projection" href="/css/merged-homepage2.min.css?dfsd" />

      
      <script async src="../libraries/analytics/gtag-custom.js?id=UA-63106610-3" ></script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         gtag('config', 'UA-63106610-3');
         console.log("<?php echo "re: ".$cj_cookies["devicetype"]." file end: ".$mobile." logic: ".$logic; ?>");
      </script>
      <script type="application/ld+json">
         {
           "@context": "http://schema.org",
           "@type": "http://schema.org/EducationalOrganization",
           "description": "An eco-systems of vibrant student-run coding clubs in Schools in Africa",
           "name": "CodeJIKA"
         }
      </script>
      <style type="text/css">
        
        .page-header:before {
            background-color: rgba(0,0,0,0.65);
        }

        @media screen and (max-width: 767px) {
          .container.lesson-cards {
              tXop: -150px;
          }
          #h1-seo { font-size: 3em!important }


        }

        .arrow_box {

    background: #9d6fd2;
  }
      </style>
      
     </head>
   <body class="index-page sidebXar-collapse">
      <div class="wrapper" style="">
      <div class="page-header clear-filter" style="background-image: url(/img/homepage_header_cse2020.png)">
         <!-- Navbar -->
        <div  class="award " style="z-index: 1049;position: relative; left: 0; top: 0; border: 0; width: 100%; height:auto; font-size:12px;background: #7c43cf;text-align: center;">
          <a style="text-transform:none;color: #fff;text-align: center;font-size:12px;line-height: 20px;width: 100%; height:100%;padding: 4px 8px;" href="/kickstarter" target="_blank">Click here to join the Coding Kickstarter Campaign</a>
        </div>   
         <div class="codejika-header">
            <nav class="navbar fixed-top navbar-expand-sm navbar-dark bg-dark">
               <div class="container ">
                  <!-- Brand -->
                  <a class="navbar-brand mr-auto" href="<?php echo pll_home_url();?>"><img alt="jika-logo" src="<?php echo $home_url;?>/img/logo-jika.png" style="wiXdth: 120px;"> <span class="d-none d-lg-inline " style="font-size: 30px;
                     font-family: 'rajdhani';
                     font-weight: 500;
                     padding-left: 36px; top: 7px; position: relative;">CODE<strong>JIKA</strong>.com</span></a>
                  <!-- Links -->
                  <div class="collapse navbar-collapse" id="nav-content">
                     <ul class="navbar-nav" style="color:#338db9 !important;">
                        <li class="nav-item d-none d-md-block">
                           <a class="nav-link" href="/learn/projects">Start Coding</a>
                       </li>
                        <li class="nav-item">
                           <a href="#" class="nav-menu-icon" id="menu-modal" data-toggle="modal" data-target="#menuModal" style=""><i class="icon-navicon fs-2"></i></a>
                        </li>
                     </ul>
                  </div>
               </div>
            </nav>
         </div>
         <!-- End Navbar -->
         <div class="page-header-image" data-parallax="false" style="">
         </div>
         <div class="container header-intro text-center">
            <div class="row headHeroText">
               <div class="col-md-14 offset-md-5 col-xs-24">
                  <h2 id="h1-seo" class="subtitle mt-4 mb-3" style="font-size: 4rem;font-family: 'Rajdhani';">JOIN THE MOVEMENT TO COMPUTE, CREATE, AND INNOVATE!!!</h2>
                  <h1 class="subtitle mt-1 mb-0" style="font-weight: 700;font-size: 2.8em;color:#fff;
    display: block;">It’s Computer Science Education Week</h1>
                  <p class=" mt-1 mb-5" style="font-weight: 300;color:#fff;">DEC 7-13, 2020</p>
               </div>
            </div>
         </div>
      </div>
      <div class="container lesson-cards ">
         <div   style="toXp:-100px; position: rXelative;backXground-color: #fff;" >
            <h2 class="title text-center pb-4 hide" style="color:#fff; position: relative; font-weight: 300;">Join the movement to compute, create, and innovate.</h2>
            <div class="card-deck col-md-24  mx-auto">
               <div class="card p-0 ordXer-md-1 order-sm-0 col-md-12 col-md-8" style="border-radius: 0px;">
                  <div class="card-body">
                     <h5 class="card-title" style="font-weight: bold;">5&#8209;Minute&#8209;Website</h5>
                     <p class="card-text" style="font-weight: bold;">Code your first website in five minutes.<br><br></p>
                     <div class="btn btn-lg bg-primary white " style="background-color: #14b0bf!important;cursor:initial; white-space: initial;margin: 0;">CODE NOW!</div>
                      <a href="/learn/5-minute-website" class=" divLink" ></a>
                  </div>
               </div>
               <div class="card p-0 ordXer-md-1 order-sm-0 col-md-12 col-md-8" style="border-radius: 0px;">
                  <div class="card-body">
                     <h5 class="card-title" style=" font-weight: bold;">Project 1</h5>
                     <p class="card-text" style="font-weight: bold;">Create a landing page and learn the basics of HTML<br><br></p>
                     <div class="btn btn-lg bg-primary white " style="background-color: #ff00a5!important;cursor:initial; white-space: initial;margin: 0;">START NOW!</div>
                      <a href="/learn/P1Training1" class=" divLink" ></a>
                  </div>
               </div>
               <div class="card p-0 ordXer-md-1 order-sm-0 col-md-12 col-md-8" style="border-radius: 0px;">
                  <div class="card-body">
                     <h5 class="card-title" style=" font-weight: bold;">Project 2</h5>
                     <p class="card-text" style="font-weight: bold;">Create an attractive and engaging web CV using HTML, CSS, and emojis</p>
                     <div class="btn btn-lg bg-primary white " style="background-color: #9d6fd2!important;cursor:initial; white-space: initial;margin: 0;">LET'S CODE!</div>
                      <a href="/learn/P2Training1" class=" divLink" ></a>
                  </div>
               </div>
            </div>

  <div class="container block-1 mb-5 pl-4 pr-4" style="backgrouXnd-color: white;   margin-top: 6rem !important;">
          <div class="row pt-5 pb-5 take-action" style="background-color: white; box-shadow: 2px 2px 25px 5px rgba(0, 0, 0, .2);   ">
            <div class="col-md-24 text-Xmd-left text-center mb-5 mb-sm-3"  >
               <h2 class="title mb-0" style="text-align: center;">Take Action!</h2>
            </div>
            <div class="col-md-8 text-Xmd-left text-left pb-5 mb-5 mb-sm-0"  >
            <div class="icon"><img src="<?php echo $home_url;?>/img/emoji/smiling-face-with-open-mouth-and-smiling-eyes_1f604.png" alt="Grinning Face With Smiling Eyes" title="Grinning Face With Smiling Eyes" width="64" height="64"></div>
               <p style="font-size: 20px;" class="pb-0 mb-0"><b>01. Learn how to create a landing page</b> by taking this coding lesson:<br><a style="font-size: 85%;" href="learn/P1Training1" target="_blank">Project 01 - Training 01</a></p>
            </div>
            <div class="col-md-8 text-Xmd-left text-left pb-5 mb-5 mb-sm-0"  >
            <div class="icon"><img src="<?php echo $home_url;?>/img/emoji/smiling-face-with-sunglasses_1f60e.png" alt="Smiling Face With Sunglasses" title="Smiling Face With Sunglasses" width="64" height="64"></div>
               <p style="font-size: 20px;" class="pb-0 mb-0"><b>02. Help your teacher or child’s teacher plan an Hour of Code</b> using the CodeJIKA curriculum.<br><a style="font-size: 85%;"href="/learn/" target="_blank">Coding Projects</a></p>
            </div>
            <div class="col-md-8 text-Xmd-left text-left pb-5 "  >
            <div class="icon"><img class=" lazyloaded" src="<?php echo $home_url;?>/img/emoji/person-raising-both-hands-in-celebration_1f64c.png" alt="Raising Hands" title="Raising Hands" width="64" height="64" ></div>
               <p style="font-size: 20px;" class="pb-0 mb-0"><b>03. Share your 5‑Minute‑Website or your Project Website event on social media</b><span style="font-size: 14px;" > (and stand a chance to win a Dell laptop or a Dell 2-in-1 tablet)</span></p><p style="font-size: 17px; color:#f96332;">#codeJIKA</span></p>
            </div>
            <div class="col-md-24 text-Xmd-left text-center mt-5 mt-sm-2"  >
              <p style="font-size: 20px;" class="pb-4 mb-0">“Every student should have the opportunity to learn computer science. Basic programming activities help nurture creativity and problem-solving skills. By starting early, students will have a foundation for success in any future career path”. - csedweek.org
</p>
            <a class="btn btn-md bg-primary white hide " style="font-size: 1.3em;    white-space: normal;" href="https://www.codejika.com/downloads/201811 CodeJIKA.com CSEDWeek one-pager v3.pdf" target="_blank">Check out the CSEd Week 2020 1&#8209;pager PDF</a>

            <p style="font-size: 24px;" class="pt-2 pb-4 mb-0">DELL LAPTOPS AND 2-IN-1 TABLETS to be won!
            </div>
            </div>
            </div>                                                     
                                                         
                                                                                               
                
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                  
         </div>
      </div>
      <div class="clearfix"> </div>
      <div class="main">
         <div class="section section-about block-1 " style="">

            <div style="..." id="yRibbon" class="block-2 " >
            <div class="slider-heading">
                     <h2 style="margin-top: 0px;">Vocational Coding* in High-Schools</h2>
                     <div class="headerLine" style="margin-bottom: 15px;"></div>
                     <p style="text-align:center;" class="mb-2">*Frontend Web-Development</p>
                  </div>
               <div class="content container">
                  <div id="studentXProfiles" class="row justify-content-center m-0">
                          
                     
                     <div id="left" class="col-md-10 col-sm-20 text-center pt-3">
                        <div class="lrsec-img">
                           <img class="lazy" data-src="../../img/2018-cj-girl_code_m.jpg" >
                        </div>
                        <div class="lrsec-content pr-3 pl-3 ">
                           <h3 class="mb-2" style="line-height: 1.2em;">"I make websites to give a message."</h3>
                           <p>Lebohang Maponya (13)<br> Itirele Zenzele, Diepsloot</p>
                        </div>
                     </div>
                     <div id="right" class="col-md-10 col-sm-20 text-center pt-3">
                        <div class="lrsec_img">
                           <img class="lazy" data-src="../../img/2018-cj-boy_code_m.jpg" >
                        </div>
                        <div class="lrsec-content pr-3 pl-3 ">
                           <h3 class="mb-2" style="line-height: 1.2em;">"I love the design process in making my own website."</h3>
                           <p>Web-Design Bootcamp <br> Blue Eagle, Cosmo City</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>


                              

            <div id="mediaLogos" class="pb-5 mb-5">
               <h2 class="title mb-0" style="text-align: center; color:black; margin-top:40px;">You might know us from</h2>
               <div class="headerLine"></div>
               <img class="lazy" data-src="/img/201906_cj_media_logos_grey_v1_sml.png">
            </div>

            <div class="container block-1 faq-collapse">
               <h2 class="title" style="padding-top: 20px; position: relative; color:#000;text-align: center;">FAQ//: Freakin' Awesome Questions</h2>
               <div class="headerLine"></div>
               <div id="FAQs" class="accordion"  style=" ">
                  <div class="card mb-0">
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" href="#FAQ-what-CJ">
                        What is CodeJika?
                        </a>
                     </div>
                     <div id="FAQ-what-CJ" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">
                           <p>A vibrant eco-system of student-run coding clubs in secondary schools.</p>
                           <p>Operationally it is divided into 3 Pillars;
                           <ol>
                              <li>Online (platform and partners), </li>
                              <li>Awareness (media & advocacy) and </li>
                              <li>Hands-on (In-school Clubs & Events)</li>
                           </ol>
                           </p>
                           </p>
                        </div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" href="#codejika_mean">
                        What does JIKA mean?
                        </a>
                     </div>
                     <div id="codejika_mean" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">
                           <p>PRONOUNCED: CODE–GEE-KA</p>
                           <p>JIKA MEANS “DANCE” OR TURN IN ZULU, A SOUTH AFRICAN LANGUAGE.</p>
                        </div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" href="#FAQ-what-learn">What do I learn at CodeJIKA?</a>
                     </div>
                     <div id="FAQ-what-learn" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">
                           <p>You learn how to code, starting with "1&#8209;Hour&#8209;Website" - How to code a simple website using HTML & CSS. Each consecutive project builds on this skill until you can customize and build beautiful business websites for SMEs from scratch – Like a PRO.
                           </p>
                        </div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-how-help">How can I help?</a>
                     </div>
                     <div id="FAQ-how-help" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">
                           <p>Run an event or assist schools or other organizations who are.</p>
                        </div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-run-CJ-event">How do I run a CodeJIKA event?</a>
                     </div>
                     <div id="FAQ-run-CJ-event" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">Easy. Go to the “How to run an event” page here to get started. There you’ll find the curriculum, suggested format, easy to use curriculum and tools to engage volunteers and even fundraise for the event.</div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-be-programmer">Do I have to be a programmer to run an event?</a>
                     </div>
                     <div id="FAQ-be-programmer" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">Anyone can run an event. It’s real fun and it’s about learning together and allowing others to have space to grow and someone who believes in them. You should have some desire to learn and work with the kids to find solutions though.</div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-for-primary-schools">Do you have a version for primary schools?
                        </a>
                     </div>
                     <div id="FAQ-for-primary-schools" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">There is no official version for primary schools, but from what we’ve heard the 5th, 6th and 7th graders love the program as well.</div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-not-secondary-school">Can I join if I’m not in secondary school?
                        </a>
                     </div>
                     <div id="FAQ-not-secondary-school" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">Of course. If you’ve already left school you can join the online CodeJIKA program and can help in organizing events and mentoring. The clubs and competitions are only for secondary school learners at this time.</div>
                     </div>
                     <div class="card-header1">
                        <a class="card-head collapsed card-title" data-toggle="collapse" data-parent="#FAQs" href="#FAQ-different-from-HOC">How is CodeJIKA different than Hour of Code?
                        </a>
                     </div>
                     <div id="FAQ-different-from-HOC" class="collapse" data-parent="#FAQs" >
                        <div class="card-body">CodeJIKA teaches you how to become a Junior Frontend Web-developer, as fast as possible. It’s about building communites and creating revolutions within the existing educational system, stemming from the student base rather than a teacher-driven approach.
                           CodeJIKA’s projected timeline is 3 years per group. You can go from never touching a PC to learning to hard-code professional websites in 6 months while still in school.
                           Our Belief: Our youth are engines of the new economy and are instinctively infused with a desire to build - Nothing can stop them!
                           We’re here to provide tools. That’s all.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="section " id="" style="background-color: #fff!important; coloXr:white;padding: 30px 0 50px 0;">
               <div class="container" >
                  <div class="row">
                     <div class="col-md-24 text-center"  >
                        <h2 class="title mb-1" style="padding-top: 20px; position: relative; color:#000;text-align: center;">PEOPLE THAT CARE ABOUT YOU:</h2>
                        <div class="headerLine"></div>
                     </div>
                     <div class="col-md-24 text-Xmd-left text-center logos"  >
                        <a href="https://www.dell.co.za/" target="_blank"><img alt="dell-logo"  data-src="<?php echo $home_url;?>/img/logo/logo-dell.jpg" class="mb-md-0 mb-3 lazy" style="max-width: 170px;" ></a>
                        <a href="https://www.verifone.com/en/za" target="_blank"><img alt="verifone-logo"  data-src="<?php echo $home_url;?>/img/logo/logo-verifone.png" class="mb-md-0 mb-3 lazy" style="max-width: 150px;" ></a>
                        <a href="https://www.microsoft.com/" target="_blank"><img alt="ms-logo"  data-src="<?php echo $home_url;?>/img/logo/logo-microsoft.jpg" class="mb-md-0 mb-3 lazy" style="max-width: 170px;" ></a><br>
                        <a href="https://www.cloudbees.com/" target="_blank"><img alt="CloudBees-logo" data-src="<?php echo $home_url;?>/img/logo/CloudBees-Logo.png" class="mb-md-0 mb-3 lazy"  style="max-width: 190px;" > </a>
                        <a href="https://www.iress.com/za/" target="_blank"><img alt="iress-logo" data-src="<?php echo $home_url;?>/img/logo/logo-iress.jpg" class="mb-md-0 mb-3 lazy" style="" > </a>
                        <a href="https://www.pnet.co.za/" target="_blank"><img alt="pnet-logo" data-src="<?php echo $home_url;?>/img/logo/logo-pnet.jpg" class="mb-md-0 mb-3 lazy" style="" > </a>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-24 text-center"  >
                        <h2 class="title mb-1" style="padding-top: 60px; posXition: relative;font-weight: 300;"><span class="aqua">CODE<strong>JIKA</strong></span> PARTNERS:</h2>
                     </div>
                     <div class="col-md-24 text-Xmd-left text-center logos"  >
                        <a href="https://codeforchange.co.za/" target="_blank"><img alt="C4C-logo" data-src="<?php echo $home_url;?>/img/logo/logo-C4C.jpg" class="mb-md-0 mb-3 lazy" style="" >  </a>
                        <a href="https://www.education.gov.za/" target="_blank"><img alt="Gov-Edu-logo" data-src="<?php echo $home_url;?>/img/logo/logo-Basic-Education.jpg" class="mb-md-0 mb-3 lazy" style="" > </a>
                        <a href="https://www.code.org/" target="_blank"><img alt="-logo" data-src="<?php echo $home_url;?>/img/logo/logo-code-org.jpg" class="mb-md-0 mb-3 lazy" style="" > </a>
                        <a href="https://africacodeweek.org/" target="_blank"><img alt="africacodeweek-logo" data-src="<?php echo $home_url;?>/img/logo/logo-africacodeweek.jpg" class="mb-md-0 mb-3 lazy" style="" > </a>
                        <a href="https://www.hourofcode.com/" target="_blank"><img alt="HoC-logo" data-src="<?php echo $home_url;?>/img/logo/logo-hourofcode.jpg" class="mb-md-0 mb-3 lazy" style="" >  </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer class="footer" style="" data-background-color="black">
            <div class="container">
               <div class="row">
                  <div class="col-md-3  col-24 powered-by">
                     <div class="row" class="c4c-logo pb-2">
                        <div class="col-md-24 col-9 offset-md-0 offset-4" style="">
                           <h4 class="pb-1">POWERED BY:</h4>
                           <a href="https://code4change.co.za/" target="_blank"><img alt="C4C-logo" class="lazy" data-src="<?php echo $home_url;?>/img/C4C-logo-white.png" style="max-width:80px; padding-bottom:10px" >  </a>
                        </div>
                        <div class="col-md-24 col-8 oXffset-md-2" style="">
                           <ul>
                              <li><a href="https://code4change.co.za/" target="_blank">Learn More</a></li>
                              <li><a href="https://code4change.co.za/about" target="_blank">Team</a></li>
                              <li><a href="https://code4change.co.za/about/sponsors" target="_blank">Sponsors</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-14" style="alXign-self: flex-end;">
                     <div class="row">
                        <div class="col-md-6 col-12" style="alXign-self: flex-end;texXt-align: right;">
                           <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                              <h4>CodeJika:</h4>
                              <ul>
                              <li class="hide">Sign Up</li>
                              <li><a href="<?php echo $home_url;?>learn/5-minute-website" target="_blank">Start Coding</a></li>
                              <li><a href="<?php echo pll_home_url();?>5-minute-website">5 Minute Website</a></li>
                              <li><a href="<?php echo pll_home_url();?>schools-closed-whats-next">Schools Closed - What’s next</a></li>

                              <ul>
                           </div>
                        </div>
                        <div class="col-md-6 col-12" style="alXign-self: flex-end;text-align: center;">
                           <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;text-align: left;">
                              <h4>Education:</h4>
                              <ul>
                              <li><a href="<?php echo pll_home_url();?>news">News and Articles</a></li>
                              <li><a href="<?php echo pll_home_url();?>coding-resources">CodeJIKA Resources</a></li>
                              <li><a href="https://hourofcode.co.za" target="_blank">Hour of Code</a></li>
                              <ul>
                           </div>
                        </div>
                        <div class="col-md-6 col-12" style="alXign-self: flex-end;">
                           <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                              <h4>Partners:</h4>
                              <ul>
                              <li><a href="<?php echo pll_home_url();?>business">Corporates</a></li>
                              <li><a href="<?php echo pll_home_url();?>schools">Schools</a></li>
                              <li><a href="<?php echo pll_home_url();?>schools">Districts</a></li>
                              <li><a href="<?php echo pll_home_url();?>media-corner">Media Corner</a></li>
                              <li>Volunteers</li>
                              <li>Launch in your Country</li>
                              <ul>
                           </div>
                        </div>
                        <div class="col-md-6 col-12" style="alXign-self: flex-end;">
                           <div class="footer-menu" style="display:inline-block; padding: 20px 0 0;">
                              <h4>Country/Region:</h4>
                              <ul class="wpXm-language-switcher switXcher-list">
                                 <li class="item-language-en <?php if ($lang_path == "" || $lang_path == "/" || $lang_path == "/index.php") { echo 'hide';} ?>" >
                                    <a href="<?php echo $home_url;?>/"  data-lang="en">
                                       <img  class="hide" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMraHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjJGNDdGODZDODlDQTExRThBNEYxRERFNjc0OEQyNjk5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjJGNDdGODZEODlDQTExRThBNEYxRERFNjc0OEQyNjk5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MkY0N0Y4NkE4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MkY0N0Y4NkI4OUNBMTFFOEE0RjFEREU2NzQ4RDI2OTkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AAAUHAAAFoAAABfUAAAZf/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wgARCAALABADAREAAhEBAxEB/8QAngABAQEAAAAAAAAAAAAAAAAABgUIAQEBAQAAAAAAAAAAAAAAAAABBAUQAAICAgMBAAAAAAAAAAAAAAABBAYCFAMTFQcRAAECBAUCBwAAAAAAAAAAAAQCAwEREgUAIhMUFTEWIVNzJCU1BhIBAAAAAAAAAAAAAAAAAAAAIBMAAgEDAwQDAAAAAAAAAAAAAREAITFBYXGB8FGhwRAgkf/aAAwDAQACEQMRAAAB3BXlJ0PDYT//2gAIAQEAAQUCvU2fhJpEiY+K+6m5876PG//aAAgBAgABBQJiGI//2gAIAQMAAQUCQxGR/9oACAECAgY/Ah//2gAIAQMCBj8CH//aAAgBAQEGPwJhhi5G28cYeBq0BkBiQOcixd3EskvuXG3H6SVW5EIJZWmEa41TywwcETcC7ogVysco50J9+hw65j6O4DLMi80hASVQi6uLufrGEsfm952/pbx6W/5vn66UU9v8D7z1p5JSngnbdu6HJlUdv8l0yfb8v8hy/manjKWP/9oACAEBAwE/IbhvebTdnCMKCHdmcZFvPVE386vCEVT/AM7vZNb9bU//2gAIAQIDAT8h+gf/2gAIAQMDAT8hEfHO8uzP/9oADAMBAAIRAxEAABBjH//aAAgBAQMBPxATZ6keXCUYEamC321NGkROi90y5wuP8IU9nHnJ3Ysv/9oACAECAwE/ECLuhGqCXEYa8vhRFRXx7c//2gAIAQMDAT8QMDIBJptbQjOYAYIAD7PTvHSVLRcum0a+pZXhUU//2Q==" alt="International">
                                       <span>Africa</span>
                                       </span>
                                 </li>
                                 <li class="item-language-za <?php if ($lang_path == "/southafrica") { echo 'hide';} ?>" >
                                 <a href="<?php echo $home_url;?>/southafrica/"  data-lang="southafrica">
                                 <img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIUSURBVHjaYmBoFWZo5ffeX37z4unP6up/GRgg6DcDw08Ghu8MDF8ZGD4zMHxkYHjPwPCWgQEggBgbDzP8Yp3C8O+PuJB0kJit6N4jLEePMfz9/f/PH4Y/f/7/BjHg5JdNmwACiMXoBoNZ+OfaM18ePnx56sXlcodE1W9fWI6d/v/gHkjdr9//f//6/+sXkM0oK/uPgQEgAAAxAM7/AQAAAMnBfiEn6oPXCuf4BP3993MxaCQGDxLe5v/19f/+/v/+/f/9/v/+/gEJCvGrqwIIpKGsrExH44WDLcPEB5xP/7C++/Xz738GDmaOv//+/P4LQSA3yfCIb5g0ESCAWIAa/vz5u3Hr19fvmcv8vnfe53j9j+vHn2+fP7/49ff3r7+/gKp//fsN1Mb+9yfDCwaAAAJp+Pv3j5sTk7P9v9kP2B78ZP3x5+uf//+4uIXZ/v4Dmf33zx+ghn9/eLhEGHgYAAIIpMHfnVFDl7HjJvelzyy/fn2dbFPPzcT95i73ty9///4F++If0Bf/eLhZZNTSAAKIZX4zg7oZS+5Jvjdf/zCw/i42Sdi9nHXz2vcvXj8DGgsOpH9AK4BIRYXz4sVdAAHE8s+LoeYiNxcTs4W8aJiU/455nGfOfeHmY5Dn4gS54w8wAv4B7fn7F0gCXfMPIIAYGTKBvmYQt7CuE5iQHfyKAegvhn9g9AvG+ANGDGCSDSDAAOBCLl8bj6ZDAAAAAElFTkSuQmCC" alt="South Africa">
                                 <span>South Africa</span>
                                 </span>
                                 </li>
                                 <li class="item-language-zambia <?php if ($lang_path == '/zambia') { echo 'hide';} ?>">
                                 <a href="<?php echo $home_url;?>/zambia/"  data-lang="zambia">
                                 <img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAGGSURBVHjaYmRuZGaAgb///jL8Y2D4A0ZAxi8w4xcKAyCAWIDqJnlOApL/////+//vv///gACoE8gGkhIfPvp8+fD7369ff/9w//qj2bcSIIBYIGa/+voKpPrfvz///vz9++fP/78g6t/vqyz/D3H96Lz3iPXPrwlq+u+lGAACiAVo9f9//8FGAg3+8wekGkT+/fv397/frL/+/P77J1dSFCgiwQhyEkAAsYCUMoAdADLyDxT9/f3nH1DT797+a1wffzj9+vXm3++F6er1RxkAAghkA9ApCNV//wANBtnzD2g20PG///z6BUL/fgPtBwYEQACxAL0PZIEUQdwDVgfX9u/3L6CHfwMRw2+gv4AaAAIIrAHkRahLfsOUgrT9+/P/1y8g+g0EQHVABQwMAAEE0gBSjaQOohRCAm0AafgD1PCXAaSeASCAWIAeB0qIcIvALPkD9zowPFjkPjMJ/pT6/1cAEkUMDAABxMiQjBqj8HgFixw9yyDEwKAJjqvzDAx8DAwAAQYAza93S9217P4AAAAASUVORK5CYII=" alt="Zambia">
                                 <span>Zambia</span>
                                 </a>
                                 </li>
                                 <li class="item-language-namibia <?php if ($lang_path == '/namibia') { echo 'hide';} ?>">
                                    <a href="<?php echo $home_url;?>/namibia/" data-lang="namibia">
                                    <img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIZSURBVHjaYmRgmMjA8IOBgQWMgIxfDAz/GBj+gBGQLTJxqo+b2i8pV+N/YAmAAAAxAM7/AQAAh359Qw4L9wgH+OnsDPf5Avz8/v39/vz8/TI2GE9LHAnm3waRjP/y9P/7+/SysgIIaCobG+vvBK9VHOzfO+a4PHoo9vvPb05O5ugETTN1Dr6+hv/7D//79ev/7z+MtXXnKuMBAgiogYGR4R8jw29mxt9AEqhaRU04P1eD9/1Tjvz0/8/f/P/1+7+00uf6wjvijO6V1gABBNTw69MX1qYZHlxsX+8/FwoKV/e0F+RbOuP/+k3/f//6DzQ7LOJFjO+8l+sOn9zD8IQBIICAGv78/vPvyUteERGJzm4dwd/vOcrS/997CnTxf26hr13Ft5V5Ks7l3n55Q1FAERgQAAEEsuHv3782TtJxIdI829YyLl74/+cvkDOcXF9nRG75drJ/X+eXX19+A1377zcw2AACCKiBq7BIX4j5O2d71f/zV0AGM3N+rS1+Yqzccr3z1IPDvxh+AZX++fcHGHJAGwACiOXu9QTJBxcYu5r/f/kFUq1j/Koo/iTr4/aDSW9+vPkFNPjvrz8gDX+BCKgBIIBYpMwUmEKi/wtJ/Bdg+hXu/8pCb/Wzjfuu7RTgEOBm4/7z98+f/3+Abv77Hxg3f4FOAgggxq/g+AOiWysnb1P4Vj+nHBgUIP4vpOj+A4t9HgaAAAMAQO8VmfiJ/b4AAAAASUVORK5CYII=" alt="Namibia">
                                    <span>Namibia</span>
                                    </a>
                                 </li>
                                 <li class="item-language-botswana <?php if ($lang_path == '/botswana') { echo 'hide';} ?>">
                                    <a href="<?php echo $home_url;?>/botswana/"  data-lang="botswana">
                                    <img  class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Botswana">
                                    <span>Botswana</span>
                                    </a>
                                 </li>
                                 <li class="item-language-mozambique <?php if ($lang_path == '/mozambique') { echo 'hide';} ?>">
                                    <a href="<?php echo $home_url;?>/mozambique/"  data-lang="mozambique">
                                    <img class="hide" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFNSURBVHjaYlQ9+f/DHwYI+PPv369/QJIBSP7/94/hH5jzB8yAsBkYAAIwLMZWAEBAFLvDJIZVmsswShVO/C7vJSlzW6uGmRrwkNKJer/kIAXcx3kI+lhfALEwAZX+Z3j1G8gHm/IfxACq+ANWByR/g1UD2eLsjEAJgABi/PjxIy8vL8RJQCsgJFbAyMh4//59gABiBKooKyt79OjRHyD4DYIQ8OsXmIBRQFJeXv7gwYMAAcQC8uufv0A+XANEGkU5mAbKAxUDBBBIw9+/IMW/fv+CmQ9SgGw23AigYoAAYnz79i0/Pz9+10NkgX64desWQACxCJ/iaVFnePIDGBQMyGECIUEMSHD9Y5DjZFi9RwgggFhAsfWfSYwNEpQM4DBl+P2PEcIARstfkCAwfv7/A0ctQAAxMmz/yvADKVL/MECj9hdaHIOl2BgAAgwAZ3V9YV7lZU4AAAAASUVORK5CYII=" alt="Mozambique">
                                    <span>Mozambique</span>
                                    </a>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-7  col-24 offsXet-md-1">
                     <p style="padding-top:20px; font-siXze: 12px">CodeJIKA.com is a vocational coding program for high-schools.</p>
                     <p style="font-sXize: 12px">Our mission is to create awesome experiences, cool online tools and fun curriculum to support the youth of today and leaders of tomorrow.</p>
                     <p style="font-sXize: 12px">This includes coding clubs, challenges and "Coding League". </p>
                     <p style="font-sXize: 12px">CodeJIKA.com is a social impact project driven by Code for Change, supporters & partners.</p>
                     <p style="font-sXize: 12px">
                        <a href="https://www.facebook.com/codejika/" class="mr-2" target="_blank"><i class="icon-facebook-official fs2"></i></a>
                        <a href="https://twitter.com/codejika" class="mr-2" target="_blank"><i class="icon-twitter-square fs2"></i></a>
                        <a href="https://www.instagram.com/codejika/" class="mr-2" target="_blank"><i class="icon-instagram fs2"></i></a>
                        <a href="http://www.linkedin.com/company/codejika/" class="mr-2" target="_blank"><i class="icon-linkedin fs2"></i></a>
                        <a href="https://www.youtube.com/channel/UCkmsiNfz3SnmNCUJv4P5YUA" class="" target="_blank"><i class="icon-youtube-square fs2"></i></a>
                     </p>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row">
                  <div class="col-md-24 pl-1 pr-1  pl-sm-2 pr-sm-2" style="background-color: #292929;">
                     <div class="copyright ">
                        <a id="switch-device" class="d-none switXch-mode mr-2  mr-sm-4" href="<?php echo pll_home_url();?>">View as Mobile</a> &copy;
                        <script>
                           document.write(new Date().getFullYear())
                        </script>
                        <a href="https://code4change.co.za/" target="_blank">Code for Change</a> | CodeJIKA
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <div class="modal fullscreen" id="menuModal">
        <button id="modal-close" type="button" class="close" data-dismiss="modal">&times;</button>
         <div class="modal-dialog" style="height: unset; max-height: unset;">
               
            <div class="modal-content">

               <!-- Modal body -->
               <div class="cmain-box-wrap">
                  <?php echo $dp; ?>
                  <?php echo $curriculum; ?>
               </div>
            </div>
         </div>
      </div>
      <!--script type="text/javascript" defer src="<?php echo $home_url;?>/js/js-compress.php"></script-->
      <script type="text/javascript" async src="<?php echo $home_url;?>/js/merged-homepage2.js"></script>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"OEZCv1zDGU20kU", domain:"codejika.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=OEZCv1zDGU20kU" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> 
   </body>
</html>

