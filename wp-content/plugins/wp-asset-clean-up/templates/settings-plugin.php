<?php
/*
 * No direct access to this file
 */
if (! isset($data)) {
    exit;
}
?>
<div class="wrap">
    <h1><?php echo apply_filters('wpacu_plugin_page_title', __('WP Asset CleanUp Lite', WPACU_PLUGIN_NAME)); ?></h1>

    <form method="post" action="">
        <input type="hidden" name="wpacu_settings_page" value="1" />
        <h2><?php _e('Plugin Usage Settings', WPACU_PLUGIN_NAME); ?></h2>

        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="wpacu_dashboard">Manage in the Dashboard?</label>
                </th>
                <td>
                    <label class="wpacu_switch">
                    <input id="wpacu_dashboard" type="checkbox"
                        <?php echo (($data['dashboard_show'] == 1) ? 'checked="checked"' : ''); ?>
                           name="<?php echo WPACU_PLUGIN_NAME.'_settings'; ?>[dashboard_show]"
                           value="1" /> <span class="wpacu_slider wpacu_round"></span> </label>
                    &nbsp;
                    <label for="wpacu_dashboard"><small>This will show the list of assets in a meta box on edit the post (any type) / page within the Dashboard</small></label>
                    <p><small>The assets would be retrieved via AJAX call(s) that will fetch the post/page URL and extract all the styles &amp; scripts that are enqueued.</small></p>
                    <p><small>Note that sometimes the assets list is not loading within the Dashboard. That could be because "mod_security" Apache module is enabled or some securiy plugins are blocking the AJAX request. If this option doesn't work, consider managing the list in the front-end view.</small></p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <label for="wpacu_dom_get_type">Assets Retrieval Mode (if managed in the Dashboard)</label>
                </th>
                <td><select id="wpacu_dom_get_type" name="<?php echo WPACU_PLUGIN_NAME.'_settings'; ?>[dom_get_type]">
                        <option <?php if ($data['dom_get_type'] === 'direct') { ?>selected="selected"<?php } ?> value="direct">Direct</option>
                        <option <?php if ($data['dom_get_type'] === 'wp_remote_post') { ?>selected="selected"<?php } ?> value="wp_remote_post">WP Remote Post</option>
                    </select>
                    <ul>
                        <li style="margin-bottom: 20px;"><strong>Direct</strong> - <small>This one makes an AJAX call directly on the URL for which the assets are retrieved, then an extra WordPress AJAX call to process the list. Sometimes, due to some external factors (e.g. mod_security module from Apache, security plugin or the fact that non-http is forced for the front-end view and the AJAX request will be blocked), this might not work and another choice method might work better. This used to be the only option available, prior to version 1.2.4.4 and is set as default.</small></li>
                        <li><strong>WP Remote Post</strong> - <small>It makes a WordPress AJAX call and gets the HTML source code through wp_remote_post(). This one is less likely to be blocked as it is made on the same protocol (no HTTP request from HTTPS). However, in some cases (e.g. a different load balancer configuration), this might not work when the call to fetch a domain's URL (your website) is actually made from the same domain.</small></li>
                    </ul>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <label for="wpacu_frontend">Manage in the Front-end?</label>
                </th>
                <td>
                    <label class="wpacu_switch">
                        <input id="wpacu_frontend"
                               type="checkbox"
                               <?php echo (($data['frontend_show'] == 1) ? 'checked="checked"' : ''); ?>
                               name="<?php echo WPACU_PLUGIN_NAME.'_settings'; ?>[frontend_show]"
                              value="1" /> <span class="wpacu_slider wpacu_round"></span> </label>
                    &nbsp;
                    <label for="wpacu_frontend"><small>If you are logged in, this will make the list of assets show below the page that you view (either home page, a post or a page).</small></label>
                    <p><small>The area will be shown through the <code>wp_footer</code> action so in case you do not see the asset list at the bottom of the page, make sure the theme is using <a href="https://codex.wordpress.org/Function_Reference/wp_footer"><code>wp_footer()</code></a> function before the <code>&lt;/body&gt;</code> tag. Any theme that follows the standards should have it. If not, you will have to add it to make sure other plugins and code from functions.php will work fine.</small></p>
                </td>
            </tr>
        </table>

        <h2><?php _e('Site-Wide Unload For Common WordPress Core CSS &amp; JS Files', WPACU_PLUGIN_NAME); ?></h2>

        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="wpacu_disable_emojis">Disable Emojis Site-Wide?</label>
                </th>
                <td>
                    <label class="wpacu_switch">
                        <input id="wpacu_disable_emojis" type="checkbox"
                        <?php echo (($data['disable_emojis'] == 1) ? 'checked="checked"' : ''); ?>
                           name="<?php echo WPACU_PLUGIN_NAME.'_settings'; ?>[disable_emojis]"
                           value="1" /> <span class="wpacu_slider wpacu_round"></span> </label>
                    &nbsp;
                    <label for="wpacu_disable_emojis"><small>This will unload WordPress' Emojis (the smiley icons)</small></label>
                    <p><small>As of WordPress 4.2, a new feature was introduced that allows you to use the new Emojis. While on some WordPress setups is useful, in many situations (especially when you are not using WordPress as a blog), you just don’t need them and the file /wp-includes/js/wp-emoji-release.min.js is loaded along with extra inline JavaScript code which add up to the number of loaded HTTP requests.</small></p>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    <label for="wpacu_disable_jquery_migrate">Disable jQuery Migrate Site-Wide?</label>
                </th>
                <td>
                    <label class="wpacu_switch">
                        <input id="wpacu_disable_jquery_migrate" type="checkbox"
					        <?php echo (($data['disable_jquery_migrate'] == 1) ? 'checked="checked"' : ''); ?>
                               name="<?php echo WPACU_PLUGIN_NAME.'_global_unloads'; ?>[disable_jquery_migrate]"
                               value="1" /> <span class="wpacu_slider wpacu_round"></span> </label>
                    &nbsp;
                    <label for="wpacu_disable_jquery_migrate"><small>This will unload jQuery Migrate (<em>jquery-migrate(.min).js</em>)</small></label>
                    <p><small>This is a JavaScript library that allows older jQuery code (up to version jQuery 1.9) to run on the latest version of jQuery avoiding incompatibility problems. Unless your website is using an old theme or has a jQuery plugin that was written a long time ago, this file is likely not needed to load. Consider disabling it to improve page loading time. Make sure to properly test the website.</small></p>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    <label for="wpacu_disable_comment_reply">Disable Comment Reply Site-Wide?</label>
                </th>
                <td>
                    <label class="wpacu_switch">
                        <input id="wpacu_disable_comment_reply" type="checkbox"
					        <?php echo (($data['disable_comment_reply'] == 1) ? 'checked="checked"' : ''); ?>
                               name="<?php echo WPACU_PLUGIN_NAME.'_global_unloads'; ?>[disable_comment_reply]"
                               value="1" /> <span class="wpacu_slider wpacu_round"></span> </label>
                    &nbsp;
                    <label for="wpacu_disable_comment_reply"><small>This will unload Comment Reply (<em>/wp-includes/js/comment-reply(.min).js</em>)</small></label>
                    <p><small>This is safe to unload if you're not using WordPress as a blog, do not want visitors to leave comments or you've replaced the default WordPress comments with a comment platform such as Disqus or Facebook.</small></p>
                </td>
            </tr>
        </table>

        <?php submit_button(); ?>
    </form>
</div>