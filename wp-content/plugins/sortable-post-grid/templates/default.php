<?php
$output .= '<div class="spg-thumb">';

if ( ! $img_overlay_hide ) {
	$output .= '<span class="spg-clip"><img src="' . $thumbnail . '"' . $image_alt_text . ' />';
	$output .= '<span class="spg-overlay" style="' . $overlay . '">';

	if ( '' !== $post_quickview ) {
		$n       = $post_count--;
		$output .= '<a href="#" class="spg-clip-overview post-id-' . $post->ID . $single_column_quick_view . '" data-post-id="' . $id . '" data-title="' . $post_quickview_title . '">
		<span class="spg-overlay-quick-view"></span></a>';
	} else {
		$overlay_link_img .= ' left:0;';
	}

	$output .= '<a href="' . get_permalink( $id ) . '" class="spg-clip-link">
	  <span class="spg-overlay-link" style="' . $overlay_link_img . '"></span></a>';
	$output .= '</span>';
	$output .= '</span>';
} else {
	$output .= '<span class="spg-clip"><img src="' . $thumbnail . '"' . $image_alt_text . ' /></span>';
}

$output .= '</div>';
$output .= '<div class="spg-data" style="font-size:' . $font_size . 'px;">';
$output .= '<span class="spg-entry-title"><h3 style="font-size:' . $font_size . 'px;">';

if ( 'yes' == $post_title_link ) {
	$output .= '<a href="' . get_permalink( $id ) . '" class="spg-clip-link">' . get_the_title( $id ) . '</a>';
} else {
	$output .= get_the_title( $id );
}

$output .= '</h3></span>';

if ( 'hide' !== $hide_date ) {
	$output .= '<span class="spg-post-date">' . get_the_date() . '</span>';
}

if ( 'display' == $tax_meta ) {
	$output .= '<span class="spg-post-meta">' . spg_get_post_meta( $id ) . '</span>';
}

$output .= '<p class="spg-entry-summary" style="' . $excerpt_style . '">' . $excerpt . '</p></div>';
