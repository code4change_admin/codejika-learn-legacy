<?php
//Header('Vary: User-Agent, Accept');
$home_url = 'https://' . $_SERVER['SERVER_NAME'];
?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <script type="text/javascript">
    var start = Date.now();
  </script>
  
  <title>Code JIKA - Learn HTML, CSS and Javascript</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <meta id="description" name="description" content="CodeJIKA Learn">
  <meta id="keywords" name="keywords" content="coding, learn to code, codejika, africa, south africa">
  <link href='../libraries/bootstrap/css/bootstrap.min.css' rel='stylesheet' />
  <link rel="stylesheet" href="../libraries/swiper/css/swiper.min.css"/>
  <link rel="stylesheet" href="../css/jquery.modal.min.css" />  
  <link href="../libraries/fontawesome/css/all.css" rel="stylesheet">

  <!--link href="<?php echo $home_url; ?>/css/merged.min.css" rel="stylesheet" type="text/css" /-->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" /-->
  <!--script src="../js/merged_single_cdns.js"></script-->

  <link href="../libraries/fontawesome/css/all.css" rel="stylesheet">

  <!--script defer src="../libraries/fontawesome/js/all.js"></script-->

  <script type="text/javascript">
    //<![CDATA[
    function randTxt() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
    }

    var randText = randTxt();

    document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url; ?>/css/learn.css?" + randText + "'\/>");
    document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url; ?>/css/mobile-projects.css?" + randText + "'\/>");
  </script>
  <style>

  </style>
</head>

<body class="is-projects">
  <div class="swiper-container swiper-container-m">
    <div class="swiper-wrapper justifyX-content-end">

      <div class="swiper-slide" id="content_page">
        <div class="menu-button toggle-menu">
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
        </div>        
        <div id="header" class="embed-nav" style="">
          <a href="<?php echo $home_url; ?>" class="p-0 m-0"><img src="<?php echo $home_url; ?>/img/jika_logo.png" alt="logo" id="logo" style="" /></a>

          <ul id="" class="lesson-navbar" style="">
            <li id="menu-lesson" class="active"><a><span>PROJECTS</span><i class="icon-local_library"></i></a></li>
            <li id="menu-certifcates"><a><span>MY CERTFICATES</span><i class="fas fa-award"></i></a></li>
            <!-- <li id="menu-skills" class="hide"><a><span>SKILLS</span><i class="icon-school"></i></a></li> -->
            <!-- <li id="menu-gallery"><a><span>GALLERY</span><i class="icon-images new-icon" style="font-family: 'icomoon' !important;"></i></a></li> -->
          </ul>
          <ul id="menu-navbar" class="hide" style="background-color: #009688;padding:0;position: absolute; right: 0; top: 0; border: 0;">
            <li id="menu-profile"><a><i class="icon-dehaze"></i></a></li>
          </ul>
        </div>        
        <div class="swiper-container swiper-container-h">
          <div class="fade-background"></div>
          <div class="swiper-wrapper">
            <div class="swiper-slide" id="project_page">
              <div class="swiXper-container swiXper-container-v list_projects">
                <div class="swiXper-wrapper">
                  <div class="swiXper-slide pl-3 pr-3" id="slide1">
                    <div class="container 5MW-intro mx-auto">
                      <div class="row">
                        <div class="col-3 pb-3 p-0 pl-sm-3 pr-sm-3">
                          <img src="<?php echo $home_url; ?>/img/projects/5MW_preview.jpg" class="w-100 mx-auto" style="max-width: 150px;">
                        </div>
                        <div class="col-9 pl-2">
                          <h3 class="mb-2">5-Minute-Website</h3>
                          <p class="">Waiting for the bus? Code your first website in HTML. Don't believe me?</p>
                          <p class="mb-2 more-intro d-none"><strong>Level:</strong> Intro</p>
                          <p class="mb-2 more-intro d-none"><strong>Fun:</strong> Coding is SO easy!</p>
                          <p class="mb-2 more-intro d-none"><strong>Time:</strong> 20-30 mins</p>
                          <p class="mb-0 more-intro d-none"><strong>Project Outline:</strong></p>
                          <ul class="mb-3 pl-4 more-intro d-none">
                            <li>1 x Trainings</li>
                            <li>Certificate Awarded</li>
                          </ul>
                          <a class="btn btn-primary bg-green mb-2 mb-Xsm-0 project-start light " style=""  href="/learn/5-minute-website" style="">Start Project <i class="fas fa-play ml-1"></i><br></a>
                          <a class="btn btn-primary bg-pink mb-2 mt-sXm-2 certificate-goto light d-none "  style="">Certificate <i class="fas fa-award ml-1"></i></a>
                          <p class="project-status 5MW-completed mt-0 mb-0 pt-0">Training completed: 00 of 01</p>
                        </div>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-more" style="" data-project-id="5MW" style=""><i class="fas fa-chevron-circle-down fa-3x ml-1 silver "></i></a>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-less d-none" style="" data-project-id="5MW" style=""><i class="fas fa-chevron-circle-up fa-3x ml-1 silver"></i></a>
                      </div>

                    </div>
                  </div>                  

                  <div class="swiXper-slide pl-3 pr-3" id="slide2">
                    <div class="container P1T-intro" mx-auto">
                      <div class="row">
                        <div class="col-3 pb-3 p-0 pl-sm-3 pr-sm-3">
                          <img src="<?php echo $home_url; ?>/img/projects/P001_preview.jpg" class="w-100 mx-auto" style="max-width: 150px;">
                        </div>
                        <div class="col-9 pl-2">
                          <h3 class="mb-2">Project 1: My Landing Page</h3>
                          <p class="">Introduces you to building a website. The outcome is a simple, colorful landing page.</p>
                          <p class="mb-2 more-intro d-none"><strong>Level:</strong> Beginner</p>
                          <p class="mb-2 more-intro d-none"><strong>Fun:</strong> Freaking Amazing</p>
                          <p class="mb-2 more-intro d-none"><strong>Time:</strong>  1.5 - 3.0 hrs</p>
                          <p class="mb-0 more-intro d-none"><strong>Project Outline:</strong></p>
                          <ul class="mb-3 pl-4 more-intro d-none">
                            <li>4 x Trainings</li>
                          </ul>
                          <a class="btn btn-primary bg-green mb-2 project-view light " style="" data-project-id="project-1" style="">Start Project <i class="fas fa-play ml-1"></i></a>
                          <p class="project-status P1T-completed mb-0 pt-0">Course Progress: 00 of 04</p>
                        </div>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-more" style="" data-project-id="P1T" style=""><i class="fas fa-chevron-circle-down fa-3x ml-1 silver "></i></a>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-less d-none" style="" data-project-id="P1T" style=""><i class="fas fa-chevron-circle-up fa-3x ml-1 silver"></i></a>
                      </div>
                    </div>
                  </div>

                  <div class="swiXper-slide pl-3 pr-3" id="slide3">
                    <div class="container P2T-intro" mx-auto">
                      <div class="row">
                        <div class="col-3 pb-3 p-0 pl-sm-3 pr-sm-3">
                          <img src="<?php echo $home_url; ?>/img/projects/P002_preview.jpg" class="w-100 mx-auto" style="max-width: 150px;">
                        </div>
                        <div class="col-9 pl-2">
                          <h3 class="mb-2">Project 2: My Web CV</h3>
                          <p class="lesson1t1">Present your skills and plans in an attractive and engaging Website CV built with HTML, CSS and emojies.</p>
                          <p class="mb-2 more-intro d-none"><strong>Level:</strong> Beginner</p>
                          <p class="mb-2 more-intro d-none"><strong>Fun:</strong> Let's learn more</p>
                          <p class="mb-2 more-intro d-none"><strong>Time:</strong>  3.5 - 5.0 hrs</p>
                          <p class="mb-0 more-intro d-none"><strong>Project Outline:</strong></p>
                          <ul class="mb-3 pl-4 more-intro d-none">
                            <li>4 x Trainings</li>
                          </ul>
                          <a class="btn btn-primary bg-green mb-2 project-view light " style="" data-project-id="project-2" style="">Start Project <i class="fas fa-play ml-1"></i></a>
                          <p class="project-status P2T-completed mb-0 pt-0">Course Progress: 00 of 06</p>
                        </div>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-more" style="" data-project-id="P2T" style=""><i class="fas fa-chevron-circle-down fa-3x ml-1 silver "></i></a>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-less d-none" style="" data-project-id="P2T" style=""><i class="fas fa-chevron-circle-up fa-3x ml-1 silver"></i></a>
                      </div>
                    </div>
                  </div>

                  <div class="swiXper-slide pl-3 pr-3" id="slide3">
                    <div class="container P3T-intro" mx-auto">
                      <div class="row">
                        <div class="col-3 pb-3 p-0 pl-sm-3 pr-sm-3">
                          <img src="<?php echo $home_url; ?>/img/projects/P003_preview.jpg" class="w-100 mx-auto" style="max-width: 150px;">
                        </div>
                        <div class="col-9 pl-2">
                          <h3 class="mb-2">Project 3: Build a small business website</h3>
                          <p class="lesson1t1">You are doing great! In our final project we will learn how we can further improve our website design.</p>
                          <p class="mb-2 more-intro d-none"><strong>Level:</strong> Beginner</p>
                          <p class="mb-2 more-intro d-none"><strong>Fun:</strong> Let's get that certificate</p>
                          <p class="mb-2 more-intro d-none"><strong>Time:</strong>  3.5 - 5.0 hrs</p>
                          <p class="mb-0 more-intro d-none"><strong>Project Outline:</strong></p>
                          <ul class="mb-3 pl-4 more-intro d-none">
                            <li>4 x Trainings</li>
                            <li>Certificate Awarded for Projects 01 - 03</li>
                          </ul>
                          <a class="btn btn-primary bg-green mb-2 project-view light d-none" style="" data-project-id="project-3">Start Project <i class="fas fa-play ml-1"></i></a>
                          <a class="btn btn-primary bg-silver mb-2 light " style="cursor: initial;" >Coming Soon!</a>
                          <a class="btn btn-primary bg-pink mt-0 mt-sXm-2 certificate-goto light d-none " style="">Certificate <i class="fas fa-award ml-1"></i></a>
                          <p class="project-status P3T-completed mb-0 pt-0">Course Progress: 00 of 06</p>
                        </div>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-more" style="" data-project-id="P3T"><i class="fas fa-chevron-circle-down fa-3x ml-1 silver "></i></a>
                        <a class=" mb-2 mb-sXm-2 light see-more-less see-less d-none" style="" data-project-id="P3T"><i class="fas fa-chevron-circle-up fa-3x ml-1 silver"></i></a>
                      </div>
                    </div>
                  </div>

                  <!-- <div class="swiXper-slide project_section" id="slide3" >
                  <div class="container lesson1">
                    <h2>Project 3: Build a small business website</h2>
                    <img src="<?php echo $home_url; ?>/img/projects/project-03_300x190.jpg" style="width:100%; padding-bottom: 20px;">
                    <p>Rising star Chef Esha is opening a new restaurant and needs a website</p>
                    <a class="btn btn-primary project-view light" data-project-id="project-3" style="">Start Project 03</a>
                    <p class="project-status">Lessons completed: 00 of 04</p>
                  </div>
                  </div>             -->

                </div>
                <div class="swiper-pagination swiper-pagination-v"></div>
              </div>

              <div class="lesson-details hide pl-3 pr-3" id="project-1">
                <div class="container container-fluid pl-2 pr-2 pl-sm-3 pr-sm-3">
                  <body>
                    <h3 class="pb-3">Project 1: My Landing Page</h3>
                    <h3>Objective</h3>
                    <p style="margin-bottom: 20px;">In these first 4 intro lessons you'll learn the basics of how to code in HTML</p>
                    <h3>Training:</h3>
                    <div class="menu">
                      <div class="row unlocked P1Training1">
                        <div class="col p-0">
                          <div class="row m-0 align-items-center ">
                            <div class="col-2 num-list">
                              01.
                            </div>
                            <div class="col-8 pl-0 lesson-list">
                              <p class="mb-0">Struture &lt;body&gt; </p>
                              <p class="mb-0">// &lt;h1&gt; &lt;h3&gt; & &lt;p&gt;</p>
                            </div>
                          </div>
                          <div class="lesson-progress-bar"><div></div></div>
                        </div>
                        <div class="col-auto align-self-center p-0 pr-2">
                          <div class="icon-list bg-orange text-center">
                            <i class="fas fa-play"></i>
                          </div>
                        </div><a class="divLink" href="<?php echo $home_url; ?>/learn/P1Training1"></a>
                      </div>

                      <div class="row locked P1Training2">
                        <div class="col p-0">
                          <div class="row m-0 align-items-center ">
                            <div class="col-2 num-list">
                              02.
                            </div>
                            <div class="col-8 pl-0 lesson-list">
                              <p class="mb-0">CSS &lt;style&gt; </p>
                              <p class="mb-0">// Type Selector</p>
                            </div>
                          </div>
                          <div class="lesson-progress-bar"><div></div></div>
                        </div>
                        <div class="col-auto align-self-center p-0 pr-2">
                          <div class="icon-list bg-silver text-center">
                            <i class="fas fa-lock"></i>
                          </div>
                        </div><a class="divLink" href="<?php echo $home_url; ?>/learn/P1Training2"></a>
                      </div>
                      
                      <div class="row locked P1Training3">
                        <div class="col p-0">
                          <div class="row m-0 align-items-center ">
                            <div class="col-2 num-list">
                              03.
                            </div>
                            <div class="col-8 pl-0 lesson-list">
                              <p class="mb-0">Page Sections</p>
                              <p class="mb-0">// Header, Section & Footer</p>
                            </div>
                          </div>
                          <div class="lesson-progress-bar"><div></div></div>
                        </div>
                        <div class="col-auto align-self-center p-0 pr-2">
                          <div class="icon-list bg-silver text-center">
                            <i class="fas fa-lock"></i>
                          </div>
                        </div><a class="divLink" href="<?php echo $home_url; ?>/learn/P1Training3"></a>
                      </div>
                      
                      <div class="row locked P1Training4">
                        <div class="col p-0">
                          <div class="row m-0 align-items-center ">
                            <div class="col-2 num-list">
                              04.
                            </div>
                            <div class="col-8 pl-0 lesson-list">
                              <p class="mb-0">&lt;div&gt; </p>
                              <p class="mb-0">// Fonts, borders & padding</p>
                            </div>
                          </div>
                          <div class="lesson-progress-bar"><div></div></div>
                        </div>
                        <div class="col-auto align-self-center p-0 pr-2">
                          <div class="icon-list bg-silver text-center">
                            <i class="fas fa-lock"></i>
                          </div>
                        </div> <a class="divLink" href="<?php echo $home_url; ?>/learn/P1Training4"></a>
                      </div>

                    </div>
                  </body>
                  <!-- <h3 style="">Challenge</h3>
                    <div class="menu">
                      <ol class="lessons">
                        <li class="" data-progress-completed="0"><i class="far fa-circle fa-2x"></i> 01. Build your own personal website</li>
                      </ol>
                    </div> -->
                  <a class="btn btn-primary bg-silver project-return light mr-3" style="">Back</a>
                  <a class="btn btn-primary bg-silver project-reset light mt-0 " style="">Reset Project</a>
                </div>
              </div>
              <div class="lesson-details hide pl-3 pr-3" id="project-2">
                <div class="container container-fluid pl-3 pr-3">
                  <body>
                    <h3 class="pb-3">Project 2: My Web CV</h3>
                    <h3>Objective</h3>
                    <p style="margin-bottom: 20px;">You have learnt the basics about HTML, now let's add some navigation to your webpage and style it using CSS</p>
                    <h3>Training:</h3>
                    <div class="menu">
                      <div class="row unlocked P2Training1">
                        <div class="col-2 num-list">
                          01.
                        </div>
                        <div class="col-8 pl-0 lesson-list">
                          <p class="mb-0">Review: HTML Structure <br>Setup: Header</p>
                        </div>
                        <div class="col-2 icon-list bg-green text-center">
                          <i class="fas fa-play ml-1"></i>
                          <a class="divLink" href="<?php echo $home_url; ?>/learn/P2Training1"></a>
                        </div>
                        
                      </div>
                     <div class="row locked P2Training2">
                        <div class="col-2 num-list">
                          02.
                        </div>
                        <div class="col-8 pl-0 lesson-list">
                          <p class="mb-0">CSS Classes & customize.</p>
                        </div>
                        <div class="col-2 icon-list bg-green text-center ">
                          <div class="my-auto"><i class="fas fa-play ml-1"></i></div>
                          <a class="divLink" href="<?php echo $home_url; ?>/learn/P2Training2"></a>
                        </div>
                      </div>
                      <div class="row locked P2Training3">
                        <div class="col-2 num-list">
                          03.
                        </div>
                        <div class="col-8 pl-0 lesson-list">
                          <p class="mb-0">Box, Circle & center classes.</p>
                        </div>
                        <div class="col-2 icon-list bg-green text-center ">
                          <div class="my-auto"><i class="fas fa-play ml-1"></i>
                          <a class="divLink" href="<?php echo $home_url; ?>/learn/P2Training3"></a></div>
                        </div>
                      </div>
                      <div class="row locked P2Training4">
                        <div class="col-2 num-list">
                          04.
                        </div>
                        <div class="col-8 pl-0 lesson-list">
                           <p class="mb-0">Skills section & lists</p>
                        </div>
                        <div class="col-2 icon-list bg-green text-center ">
                          <i class="fas fa-play ml-1"></i>
                          <a class="divLink" href="<?php echo $home_url; ?>/learn/P2Training4"></a>
                        </div>        
                      </div>
                      <div class="row locked P2Training5">
                        <div class="col-2 num-list">
                          05.
                        </div>
                        <div class="col-8 pl-0 lesson-list">
                           <p class="mb-0">Skills section & lists</p>
                        </div>
                        <div class="col-2 icon-list bg-green text-center ">
                          <i class="fas fa-play ml-1"></i>
                          <a class="divLink" href="<?php echo $home_url; ?>/learn/P2Training5"></a>
                        </div>        
                      </div>
                      <div class="row locked P2Training6">
                        <div class="col-2 num-list">
                          06.
                        </div>
                        <div class="col-8 pl-0 lesson-list">
                           <p class="mb-0">Emoji's & customize</p>
                        </div>
                        <div class="col-2 icon-list bg-green text-center ">
                          <i class="fas fa-play ml-1"></i>
                          <a class="divLink" href="<?php echo $home_url; ?>/learn/P2Training6"></a>
                        </div>        
                      </div>
                    </div>
                  </body>
                  <!-- <h3 style="">Challenge</h3>
                    <div class="menu">
                      <ol class="lessons">
                        <li class="" data-progress-completed="0"><i class="far fa-circle fa-2x"></i> 01. Build your own personal website</li>
                      </ol>
                    </div> -->
                  <a class="btn btn-primary bg-silver project-return light mr-3" style="">Back</a>
                  <a class="btn btn-primary bg-silver project-reset light mt-0 " style="">Reset Project</a>
                </div>
              </div>              
              <div class="lesson-details hide" id="project-3">
                <div class="container">
                  <h2 style="margin-bottom: 14px;">Project 3: Build a small business website</h2>
                  <h3>Objective</h3>
                  <p style="margin-bottom: 14px;">You are doing great! In our final project we will learn how we can further improve our website design</p>
                  <h3>Lessons</h3>
                  <div class="menu">
                    <ol class="lessons">
                      <li class="" data-progress-completed="0"><i class="far fa-circle fa-2x"></i> 01. Position images & text</li>
                      <li class="" data-progress-completed="0"><i class="far fa-circle fa-2x"></i> 02. Add webfonts and color fades</li>
                      <li class="" data-progress-completed="0"><i class="far fa-circle fa-2x"></i> 03. Make it mobile and interactive</li>
                    </ol>
                  </div>
                  <h3 style="margin-top: 20px;">Challenge</h3>
                  <div class="menu">
                    <ol class="lessons">
                      <li class="" data-progress-completed="0"><i class="far fa-circle fa-2x"></i> 01. Build your own business website</li>
                    </ol>
                  </div>
                  <a class="btn btn-primary bg-silver project-return light mr-3" style="">Back</a>
                  <a class="btn btn-primary bg-silver project-reset light mt-0 " style="">Reset Project</a>
                </div>
              </div>
            </div>
            <div class="swiper-slide" id="certifcates_page">
              <div class="swiXper-container swiXper-container-c list_certifcates">
                <div class="swiXper-wrapper certs">

                </div>

                <div class="swiper-pagination swiper-pagination-c"></div>
              </div>
            </div>
            <div class="swiper-pagination swiper-pagination-h"></div>
          </div>
        </div>
      </div>
      <div class="swiper-slide" id="profile_page">
        <div class="container pt-0">
          <i class="icon-account_circle fs5 d-none" style="display: block;"></i>
          <h2 class="profile_name mt-4 mb-3">Hello</h2>
          <div class="menu">
            <ul>
               <h4 class="profile_name text-center white mb-1 " >Menu</h4>             
              <li class=""><a class="divLink" href="<?php echo $home_url; ?>"></a><i class="icon-home fs2"></i> Homepage</li>
              <li class="create-GalleryPage"><i class="icon-images icomoon"></i> Code Gallery</li>
              <li  class="d-none"><i class="icon-personal_video"></i> View as Desktop</li>             
               <h4 class="profile_name text-center white mb-1 mt-3" >My Profile</h4>
              <li class="login-button pointer" style="width: 45%;display: inline-block;" data-dismiss="modal"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i> Login</li>
              <li class="register-button pointer" style="width: 53%;display: inline-block;" data-dismiss="modal"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i> Register</li>
              <li class="create-profilePage profile-button d-none" ><i class="icon-account_circle fs2"></i>Edit My Profile Details</li>
              <li class=""><i class="icon-face fs2"></i> Instructor <div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
                <div class="option_unselected select_lego">Lego</div>
              </li>
              <li class="logout-button pointer d-none" data-dismiss="modal"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i> Logout</li> 
              <li id="share-link" class="hide"><i class="icon-assignment fs2"></i> Share on: <div class="option_unselected facXebook-share" data-js="facebook-share" style="margin-left: 5px;">FB</div>
                <div class="option_unselected twitXter-share" style="margin-left: 5px;" data-js="twitter-share">TW</div> <a id="whatsapp" href="">
                  <div class="option_unselected ">WA</div>
                </a>
              </li>
              <center><p id="switch-device"> View as <span class="mobile-only">Desktop</span><span class="desktop-only">Mobile</span></p></center>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </div>




  <div class="modalPlaceholder"></div>

  <div class="success-animation hide" style="">

    <span class="success-layers">
      <svg class="success-svg-icon" style="color: #fff;" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
        <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
      </svg><!-- <i class="fas fa-circle" style="color:#fff"></i> -->
      <svg class="success-svg-icon" style="color: #00A921;" aria-hidden="true" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
        <path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path>
      </svg><!-- <i class="fas fa-inverse fa-check-circle" style="color:#00A921"></i> -->
    </span>

    </span>
  </div>

<!--   Core JS Files   -->
<script src='../js/core/jquery.3.2.1.min.js' type='text/javascript'></script>
<script src='../libraries/bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
<script defer src='../libraries/bootstrap/js/vendor/popper.min.js' type='text/javascript'></script>
<script src="../libraries/firebase/firebase-app.js"></script>
<script src="../libraries/firebase/firebase-database-custom.js"></script>
<script src="../libraries/swiper/js/swiper.min.js" type="text/javascript"></script>
<script src='../js/jquery.modal.min.js' type='text/javascript'></script>
<script defer src="../libraries/fontawesome/js/all.js"></script>

<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<!--script src='../js/plugins/bootstrap-switch.js'></script-->
<script type='text/javascript' src='../js/jquery.dateFormat.min.js'></script>
<script src="../libraries/jspdf/jspdf.min.js"></script>
<script type='text/javascript' src='../fonts/Rajdhani-Medium-normal.js'></script>
<script src="../js/pdf.js"></script>
<script src="../js/js.cookie.js"></script>

<?php
function get_all_get()
{
        $output = "?"; 
        $firstRun = true; 
        foreach($_GET as $key=>$val) { 
        if($key != $parameter) { 
            if(!$firstRun) { 
                $output .= "&"; 
            } else { 
                $firstRun = false; 
            } 
            $output .= $key."=".$val;
         } 
    } 

    return $output;
} 

/*
function dev_type()
{
  if ( getenv("DEVICE_TYPE") == "desktop" ) {
    return "desktop";
  } 
  else if ( getenv("DEVICE_TYPE") == "mobile" ) {
    return "mobile";
  } 
  else {
    return "unknown";
  }     
}  
*/
  
?>

<script type="text/javascript">

  var page_template = "projects";

  if (Cookies.get('devicetype') == 'desktop'){
    var device_type = "desktop";
  } else {
    var device_type = "mobile";
  }

  var set_user_id = "<?php echo htmlspecialchars($_GET["user_id"]); ?>";
  console.log("set_user_id: " + set_user_id);
 console.log("PHP GET/query string: <?php echo get_all_get(); ?>" );

   document.write("<script type='text/javascript' src='../js/common-learn.js?" + randText + "'><\/script>");
  document.write("<script type='text/javascript' src='../js/mobile-lesson.js?" + randText + "'><\/script>");


</script>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"OEZCv1zDGU20kU", domain:"codejika.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=OEZCv1zDGU20kU" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> 

</body>

</html>
