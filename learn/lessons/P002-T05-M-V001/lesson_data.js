var lesson_data = 
{
    'defaultCode': // default code, if user has not already started coding   
`
<head>
	<style>
		
		header{
		background: lightblue;
		border: solid blue;
		border-width: 10px 0px 10px 0px;
		margin: 0 auto;
		text-align: center;
		}

		h1{
		font-size: 3em;
		}

		.circle{
		height: 250px;
		width: 250px;
		background-color: blue;
		border-radius: 50%;
		border: 10px solid white;
		display: inline-block;
		}
		
		h3{
		background-color: #ce54c7;
		padding: 15px;
		font-size: 1.5em;
		border-left: 10px solid #d6d5cd;
		}

		header, h3{
		color: white;
		font-family: "Arial Black",Gadget, sans serif;
		}

		body{
		font-family : helvetica , arial ,san serif;
		}
	</style>
</head>

<body>
	<header>
		<div class="circle">
		</div>

		<h1>My Name</h1>

		<i>
		Aspiring Software Developer
		</i>
		<br><br>
	</header>
		<br>
		<h3>DETAILS</h3>
		<p>Date of Birth: 01 February 2000</p>
		<p>School: Diepsloot Combined School</p>
		<p>Grade: 8</p>
        
        
</body>

` ,
    'kbLayout': '', // not currently in use
    'loadJS': 'https://cdnjs.cloudflare.com/ajax/libs/geopattern/1.2.3/js/geopattern.min.js"></script>', // not currently in use
    'prevLessonID': 'P2Training4', // Lesson ID of previous lesson where to load user's code
    'nextLessonSlug': '', // not currently in use
    'pageDesc': 'Learn how to build your first website with these easy intro lessons to coding.', 
    'pageKeywords': 'coding, code, learn to code, code website, my first website', 
    'pageTitle': 'CodeJIKA - Project 2, Training 5',
    'save_lesson_id': 'P2Training5', // This is id that will be used to store save code in Firebase
  'slides' : [ {
    'slide_number' : 1,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'intro',      
    'html_content' : `
      <div>    
      	<p class='slide-title h1 mt-5'>TRAINING 5</p>
      </div>      
      <div class=''>
        <p class='h1'>Let's<br>
        <strong class='fs-x2 aqua'>DO</strong><br>
        this!</p>
        <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/grinning-face-with-star-eyes.png' alt=''>
      </div>      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 2,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">OK.<p> 
	  <p class="h3 pb-4">So... You've done some amazing work so far.</p>
	  <img class="swiper-lazy w-20 swiper-lazy-loaded" alt="" src="../img/emoji/72/winking-face.png">
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 3,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">So now it's time for you to add some <span class="pink">important information</span>.</p>
	  <img class="swiper-lazy w-20 swiper-lazy-loaded" alt="" src="../img/emoji/72/thinking-face.png">
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 4,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'snapshot',    
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Snapshot</p>
  	  <p class='white fs75'>(Today its' all about you:)
  	</div>      
	<div>       
		<ol class='h4'>
		  <li>Your dreams.</li>
		  <li>Your goals.</li>
		  <li>You'll also learn HTML Lists.</li>
		</ol>
	</div>
	<div>
		<p class='slide-footer white'>How it will look:</p>
		<div class='btn btn-aqua next preview-output'>Preview</div>
	</div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  	
    'slide_number' : 5,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Mission</p>
  	</div>
    <div>
      <p class='h3 pb-4'>Add an "About Me" & "Experiences" to your CV.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 4 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 6,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (1 of 2): CV Creation</p>
    </div> 
    <div>
	  <p class="h2 pb-4">CV Creation:</p>
	  <p class='h5 pb-4'>A CV is your personal advertisement and it tells your story.</p>
	  <img class="swiper-lazy w-20 swiper-lazy-loaded" alt="" src="../img/emoji/72/newspaper.png">
	  <p class='h5 pt-4'>Most people use it to apply for jobs.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 7,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (2 of 2): CV Creation</p>
    </div> 
    <div>
	  <p class="h2 pb-4">CV Creation:</p>
	  <p class='h5 pb-4'>These are sections that are suggested:</p>
	  <ul class="h5">
		<li>DETAILS</li>
		<li>EDUCATION</li>
		<li>EXPERIENCE</li>
		<li>SKILLS</li>
		<li>CONTACT DETAILS</li>
	  </ul>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 8,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 1,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add an h3 with an "ABOUT ME" section.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         &lt;h3&gt;<br>
		 &nbsp;&nbsp;ABOUT ME<br>
         &lt;/h3&gt;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> Below the "DETAILS" section.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 9,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <p class='h3 pb-4 w-75'>PREPARE</p>
      <p class='h4 pt-3 text-left aqua'>CODE:</p>
      <ul class="h5">
		<li>Write about yourself in a &lt;p&gt;.</li>
	  </ul>
	<p class='h4 pt-3 text-left aqua'>WRITE:</p>
      <ul class="h5">
		<li>What you are excited about.</li>
		<li>What you love doing & your dream.</li>
	  </ul>
      </div>	  
    </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 10,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 2,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Write about yourself in a <span inline-code>&lt;p&gt;</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         &lt;p&gt; I love...&lt;/p&gt;<br>
         &lt;p&gt; My dream is...&lt;/p&gt;<br>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> You can add different lines by making a new <span inline-code>&lt;p&gt;</span> for each line.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 11,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">GREAT.<p> 
	  <p class="h3">Did you manage?</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 12,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3">Now we're going to add a few more sections.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  	
    'slide_number' : 13,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Mission</p>
  	</div>
    <div>
      <p class='h3 pb-4'>Add an "Education" & "Experiences" sections.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 2 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 14,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 3,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(EDUCATION)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add an h3 "EDUCATION" section. Add the text in a few <span inline-code>&lt;p&gt;</span>s.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         &lt;p&gt; 2011-2015: East-gate Primary, Boksburg&lt;/p&gt;<br>
         &lt;p&gt; 2015-Ongoing: Itirele-Zenzele Secondary&lt;/p&gt;<br>
         &lt;p&gt; Future: 2020-2024: BSC in Computer Science, Univ of PTA&lt;/p&gt;<br>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>             
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 15,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">We want to know more about you.</p>
	  <img class="swiper-lazy w-20 swiper-lazy-loaded" alt="" src="../img/emoji/72/smiling-face-with-open-mouth-and-smiling-eyes.png">
	  <p class="h4 pt-4">Special things you've lived through which make you, you.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 16,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 4,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(EXPERIENCES)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add an "EXPERIENCES" section. You can write about anything you want in the <span inline-code>&lt;p&gt;</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         &lt;p&gt; I was born in... &lt;/p&gt;<br>
         &lt;p&gt; I enjoy... &lt;/p&gt;<br>
         &lt;p&gt; I once saw... &lt;/p&gt;<br>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> You can add different lines by making a new <span inline-code>&lt;p&gt;</span> for each line.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 17,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">We want to know more about you.</p>
	  <img class="swiper-lazy w-20 swiper-lazy-loaded" alt="" src="../img/emoji/72/smiling-face-with-open-mouth-and-smiling-eyes.png">
	  <p class="h4 pt-4">Special things you've lived through which make you, you.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  	
    'slide_number' : 18,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Mission</p>
  	</div>
    <div>
      <p class='h3 pb-4'>Make a section with a list & bullet points..</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 6 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 19,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">In order to make that look "PRO" you're going to use a list with bullet points.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },{   
    'slide_number' : 20,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">AWESOME.<p> 
	  <p class="h3">Now we're going to learn about lists.</p>
	  <img class='swiper-lazy' data-src='../img/emoji/72/female-technologist-type-4.png' alt=''> 
	  <img class='swiper-lazy' data-src='../img/emoji/72/thumbs-up-sign.png' alt=''> 
	  <img class='swiper-lazy' data-src='../img/emoji/72/female-technologist-type-4.png' alt=''> 
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 21,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING (1 of 5): LISTS</p>
      </div>    
      <div>
        <p class='h2 pb-0 mb-4 blue'>LISTS</p>
		<p class=''>There are two types of lists:</p>
		<p class='h4 aqua'><span class="inline-code">&lt;ol&gt;</span> Ordered</p>
        <ol class='h5'>
		  <li>First Item</li>
		  <li>Second item</li>
		  <li>Third item</li>
		</ol>
		<p class='h4 pink'><span class="inline-code">&lt;ul&gt;</span> Unordered</p>
        <ul class='h5'>
		  <li>Item</li>
		  <li>Item</li>
		  <li>Item</li>
		</ul>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 22,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING (2 of 5): LISTS</p>
      </div>    
      <div>
        <p class='h2 pb-0 mb-4 blue'>LISTS</p>
		<p class='h4 aqua'><span class="inline-code">&lt;ol&gt;</span> Ordered List</p>
        <ol class='h5'>
		  <li>Starts with <span class="inline-code">&lt;ol&gt;</span></li>
		  <li>Each item is wrapped with <span class="inline-code">&lt;li&gt;</span> and <span class="inline-code">&lt;/li&gt;</span></li>
		  <li>Ends with <span class="inline-code">&lt;/ol&gt;</span></li>
		</ol>
		<div class='html-code-box'>
         &lt;ol&gt;<br>
		 &nbsp;&lt;li&gt;First item&lt;/li&gt;<br>
		 &lt;/ol&gt;
        </div>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 23,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING (3 of 5): LISTS</p>
      </div>    
      <div>
        <p class='h2 pb-0 mb-4 blue'>LISTS</p>
		<p class='h4 aqua'><span class="inline-code">&lt;ol&gt;</span> Ordered List</p>
		<p class="h5">LIST EXAMPLE:<P>
        <ol class='h5'>
		  <li>First Item</li>
		  <li>Second item</li>
		  <li>Third item</li>
		</ol>
		<div class='html-code-box'>
         &lt;ol&gt;<br>
		 &nbsp;&lt;li&gt;First item&lt;/li&gt;<br>
		 &nbsp;&lt;li&gt;Second item&lt;/li&gt;<br>
		 &nbsp;&lt;li&gt;Third item&lt;/li&gt;<br>
		 &lt;/ol&gt;
        </div>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  { 
    'slide_number' : 24,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING (4 of 5): LISTS</p>
      </div>    
      <div>
        <p class='h2 pb-0 mb-4 blue'>LISTS</p>
		<p class='h4 pink'><span class="inline-code">&lt;ul&gt;</span> Unordered List</p>
        <ol class='h5'>
		  <li>Starts with <span class="inline-code">&lt;ul&gt;</span></li>
		  <li>Each item is wrapped with <span class="inline-code">&lt;li&gt;</span> and <span class="inline-code">&lt;/li&gt;</span></li>
		  <li>Ends with <span class="inline-code">&lt;/ul&gt;</span></li>
		</ol>
		<div class='html-code-box'>
         &lt;ul&gt;<br>
		 &nbsp;&lt;li&gt;First item&lt;/li&gt;<br>
		 &lt;/ul&gt;
        </div>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 25,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING (5 of 5): LISTS</p>
      </div>    
      <div>
        <p class='h2 pb-0 mb-4 blue'>LISTS</p>
		<p class='h4 pink'><span class="inline-code">&lt;ol&gt;</span> unordered List</p>
		<p class="h5">LIST EXAMPLE:<P>
        <ul class='h5'>
		  <li>Item</li>
		  <li>Item</li>
		  <li>Item</li>
		</ul>
		<div class='html-code-box'>
         &lt;ul&gt;<br>
		 &nbsp;&lt;li&gt;Item&lt;/li&gt;<br>
		 &nbsp;&lt;li&gt;Item&lt;/li&gt;<br>
		 &nbsp;&lt;li&gt;Item&lt;/li&gt;<br>
		 &lt;/ul&gt;
        </div>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 26,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">Simple, right?</p>
	  <img class='swiper-lazy' data-src='../img/emoji/72/thumbs-up-sign.png' alt=''> 
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 27,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">FANTASTIC!<p> 
	  <p class="h3 pb-4">Now the fun part. Tell us about what you are good at.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 28,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 5,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(SKILLS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<ul>((.|\n)*)\s*<li>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/li>((.|\n)*)\s*<\/ul>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a "SKILLS" section.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         &lt;h3&gt;SKILLS &lt;/h3&gt;<br>
         &lt;ul&gt;<br>
         &nbsp;&lt;li&gt;I can make amazing spaghetti. &lt;/li&gt;<br>
         &nbsp;&lt;li&gt;Taught Michael Jackson to dance. &lt;/li&gt;<br>
         &lt;/ul&gt;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> You can add different lines by making a new <span inline-code>&lt;p&gt;</span> for each line.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 29,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">Did you notice that the list doesn't look amazing right?</p>
	  <img class='swiper-lazy' data-src='../img/emoji/72/speak-no-evil-monkey.png' alt=''> 
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 30,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pb-4">What do you think?</p>
	  <p class="h3 pb-4">Should we add a bit of good old CSS?</p>
	  <img class='swiper-lazy' data-src='../img/emoji/72/eyes.png' alt=''> 
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 31,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 6,
    'reg' : [ '((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*(})((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Provide some padding and increase size of <span class="inline-code">&lt;ul&gt;</span> and <span class="inline-code">&lt;p&gt;</span></li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         p, ul {<br>
		 }
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 32,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 7,
    'reg' : [ '((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Try 50px of padding on the left only.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         p, ul {<br>
		 padding-left: 50px;<br>
		 }
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 33,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 8,
    'reg' : [ '((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*font-size:((.|\n)*)\s*1.3em((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Increase the size of these selectors by 30%.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         <span class="code-fade">p, ul {<br>
		 padding-left: 50px;</span><br>
		 font-size: 1.3em;<br>
		  <span class="code-fade">}</span>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 34,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">GREAT JOB!<p> 
	  <img class='swiper-lazy' data-src='../img/emoji/72/flexed-biceps.png' alt=''>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 35,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 pink text-uppercase bold">ALMOST DONE.<p>
	   <p class='h3 pb-5 w-75'>Add a "CONTACT" section.</p>
	  <img class='swiper-lazy' data-src='../img/emoji/72/house-building.png' alt=''>
	  <img class='swiper-lazy' data-src='../img/emoji/72/mobile-phone.png' alt=''>
	  <img class='swiper-lazy' data-src='../img/emoji/72/e-mail-symbol.png' alt=''>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 36,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <p class='h3 pb-5 w-75'>Here is an example of how your address can look.</p>
	  <div class='html-code-box'>
         Address: Zone 1, Diepsloot, South Africa |<br>
		 Phone: +27 76 899 9999 | <br>
		 Email: thandi1994@gmail.com
    </div>
    </div>
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 37,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING: PRIVACY</p>
      </div>    
      <div>
        <p class='h2 pb-0 mb-4 blue'>PRIVACY</p>
		<p class='h3'>Use a <strong>fake phone number</strong> and email.</p>
		<p class='h3'>Your information is private and <strong>you may choose to share</strong> your CV design online.</p>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  { 
    'slide_number' : 38,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 9,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(CONTACT)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a CONTACT section.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         Add a paragraph with town, area and country you live in.<br><br>
		 Add a phone number and email too.
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 39,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">DONE!<p>
	  <p class="h3 pb-4">Wow. You finished all that writing.</p>
	  <p class="h3 pb-4">That was a lot.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 40,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 pb-4'>CONGRATS!</div>
      <div style='background-color: #6aa2c7;border-radius:50%;margin: 40px auto;width: 200px;height: 200px;'>
        <div class='lesson-instructions '><img style='padding-top:20px;' class='w-75 swiper-lazy mb-3' data-src='../img/lessons/congrats_training_sml.png'></div>
      </div>
      <div class='h3 pb-4'>You've finished<br> Training 5</div>  
    </div>    
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 41,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 pb-4 w-75'>Ready for Training 6?</p>
      <a class="btn-primary next"" href="../learn/P2Training6">Start Now</a>
      <a class="btn-primary-alt next check" href='../learn/projects'>Projects Page</a>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  } ]
}
    
var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 
 
/// Add custom JS for lesson below here


function previewLessonModal() {
  $.galleryDialog({
    modalName: 'preview-lesson-modal',
    htmlContent: `
    <iframe  srcdoc="
<head>
  <style>
    h1 {
      font-size: 75px;
      color: deeppink;
    }
    i {
      font-size: 25px;
    }
    header {
      background: linear-gradient(110deg, yellow 40%, pink 40%);
    }
    section {
      background: lightgrey;
    }
    footer {
      background: black;
      color: white;
    }
    div {
      text-align: center;
      padding: 40px;
    }
    h3 {
      border: 2px solid white;
      font-size: 45px;
      padding: 15px;
      margin: auto;
      max-width: 400px;
    }
  </style>
</head>

<body>
  <header>
    <div>
      <h1>My Name</h1>
      <section>
        <div>
          <h3>Motivation:
            <br><br>
            <i>I want to learn to code because I love building new things.</i>
          </h3>
        </div>
      </section>
      <h3>Launching Soon...</h3>
      <p>01 January 2000</p>
    </div>
  </header>
  <footer>
    <div>
      &copy 2021 My Name
    </div>
  </footer>
</body>

  "></iframe>
    `
  })
}



/// Add custom JS for lesson below here

function onlessonLoaded() {

  $('.submit-CodeChallenge').click(function() {
    console.log('submitCodeChallengeModal');
    submitCodeChallengeModal('submit-lesson-url');
  }) 
}

