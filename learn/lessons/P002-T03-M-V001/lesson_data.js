var lesson_data = 
{
    'defaultCode': // default code, if user has not already started coding   
`
<head>
	<style>
		header{
		background: lightblue;
		border: solid blue;
		border-width: 10px 0px 10px 0px;
		margin: 0 auto;
		text-align: center;
		}
                
		h1{
		font-size: 3em;
		}
	</style>
</head>

<body>
	<header>
		<h1>My Name</h1>       
		<i>Aspiring Software Developer</i>
		<br><br>
		
	</header>
        <br>
        <h3>DETAILS</h3>
        <p>Date of Birth: 01 February 2000</p>
        <p>School: Diepsloot Combined School</p>
        <p>Grade: 8</p>
</body>
` ,
    'kbLayout': '', // not currently in use
    'loadJS': 'https://cdnjs.cloudflare.com/ajax/libs/geopattern/1.2.3/js/geopattern.min.js"></script>', // not currently in use
    'prevLessonID': 'P2Training2', // Lesson ID of previous lesson where to load user's code
    'nextLessonSlug': '', // not currently in use
    'pageDesc': 'Learn how to build your first website with these easy intro lessons to coding.', 
    'pageKeywords': 'coding, code, learn to code, code website, my first website', 
    'pageTitle': 'CodeJIKA - Project 2, Training 3',
    'save_lesson_id': 'P1Training3', // This is id that will be used to store save code in Firebase
  'slides' : [ {
    'slide_number' : 1,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'intro',      
    'html_content' : `
      <div>    
      	<p class='slide-title h1 mt-5'>TRAINING 3</p>
      </div>      
      <div class=''>
        <p class='h1'>Let's<br>
        <strong class='fs-x2 aqua'>ROCK</strong><br>
        this!</p>
        <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-open-mouth-and-tightly-closed-eyes.png' alt=''>
      </div>      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 2,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
    </div>
    <div>
      <p class='h2 pb-4 aqua text-uppercase bold'>OMG</p>
      <p class='h4'>This is a SUPER FUN Lesson</p>
	  <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/grinning-face-with-star-eyes.png' alt=''>
	  <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/grinning-face-with-star-eyes.png' alt=''>
	  <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/grinning-face-with-star-eyes.png' alt=''>
	  <p class='h4 mt-4'>But the next one (Training 4) is even better...</p>
    </div>     
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 3,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'snapshot',    
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Snapshot</p>
  	  <p class='white fs75'>(These are your missions for today.)</p>
  	</div>      
	<div>       
		<ol class='h4'>
		  <li>Make a <strong>box</strong>.</li>
		  <li>Transform it into a  <strong>circle</strong> with one line.</li>
		  <li>Remember <strong>3 things about a class</strong>.</li>
		</ol>
	</div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {      
    'slide_number' : 4,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
    </div> 
    <div class=''>
      <p class='h2 pb-4'>What if,</p>
      <p class='h3'>all of a sudden...</p>
	  <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/high-voltage-sign.png' alt=''>
	  <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/high-voltage-sign.png' alt=''>
	  <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/high-voltage-sign.png' alt=''>
	  <p class='h3 pt-4 pb-'>You decided you wanted...</p>
	  <img class='mt-4 mb-4 w-20 swiper-lazy' data-src='../img/emoji/72/package.png' alt=''>
	  <p class='h3 pt-2 pb-4'>a mysterious box.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {      
    'slide_number' : 5,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
    </div> 
    <div class=''>
      <p class='h3'>In the middle of your website.</p>
	  <img class='mt-4 mb-4 swiper-lazy' data-src='../learn/lessons/P002-T03-M-V001/img/box-dimensions.PNG' alt=''>
	  
	  <p class='h3 pt-2 pb-4'>What would you do?!?</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 6,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'>Make a <strong>box</strong></p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 3 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 7,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 1 of 4 : Make a box</p>
    </div> 
    <div class=''>
      <p class='h2 pb-4'>MAKE A BOX</p>
      <p class='h5 pb-1 text-left'>All you have to do is write a simple CSS Class and:</p>
      <ol class='h5 pb-5'>
        <li>Define width</li>
        <li>Define height</li>
      </ol>
     <div class='html-code-box'>
       .box { <br>
		   &nbsp;&nbsp;<span class="green">width:</span> 100px;<br>
		   &nbsp;&nbsp;<span class="blue">height:</span> 100px;<br>
	   &nbsp;}
        </div>
    </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 8,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 2 of 4 : Make a box</p>
    </div> 
    <div class=''>
     <p class='h2 pb-4'>MAKE A BOX</p>
     <p class='h5 pb-1 text-center'>And a "magical" CSS does all the rest.</p>
	 <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''> 
     <div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 9,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 3 of 4 : Make a box</p>
    </div> 
    <div class=''>
      <p class='h2 pb-4'>MAKE A BOX</p>
      <p class='h6 pb-1 text-left'>Use: <span class="inline-code">class="<span class="green">box</span>"</span> to say where you want it.</p>
	  <h3 pt-4 pb-4 class="pink">CSS</h3>
     <div class='html-code-box'>
       .box { <br>
		   &nbsp;&nbsp;<span class="green">width:</span> 100px;<br>
		   &nbsp;&nbsp;<span class="blue">height:</span> 100px;<br>
	   &nbsp;}
        </div><br>
		<h3 pt-4 pb-4 class="blue">HTML</h3>
		<div class='html-code-box'>
		   &lt;div class="<span class="green">box</span>"&gt;<br>
		  &lt;/div&gt;
	
        </div>
    </div> 
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{   
    'slide_number' : 8,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 4 of 4 : Make a box</p>
    </div> 
    <div class=''>
     <p class='h2 pb-4'>MAKE A BOX</p>
     <p class='h5 pb-1 text-center'>In this case its a <span class="inline-code">&lt;div&gt;</span>.</p>
     <div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 11,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 1,
    'reg' : [ '((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ol>
            <li class='tasks'>Make a <span class="inline-code">.box</span> css class.</li>
            <li class='tasks'>Make it 250px wide & high.</li>
          </ol>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
          .box { <br>
		   &nbsp;&nbsp;<span class="green">width:</span> 250px;<br>
		   &nbsp;&nbsp;<span class="blue">height:</span> 250px;<br>
	   &nbsp;}
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP</span>: In <span class="inline-code">style</span> section. </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 12,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 2,
    'reg' : [ '((.|\n)*)\s*(<div((.)*)\s* class=\"box\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ol>
            <li class='tasks'>Make a <span class="inline-code">&lt;div&gt;</span> in the <span class="inline-code">&lt;header&gt;</span> section above <span class="inline-code">&lt;h1&gt;</span>.</li>
            <li class='tasks'>Add the .box class to the <span class="inline-code">&lt;div&gt;</span>.</li>
          </ol>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
		   &lt;div class="<span class="green">box</span>"&gt;<br>
		  &lt;/div&gt;
		 
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP</span>: In the <span class="inline-code">&lt;header&gt;</span> above <span class="inline-code">&lt;h1&gt;</span>. </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 13,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'><span class="blue">To see</span> your box, you have to <span class="blue">add</span> a border or <span class="blue">background</span>.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 14,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 3,
    'reg' : [ '((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class="list-none">
            <li class='tasks'>Add this code to the <span class="inline-code">.box</span> class.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
		  background: blue;
		 
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP</span>: In <span class="inline-code">.box{ }</span>.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 15,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
	  
	  <p class="fs75">Your code should look something like this:</p>
    </div>
	<div class='html-code-box'>
	<span class='html-code'>
	<span class="code-fade">&lt;head&gt;</span><br>
	&nbsp;&lt;style&gt;<br>
		&nbsp;&nbsp;.box {<br>
        &nbsp;&nbsp;height: 250px;<br>
        &nbsp;&nbsp;border: width: 250px;<br>
        &nbsp;&nbsp;background-color: blue<br>
        &nbsp;}<br>
	    &nbsp;&lt;/style&gt;<br>
	    <span class="code-fade">&lt;/head&gt;</span><br><br>

		<span class="code-fade">&lt;body&gt;<br>
		&nbsp;&lt;header&gt;</span><br>
		&nbsp;&nbsp;&ltdiv class="box"&gt;<br>
        &nbsp;&nbsp;&lt;/div&gt;<br>
        <span class="code-fade">&nbsp;&nbsp;&lt;h1&gt;<br>
		&nbsp;&nbsp;Thandi Ndlovu<br>
		&nbsp;&nbsp;&lt;/h1&gt;<br>
	    &nbsp;&lt;/header&gt;<br>
	    &lt;/body&gt;</span><br>
    
	</span>
	
      </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 16,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 green pb-4 bold'>WOW</p>
      <p class='h4 pb-4'>You are on</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/fire.png' alt=''> 
      <img class='swiper-lazy' data-src='../img/emoji/72/fire.png' alt=''> 
      <img class='swiper-lazy' data-src='../img/emoji/72/fire.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 17,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'></p>
    </div> 
    <div>
      <div class='h4 pb-4'>Did you wonder why we had to use <span class='inline-code'>&lt;div&gt;</span> ?</div>
      <div class='h5 pb-4'>Here's a quick review...</div>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 18,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : &lt;div&gt; Tag</p>
    </div> 
    <div>
      <div class='h4 pb-4'>What's a <strong>DIV</strong>?</div>
	  <img style="width:35%" class='mt-4 mb-4 swiper-lazy' data-src='../learn/lessons/P002-T03-M-V001/img/briefcase.png' alt=''>
	   <div class='h4 pb-4 text-left'><strong class="inline-code green">&lt;div&gt;</strong><br><br>It's like a <strong class="green">magical stretchy container</strong> & you can put any type of object in it.<br><br><strong class="inline-code green">&lt;/div&gt;</strong></div>
     </div>
  
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 19,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'></p>
    </div> 
    <div>
      <div class='h3 pb-4'><strong>Ok.</strong></div>
     </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 20,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'></p>
    </div> 
    <div>
      <div class='h3 pb-4'>Did you enjoy making a box?</div>
	     <img class='swiper-lazy pb-4 pt-4' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''> 
	   <div class='h4 pt-4'>We are going to do something super cool now.</div>
     </div>
  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 21,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'></p>
    </div> 
    <div>
      <div class='h3 pb-4'>With one line of code we are going to turn the box into a circle.</div>
	     <img class='swiper-lazy pb-4 pt-4' data-src='../img/emoji/72/weary-cat-face.png' alt=''> 
	     <img class='swiper-lazy pb-4 pt-4' data-src='../img/emoji/72/weary-cat-face.png' alt=''> 
	     <img class='swiper-lazy pb-4 pt-4' data-src='../img/emoji/72/weary-cat-face.png' alt=''> 
	   <div class='h4 pt-4'>Ready?</div>
     </div>
  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 22,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'>Make a circle.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 4 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 23,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : border-radius</p>
    </div> 
    <div>
      <div class='h4 pb-4'><span class="html-code">border-radius:</span></div>
	  <p class="h5 pb-4">Make a circle out of a square(50%)</p>
	  <img style="width:55%" class='mt-4 mb-4 swiper-lazy' data-src='../learn/lessons/P002-T03-M-V001/img/50-radius.PNG' alt=''>
	  <p class="h5 pb-4">or add rounded corners. (5%)</p>
	  <img style="width:80%" class='mt-4 mb-4 swiper-lazy' data-src='../learn/lessons/P002-T03-M-V001/img/5-radius.PNG' alt=''>
     </div>
  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 24,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 4,
    'reg' : [ '((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a  <span class='inline-code'>border-radius</span> to <span class='inline-code'>.box</span> class with a value of 50%</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
        border-radius: 50%;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 25,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 5,
    'reg' : [ '((.|\n)*)\s*.circle((.|\n)*)\s*(<div((.)*)\s* class=\"circle\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2</p>

      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ol>
            <li class='tasks'>Change CSS class <span class='inline-code'>.box</span> to <span class='inline-code'>.circle</span></li>
            <li class='tasks'>Replace "box" in: <span class='inline-code'>&lt;div class="box"&gt;</span> with circle.</li>
          </ol>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
          &lt;div class="circle"&gt;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>      
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 26,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
	  
	  <p class="fs75">Your code should look something like this:</p>
    </div>
	<div class='html-code-box'>
	<span class='html-code'>
	<span class="code-fade">&lt;head&gt;</span><br>
	&nbsp;&lt;style&gt;<br>
		&nbsp;&nbsp;.circle {<br>
        &nbsp;&nbsp;height: 250px;<br>
        &nbsp;&nbsp;border: width: 250px;<br>
        &nbsp;&nbsp;background-color: blue<br>
        &nbsp;&nbsp;border-radius: 50%<br>
        &nbsp;}<br>
	    &nbsp;&lt;/style&gt;<br>
	    <span class="code-fade">&lt;/head&gt;</span><br><br>

		<span class="code-fade">&lt;body&gt;<br>
		&nbsp;&lt;header&gt;</span><br>
		&nbsp;&nbsp;&ltdiv class="circle"&gt;<br>
        &nbsp;&nbsp;&lt;/div&gt;<br>
        <span class="code-fade">&nbsp;&nbsp;&lt;h1&gt;<br>
		&nbsp;&nbsp;Thandi Ndlovu<br>
		&nbsp;&nbsp;&lt;/h1&gt;<br>
	    &nbsp;&lt;/header&gt;<br>
	    &lt;/body&gt;</span><br>
    
	</span>
	
      </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },{    
    'slide_number' : 27,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 green pb-4 bold'>COOL</p>
      <p class='h4 pb-4'>just two small things to finish the circle.</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''>  
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 28,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 6,
    'reg' : [ '((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*solid((.|\n)*)\s*white;((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a solid, 10px wide, white border to the <span class='inline-code'>.circle</span> to CSS class.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
        border: 10px solid white;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 29,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h4 pb-4'>In order to make sure the circle stays centered around text, use this layout trick:</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''>  
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 30,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 7,
    'reg' : [ '((.|\n)*)\s*.circle((.|\n)*)\s*{((.|\n)*)\s*display((.|\n)*)\s*:((.|\n)*)\s*inline-block;' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class="list-none">
            <li class='tasks'>Add to the circle class.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
		  display: inline-block;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP</span>: In <span class="inline-code">.circle{ }</span>.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 31,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 green pb-4 bold'>GREAT!</p>
	  <p class='h4 pb-4'>You are done with the circle.</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/flexed-biceps.png' alt=''>
      <img class='swiper-lazy' data-src='../img/emoji/72/thumbs-up-sign.png' alt=''>
	  <p class='h5 pb-4 pt-4'>Keep it up. You're doing amazing!</p>
	  <p class='h5 pb-4'>We believe in you!</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 32,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'>Learn <strong>3 things</strong> about <strong>CSS Classes.</strong></span></p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 1 "Remember"</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 33,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
	  <p class='h3 pb-4'>Don't worry if you don't understand this part.</p>
	  <p class='h4 pb-4'>It's just so you have an idea of how they work.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 34,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
	  <p class='h3 pb-4'>Here's a simple CSS class.</p>
	  <p class='fs75 pb-0 text-left'>EXAMPLE: </p>
	<div class='html-code-box'>
	  .banner {<br>
	  &nbsp;&nbsp;color: pink;<br>
	  &nbsp;}
	</div>
    </div>
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 35,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 1 of 4 : Make a box<br><strong class="pink">CSS CLASS</strong> OVERVIEW</p>
    </div> 
    <div class=''>
      <p class='h3 pb-4'>CSS Classes have <strong>two parts</strong>:</p>
	  <h3 pt-4 pb-4 style="text-align: left; color: white;"><span class="pink">A.</span> CSS: The styling</h3>
     <div class='html-code-box'>
		   .<span class="pink">banner</span> {<br>
			   &nbsp;&nbsp;color: pink;<br>
		   &nbsp;}
        </div><br>
		 <h3 pt-4 pb-4 style="text-align: left; color: white;"><span class="pink">B.</span> HTML: Where it's applied.</h3>
		<div class='html-code-box'>
		   &lt;h3 class="<span class="pink">banner</span>"&gt;
        </div>
    </div> 
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{    
    'slide_number' : 36,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
	  <p class='h3 pb-4'>Here are <strong class="pink">3 things</strong> you should know about CSS Classes.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 37,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 2 of 4 : Make a box<br><strong class="pink">CSS CLASS</strong> OVERVIEW</p>
    </div> 
    <div class=''>
      <p class='h2 pb-4'><strong class="pink">1.</strong></p>
      <p class='h3 pb-4'>Classes are<br><span class="pink">SUPER POWERFUL</span> and make up most of CSS.</p>
    </div> 
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 38,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 3 of 4 : Make a box<br><strong class="pink">CSS CLASS</strong> OVERVIEW</p>
    </div> 
    <div class=''>
      <p class='h2 pb-4'><strong class="pink">2.</strong></p>
      <p class='h3 pb-4'>Classes always start with a dot <br>"<strong class="pink">.</strong>".</p>
	  <div class='html-code-box'>
		   <strong class="pink">.</strong>banner {<br>
			   &nbsp;&nbsp;color: pink;<br>
		   &nbsp;}
        </div>
    </div> 
	
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 39,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 4 of 4 : Make a box<br><strong class="pink">CSS CLASS</strong> OVERVIEW</p>
    </div> 
    <div class=''>
      <p class='h2 pb-4'><strong class="pink">3.</strong></p>
      <p class='h3 pb-4'>Write them once and <span class="pink">use</span> them<br><span class="pink">ANYWHERE</span>.</p>
    </div> 
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 40,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'remember',
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Remember</p>
  	</div>
    <div>
      <p class='h2'>Can you remember the <strong>3</strong> main <strong>things</strong> about CSS classes.</p>
    </div>
	 <div class=' tips text-left'>
        <div><span class='red'>TIP</span>: Write in your notebook.</div>
      </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {     
    'slide_number' : 41,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'> </p>
    </div> 
    <div class=''>
      <p class='h3 pb-4'>That was a hard lesson.</p>
	  <img class='pt-5 swiper-lazy' data-src='../img/emoji/72/female-technologist-type-4.png' alt=''>
	  <img class='pt-5 swiper-lazy' data-src='../img/emoji/72/male-technologist-type-6.png' alt=''> 
	  <img class='pt-5 swiper-lazy' data-src='../img/emoji/72/female-technologist-type-3.png' alt=''>
	  <p class='h4 pb-4 pt-4'>Do you agree?</p><br>
	  <p class='h4 pb-4'>Invite a friend to work together to learn and "debug".</p>
	  <p class='h6 pb-4'>(when code isn't working).</p>
    </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 42,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 pb-4'>CONGRATS!</div>
      <div style='background-color: #6aa2c7;border-radius:50%;margin: 40px auto;width: 200px;height: 200px;'>
        <div class='lesson-instructions '><img style='padding-top:20px;' class='w-75 swiper-lazy mb-3' data-src='../img/lessons/congrats_training_sml.png'></div>
      </div>
      <div class='h3 pb-4'>You've finished<br> Training 3</div>  
    </div>    
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 43,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 pb-4'>TRAINING 4 is SUPER FUN!</div>
      <p class='h5 pb-4'>It's actually magical.</p>
      <p class='h5'><img style="padding-top: 0 !important;" class='pt-5 swiper-lazy' data-src='../img/emoji/72/rainbow.png' alt=''> = Hint</p>
	  <p class='h5 pb-4'>See you there.</p>
    </div>    
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 44,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 pb-4 w-75'>Ready for Training 4?</p>
      <a class="btn-primary next"" href="../learn/P2Training4">Start Now</a>
      <a class="btn-primary-alt next check" href='../learn/projects'>Projects Page</a>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  } ]
}
    
var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 
 
/// Add custom JS for lesson below here


function previewLessonModal() {
  $.galleryDialog({
    modalName: 'preview-lesson-modal',
    htmlContent: `
    <iframe  srcdoc="
<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
  </style>
</head>
<body>
<header>
    <h1>My Name</h1>
      <h3>Motivation:
        <br><br>
        <i>I want to become a coder</i>
      </h3>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
</header>
</body>

  "></iframe>
    `
  })
}