var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`
<head>
	<style>
		
		header{
		background: lightblue;
		border: solid blue;
		border-width: 10px 0px 10px 0px;
		margin: 0 auto;
		text-align: center;
		}

		h1{
		font-size: 3em;
		}

		.circle{
		height: 250px;
		width: 250px;
		background-color: blue;
		border-radius: 50%;
		border: 10px solid white;
		display: inline-block;
		}
		
		h3{
		background-color: #ce54c7;
		padding: 15px;
		font-size: 1.5em;
		border-left: 10px solid #d6d5cd;
		}

		header, h3{
		color: white;
		font-family: "Arial Black",Gadget, sans serif;
		}

		body{
		font-family : helvetica , arial ,san serif;
		}
	</style>
</head>

<body>
	<header>
		<div class="circle">
		</div>

		<h1>My Name</h1>

		<i>
		Aspiring Software Developer
		</i>
		<br><br>
	</header>
		<br>
		<h3>DETAILS</h3>
		<p>Date of Birth: 01 February 2000</p>
		<p>School: Diepsloot Combined School</p>
		<p>Grade: 8</p>
        
        
</body>
        
        



`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P2Training4", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Present your skills and plans in an attractive and engaging Website CV built with HTML, CSS and emojies.", 
    "pageKeywords": "coding, code, learn to code, code website, CV website", 
    "pageTitle": "CodeJIKA - Project 2, Training 5",
    "save_lesson_id": "P2Training5", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
           Add an h3 with an &#8220;ABOUT ME&#8221; section.
            <div class="html-code-box">
            &lt;h3&gt; <br>
			&nbsp; ABOUT ME<br>
			&lt;\/h3&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>Where:</span>
			Below the &#8220;DETAILS&#8221; section.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 12,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Write about yourself in a <span class='html-code'>&lt;p&gt;</span>
            <div class="html-code-box">
				&lt;p&gt; I love.... &lt;/p&gt;<br>
				&lt;p&gt; My dream is.... &lt;/p&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>Tip:</span>
			You can add different lines by making a new <span class="html-code"> &lt;p&gt;</span> for each line.
		</div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide13.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 16,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(EDUCATION)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 3 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
           Add an h3 &#8220;EDUCATION&#8221; section. Add the text in a few <span class='html-code'>&lt;p&gt;</span>s.
            <div class="html-code-box">
				&lt;p&gt; 2011-2015: East-gate Primary, Boksburg&lt;/p&gt;<br>
				&lt;p&gt; 2015-Ongoing Iterele-Zenzele Secondary&lt;/p&gt;<br>
				&lt;p&gt; Fure: 2020-2024: BSC in Computer Science, University of Pretoria&lt;/p&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide17.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 18,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(EXPERIENCES)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 4 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add an &#8220;EXPERIENCES&#8221; section. You can write about anything you want in the <span class='html-code'>&lt;p&gt;</span>.
            <div class="html-code-box">
				&lt;p&gt; I was born in... &lt;/p&gt;<br>
				&lt;p&gt; I enjoy... &lt;/p&gt;<br>
				&lt;p&gt; I once saw... &lt;/p&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>Tip:</span>
          You can add different lines by making a new <span class="html-code"> &lt;p&gt;</span> for each line.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide22.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide23.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide24.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 30,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(SKILLS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<ul>((.|\n)*)\s*<li>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/li>((.|\n)*)\s*<\/ul>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add an &#8220;SKILLS&#8221; section.
            <div class="html-code-box">
				&lt;h3&gt; SKILLS &lt;/h3&gt;<br>
				&lt;ul&gt; <br>
				&nbsp; &lt;li&gt;
                I can make amazing spaghetti. &lt;/li&gt;<br>
				&nbsp; &lt;li&gt;
                Taught Michael Jackson to dance.
                &nbsp;&lt;/li&gt; <br>
				&lt;/ul&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>Tip:</span>
           You can add different lines by making a new <span class="html-code"> &lt;p&gt;</span> for each line.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 32,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide32.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 33,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*(})((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Provide some padding and increase size of <span class='html-code'>&lt;ul&gt;</span> and <span class='html-code'>&lt;p&gt;</span>.
            <div class="html-code-box">
              p, ul {
              <br>
             }
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 34,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 3 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Try 50px of padding on the left only.
            <div class="html-code-box">
              p, ul {
                <br>
                padding-left: 50px;
                <br>
              }
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 35,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "((.|\n)\s*)(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*({)((.|\n)*)\s*padding-left:((.|\n)*)\s*50px((.|\n)*)\s*;((.|\n)*)\s*font-size:((.|\n)*)\s*1.3em((.|\n)*)\s*;((.|\n)*)\s*(})((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 4 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Increase the size of the these selectors by 30%.
            <div class="html-code-box">
             <span class="code-fade"> p, ul {
                    <br>
                  padding-left: 50px;
                  <br></span>
                  font-size: 1.3em;
                  <span class="code-fade"><br>
              }</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 37,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide37.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide39.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(CONTACT)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/p>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a "CONTACT" section.
            <div class="html-code-box">
             Add a paragraph with town, area and country you live in.<br><br>
             Add a phone number and email too.

            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide41.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide42.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 43,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide43.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{   
      "slide_number" : 44,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T05-D-V002/img/Slide44.PNG">
         <a href="/learn/P2Training6" class="btn btn-primary" style="top:65%;">Start next training →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}

var check_points = {
  16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
/*  16:"A closing tag has a forward slash <strong>/<strong>",
  17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
  23:"In the body section. Hint given as tip on bottom of the slide.",
  24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
  32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
  36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"*/
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

` 


/// Add custom JS for lesson below here