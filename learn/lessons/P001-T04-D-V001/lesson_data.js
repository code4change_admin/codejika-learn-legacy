var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
    header {
      background: linear-gradient(110deg, yellow 40%, pink 40%);
    }
    section {
      background: lightgrey;
    }
    footer {
      background: black;
      color: white;
    }
  </style>
</head>
<body>
  <header>
    <h1>My Name</h1>
    <section>
      <h3>Motivation:
        <br><br>
        <i>I want to learn to code because I love building new things.</i>
      </h3>
    </section>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
  </header>
  <footer>&copy 2020 My Name</footer>
</body>



` ,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P1Training3", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 4",
    "save_lesson_id": "P1Training4", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Insert a <b>&lt;div&gt;</b> in &lt;header&gt;
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

          <div class="challenge-tip"><span>Where:</span><br>
            1. Open it before &lt;h1&gt;.<br>
            2. Close it after the &lt;\/p&gt; tags.
          </div>
        </div>
      </div>        
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Insert a <b>&lt;div&gt;</b> in &lt;section&gt;
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

          <div class="challenge-tip"><span>Where:</span><br>
            1. Open the &lt;div&gt; before your &lt;h3&gt;.<br>
            2. Close it after the &lt;\/h3&gt; tags.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide13.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Wrap the content in <b>&lt;footer&gt;</b> with a &lt;div&gt;
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide17.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide18.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide22.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide23.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">

                Add a <b>div { }</b> selector in the <b>&lt;style&gt;</b> section.<br>
				
                <div class="html-code-box">
                  div{ <br>
                  &nbsp;}
                </div>
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*text-align((.|\n)*)\s*:((.|\n)*)\s*center;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Center align all the content in the &lt;div&gt;s.
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 3</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Using CSS, add <b>40px padding</b> to div.
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide30.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 32,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide32.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Add the h3 Selector to &lt;style&gt; section.
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 34,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide34.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide35.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*((white((.|\n)*)\s*solid((.|\n)*)\s*2px;)|(solid((.|\n)*)\s*2px((.|\n)*)\s*white;)|(2px((.|\n)*)\s*solid((.|\n)*)\s*white;))((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challenge-content">
                Add a white solid 2px border to <b>h3 { }</b>
              </div>

            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 37,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*font-size:((.|\n)*)\s*45px;((.|\n)*)\s*padding:((.|\n)*)\s*15px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 3</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              <div class="challeng-content">
                Add a font-size 45px &amp; a padding of 15px to <b>h3 { }</b>.<br>

                <span class="code">
                <div class="html-code-box">
                  font-size: 45px; <br>
                  padding: 15px;
                </div>
                </span>

              </div>
            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 10,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*auto((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 4</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>
              <div class="challenge-content">

                Center your <b>h3 { } </b> by using the <b>margin: auto;</b> trick.<br>

                <span class="code">
                <div class="html-code-box">
                  &nbsp; margin: auto;
                </div>
                </span>

              </div>
            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide40.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 11,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*:((.|\n)*)\s*400px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 5</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>
              <div class="challenge-content">

                Beautify your h3 border by giving it a max-width of 400px.<br>

                <span class="code">
                <div class="html-code-box">
                  &nbsp; max-width: 400px;
                </div>
                </span>

              </div>
            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide42.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 43,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide43.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 44,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide44.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 45,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 12,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*white;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 6</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>
              <div class="challenge-content">
                Make your h1 text white.
              </div>
            </li>
          </ol>
                  <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 46,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide46.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 47,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide47.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 48,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide48.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 49,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T04-D-V001/img/Slide49.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 50,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T04-D-V001/img/Slide50.PNG">
         <a href="/learn/P2Training1" class="btn btn-primary" style="top:65%;">Start next training →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}
    
var hintsForCheckPonts = {


}
  
var hints_data =   `


    ` 
  

/// Add custom JS for lesson below here