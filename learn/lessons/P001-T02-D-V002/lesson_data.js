var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`<head>
</head>
<body>
  <h1>My Name</h1>
  <h3>Motivation:</h3>
  <h3>Launching Soon...</h3>
  <p>01 January 2000</p>
</body>



`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P1Training1", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 2",
    "save_lesson_id": "P1Training2", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide1.PNG">
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Add a <span class="html-code">&lt;style&gt;</span> section.
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

          <div class="challenge-tip"><span>Tip:</span>
            Remember to close <b>&lt;style&gt;</b> tag.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide12.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide13.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide16.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Add a <span class="html-code">h1</span> selector.
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

          <div class="challenge-tip"><span>Tip:</span>
            In &lt;style&gt; section.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*75px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Add a <span class="html-code">font-size: 75px;</span>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

          <div class="challenge-tip"><span>Tip:</span>
            Between the curly brackets.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*Motivation" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Type: <span class="html-code">Motivation:</span> <br>Place it below the <span class="html-code">&lt;h1&gt;</span> tags

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation:|MOTIVATION:)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<h3>((.|\n)*)\s*Launching((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Wrap it in <span class="html-code">&lt;h3&gt;</span> tags.  
              <div class="html-code-box">
              &lt;h3&gt;<br>&nbsp;&nbsp;Motivation: <br>&lt;/h3&gt;
              </div>

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

          <div class="challenge-tip"><span>Tip:</span>
            Between the &lt;body&gt; tags.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide24.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation|MOTIVATION)((.|\n)*)\s*\[a-z]{4}((.|\n)*)\s*<\/h3>((.|\n)*)\s*<h3>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Type a <u>short sentence</u> about why you are learning coding.

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

          <div class="challenge-tip"><span>Where:</span>
            Below “Motivation” inside &lt;h3&gt; tags.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide30.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 32,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "(<h3>|<h3 [^>]*>)((.|\n)*)\s*Motivation((.|\n)*)\s*(<i>|<i [^>]*>)((.|\n)*)\s*<\/i>((.|\n)*)\s*<\/h3>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Put your sentence into italics with the <span class="html-code">&lt;i&gt;</span> tag.
              <div class="html-code-box">
              <span class="code-fade">&lt;h3&gt; <br>Motivation:<br></span>
              &nbsp;&lt;i&gt;<br> <span class="code-fade">&nbsp;&nbsp;Sentence...</span><br>&nbsp;&lt;i&gt; <br>
               <span class="code-fade">&lt;/h3&gt;</span>
              </div>

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide33.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 34,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*:((.|\n)*)\s*25px((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Using CSS reduce the size of the italics text to <span class="html-code">25px</span>.

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

          <div class="challenge-tip"><span>Tip:</span>
            Start with i{ } in style.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide35.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 37,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide37.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "(<h3>|<h3 [^>]*>)((.|\n)*)\s*(Motivation:|Motivation)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*(<br>|<br [^>]*>)((.|\n)*)\s*<\/h3>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Add <span class="html-code">&lt;br&gt;</span> <u>twice</u> immediately after <span class="html-code">MOTIVATION:</span><br>
              <div class="html-code-box">
                &lt;br&gt;<br>
                &lt;br&gt;
              </div>

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div> 

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide40.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide41.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide42.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 43,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T02-D-V002/img/Slide43.PNG">
        <a href="/learn/P1Training3" class="btn btn-primary" style="top:65%;">Start next training →</a>        
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}
    
var hintsForCheckPonts = {
  /* 12:"Inside the &lt;head&gt; <strong>Type Code Here</strong> &lt;\/head&gt; tags",
  22:"Inside the &lt;style&gt; <strong>Type Code Here</strong> &lt;\/style&gt; section",
  23:"Remember to wrap your content inside the curley braces h1<strong>{Type Code Here}</strong>" */
}
  
var hints_data =   `



    ` 
 
/// Add custom JS for lesson below here