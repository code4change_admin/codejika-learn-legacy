var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`



`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Present your skills and plans in an attractive and engaging Website CV built with HTML, CSS and emojies.", 
    "pageKeywords": "coding, code, learn to code, code website, CV website", 
    "pageTitle": "CodeJIKA - Project 2, Training 1",
    "save_lesson_id": "P2Training1", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add this part:<br>
            <div class="html-code-box">
				&lt;head&gt;<br>
				&nbsp&nbsp&lt;style&gt;<br>
				&nbsp&nbsp&lt;/style&gt;<br>
				&lt;/head&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Now add the <span class="html-code">&lt;body&gt;</span> section.<br>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/
		Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide12.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a <span class="html-code">&lt;header&gt;</span>.<br>
            <div class="html-code-box">
				&lt;header&gt;<br>
				&lt;/header&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 14,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<h1>((.|\n)*)\s*[A-Z][A-Z][A-Z]<\/h1>((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add your name in all-caps (capital letters) in an <span class="html-code">&lt;h1&gt;</span> in <span class="html-code">&lt;header&gt;</span>.
            <div class="html-code-box">
				&lt;h1&gt;<br> 
				&nbsp;THANDI NDLOVU <br>
				&lt;/h1&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide16.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide17.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide18.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 20,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a CSS selector called header.
            <div class="html-code-box">
				<span class="code-fade">
				&lt;style&gt;<br></span>
				&nbsp header {<br>
				&nbsp&nbsp}<br>
				<span class="code-fade">&lt;/style&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 21,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightblue;((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a lightblue background to the header element.
            <div class="html-code-box">
              background: lightblue;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 22,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border)((.|\n)*)\s*:((.|\n)*)\s*solid((.|\n)*)\s*blue;((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a simple blue border to &lt;header&gt;.
            <div class="html-code-box">
				border: solid blue;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide23.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide24.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 25,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border-width)((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*0px((.|\n)*)\s*10px((.|\n)*)\s*0px;((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a top  and bottom border to header.
            <div class="html-code-box">
              border-width: 10px 0px 10px  0px;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide30.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 32,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide32.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide33.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 34,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide34.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide35.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T01-D-V001/img/Slide36.PNG">
         <a href="/learn/P2Training2" class="btn btn-primary" style="top:65%;">Start next training →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}

var check_points = {
  16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
/*  16:"A closing tag has a forward slash <strong>/<strong>",
  17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
  23:"In the body section. Hint given as tip on bottom of the slide.",
  24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
  32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
  36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"*/
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

` 


/// Add custom JS for lesson below here