var lesson_data = 
{
    'defaultCode': // default code, if user has not already started coding   
`
` ,
    'kbLayout': '', // not currently in use
    'loadJS': 'https://cdnjs.cloudflare.com/ajax/libs/geopattern/1.2.3/js/geopattern.min.js"></script>', // not currently in use
    'prevLessonID': '', // Lesson ID of previous lesson where to load user's code
    'nextLessonSlug': '', // not currently in use
    'pageDesc': 'Learn how to build your first website with these easy intro lessons to coding.', 
    'pageKeywords': 'coding, code, learn to code, code website, my first website', 
    'pageTitle': 'CodeJIKA - Project 2, Training 1',
    'save_lesson_id': 'P2Training1', // This is id that will be used to store save code in Firebase
  'slides' : [ {
    'slide_number' : 1,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'intro',      
    'html_content' : `
	<div class="container">
        <h1 class='yellow'>PROJECT 2</h1>
        <h2 class="mt-2 lesson-title font-weight-normal">YAAAAAYYYYY!</h2>
      </div>      
	  `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 2,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'definition',    
    'html_content' : `
	<div>
      <p class='slide-header h2'>VISION</p>
    </div>     
    <div>
      <p class='h4 pb-3'>In Project 2 you'll create a <span class="blue">CV Website.</span></p>
      <ol  class='h5'>
        <li>You'll build it with HTML.</li>
        <li>Style it with CSS.</li>
		<li>and there's a <span class="blue">suprise</span> at the end.</li>
      </ol>
    </div>
      
	`,
    'created_at' : '',  
    'updated_at' : ''
  }, {  	
    'slide_number' : 3,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',
    'html_content' : `
  	<div>
      <p class='slide-header h2 blue'>PREVIEW</p>
    </div>
    <div>
      <p class='h4 pb-3'>Your CV Website will look like this:</p>
	  <img class='swiper-lazy' data-src='../learn/lessons/P002-T01-M-V001/img/project2-final-preview.png' alt=''>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 	
    'slide_number' : 4,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'snapshot',    
    'html_content' : `
      <div>
  	  <p class='slide-header h2'>Snapshot</p>
  	  <p class='white fs75'>(These are your missions for today.)</p>
  	</div>   
      <div>
       <ol  class='h4'>
        <li>Review the Website Structer.</li>
        <li>Add the <span class='html-code'>&lt;header&gt;</span> section and a border.</li>
       </ol> 
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 5,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class='slide-header h2 blue'>START</p>
	</div>   
	<div>
	   <p class='h4 pb-3'>Start by setting up the skeleton of code.</p>
	   <img class='swiper-lazy' data-src='../learn/lessons/P002-T01-M-V001/img/html-skeleton-structure.PNG' alt=''>
	</div>
    <div class='slide-footer tips'>
		<span class='red'>TIP:</span> You learnt this in your last project.
	</div>
       
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 6,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 1,
    'reg' : [ '(<head>|<head [^>]*>)((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*<\/style>((.|\n)*)\s*<\/head>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add this part:</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			&lt;head&gt;<br>
			&nbsp&nbsp&lt;style&gt;<br>
			&nbsp&nbsp&lt;/style&gt;<br>
			&lt;/head&gt;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 7,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 2,
    'reg' : [ '((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2</p>
      </div>
      <div>
        <div class='text-left pb-5'>
          <p class='blue text-uppercase text-left'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Now add the <span class='inline-code'>&lt;body&gt;</span> section.</li>
          </ul>
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
             
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 8,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
	  
	  <p class="fs75">Your code should look something like this:</p>
    </div>
	<div class='text-left mx-auto w-50'>
	<span class='html-code'>
		<p class='h4 pb-1 mb-0  blue'>&lt;head&gt;</p>
		<p class='h4 pl-5 pb-1 mb-0 '>&lt;style&gt;</p>
        <p class='h4 pl-5 pb-1 mb-0 '>&lt;/style&gt;</p>
        <p class='h4 pl-1 pb-2 mb-0  blue'>&lt;/head&gt;</p>
        <p class='h4 pl-1x pb-1 red'>&lt;body&gt;</p>
        <p class='h4 pl-1x pb-0 mb-0  red'>&lt;/body&gt;</p>
	</span>
      </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 9,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : &lt;header&gt; Tag</p>
    </div> 
    <div>
	<p class="h5 pb-3">The <span class='html-code'>&lt;header&gt;</span> of a website usually includes:</p>
      <ol class="h5 w-75">
          <li>Company name</li>
          <li>Logo &</li>
		  <li>Menu</li>
        </ol>
    </div>
	<div class='slide-footer tips'>
      <span class='red'>TIP:</span> Don't get confused. It's not the &lt;head&gt;.<br>
	  The &lt;header&gt; goes in the &lt;body&gt; section.
    </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 10,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 3,
    'reg' : [ '((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a <span class='inline-code'>&lt;header&gt;</span>:</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			&lt;header&gt;<br>
			
			&lt;/header&gt;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 11,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 4,
    'reg' : [ '((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<header>|<header [^>]*>)((.|\n)*)\s*<h1>((.|\n)*)\s*[A-Z][A-Z][A-Z]((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<\/header>)((.|\n)*)\s*<\/body>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add your name in all-caps(Capital Letters) in an <span class='html-code'>&lt;h1&gt;</span> in <span class='html-code'>&lt;header&gt;</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			&lt;h1&gt;<br>
			&nbsp;Thandi Ndlovu <br>
			&lt;/h1&gt;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 12,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'><span class="red"><b>TIP:</b></span><br> Don't forget to close your tags.</p>
      
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 13,
	'checkpoint' : false,	
    'action' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class='slide-header h2 blue'>GOOD JOB!</p>
	</div>
    <div>
      <p class='h3 pb-5'>You'll be a pro in no time!</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 14,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'>Style your <span class='html-code'>&lt;header&gt;</span>.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 4 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 15,
	'checkpoint' : false,	
    'action' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	
    <div>
      <p class='h3 pb-5'>You got this.</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/face-with-stuck-out-tongue-and-winking-eye.png' alt=''>
      <img class='swiper-lazy' data-src='../img/emoji/72/face-with-stuck-out-tongue-and-tightly-closed-eyes.png' alt=''>
      <img class='swiper-lazy' data-src='../img/emoji/72/flexed-biceps.png' alt=''>	  
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 16,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 5,
    'reg' : [ '((.|\n)*)\s*(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a CSS selector called <span class="inline-code">header.</span></li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			<span class="code-fade">&lt;style&gt;</span><br>
			&nbsp;header{<br>&nbsp&nbsp} <br>
			<span class="code-fade">&lt;/style&gt;</span>
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 17,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 6,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightblue;((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a lightblue background to the <span class="inline-code">header</span> element.	</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			background: lightblue;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 18,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 7,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border)((.|\n)*)\s*:((.|\n)*)\s*solid((.|\n)*)\s*blue;((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a simple blue border to <span class="inline-code">header</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			border: solid blue;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 19,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'play',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>THINK</p>
    </div>
    <div>
      <p class='h3 pb-4'>If you want a border only on the TOP and the BOTTOM.</p>
      <p class='h4 pb-1 w-75'><u>Not</u> all the way around.</p>
	  <img class='swiper-lazy' data-src='../img/emoji/72/thought-balloon.png' alt=''>	
    </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 20,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : Top and Bottom Border</p>
    </div> 
    <div>
	  <p class="h5 pb-3">The <span class='html-code'>border-width:</span></p>
	  <img class='swiper-lazy' data-src='../learn/lessons/P002-T01-M-V001/img/border-top-bottom.PNG' alt=''>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  } ,{ 
    'slide_number' : 21,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 8,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)header((.|\n)*)\s*{((.|\n)*)\s*(border-width)((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*0px((.|\n)*)\s*10px((.|\n)*)\s*0px;((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a top and bottom border to <span class="inline-code">header</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
			border-width: 10px 0px 10px 0px;
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 22,
	'checkpoint' : false,	
    'action' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	
    <div>
      <p class='h3 pb-5'>How did you do?</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/thinking-face.png' alt=''><br>
	  <p class='h3 pb-5 pt-5'>EXCITING!!!</p>
	  <p class='h3 pb-5'>Go to the next slide and compare your code.</p>
        
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{   
    'slide_number' : 23,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
	  
	  <p class="fs75">Your code should look something like this:</p>
    </div>
	<div class='html-code-box'>
	<span class='html-code'>
	&lt;head&gt;<br> 
	&nbsp;&lt;style&gt;<br> 
	  &nbsp;&nbsp;header {<br>
	  &nbsp;&nbsp;background: lightblue;<br>
	  &nbsp;&nbsp;border: solid blue;<br>
	  &nbsp;&nbsp;border-width: 10px 0px 10px 0px;<br>
	  &nbsp;&nbsp;}<br>
	&nbsp;&lt;/style&gt;<br>
	&lt;/head&gt;<br><br>
	&lt;body&gt;<br>
	&nbsp;&lt;header&gt;<br>
	&nbsp;&nbsp;&lt;h1&gt;Thandi Ndlovu&lt;/h1&gt;<br>
	 &nbsp;&lt;/header&gt;<br>
	&lt;/body&gt;

	</span>
      </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 24,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>FABULOUS</p>
    </div>
	<div>
	 <p class='h3 pb-5'>Now your code looks like a bomb...</p>
	 <img class='swiper-lazy' data-src='../img/emoji/72/bomb.png' alt=''>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },{   
    'slide_number' : 25,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
	  <p class="fs75">Your code should look something like this:</p>
    </div>
	<div>
	<p class='slide-header h5'>WEB CHECK:</p>
	<img class='swiper-lazy' data-src='../learn/lessons/P002-T01-M-V001/img/web-check-1.png' alt=''>
	</div>
	
    `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 26,
	'checkpoint' : false,	
    'action' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	
    <div>
      <p class='h3 pb-4'>Did you know?</p>
	  <p class='h4 pb-4'>The best way to learn code is <span class="pink"><strong>with friends</strong></span>?</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''>
      <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''>
      <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''><br><br>
	  <p class='h5 pb-4'>When it gets complicated you can help each other.</p>
      
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 27,
	'checkpoint' : false,	
    'action' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	
    <div>
      <p class='h3 pb-4'>Ready for Training 2?</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/winking-face.png' alt=''>
      
      
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 28,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 pb-4 w-75'>Lets Go...</p>
      <a class="btn-primary next"" href="../learn/P2Training2">Start Now</a>
      <a class="btn-primary-alt next check" href='../learn/projects'>Projects Page</a>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  } ]
}
    
var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 
 
/// Add custom JS for lesson below here


function previewLessonModal() {
  $.galleryDialog({
    modalName: 'preview-lesson-modal',
    htmlContent: `
    <iframe  srcdoc="
<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
  </style>
</head>
<body>
<header>
    <h1>My Name</h1>
      <h3>Motivation:
        <br><br>
        <i>I want to become a coder</i>
      </h3>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
</header>
</body>

  "></iframe>
    `
  })
}