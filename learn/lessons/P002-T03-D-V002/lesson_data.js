var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`

<head>
	<style>
		header{
		background: lightblue;
		border: solid blue;
		border-width: 10px 0px 10px 0px;
		margin: 0 auto;
		text-align: center;
		}
                
		h1{
		font-size: 3em;
		}
	</style>
</head>

<body>
	<header>
		<h1>My Name</h1>       
		<i>Aspiring Software Developer</i>
		<br><br>
		
	</header>
        <br>
        <h3>DETAILS</h3>
        <p>Date of Birth: 01 February 2000</p>
        <p>School: Diepsloot Combined School</p>
        <p>Grade: 8</p>
</body>

`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P2Training2", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Present your skills and plans in an attractive and engaging Website CV built with HTML, CSS and emojies.", 
    "pageKeywords": "coding, code, learn to code, code website, CV website", 
    "pageTitle": "CodeJIKA - Project 2, Training 3",
    "save_lesson_id": "P2Training3", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            1. Make a <span class="html-code">.box</span> CSS class.<br>
			2. Make it 250px wide & high.
            <div class="html-code-box">
				.box {<br>
				&nbsp; height: 250px;<br>
				&nbsp; width: 250px; <br>
				}
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>Tip:</span>
           In <span class="html-code">&lt;style&gt;</span> section.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "((.|\n)*)\s*(<div((.)*)\s* class=\"box\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            1. Make a <span class="html-code">&lt;div&gt;</span> in the <span class="html-code">&lt;header&gt;</span> section above <span class="html-code">&lt;h1&gt;</span>.<br>
			2. Add the <span class="html-code">.box</span> class to the <span class="html-code">&lt;div&gt;</span>.
            <div class="html-code-box">
				&lt;div class=\"box\"&gt;<br>
				&lt;/div&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>Tip:</span>
			In <span class="html-code"> &lt;header&gt;</span> above <span class="html-code"> &lt;h1&gt;</span>.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide13.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 14,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "((.|\n)\s*)(.box)((.|\n)*)\s*{((.|\n)*)\s*(height)((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add this code to the <span class="html-code">.box</span> class:
            <div class="html-code-box">
              background: blue;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>Tip:</span>
          In <b>.box{ }</b>
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide16.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide17.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide18.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide22.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide23.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 24,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add <span class="html-code">border-radius:</span> to <span class="html-code">.box</span> class with a value of 50%.
            <div class="html-code-box">
				border-radius: 50%;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 25,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "((.|\n)*)\s*.circle((.|\n)*)\s*(<div((.)*)\s* class=\"circle\"((.|\n)*)\s*>)((.|\n)*)\s*(<\/div>)((.|\n)*)\s*(<h1>)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            1. Change CSS Class <span class="html-code">.box</span> to <span class="html-code">.circle</span><br>
			2. Replace &#8220;box&#8221 in: <span class="html-code">&lt;div class=\"circle\"&gt;</span> with circle.
            <div class="html-code-box">
            &lt;div class=\"circle\"&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "((.|\n)*)\s*width((.|\n)*)\s*:((.|\n)*)\s*250px;((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*blue;((.|\n)*)\s*border-radius((.|\n)*)\s*:((.|\n)*)\s*50%;((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*10px((.|\n)*)\s*solid((.|\n)*)\s*white;((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
           Add a solid, 10px wide, white border to the <span class="html-code">.circle</span> CSS class.
            <div class="html-code-box">
               border: 10px solid white;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "((.|\n)*)\s*.circle((.|\n)*)\s*{((.|\n)*)\s*display((.|\n)*)\s*:((.|\n)*)\s*inline-block;" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add to the <span class="html-code">s.circle</span> class:
            <div class="html-code-box">
              display: inline-block;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>  
		<div class='challenge-tip'><span>Tip:</span>
			In <b>.circle{ }.</b>
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 32,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide32.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide33.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 34,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide34.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide35.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 37,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide37.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide39.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide40.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide41.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide42.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 43,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide43.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 44,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T03-D-V002/img/Slide44.PNG">
         <a href="/learn/P2Training4" class="btn btn-primary" style="top:65%;">Start next training →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}

var check_points = {
  16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
/*  16:"A closing tag has a forward slash <strong>/<strong>",
  17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
  23:"In the body section. Hint given as tip on bottom of the slide.",
  24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
  32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
  36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"*/
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

` 


/// Add custom JS for lesson below here