var lesson_data = 
{
    'defaultCode': // default code, if user has not already started coding   
`

` ,
    'kbLayout': '', // not currently in use
    'loadJS': 'https://cdnjs.cloudflare.com/ajax/libs/geopattern/1.2.3/js/geopattern.min.js"></script>', // not currently in use
    'prevLessonID': 'P2Training5"', // Lesson ID of previous lesson where to load user's code
    'nextLessonSlug': '', // not currently in use
    'pageDesc': 'Learn how to build your first website with these easy intro lessons to coding.', 
    'pageKeywords': 'coding, code, learn to code, code website, my first website', 
    'pageTitle': 'CodeJIKA - Project 2, Training 6',
    'save_lesson_id': 'P2Training6', // This is id that will be used to store save code in Firebase
  'slides' : [ {
    'slide_number' : 1,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'intro',      
    'html_content' : `
      <div>    
      	<p class='slide-title h1 mt-5'>TRAINING 6</p>
      </div>      
      <div class=''>
        <p class='h1'>Let's<br>
        <strong class='fs-x2 aqua'>ROCK</strong><br>
        this!</p>
        <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-open-mouth-and-tightly-closed-eyes.png' alt=''>
      </div>      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 2,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
    </div>
    <div>
      <p class='h3'>This is the lesson you have been waiting for your whole life,</p>
    </div>     
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 3,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'emoji time',    
    'html_content' : `
  	<div>
  	  <p class='slide-header h2 aqua'>Emoji Time!?!</p>
  	  <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-1.png' alt=''> 
      <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-2.png' alt=''>
      <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-3.png' alt=''> 
      <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-4.png' alt=''> 
      <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-2.png' alt=''>  
      <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-1.png' alt=''> 
      <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-4.png' alt=''> 
  	</div>      

      `,
    'created_at' : '',  
    'updated_at' : ''
  }, 

  {      
    'slide_number' : 4,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'snapshot',    
    'html_content' : `
          <div>
            <p class='slide-header h2'>SNAPSHOT</p>
            <p class='white fs75'>(This one is all about:)</p>
          </div>      
        <div>       
        <strong class='h3'>Emojies</strong><br>
        </div>
        <div>
                <p class='slide-footer white'>How it will look:</p>
                <div class='btn btn-aqua next preview-output'>Preview</div>
        </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  /* {      
    'slide_number' : 5,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'training sessions',     
    'html_content' : `
    <div>
    <p class='slide-header h6'>PROJECT 2</p>
    </div> 
    <div class=''>
	  <p class='h3 pb-1'>Training Sessions</p>
    <ol class="border-list">
    <li><p>Review: HTML Structure / Setup: Header</p></li>
    <li><p>Dream job, your birthday & center the header. </p></li>
    <li><p>The Incredible Box [ and classes,]</p></li>
    <li><p>Learn HTML Colors & Fonts.</p></li>
    <li><p>Add your skills section & HTML lists.</p></li>
    <li class="list-color"><p>The "Return" of the Emoji.</p></li>
    </ol>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, */  {
    'slide_number' : 6,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing emojies',
    'html_content' : `
    <div>
    <p class='slide-header h6'>BRIEFING 1 of 4 : EMOJIES</p>
  </div> 
  <div class=''>
    <p class='h2 pb-4 aqua'>Emojies</p>
    <p class='h4 pb-1 text-center '>Most internet browsers have a few emoji 
    images locked inside their system</p>
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-7.png' alt=''> 
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-8.png' alt=''> 
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-7.png' alt=''> 
    
  </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 7,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
    <p class='slide-header h6'>BRIEFING 2 of 4 : EMOJIES</p>
  </div> 
  <div class=''>
    <p class='h2 pb-4 aqua'>Emojies</p>
    <p class='h4 pb-1 text-center '>Each emoji has a special password.</p>
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-5.png' alt=''> 
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-5.png' alt=''> 
    <p class='h4 pt-1  mt-2 text-center '>And you can release it if you know it.</p>
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-6.png' alt=''> 
    <img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/emoji-6.png' alt=''> 
    
  </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 8,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING 3 of 4 : EMOJIES</p>
    </div> 
    <div class=''>
     <p class='h2 pb-4 aqua'>EMOJIES</p>
     <p class='h4 pb-2 text-center'>Emojies are "Speacial Character",</p>
     <p class='h4 pb-4 text-center'>They have between 4 and 6 letters of numbers,</p>
     <p class='pb-4 text-left'>Example:</p>
     <div class="example-box">
      <p class="h3">  <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''> = <span class="pink">#</span>x1F60E</p>
     </div>
     <div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 9,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   

    <div>
    <p class='slide-header h6'>BRIEFING 4 of 4 : EMOJIES</p>
  </div> 
  <div class=''>
   <p class='h2 pb-4 aqua'>EMOJIES</p>
   <p class='h4 pb-2 text-center'>Tell the browser that this is not just random text;</p>
   <p class='h4 pb-4 text-center'>In front put a: <b>&</b> [ampersand]</p>
   <p class='pb-4 text-left'>Example:</p>
   <div class="example-box">
   <p class="h3">  <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt=''> = <strong><span class="pink">&</span></strong>#x1F60E</p>
   </div>
   <div> 



	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  {          
    'slide_number' : 10,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
          <div>
            <p class='slide-header h2'>Mission</p>
          </div>
    <div>
      <p class='h3 pb-4'>1. Add a big emoji in a new <span class="inline-code">&lt;div&gt;</span>.</p>
      <p class='h3 pb-4'>2. Style it with class .emoji ,</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>This mission will include 4 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  

  {   
    'slide_number' : 11,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : '',     
    'html_content' : `
    <div>
    <p class='h1 text-center'><b>READY?</b></p>
    
     <div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },


  
  { 
    'slide_number' : 12,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 1,
    'reg' : [ '((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 4</p>
      </div>
      <div>
      <div class='text-left pb-3'>
      <p class='blue text-uppercase'>Code:</p>
      <ul class='list-none'>
        <li class='tasks'>Add a <u>new</u> div</li>
      </ul>
    </div>
    <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
          <div class="disable"> &lt;div class="circle"&gt;</div>
          &nbsp;&nbsp;&lt;div&gt;<br>
          &nbsp;&nbsp;&lt;/div&gt;<br>
          <div class="disable"> &lt;/div&gt;</div>

        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>Where</span>: <u>inside</u> the &lt;div class="circle"&gt;</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 13,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 2,
    'reg' : [ '((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 4</p>
      </div>
      <div>
      <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Place an emoji into the new &lt;div&gt;<br>Use this one: <b>#x1F60E</b>;</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
          <div class="disable"> &lt;div class="circle"&gt;<br>
          &nbsp;&nbsp;&lt;div&gt;</div>
          &nbsp;&nbsp;&nbsp; #x1F60E
          <div class="disable"> &nbsp;&nbsp;&lt;/div&gt;<br></div>
          <div class="disable"> &lt;/div&gt;</div>

        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>            
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 14,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class="h3 text-left green pb-4"><b>Check</b></P>
      <p class='h3 pb-5'>Do you see the emoji you added<br><br> isn't it too small and out of place?</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 15,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class='h1 text-center'>Lets fix that.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 16,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',
    'html_content' : `
    <div>
    <p class='slide-header h6'>BRIEFING 1 of 1 : font-size: em;</p>
    </div>
    <div>
    <p class='h3 text-center aqua pt-4'>font-size: em;</p><br>
    <p class='h3 text-center white pb-5'><b>em</b> is a simple way to make fonts larger.</p>
    <p class='pb-2 text-left'>Example:</p>
    <div class='html-code-box '>
    font-size: 8em;
    </div>
    </div>

    <div class='slide-footer tips text-left'>
    <div><span class='red'>TIP:</span> 1 em = 16px</div>
  </div>   
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
{ 
    'slide_number' : 17,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 3,
    'reg' : [ '((.|\n)\s*)<style>((.|\n)*)\s*(.emoji)((.|\n)*)\s*{((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(8em;)((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>CODE:</p>
          <ul class="list-none">
          <li class='tasks'>Create a new CSS Class: .emoji and increase the font size to 8 em.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
		  .emoji {<br>
        &nbsp;font-size: 8em;<br>
      }
		 
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 18,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 4,
    'reg' : [ '((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*emoji((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>CODE:</p>
          <ul class="list-none">
          <li class='tasks'>Link the .emoji class to the new &lt;div&gt;.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
          <span class="disable"> &lt;div class="circle"&gt;<br>
          &nbsp;&nbsp;&lt;div</span> class="emoji"<sapn class="disable">&gt;<br>
          &nbsp;&nbsp;&nbsp; #x1F60E,<br>
          &nbsp;&nbsp;&lt;/div&gt;<br>
          &lt;/div&gt;</span>

        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 19,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class="h3 text-left green pb-4"><b>Check</b></P>
      <p class='h3 pb-5'>How does it looks now?<br><br>Are you happy with it?</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{ 
    'slide_number' : 20,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'>That was fun,<br><br>Now let's improve the section titles,</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  
  {          
    'slide_number' : 21,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
          <div>
            <p class='slide-header h2'>Mission</p>
          </div>
    <div>
      <p class='h3 pb-4'>Put an emoji in front of each section title</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>This mission will include 2 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 22,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'>Put an emoji inside each section of your CV.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  
  { 
    'slide_number' : 23,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 5,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(&#9737)((.|\n)*)\s*(DETAILS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 2</p>
      </div>
      <div>
      <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Insert an emoji before your h3 text.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
          <div class="disable"> &lt;h3&gt;</div>
          &nbsp;<span>&</span>#9737; <span class="disable">DETAILS</span>
          <div class="disable"> &lt;/h3&gt;</div>

        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
             
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  { 
    'slide_number' : 24,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 6,
    'reg' : [ '((.|\n)\s*)<h3>((.|\n)*)\s*(&#[0-9][0-9][0-9][0-9])((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 2</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Here are suggested ones of the rest:</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
        <span>&</span>#9737; ABOUT ME<br>
        <span>&</span>#9998; EDUCATION<br>
        <span>&</span>#10004; EXPERIENCE<br>
        <span>&</span>#9917; SKILLS<br>
        <span>&</span>#9993; CONTACT<br>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
              
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 25,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class="h3 text-left green pb-4"><b>Check</b></P>
      <p class='h3 pb-5'>Do you like how it looks</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  { 
    'slide_number' : 26,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'>If not, here are few others to play with... </p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, 

  {
    'slide_number' : 27,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : '',
    'html_content' : `
    <div>
      <p class='slide-header pink h2'>REFFERENCE</p>
      <p class='slide-header h6'>BROWSER EMBEDDED EMOJIES</p>
    </div>
    <div>
      <p class='h6 text-left'>SET 1</p>
      <table class="table step-table border">
    <thead>
      <tr class="dark-yellow">
        <th class="border-right">Emoji</th>
        <th>Code:</th>
      </tr>
    </thead>
    <tbody>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si1.png' alt=''> </td>
        <td>9749</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si2.png' alt=''></td>
        <td>9835</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si3.png' alt=''></td>
        <td>9734</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si4.png' alt=''></td>
        <td>9728</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si5.png' alt=''></td>
        <td>9786</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si6.png' alt=''></td>
        <td>9917</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si7.png' alt=''></td>
        <td>10048</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si8.png' alt=''></td>
        <td>9748</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si9.png' alt=''></td>
        <td>infine;</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si10.png' alt=''></td>
        <td>10140</td>
      </tr>
    </tbody>
  </table>
    </div>
    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  {
    'slide_number' : 28,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : '',
    'html_content' : `
   
    <div>
      <p class='h6 text-left'>SET 2</p>
      <table class="table step-table border">
    <thead>
      <tr class="dark-yellow">
        <th class="border-right">Emoji</th>
        <th>Code:</th>
      </tr>
    </thead>
    <tbody>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si11.png' alt=''></td>
        <td>10084</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si12.png' alt=''></td>
        <td>10045</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si13.png' alt=''></td>
        <td>10047</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si14.png' alt=''></td>
        <td>10014</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si15.png' alt=''></td>
        <td>9889</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si16.png' alt=''></td>
        <td>9898</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si17.png' alt=''></td>
        <td>9899</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si18.png' alt=''></td>
        <td>9888</td>
      </tr>
      <tr class="egg-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si19.png' alt=''></td>
        <td>10010</td>
      </tr>
      <tr class="light-yellow">
        <td class="border-right"><img class='swiper-lazy mt-1 m-b1' data-src='../img/emoji/72/si20.png' alt=''></td>
        <td>Copy;</td>
      </tr>
    </tbody>
  </table>
    </div>
    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 29,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'>Now let's finish off this training with tweaking our SKILLS list.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  {          
    'slide_number' : 30,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
          <div>
            <p class='slide-header h2'>Mission</p>
          </div>
    <div>
      <p class='h3 pb-4'>Replace bullet points with emojies.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>This mission will include 2 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 31,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 7,
    'reg' : [ '((.|\n)\s*)<style>((.|\n)*)\s*(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*{((.|\n)*)\s*(list-style)((.|\n)*)\s*:((.|\n)*)\s*(none)((.|\n)*)\s*;((.|\n)*)\s*(padding-left)((.|\n)*)\s*:((.|\n)*)\s*(50px)((.|\n)*)\s*;((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.3em)((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 2</p>
      </div>
      <div>
        <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add <span class="inline-code">list-style: none;</span> to the p, ul CSS. This removes the black bullet-points.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box '>
          <div class="disable">p,ul {</div>
          <div>list-style: none;</div>
          <div class="disable">padding-left: 50px;</div>
          <div class="disable">font-size: 1.3em;  </div>
          <span class="disable">}</span>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>         
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 32,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class="h3 text-left green pb-4"><b>Check</b></P>
      <p class='h3 pb-5'>How does it looks without the bullet-points?</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 33,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 8,
    'reg' : [ '((.|\n)\s*)<li>((.|\n)*)\s*(&#9898)((.|\n)*)\s*<\/li>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 2</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add <b><span>&</span>#9898</b> in front of each item list</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
        &lt;li&gt;<span>&</span>#9898; Awesome at...&lt;/li&gt;<br>
        &lt;li&gt;<span>&</span>#9898; Dance like...&lt;/li&gt;<br>
        &lt;li&gt;<span>&</span>#9898; Level 5...&lt;/li&gt;<br>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>         
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 34,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class="h3 text-left green pb-4"><b>Check</b></P>
      <p class='h3 pb-5'>Do you prefer this look?</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  { 
    'slide_number' : 35,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
      <p class='h3 pb-5'>We're going to finish now with a very simple, but important "Special character".</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  {   
    'slide_number' : 36,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   

    <div>
    <p class='slide-header h6'>BRIEFING 4 of 4 : COPYRIGHT</p>
  </div> 
  <div class=''>
   <p class='h2 pb-4 aqua'>COPYRIGHT</p>
   <p class='h3 pb-2 text-center'>Tell everyone that the content you;ve written is your original work.</p>
   <p class='pb-4 text-left'>Example:</p>
   <div class="example-box">
    <p class="h3">  <img class='swiper-lazy' data-src='../img/emoji/72/si21.png' alt=''> = <span class="pink">&</span>copy</p>
   </div>
   <div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  { 
    'slide_number' : 37,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    <div>
    <p class='slide-header h6'>BRIEFING 1 of 1 : Copyright</p>
    </div>
    <div>
  
    <p class='pb-2 text-left'>It could look like this:</p>
    <div class='html-code-box '>
    <p class="h5 text-left mt-1 mb-1 white">&lt;br&gt;<br>
      &copy Copyright 2021 Thandi Ndlovu Inc.
    </p>
    </div>
    </div>

    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  { 
    'slide_number' : 38,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 9,
    'reg' : [ '((.|\n)*)\s*<br>((.|\n)*)\s*(&copy;)((.|\n)*)\s*Copyright((.|\n)*)\s*[0-9][0-9][0-9][0-9]((.|\n)*)\s*[a-z][a-z]((.|\n)*)\s*<\/body>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 4</p>
      </div>
      <div>
        <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a copyright lineat the end of your website right before &lt;/body&gt;.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
      <span class="disable">...</span><br>
      <span>&</span>copy; Copyright 2021 Thandi NdlovuInc.<br>
      <span class="disable">&lt;body&gt;</span>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>         
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  { 
    'slide_number' : 39,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'html_content' : `
    
    <div>
  
    <p class='h3 text-center'>Here's a quick way to centeryour copyright line:</p>
  
    </div>

    
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  { 
    'slide_number' : 40,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 10,
    'reg' : [ '((.|\n)*)\s*<center>((.|\n)*)\s*(&copy)((.|\n)*)\s*Copyright((.|\n)*)\s*[0-9][0-9][0-9][0-9]((.|\n)*)\s*[a-z][a-z]((.|\n)*)\s*<\/center>((.|\n)*)\s*<\/body>((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 4</p>
      </div>
      <div>
        <div class='text-left pb-3'>
        <p class='blue text-uppercase'>Code:</p>
        <ul class='list-none'>
          <li class='tasks'>Add &lt;center&gt; tags around your copyright text.</li>
        </ul>
      </div>
      <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
      <span class="disable">...</span><br>
      &lt;center&gt;<br>
      <div class="disable"><span>&</span>copy; Copyright2021 Thandi Ndlovu Inc.</div>
      &lt;/center&gt;<br>
      <span class="disable">&lt;/body&gt;</span>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>         
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  

  {   
    'slide_number' : 41,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   
  <div class=''>
   <p class='h2 green pb-4 bold'><b>WOW</b></p>
   <p class='h3 pb-2 text-center'>Good job!</p>
   <p class='h3 pb-2 text-center'>Just double- check your code now.</p>

   <div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  
  {   
    'slide_number' : 42,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
	  
	  <p class="fs75">Your code should look like this:</p>
    </div>
	<div class='html-code-box'>
	<span class='html-code'>
	...<br>
	&nbsp<span>&</span>#10004 EXPERIENCE<br>
  &lt;/h3&gt<br>
  &lt;p&gt Stuff.... &lt;/p&gt<br>
  &lt;h3&gt<br>
	&nbsp<span>&</span>#9917 SKILLS<br>
  &lt;/h3&gt<br>
  &lt;p&gt Stuff.... &lt;/p&gt<br>
  &lt;h3&gt<br>
	&nbsp<span>&</span>#9898 LIST STUFF<br>
  &lt;/h3&gt<br>
  &lt;p&gt Stuff.... &lt;/p&gt<br>
  &lt;h3&gt<br>
	&nbsp<span>&</span>#9993 CONTACT DETAILS<br>
  &lt;/h3&gt<br>
  &lt;p&gt Stuff.... &lt;/p&gt<br>
  &lt;center&gt<br>
  &nbsp&lt;br&gt<span>&</span>copy Copyright 2018 ...<br>
  &lt;/center&gt


    
	</span>
	
      </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },
  {   
    'slide_number' : 43,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   
  <div class=''>
   <p class='h2 pb-4 '>You just finished project2.</p>
   <p class='h2 pb-2 text-center bold'>That's a <span class="aqua">MAJOR AWESOME</span> accomplishment.</p>


   <div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  {   
    'slide_number' : 44,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   
  <div class=''>
   <p class='h2 pb-4 '>Well done!</p>
   <p class='h3 pb-2 text-center bold aqua'>TIME TO CELEBRATE.</p>


   <div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },
  
  {   
    'slide_number' : 45,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   
  <div class=''>
   <p class='h2 pb-4 '>If you enjoyed Prjoect 2...</p>
   <p class='h3 pb-2 text-center '>You'll <strong class="aqua">LOVE</strong> Project 3.</p>


   <div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  {   
    'slide_number' : 46,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
   
  <div class=''>
   <p class='h3 pb-4 '>See you there.</p>
   <div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  },

  {   
    'slide_number' : 47,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h0  pb-4 yellow'>BYE.</div>
      <div class='h3'>See you soon.
</div>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, ]
}
    
var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 
 
/// Add custom JS for lesson below here


function previewLessonModal() {
  $.galleryDialog({
    modalName: 'preview-lesson-modal',
    htmlContent: `
    <iframe  srcdoc="
<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
  </style>
</head>
<body>
<header>
    <h1>My Name</h1>
      <h3>Motivation:
        <br><br>
        <i>I want to become a coder</i>
      </h3>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
</header>
</body>

  "></iframe>
    `
  })
}