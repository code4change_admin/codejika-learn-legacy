var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`



`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P2Training6", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 3, Training 1",
    "save_lesson_id": "P3Training1", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 8,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "<head(.*)>[\\s\\r\\n]*<style(.*)>[\\s\\r\\n]*</style(.*)>[\\s\\r\\n]*</head(.*)>" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 1</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Add this section:
			<div class="html-code-box">
              &lt;head&gt;<br>&nbsp;&lt;style&gt;<br>&nbsp;&lt;/style&gt;<br>&lt;/head&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span></span>
          
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "<body(.*)>[\\s\\r\\n]*<header(.*)>[\\s\\r\\n]*</header(.*)>[\\s\\r\\n]*<section(.*)>[\\s\\r\\n]*</section(.*)>[\\s\\r\\n]*</body(.*)>" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 2</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Now Add <b>&lt;header&gt;</b> and <b>&lt;section&gt;</b> in <b>&lt;body&gt;</b>
			<div class="html-code-box">
              &lt;body&gt;<br>&nbsp;&lt;header&gt;<br>&nbsp;&lt;/header&gt;<br>&nbsp;&lt;section&gt;<br>&nbsp;&lt;/section&gt;<br>&lt;/body&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span></span>
         
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 12,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide12.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 13,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "<header(.*)>[\\s\\r\\n]*<h1>[\\s\\r\\n]*([A-Z]+|[a-z]+)((.|n*)*)[\\s\\r\\n]*</h1>[\\s\\r\\n]*</header(.*)>" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 1</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Add an <b>&lt;h1&gt;</b> in <b>&lt;header&gt;</b> with your compnay name or brand.
			<div class="html-code-box">
             
			  &lt;h1&gt;<br>&nbsp;Awesome Industries Inc.<br>&lt;/h1&gt;
			  
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span></span>
       
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 14,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "</h1(.*)>[\\s\\r\\n]*<h4>[\\s\\r\\n]*([A-Z]+|[a-z]+)((.|n*)*)[\\s\\r\\n]*</h4>" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 2</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Under <b>&lt;h1&gt;</b> place an <b>&lt;h4&gt;</b> with your company tagline.
			<div class="html-code-box">
              &lt;h4&gt;<br>&nbsp;Cute Cupcakes for Besties.<br>&lt;/h4&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span></span>
         
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide16.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "<style(.*)>[\\s\\r\\n]*body[\\s\\r\\n]*{[\\s\\r\\n]*font-family[\\s\\r\\n]*:[\\s\\r\\n]*tahoma;[\\s\\r\\n]*text-align[\\s\\r\\n]*:[\\s\\r\\n]*center;[\\s\\r\\n]*}" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 3</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Add a body selecter in CSS with these styles:
			<div class="html-code-box">
              font-family: tahoma;<br>
			  text-align: center;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>TIP:</span>
          <b>body { }</b>
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide18.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 19,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "[\\s\\r\\n]*header[\\s\\r\\n]*{[\\s\\r\\n]*letter-spacing[\\s\\r\\n]*:[\\s\\r\\n]*6px;[\\s\\r\\n]*background[\\s\\r\\n]*:[\\s\\r\\n]*royalblue;[\\s\\r\\n]*padding[\\s\\r\\n]*:[\\s\\r\\n]*20px;[\\s\\r\\n]*}" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 4</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Add <b>header { }</b> with a background color and some padding:
			<div class="html-code-box">
              letter-spacing: 6px;<br>
			  background: royalblue;<br>
			  padding: 20px;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span></span>
          
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "[\\s\\r\\n]*header[\\s\\r\\n]*{[\\s\\r\\n]*((.|\n)*)color[\\s\\r\\n]*:[\\s\\r\\n]*white;((.|\n)*)[\\s\\r\\n]}" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 5</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Make the text in <b>header { }</b> white with a CSS rule.
			
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>TIP:</span>
          <b>color: ???</b>
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 22,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide22.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 23,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide23.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide24.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 25,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "<section(.*)>[\\s\\r\\n]*<h2>[\\s\\r\\n]*WELCOME[\\s\\r\\n]*:[\\s\\r\\n]*</h2>[\\s\\r\\n]*</section(.*)>[\\s\\r\\n]*" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 1</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Add an <b>&lt;h2&gt;</b> with this text in it:
			<div class="html-code-box">
			  WELCOME:
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>WHERE:</span>
          Inside <b>&lt;section&gt;</b>
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "<section(.*)>[\\s\\r\\n]*<h2>[\\s\\r\\n]*WELCOME[\\s\\r\\n]*:[\\s\\r\\n]*</h2>[\\s\\r\\n]*VISION[\\s\\r\\n]*:[\\s\\r\\n]*([A-Z]+|[a-z]+)((.|n*)*)[\\s\\r\\n]*</section(.*)>[\\s\\r\\n]*" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 2</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Write a short Vision Statement. <br>Here is an example:
			<div class="html-code-box">
              VISION: to create a beautiful cupcakes for clients in the Randburg area.
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>TIP:</span>
          Below the "WELCOME" h2 tag.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 10,  
      "reg" : [ "<section(.*)>[\\s\\r\\n]*<h2>[\\s\\r\\n]*WELCOME[\\s\\r\\n]*:[\\s\\r\\n]*</h2>[\\s\\r\\n]*((.|\n)*)<p>((.|\n)*)[\\s\\r\\n]*VISION[\\s\\r\\n]*:[\\s\\r\\n]*([A-Z]+|[a-z]+)((.|n*)*)[\\s\\r\\n]*</p>[\\s\\r\\n]*((.|\n)*)</section(.*)>((.|\n)*)" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 3</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Insert your vision statement in a <b>&lt;p&gt;</b>.
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>WHERE:</span>
          After the welcome <b>&lt;h2&gt;</b> closing tag and still inside the section.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 11,  
      "reg" : [ "</p>[\\s\\r\\n]*<br>[\\s\\r\\n]*<br>[\\s\\r\\n]*</section(.*)>[\\s\\r\\n]*" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 4</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Insert two <b>&lt;br&gt;</b> tags.
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>WHERE:</span>
          After the VISION section. After the closing tag <b>&lt;/p&gt;</b>.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 32,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 12,  
      "reg" : [ "[\\s\\r\\n]*<br>[\\s\\r\\n]*<br>[\\s\\r\\n]*ABOUT US[\\s\\r\\n]*:[\\s\\r\\n]*([A-Z]+|[a-z]+)((.|n*)*)[\\s\\r\\n]*</section(.*)>[\\s\\r\\n]*" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 5</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Create an "About Us" explanation.<br>
			Here is an example:
			<div class="html-code-box">
              ABOUT US: Founded in 2008, by Thandi Ndlovu. <br>
			  We've served over 100 clients and delivered over 10,000 tasty treats.
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>WHERE:</span>
          After the two <b>&lt;br&gt;s</b>.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 13,  
      "reg" : [ "[\\s\\r\\n]*<br>[\\s\\r\\n]*<br>[\\s\\r\\n]*<p>[\\s\\r\\n]*ABOUT US[\\s\\r\\n]*:[\\s\\r\\n]*([A-Z]+|[a-z]+)((.|n*)*)[\\s\\r\\n]*</p>[\\s\\r\\n]*</section(.*)>[\\s\\r\\n]*" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 6</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Add the "About Us" content in a <b>&lt;p&gt;</b>.
          
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>WHERE:</span>
          After the VISION and two <b>&lt;br&gt;s</b>.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 34,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 14,  
      "reg" : [ "((.|\n)*)h2[\\s\\r\\n]*{[\\s\\r\\n]*((.|\n)*)font-size[\\s\\r\\n]*:[\\s\\r\\n]*2em;((.|\n)*)[\\s\\r\\n]*width((.|\n)*)[\\s\\r\\n]*:((.|\n)*)[\\s\\r\\n]*100%;((.|\n)*)[\\s\\r\\n]*}" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 7</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Style h2 with a <b>font-size: 2em;</b> and with <b>width 100%</b>.
			
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>WHERE:</span>
          The <b>width: 100%;</b> rule will make sure that the text does not wrap around other content.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 15,  
      "reg" : [ "((.|\n)*)section[\\s\\r\\n]*{[\\s\\r\\n]*((.|\n)*)padding[\\s\\r\\n]*:[\\s\\r\\n]*30px;((.|\n)*)[\\s\\r\\n]*margin-bottom((.|\n)*)[\\s\\r\\n]*:((.|\n)*)[\\s\\r\\n]*40px;((.|\n)*)[\\s\\r\\n]*}" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 8</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>
            Give the <b>&lt;section&gt;</b>s <b>30px padding</b> and <b>margin-bottom: 40px;</b>.
			
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>TIP:</span>
          <b>section { }</b>
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 37,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide37.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide39.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P003-T01-D-V001/img/Slide40.PNG">
        <a href="/learn/P3Training2" class="btn btn-primary" style="top:65%;">Start next training →</a>        
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}
    
var hintsForCheckPonts = {
  /* 12:"Inside the &lt;head&gt; <strong>Type Code Here</strong> &lt;\/head&gt; tags",
  22:"Inside the &lt;style&gt; <strong>Type Code Here</strong> &lt;\/style&gt; section",
  23:"Remember to wrap your content inside the curley braces h1<strong>{Type Code Here}</strong>" */
}
  
var hints_data =   `



    ` 
 
/// Add custom JS for lesson below here