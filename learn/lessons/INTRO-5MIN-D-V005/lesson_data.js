var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`



`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to make your first website in only 5 minutes with this easy intro lesson to coding.", 
    "pageKeywords": "coding, code, 5-minute-website, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - 5 Minute Website",
    "save_lesson_id": "5-minute-website", // This is id that will be used to store save code in Firebase
    "cert_awarded_at": 5, // the checkpoint when to mark lesson as complete
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "<h1(.*)>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint checkpoint-first-slide">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> STEP 1 OF 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            <span class="challenge-content">Hi,... &#128075;<br>
			Type <span class="html-code-box"><strong>&lt;h1&gt;</strong></span> in the editor below.
			</span>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         DESCRIPTION: Add <strong>&lt;h1&gt;</strong>
                                                                                                                                      
        </div>
		 <img src="https://code.org/api/hour/begin_codejika.png" alt="HoC tracking pixel"> 
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "((.|\n)*)\s*<h1(.*)>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\\/h1>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> STEP 2 OF 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Type <strong>your name</strong> and a <strong>&lt;h1&gt;</strong> closing tag.
			</span>
            <div class="html-code-box">
             &lt;h1&gt; Joey Green &lt;\/h1&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
        TIP: Notice the closing tag has \/ in it.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "((.|\n)*)\s*<body(.*)>((.|\n)*)\s*<h1(.*)>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/h1>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> STEP 1 OF 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Add a &lt;body&gt; tag.<br></span>
            <div class="html-code-box">
             &lt;body&gt;<br>
			 <span class="code-fade">&lt;h1&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         TIP: <strong>&lt;body&gt;</strong> Needs to be on top, so push the <strong>&lt;h1&gt;</strong> down to line 2.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "((.|\n)*)\s*<body(.*)\s*style\\s*=\\s*\"\\s*(.*)\"\\s*>((.|\n)*)\s*<h1(.*)>((.|\n)*)\s*[a-z][a-z][a-z]((.|\n)*)\s*<\/h1>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> STEP 2 OF 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Add a <strong>style\=\"\"</strong> inside the <strong>&lt;body&gt;</strong> tag.<br>
			Before the <strong>&gt;</strong> symbol.</span>
            <div class="html-code-box">
             <span class="code-fade">&lt;body</span> <span style="color: #00ff43;">style=\" \"</span><span class="code-fade">&gt;<br>
			 &lt;h1&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         TIP: No spaces between style, = & " .
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide12.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "style[\\s\\r\\n]*=[\\s\\r\\n]*\"(.*)[\\s\\r\\n]*(background|background\-image):[\\s\\r\\n]*(.*)(pink)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> STEP 3 OF 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Add a <strong>background: pink;</strong> inside the style "quotes".
			Before the &gt; symbol.</span>
            <div class="html-code-box">
             <span class="code-fade">&lt;body</span> <span style="color: #00ff43;">style=\"</span><strong>background: pink;</strong><span style="color: #00ff43;">\"</span><span class="code-fade">&gt;<br>
			 &lt;h1&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
        TIP:  There's a colon (:) and a semicolon (;) at the end.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide16.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,
      "reg" : [ "body(.*)background:[\\s\\r\\n]*(?!pink;)[a-z]" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">BONUS</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Change <strong>pink</strong> to red or any other color you like.</span>
			
			 <div class="html-code-box">
             <span class="code-fade" >&lt;body <span style="color: #00ff43;">style</span>=\"background:</span> <span style="color:red;">red</span><span class="code-fade">;\"&gt;<br>
			 &lt;h1&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "html_content" : `
      <div class="" style=" text-align: center;">
       <h2 class='lesson-title font-weight-normal mb-2 mt-3' style='font-size: 2.5em;line-height: 1.6em;'>GREAT WORK!</h2>
       <p class='lesson-title font-weight-normal pl-4 pr-4' style='font-size: 1.6em;line-height: 1.1em;'>Click below to download your Hour of Code certificate</p>
       <div class='d-none'> <img class='w-15 swiper-lazy mb-3' data-src='../img/emoji/72/smiling-face-with-sunglasses.png' alt='' style='width: 4em !important;'> </div>
       <div>
		<a class="btn btn-primary swiper-next mb-3" href='https://code.org/api/hour/finish' target='_blank' style='position: unset;  left: unset;  transform: unset;'>Get Certificate 😎</a> <br>
         <a class="btn btn-primary skip check swiper-next"  style='position: unset;  left: unset;  transform: unset;'>Next →</a>
          </div>
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide18.PNG">
		<a class="btn btn-primary swiper-next" style="top:35%;">ADVANCED BONUS SECTION</a> <br>
		<a href="/learn/P1Training1" class="btn btn-primary" style="top:55%;">START PROJECT 1</a><br>
		<a href="/learn/projects" class="btn btn-primary" style="top:75%;">PROJECTS PAGE</a>
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,
      "reg" : [ "<h1 [\\s\\r\\n]*style[\\s\\r\\n]*=[\\s\\r\\n]*\"[\\s\\r\\n]*font-size[\\s\\r\\n]*:[\\s\\r\\n]*100px[\\s\\r\\n]*;(.*)[\\s\\r\\n]*\"[\\s\\r\\n]*>[\\s\\r\\n]*(.*)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint bonus-challenge">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">BONUS: 1 OF 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Insert this inside the <strong>&lt;h1&gt;</strong> tag:<br>
			<strong>style=\"font-size: 100px;\"</strong></span>
			 <div class="html-code-box">
             <span class="code-fade">&lt;body style=\"background: red;\"&gt;<br>
			 &lt;h1 </span>style=\"font-size: 100px;\"<span class="code-fade">&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         <strong>This section of code:</strong> Increases the size of the text.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
     "slide_number" : 23,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "<h1 [\\s\\r\\n]*style[\\s\\r\\n]*=[\\s\\r\\n]*\"[\\s\\r\\n]*font-size[\\s\\r\\n]*:[\\s\\r\\n]*100px[\\s\\r\\n]*;[\\s\\r\\n]*color[\\s\\r\\n]*:[\\s\\r\\n]*blue;(.*)[\\s\\r\\n]*\"[\\s\\r\\n]*>[\\s\\r\\n]*(.*)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint bonus-challenge">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">BONUS: 2 OF 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Add this style to change text color:<br>
			<strong>color: blue;</strong>
			<strong>style=\"font-size: 100px;\"</strong></span>
			 <div class="html-code-box">
             <span class="code-fade">&lt;body style=\"background: red;\"&gt;<br>
			 &lt;h1 </span><span style="color: #00ff43;">style=\"</span><span class="code-fade">font-size: 100px;</span> color: blue;<span style="color: #00ff43;">\"</span><span class="code-fade">&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>TIP:</span>
         Keep they styling between the \"quotes\".
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "<h1 [\\s\\r\\n]*style[\\s\\r\\n]*=[\\s\\r\\n]*\"[\\s\\r\\n]*font-size[\\s\\r\\n]*:[\\s\\r\\n]*100px[\\s\\r\\n]*;[\\s\\r\\n]*color[\\s\\r\\n]*:[\\s\\r\\n]*blue;[\\s\\r\\n]*text-align[\\s\\r\\n]*:[\\s\\r\\n]*center;(.*)[\\s\\r\\n]*\"[\\s\\r\\n]*>[\\s\\r\\n]*(.*)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint bonus-challenge">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">BONUS: 3 OF 3</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Finally, center your text with this:<br>
			<strong>text-align: center;</strong></span>
			 <div class="html-code-box">
             <span class="code-fade">&lt;body style=\"background: red;\"&gt;<br>
			 &lt;h1 </span><span style="color: #00ff43;">style=\"</span><span class="code-fade">font-size: 100px; color: blue;</span> text-align: center;<span style="color: #00ff43;">\"</span><span class="code-fade">&gt; Joey Green &lt;\/h1&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span>TIP:</span>
         Keep they styling between the \"quotes\".
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide24.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 10,  
      "reg" : [ "<body [\\s\\r\\n]*style[\\s\\r\\n]*=[\\s\\r\\n]*\"[\\s\\r\\n]*background[\\s\\r\\n]*:[\\s\\r\\n]*linear-gradient\\((.*)\\)[\\s\\r\\n]*;[\\s\\r\\n]*\"[\\s\\r\\n]*>[\\s\\r\\n]*(.*)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint bonus-challenge">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">BONUS: 1 OF 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Replace the background color from "red" to 
			linear-gradient()</span>
			 <div class="html-code-box">
             <span class="code-fade">&lt;body style=\"background:</span> linear-gradient()<span class="code-fade">;\"&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         <strong>This section of code:</strong> Creates a beautiful mixture of colors.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 11,  
      "reg" : [ "<body [\\s\\r\\n]*style[\\s\\r\\n]*=[\\s\\r\\n]*\"[\\s\\r\\n]*background[\\s\\r\\n]*:[\\s\\r\\n]*linear-gradient\\([\\s\\r\\n]*(0deg|90deg|110deg)[\\s\\r\\n]*,[\\s\\r\\n]*yellow[\\s\\r\\n]*40%[\\s\\r\\n]*,[\\s\\r\\n]*pink[\\s\\r\\n]*40%[\\s\\r\\n]*\\)[\\s\\r\\n]*;[\\s\\r\\n]*\"[\\s\\r\\n]*>[\\s\\r\\n]*(.*)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint bonus-challenge">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">BONUS: 2 OF 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Insert a cool diagonal separation(<span style="color: #3300ff;">110deg,</span> ) and some beautiful colors:<br>
			<span style="color: #3300ff;">yellow 40%, pink 40%</span>
			linear-gradient()</span>
			 <div class="html-code-box">
             <span class="code-fade">&lt;body style=\"background:</span> linear-gradient(<span style="color: #00ff43;">110deg,</span> <span style="color: #00ff43;">yellow 40%, pink 40%</span>)<span class="code-fade">;\"&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
         Don't add any spaces unless shown.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      
      "slide_number" : 30,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 12,  
      "reg" : [ "<body [\\s\\r\\n]*style[\\s\\r\\n]*=[\\s\\r\\n]*\"[\\s\\r\\n]*background[\\s\\r\\n]*:[\\s\\r\\n]*linear-gradient\\([\\s\\r\\n]*(0deg|90deg)[\\s\\r\\n]*,[\\s\\r\\n]*yellow[\\s\\r\\n]*40%[\\s\\r\\n]*,[\\s\\r\\n]*pink[\\s\\r\\n]*40%[\\s\\r\\n]*\\)[\\s\\r\\n]*;[\\s\\r\\n]*\"[\\s\\r\\n]*>[\\s\\r\\n]*(.*)" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">PLAY: 1 OF 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Try changing <strong>110deg</strong> to 90 or 0.</span>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
       "slide_number" : 31,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 13,  
      "reg" : [ ",[\\s\\r\\n]*(?![\\s\\r\\n]*pink[\\s\\r\\n]*|[\\s\\r\\n]*yellow[\\s\\r\\n]*)(.*)[\\s\\r\\n]*\"" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;">PLAY: 2 OF 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
			<span class="challenge-content2">Try playing with the colors of  the linear-gradient.</span>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 
		<div class="challenge-tip"><span></span>
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : "" 
    },{
      "slide_number" : 32,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/INTRO-5MIN-D-V004/img/Slide31.PNG">
		<a href="#" class="btn btn-primary d-none" style="top:35%;width: 50%;max-width: 400px;">SHARE TO FACEBOOK</a> <br>
		<a href="#" class="btn btn-primary" style="top:45%;width: 50%;max-width: 400px;" id='gallery' >SHARE TO GALLERY</a><br>
		<a href="/learn/P1Training1" class="btn btn-primary" style="top:65%;width: 50%;max-width: 400px;">START PROJECT 1</a>
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }

	]
}

var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 

/// Add custom JS for lesson below here