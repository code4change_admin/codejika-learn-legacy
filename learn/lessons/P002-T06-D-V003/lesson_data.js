var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`
<head>
	<style>
		header{
		background: lightblue;
		border: solid blue;
		border-width: 10px 0px 10px 0px;
		margin: 0 auto;
		text-align: center;
		}

		h1{
		font-size: 3em;
		}

		.circle{
		height: 250px;
		width: 250px;
		background-color: blue;
		border-radius: 50%;
		border: 10px solid white;
		display: inline-block;
		}

		h3{
		background-color: #ce54c7;
		padding: 15px;
		font-size: 1.5em;
		border-left: 10px solid #d6d5cd;
		}

		header, h3{
		color: white;
		font-family: "Arial Black",Gadget, sans serif;
		}

		body{
		font-family : helvetica , arial ,san serif;
		}

		p, ul{
		padding-left: 50px;
		font-size: 1.3em;
		}
	</style>
</head>

<body>
	<header>
		<div class="circle">
		</div>

		<h1>My Name</h1>

		<i>
		Aspiring Software Developer
		</i>
		<br><br>
	</header>
		<br>
		<h3>DETAILS</h3>
		<p>Date of Birth: 01 February 2000</p>
		<p>School: My School</p>
		<p>Grade: My Grade</p>

		<h3>ABOUT ME</h3>
		<p>I love coding...</p>
		<p>Grade: 8</p>


		<h3>EDUCATION</h3>
		<p>2000-2010: <br> Lim Primary School, <br> Free State.</p>
		<p>2011-2012: <br> Diep Sec School, <br> Johannesburg.</p>
		<p>2012-2019: <br> Jika, <br> Codeverse.</p>

		<h3>SKILLS</h3>
		<ul>
			<li>Coding</li>
			<li>Drawing</li>
			<li>Singing</li>
		</ul>

		<h3>CONTACT</h3>
		<p>Address: Diep, Randburg, 4456 Liberia</p>
		<p>Phone: 0123456789</p>
		<p>Email: myname@gmail.com</p>

        
</body>


`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P2Training5", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Present your skills and plans in an attractive and engaging Website CV built with HTML, CSS and emojies.", 
    "pageKeywords": "coding, code, learn to code, code website, CV website", 
    "pageTitle": "CodeJIKA - Project 2, Training 6",
    "save_lesson_id": "P2Training6", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 12,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
             Add a <u>new</u> div.
            <div class="html-code-box">
              <span class="code-fade">&lt;div class=\"circle\"&gt; <br></span>
                &nbsp;&nbsp;&lt;div&gt; <br>
                &nbsp;&nbsp;&lt;/div&gt; <br>
              <span class="code-fade"> &lt;/div&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>   
		<div class="challenge-tip"><span>Where:</span>
          <u>Inside</u> the <span class="html-code">&lt;div class=\"circle\"&gt;</span>
		</div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 13,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Place an emoji into the new &lt;div&gt;.<br>
			Use this one: &amp;#x1F60E; <br>
            <div class="html-code-box">
              <span class="code-fade">&lt;div class=\"circle\"&gt; <br>
                &nbsp;&nbsp;&lt;div&gt; <br></span>
                &nbsp;&nbsp;&nbsp;&nbsp;&amp;#x1F60E;<br>
                <span class="code-fade">&nbsp;&nbsp;&lt;/div&gt; <br>
               &lt;/div&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide16.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "((.|\n)\s*)<style>((.|\n)*)\s*(.emoji)((.|\n)*)\s*{((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(8em;)((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Create a new CSS Class: .emoji and increase the font size to 8em.
            <div class="html-code-box">
              .emoji { <br>
               font-size: 8em; <br>
               }
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 18,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*circle((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*<div((.|\n)*)\s*class=\"((.|\n)*)\s*emoji((.|\n)*)\s*\"((.|\n)*)\s*>((.|\n)*)\s*&#x1F60E((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*<\/div((.|\n)*)\s*>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 4 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Link the .emoji class to the new &lt;div&gt;
            <div class="html-code-box">
              <span class="code-fade">&lt;div class=\"circle\"&gt; <br>
                &nbsp;&nbsp;&lt;div </span>class=\"emoji\"<span class="code-fade">&gt; <br>
                &nbsp;&nbsp;&nbsp;&nbsp;&amp;#x1F60E;<br>
                &nbsp;&nbsp;&lt;/div&gt; <br>
               &lt;/div&gt;</span>
            </div>            
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide22.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(&#9737)((.|\n)*)\s*(DETAILS)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Insert an emoji before your h3 text.
            <div class="html-code-box">
              <span class="code-fade">&lt;h3&gt; <br></span>
              &nbsp;&nbsp;<strong>&amp;#9737;</strong><span class="code-fade"> DETAILS<br>
              &lt;/h3&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 24,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "((.|\n)\s*)<h3>((.|\n)*)\s*(&#[0-9][0-9][0-9][0-9])((.|\n)*)\s*(ABOUT ME)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<p>((.|\n)*)\s*<\/p>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Here are suggested ones for the rest:
            <div class="html-code-box">
              
              <strong>&amp;#9737;</strong> ABOUT ME<br>
			  <strong>&amp;#9998;</strong> EDUCATION<br>
			  <strong>&amp;#10004;</strong> EXPERIENCE<br>
			  <strong>&amp;#9917;</strong> SKILLS<br>
			  <strong>&amp;#9993;</strong> CONTACT<br>
             
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "((.|\n)\s*)<style>((.|\n)*)\s*(p)((.|\n)*)\s*(,)((.|\n)*)\s*(ul)((.|\n)*)\s*{((.|\n)*)\s*(list-style)((.|\n)*)\s*:((.|\n)*)\s*(none)((.|\n)*)\s*;((.|\n)*)\s*(padding-left)((.|\n)*)\s*:((.|\n)*)\s*(50px)((.|\n)*)\s*;((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.3em)((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add list-style: none; to the p, ul CSS. This removes the black bullet-points.
            <div class="html-code-box">
              <span class="code-fade">
              p, ul { <br></span>
              &nbsp;&nbsp;list-style: none; <br>
              <span class="code-fade">
              &nbsp;&nbsp;padding-left: 50px; <br>
              &nbsp;&nbsp;font-size: 1.3em; <br>
              }
              </span>
            </div>             
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 32,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "((.|\n)\s*)<li>((.|\n)*)\s*(&#9898)((.|\n)*)\s*<\/li>"],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 2</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add <strong>&amp;#9898</strong> in front of each item list.
            <div class="html-code-box">
              
              &lt;li&gt;<strong>&amp;#9898;</strong> Awesome at...&lt;\/li&gt;<br>
			  &lt;li&gt;<strong>&amp;#9898;</strong> Dance like...&lt;\/li&gt;<br>
			  &lt;li&gt;<strong>&amp;#9898;</strong> Level 5:...&lt;\/li&gt;<br>
			  
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide33.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 34,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide34.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide35.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 37,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "((.|\n)*)\s*<br>((.|\n)*)\s*(&copy;)((.|\n)*)\s*Copyright((.|\n)*)\s*[0-9][0-9][0-9][0-9]((.|\n)*)\s*[a-z][a-z]((.|\n)*)\s*<\/body>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add a copyright line at the end of your website right before &lt;/body&gt;.
            <div class="html-code-box">
              &nbsp;&nbsp;<span class="code-fade">...</span><br>
              &nbsp;&nbsp;&amp;copy; Copyright 2021 Thandi Ndlovu Inc.<br>
              <span class="code-fade">&lt;/body&gt;</span>
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 10,  
      "reg" : [ "((.|\n)*)\s*<center>((.|\n)*)\s*(&copy)((.|\n)*)\s*Copyright((.|\n)*)\s*[0-9][0-9][0-9][0-9]((.|\n)*)\s*[a-z][a-z]((.|\n)*)\s*<\/center>((.|\n)*)\s*<\/body>((.|\n)*)\s*" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 2 of 4</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add &lt;center&gt; tags around your copyright text.
            <div class="html-code-box">
              &nbsp;&nbsp;<span class="code-fade">...</span><br>
              &nbsp;&nbsp&lt;center&gt;<br>           
              <span class="code-fade">&nbsp;&nbsp&nbsp;&nbsp;&amp;copy; Copyright 2021 Thandi Ndlovu Inc.</span><br>
              &nbsp;&nbsp&lt;/center&gt; <br>
              <span class="code-fade">&lt;/body&gt;</span>
            </div>            
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>      
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide40.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide41.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide42.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 43,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide43.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 44,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P002-T06-D-V003/img/AmazingPrizes01.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 45,
      "html_content" : `
      <div class="register-CodeChallenge">
        <img class="swiper-lazy" src="../learn/lessons/P002-T06-D-V003/img/AmazingPrizes02-Register.PNG">
        <div class="swiper-lazy-preloader"></div>
        <div class="btn btn-primary register-button" style="top:70%;">Register Now</div>
      </div>
      <div class="enter-CodeChallenge d-none">
        <img class="swiper-lazy" src="../learn/lessons/P002-T06-D-V003/img/AmazingPrizes02-Submit.PNG">
        <div class="swiper-lazy-preloader"></div>
        <div class="btn btn-primary submit-CodeChallenge" style="top:70%;">Yes, Submit my code</div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 46,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide44.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 47,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide45.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{   
      "slide_number" : 48,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P002-T06-D-V003/img/Slide46.PNG">
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}

var check_points = {
  16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
/*  16:"A closing tag has a forward slash <strong>/<strong>",
  17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
  23:"In the body section. Hint given as tip on bottom of the slide.",
  24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
  32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
  36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"*/
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

` 

/// Add custom JS for lesson below here

function onlessonLoaded() {

  $('.submit-CodeChallenge').click(function() {
    console.log('submitCodeChallengeModal');
    submitCodeChallengeModal('submit-lesson-url');
  }) 
}