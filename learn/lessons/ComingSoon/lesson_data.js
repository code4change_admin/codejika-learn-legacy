var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`


`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "Coming Soon",
    "save_lesson_id": "", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "$('.lessonProgressText, .pagination .text-right .next, .pagination .text-left .prev, .swiper-pagination').html('Start Slideshow').addClass('d-none');",      
      "html_content" : `
        <div class="" style="text-align: center;padding-top: 40px;">
          <h2 style="color: #b5b5b5!important">
            Coming<br>soon!
          </h2>
        </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}
    
var hintsForCheckPonts = {
}
  
var hints_data =   `
    ` 
 
/// Add custom JS for lesson below here