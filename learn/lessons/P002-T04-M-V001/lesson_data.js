var lesson_data = 
{
    'defaultCode': // default code, if user has not already started coding   
`
<head>
	<style>
		header{
		background: lightblue;
		border: solid blue;
		border-width: 10px 0px 10px 0px;
		margin: 0 auto;
		text-align: center;
		}

		h1{
		font-size: 3em;
		}

		.circle{
		height: 250px;
		width: 250px;
		background-color: blue;
		border-radius: 50%;
		border: 10px solid white;
		display: inline-block;
		}
	</style>
</head>

<body>
	<header>
		<div class="circle">
		</div>

		<h1>My Name</h1>

		<i>Aspiring Software Developer</i>
		<br><br>
        </header>
        <br>
        <h3>DETAILS</h3>
        <p>Date of Birth: 01 February 2000</p>
        <p>School: Diepsloot Combined School</p>
        <p>Grade: 08</p>
        
        
</body>

` ,
    'kbLayout': '', // not currently in use
    'loadJS': 'https://cdnjs.cloudflare.com/ajax/libs/geopattern/1.2.3/js/geopattern.min.js"></script>', // not currently in use
    'prevLessonID': 'P2Training3', // Lesson ID of previous lesson where to load user's code
    'nextLessonSlug': '', // not currently in use
    'pageDesc': 'Learn how to build your first website with these easy intro lessons to coding.', 
    'pageKeywords': 'coding, code, learn to code, code website, my first website', 
    'pageTitle': 'CodeJIKA - Project 2, Training 4',
    'save_lesson_id': 'P2Training4', // This is id that will be used to store save code in Firebase
  'slides' : [ {
    'slide_number' : 1,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'intro',      
    'html_content' : `
      <div>    
      	<p class='slide-title h1 mt-5'>TRAINING 4</p>
      </div>      
      <div class=''>
        <p class='h1'>Let's<br>
        <strong class='fs-x2 aqua'>ROCK</strong><br>
        this!</p>
        <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-open-mouth-and-tightly-closed-eyes.png' alt=''>
      </div>      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 2,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='h2 pb-4'><strong>Love colors?</strong></p>
	  <img class='pb-4 mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-heart-shaped-eyes.png' alt=''>
	  <img class='pb-4 mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-heart-shaped-eyes.png' alt=''>
	  <img class='pb-4 mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/rainbow.png' alt=''>
      <p class='h3 pt-4 pb-4'>Well, the wonderful thing about web design is...</p>
    </div>      
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 3,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='h3 pb-4'>...You can use <strong class="pink">ANY color</strong> you want.</p>
      <p class='h3 pt-4 pb-4'>Yes, Even the ones that <strong class="green">don't have names</strong>.</p>
    </div>      
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 4,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'snapshot',    
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Snapshot</p>
  	  <p class='white fs75'>(These are your missions for today.)</p>
  	</div>      
	<div> 
	<p class='h5 pb-4 text-left'>We gonna be learning...</p>	
		<ol class='h4'>
		  <li><span class="pink">Colors</span> have codes.</li>
		  <li><span class="pink">Fonts</span> (text-styles) have families.</li>
		  <li>What is "<span class="pink">responsive</span>" web design.</li>
		</ol>
	</div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 5,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='h3 pb-4 text'>This is how far you've come.</p>
      <p class='h3 pt-4 pb-4'><span class="green">Two trainings left</span>, then you're done with P2.</p>
    </div>      
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 6,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'><strong>Use colors</strong> in CSS.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 6 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 7,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : Website Structure <br>(1 of 4) HTML Colors</p>
    </div> 
    <div>
      <p class='h4 pb-4 blue'><strong>HTML Colors:</strong></p>
      <p class='pb-4'>A hexadecimal color (6-Numbers) is specified with:</p>
      <img class='w-0 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/rgb-colors.PNG' alt=''>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 8,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : Website Structure <br>(2 of 4) HTML Colors</p>
    </div> 
    <div>
      <p class='h4 pb-4 blue'><strong>HTML Colors:</strong></p>
      <img class='w-0 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/rgb-blue.PNG' alt=''>
	  <p class='pb-4'>For example, &#35;0000FF is displayed as blue.</p>
	  <p class='pb-4'>That's because the blue component is set to its highest value (FF) and the others set to 00.</p>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 9,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : Website Structure <br>(3 of 4) HTML Colors</p>
    </div> 
    <div>
      <p class='h4 pb-4 blue'><strong>HTML Colors:</strong></p>
	  <p class='pb-4'>Numbers between 00 and FF specify the intensity of the color:</p>
	  <p class='h6pb-4'>EXAMPLE:</p>
      <img class='w-0 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/rgb-purple.PNG' alt=''>
    </div>
	<div class='slide-footer tips text-left'>
      <p class='pb-0'>TIP:</p>
	  <p>Here's a great place to choose colors <a href="https://www.w3schools.com/colors/colors_picker.asp">W3 Schools Color Picker</a></p>
    </div> 	
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 10,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : Website Structure <br>(4 of 4) HTML Colors</p>
    </div> 
    <div>
      <p class='h4 pb-4 blue'><strong>EXPERIEMENT:</strong></p>
	  <p class='h3 pb-4'>Try it yourself.</p>
	  <p class='pb-4'>Replace some numbers with any you want.</p>
    </div>
	<div class='html-code-box'>
        body {<br>
			&nbsp;&nbsp;background: &#35;<span class="pink">ce54c7</span>;<br>
		&nbsp;}
        </div>
		<img class='pb-4 mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/rainbow.png' alt=''>
		<p class='pb-4'>See what happens.</p>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 11,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'></p>
    </div> 
    <div>
      <p class='h3 pb-4'>Now we are going to <span class="blue">style &lt;h3&gt;</span> with:</p>
	  <div class='h5 text-left pb-3'>
          <ul class=''>
            <li class='tasks'>Color.</li>
            <li class='tasks'>Padding.</li>
            <li class='tasks'>Font-size.</li>
            <li class='tasks'>Border.</li>
          </ul>
    </div>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 12,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'></p>
    </div> 
    <div class='h1 pb-4 pink'>
     <strong>READY?</strong>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 13,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 1,
    'reg' : [ '((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Create a h3 selector.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
          h3 { <br>
          }
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> in <span class="inline-code">&lt;style&gt;</span> section.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 14,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 2,
    'reg' : [ '((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a bacground color to h3.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
          background-color: &#35;ce54c7;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> in <span class="inline-code">&lt;style&gt;</span> section.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 15,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : PADDING (REVIEW)</p>
    </div> 
    <div>
	  <p class="h2 pb-4"><span class="html-code">padding:</span></p>
	  <p class='h5 pb-4'>It's like having pillows around when you are when are inside a box.</p>
	  <img class='pb-4 pt-4 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/padding.PNG' alt=''>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 16,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 3,
    'reg' : [ '((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add 15px of padding to h3.</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>             
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 17,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 4,
    'reg' : [ '((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.5em;)((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Increase the h3 <span class="html-code">font-size</span> with em.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
          font-size: 1.5em;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 18,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 5,
    'reg' : [ '((.|\n)\s*)(h3)((.|\n)*)\s*{((.|\n)*)\s*(background-color)((.|\n)*)\s*:((.|\n)*)\s*(#ce54c7;)((.|\n)*)\s*(padding)((.|\n)*)\s*:((.|\n)*)\s*(15px;)((.|\n)*)\s*(font-size)((.|\n)*)\s*:((.|\n)*)\s*(1.5em;)((.|\n)*)\s*(border-left)((.|\n)*)\s*:((.|\n)*)\s*(10px)((.|\n)*)\s*(solid)((.|\n)*)\s*(#d6d5cd);((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 5 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a <span class="inline-code">left-border</span> with this color: &#35;d6d6cd.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
          border-left: 10px solid &#35;d6d6cd;
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> You learned about borders in Project 1.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 19,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">SUPER COOL!</p> 
	  <p class="h4 pt-5">You just styled the &lt;h3&gt; section.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 20,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h4 pb-4 ">Do you want to learn how to:</p> 
      <p class="h2 pb-4"><strong class="aqua">Style two</strong> different <strong class="blue">sections</strong> at the <strong class="aqua">same time</strong>?</p> 
	  <img class='swiper-lazy' data-src='../img/emoji/72/grinning-face-with-one-large-and-one-small-eye.png' alt=''> 
	  <img class='swiper-lazy' data-src='../img/emoji/72/face-with-stuck-out-tongue-and-winking-eye.png' alt=''> 
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 21,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING :STYLING TWO ELEMENTS</p>
    </div> 
    <div>
		<p class="h2 pb-4"><strong class="aqua">STYLING TWO ELEMENTS</strong></p> 
		<p class='h5 pb-4'>This is VERY simple.</p>
		<p class='h5 pb-4'>Just type both selectors with a <span class="pink">comma</span> (,) between them.</p>
		
	<div class='html-code-box'>
          header<span class="pink">,</span> h3 { <br>
          }
     </div>
    </div>
	
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 22,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 ">That's it. <br>Easy right?</p>  
	  <img class='swiper-lazy' data-src='../img/emoji/72/smiling-face-with-heart-shaped-eyes.png' alt=''> 
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 23,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 6,
    'reg' : [ '((.|\n)\s*)(header)((.|\n)*)\s*(\,)((.|\n)*)\s*(h3)((.|\n)*)\s*{((.|\n)*)\s*(color)((.|\n)*)\s*(:)((.|\n)*)\s*(white)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'></p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Make both <span class="inline-code">&lt;header&gt;</span> & <span class="inline-code">&lt;h3&gt;</span> white using a single CSS element.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
          header, h3 { <br>
		  color: white;<br>
          }
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>            
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 24,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'>Learn about <strong>fonts</strong>.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 1 challenge.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 25,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (1 of 5): FONTS</p>
    </div> 
    <div>
	  <p class="h2 pb-4">FONTS</span></p>
	  <p class='h5 pb-4'>Fonts are a certain style of text.</p>
	  <img class='pb-4 pt-4 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/fonts-graphic.png' alt=''>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 26,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (2 of 5): FONTS</p>
    </div> 
    <div>
	  <p class="h2 pb-4">FONTS</span></p>
	  <p class='h5 pb-4'>There are two main types of fonts:</p>
	  <p class='h2 pb-4 text-left' style="font-family: Serif !important;">1. <span class="aqua">Serif:</span> has curly flicks.</p>
	  <p class='h2 pb-4 text-left' style="font-family:Sans-Serif !important;">2. <span class="pink">San Serif:</span> Very straight.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 27,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (3 of 5): FONTS</p>
    </div> 
    <div>
	   <p class="h2 pb-4">FONTS</span></p>
	  <p class='h2 pb-4' style="font-family: Serif !important;"><span class="aqua">Serif:</span><br><span style="font-family:'Times New Roman',serif;">This is Times New Roman</span></p>
	  <p class='h2 pb-4' style="font-family: Sans-Serif !important;"><span class="pink">San Serif:</span><br>This is Arial</p>
	</div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 28,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (4 of 5): FONTS</p>
    </div> 
    <div>
	  <p class="h2 pb-4">FONTS</span></p>
	  <p class='h5 pb-4'>Do you see the differences?</p>
	  <img class='pb-4 pt-4 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/fonts-graphic-2.png' alt=''>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 29,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (5 of 5): FONTS</p>
    </div> 
    <div>
	  <p class="h2 pb-4">FONTS</span></p>
	  <p class='h4 pb-4'>For Example: (at CodeJIKA.com).</p>
	  <p class='h4 pb-4'>For text we use:</p>
	  <p class='h3 pb-4'>Radjdani</p>
	  <p class='h4 pb-4'>And for code we use:</p>
	  <p class='h3 pb-4' style="font-family: Consolas, monaco, monospace !important;">Consolas</p>
    </div>
	<div class='slide-footer tips text-left'>
      <p class='pb-0'>TIP:</p>
	  <p>Consolas is a third type of: monospace</p>
    </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 30,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">GREAT!<p> 
	  <p class="h3 pt-5">Now let's learn about font families.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 31,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING (1 of 2): font-family</p>
      </div>    
      <div>
	  <p class='h2 pb-4'><span class="html-code">font-family:</span></p>
        <p class='h4 pb-4'>Sometimes a browser will not show the font you added.</p>
        <p class='h5 pb-4'>So, add "back-up" fonts and a font-family like "sans serif" or "serif", so that it looks similar.</p>
      </div>
	  <div class='slide-footer tips text-left'>
		  <p class='pb-0'>TIP:</p>
		  <p>The next font in the font-family is used. If the first font is missing.</p>
	  </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 32,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING (2 of 2): font-family</p>
      </div>    
      <div>
	  <p class='h2 pb-4'><span class="html-code">font-family:</span></p>
        <p class='h4 pb-4'>This becomes a font-family</p>
        <p class='h5 pb-4'>Most common:</p>
		<div class='html-code-box'>
         font-family: <span class="pink">Arial, Helvetica, sans-serif;</span>
        </div>
		<p class='h5 pb-4 pt-4'>Also great:</p>
		<div class='html-code-box'>
         font-family: <span class="aqua">"Times New Roman", Times, Serif;</span>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 33,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 7,
    'reg' : [ '((.|\n)\s*)(body)((.|\n)*)\s*({)((.|\n)*)\s*(font-family)((.|\n)*)\s*(:)((.|\n)*)\s*(helvetica)((.|\n)*)\s*(,)((.|\n)*)\s*(arial)((.|\n)*)\s*(,)((.|\n)*)\s*(san-serif)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'></p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Use Helvetica, Arial and Sans-Serif as font family for body.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
         body{ <br>
			font-family: helvetica, arial, san-serif; <br>
			}
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>            
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  { 
    'slide_number' : 34,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 8,
    'reg' : [ '((.|\n)\s*)(header)((.|\n)*)\s*(\,)((.|\n)*)\s*(h3)((.|\n)*)\s*{((.|\n)*)\s*(color)((.|\n)*)\s*(:)((.|\n)*)\s*(white)((.|\n)*)\s*(;)((.|\n)*)\s*(font-family)((.|\n)*)\s*(:)((.|\n)*)\s*(\"Arial Black\")((.|\n)*)\s*(,)((.|\n)*)\s*(Gadget)((.|\n)*)\s*(,)((.|\n)*)\s*(sans-serif)((.|\n)*)\s*(;)((.|\n)*)\s*}((.|\n)*)\s*body((.|\n)*)\s*{((.|\n)*)\s*font-family((.|\n)*)\s*' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'></p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add Arial, Gadget, and San-Serif as font-family for <span class="html-code">header</span> and <span class="html-code">h3</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box'>
        header, h3 { <br> 
				color: white; <br> 
				font-family: &#8220;Arial Black&#8221;, Gadget, sans-serif;<br>
				}
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>            
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 35,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h2 w-75 mx-auto'>Understand:</p>
      <ul class="h4">
	  <li>"<strong>Responsive</strong>" web design.</li>
	  <li><strong>Pixels</strong>.</li>
	  <li>Screen sizes.</li>
	  <ul>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 0 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 36,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING (1 of 2): RESPONSIVE WEB DESIGN</p>
      </div>    
      <div>
	  <p class='h2 pb-4'>RESPONSIVE WEB DESIGN:</p>
        <p class='h3 pb-4'>A responsive website works on different screen sizes.</p>
        <p class='h5 pb-4'>What do we mean by screen size?</p>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 37,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (2 of 2): RESPONSIVE WEB DESIGN</p>
    </div> 
    <div>
       <p class='h2 pb-4'>RESPONSIVE WEB DESIGN:</p>
      <p class='h3 pb-4'>Screen size?</p>
      <img class='w-0 swiper-lazy' data-src='../learn/lessons/P002-T04-M-V001/img/screen-size.PNG' alt=''>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {   
    'slide_number' : 38,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">GOOD.<p> 
	  <p class="h3 pt-5">Now we're gonna learn about how to measure screen size.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 39,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (1 of 4): PIXELS</p>
    </div> 
    <div>
      <p class='h3 pb-4'>Pixels are a way to measure a screen.</p>
      <p class='h4 pb-4'>Your screen is made of thousands of pixels</p>
      <p class='h4 pb-4 inline-code'>pixel = px</p>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 40,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (2 of 4): PIXELS</p>
    </div> 
    <div>
      <p class='h3 pb-4'>A pixel is like a <strong class="pink">dot of color</strong>.</p>
	  <img class='w-0 swiper-lazy pb-4' data-src='../learn/lessons/P002-T04-M-V001/img/pixels.PNG' alt=''>
      <p class='h4 pb-4'>Thousand of tiny pixels make an image.</p>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 41,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (3 of 4): PIXELS</p>
    </div> 
    <div>
      <p class='h2 pb-2'>EXAMPLE:</p>
      <p class='h3 pb-2'>This screen is 720 pixels by 1280 pixels.</p>
	  <img class='w-0 swiper-lazy pb-4' data-src='../learn/lessons/P002-T04-M-V001/img/screen-pixels.PNG' alt=''>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 42,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING (4 of 4): PIXELS</p>
    </div> 
    <div>
      <p class='h2 pb-4'>EXAMPLE:</p>
      <p class='h3 pb-4'>When a box is:</p>
	  <div class='html-code-box'>
          width: 100px;
        </div>
      <p class='h3 pb-4 pt-4'>you are saying the box is 100 pixels wide.</p>
    </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{   
    'slide_number' : 43,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
      <p class="h2 pb-4 aqua text-uppercase bold">YOU'RE DOING AMAZING.<p> 
	  <p class="h3">We're are almost done.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 44,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING (1 of 2): width %</p>
      </div>    
      <div>
	  <p class='h2 pb-4'><span class="html-code">width %</span></p>
        <ol class="h4">
			<li>Not sure how wide your screen is?</li>
			<li>Want it to cover halft the screen?</li>
		</ol>
        <p class='h5 pb-4'>Make a box with % of screen size.</p>
		<p class='pb-0 text-left'>Like this:</p>
		<div class='html-code-box'>
        width: 50%
      </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{
    'slide_number' : 45,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING (2 of 2): width %</p>
      </div>    
      <div>
        <p class='h5 pb-4'>50% will cover half the screen, no matter the size.</p>
		
        <img class='w-0 swiper-lazy pb-4' data-src='../learn/lessons/P002-T04-M-V001/img/screen-size-50.PNG' alt=''>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  },{   
    'slide_number' : 46,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
	<div>
	  <p class="h3 pt-5">Here's a <span class="pink">SUPER mordern</span> way of changing the size of your screen text.</p>
	  <p class="h5 pt-5">Psst. Most web designers don't even know about it yet.</p>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  },  {
    'slide_number' : 47,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING : VIEWPORT FONT</p>
      </div>    
      <div>
	  <p class='h2 pb-4'><span class="">VIEWPORT FONT</span></p>
        <p class='h5 pb-4'>1 vw point size</p>
        <p class='h5 pb-4'>=</p>
        <p class='h5 pb-4'>1% of screen size</p>
		<p class='pb-0 text-left'>Like this:</p>
		<div class='html-code-box'>
        font-size: 10vw;
      </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 48,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 pb-4'>CONGRATS!</div>
      <div style='background-color: #6aa2c7;border-radius:50%;margin: 40px auto;width: 200px;height: 200px;'>
        <div class='lesson-instructions '><img style='padding-top:20px;' class='w-75 swiper-lazy mb-3' data-src='../img/lessons/congrats_training_sml.png'></div>
      </div>
      <div class='h3 pb-4'>You've finished<br> Training 4</div>  
    </div>    
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 49,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h0 aqua'>YIPPEEE.</div>
      <img class='pt-5 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-open-mouth.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 50,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h3 pb-4 w-75'>Ready for Training 5?</p>
      <a class="btn-primary next"" href="../learn/P2Training5">Start Now</a>
      <a class="btn-primary-alt next check" href='../learn/projects'>Projects Page</a>
    </div>
    `,
    'created_at' : '',  
    'updated_at' : ''
  } ]
}
    
var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 
 
/// Add custom JS for lesson below here


function previewLessonModal() {
  $.galleryDialog({
    modalName: 'preview-lesson-modal',
    htmlContent: `
    <iframe  srcdoc="
<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
  </style>
</head>
<body>
<header>
    <h1>My Name</h1>
      <h3>Motivation:
        <br><br>
        <i>I want to become a coder</i>
      </h3>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
</header>
</body>

  "></iframe>
    `
  })
}