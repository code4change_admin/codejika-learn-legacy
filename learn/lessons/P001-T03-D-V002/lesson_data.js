var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
  </style>
</head>
<body>
    <h1>My Name</h1>
      <h3>Motivation:
        <br><br>
        <i>I want to become a coder</i>
      </h3>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
</body>



` ,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "P1Training2", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 3",
    "save_lesson_id": "P1Training3", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide1.PNG">
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide12.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide13.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "(<header>|<header [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/header>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Insert a <span class="html-code">&lt;header&gt;</span>.

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

          <div class="challenge-tip"><span>Where:</span>
            Open it before &lt;h1&gt; and close it after the &lt;/p&gt; tag.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide17.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 18,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide18.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Add a header selector in <span class="html-code">&lt;style&gt;</span>.<br>
            <div class="html-code-box">
			header{ <br>
              &nbsp;}
			</div>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Add this <u>first half</u> of the rule:<br>
			  <div class="html-code-box">
               <span class="code-fade">header&nbsp;{</span> <br>
               background: linear-gradient <br>
               <span class="code-fade">}</span>
			</div>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)\s*header((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*linear-gradient((.|\n)*)\s*[(]100deg((.|\n)*)\s*,((.|\n)*)\s*yellow((.|\n)*)\s*40%((.|\n)*)\s*,((.|\n)*)\s*pink((.|\n)*)\s*40%((.|\n)*)\s*[)]((.|\n)*)\s*;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 3</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Complete the rule with this line:<br>
			  <div class="html-code-box">
              <span class="code-fade">header&nbsp;{</span> <br>
              background: linear-gradient<span style="color: #2ca8ff;">(100deg, yellow 40%, pink 40%);</span> <br>
              <span class="code-fade">}</span>
				</div>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

          <div class="challenge-tip"><span>Tip:</span>
            No space between <b>gradient &amp; (100 .</b>
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide30.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 32,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "(<section>|<section [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*motivation((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/section>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Place your “Motivation” <span class="html-code">&lt;h3&gt;</span> within <span class="html-code">&lt;section&gt;</span> tags.

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>


        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)section((.|\n)*)\s*{((.|\n)*)\s*(background|background-color)((.|\n)*)\s*:((.|\n)*)\s*lightgrey;((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>

              Style <span class="html-code">&lt;section&gt;</span> with a grey background.<br>
			  <div class="html-code-box">
              <span class="code-fade">section&nbsp;{</span> <br>
              background: lightgrey; <br>
              <span class="code-fade">}</span>
			  </div>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>


        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 34,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide34.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide35.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 37,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 7,  
      "reg" : [ "(<footer>|<footer [^>]*>)((.|\n)*)\s*<\/footer>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 1</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>
              Insert a <span class="html-code">&lt;footer&gt;</span> section before the closing <span class="html-code">&lt;/body&gt;</span> tag.

            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>


        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide39.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 8,  
      "reg" : [ "(<footer>|<footer [^>]*>)((.|\n)*)\s*&copy;((.|\n)*)\s*([1-9][0-9][0-9][0-9])((.|\n)*)\s*([a-zA-Z][a-zA-Z][a-zA-Z])<\/footer>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 2</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>
              Add a copyright line inside the <span class="html-code">&lt;footer&gt;</span>.<br>
			  <div class="html-code-box">
			  <span class="code-fade">&lt;footer&gt;</span><br>
              &nbsp;&amp;copy; 2021 My name<br>
              <span class="code-fade">&lt;/footer&gt;</span>
			  </div>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>

          <div class="challenge-tip"><span>Tip:</span>
            No space between <b>“&amp;” and “copy”</b>
          </div>

        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide41.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 9,  
      "reg" : [ "(<style>|<style [^>]*>)((.|\n)*)footer((.|\n)*)\s*{((.|\n)*)\s*background((.|\n)*)\s*:((.|\n)*)\s*;((.|\n)*)\s*color((.|\n)*)\s*:((.|\n)*)\s*white;((.|\n)*)\s*}((.|\n)*)\s*<\/style>" ],
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black;"> Step 3</span>
          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon"></i>
              <span class="fa fa-check"></span>
              Using a <span class="html-code">footer { }</span> selector in CSS, make your footer background black and the text white.
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>


        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 43,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide43.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 44,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide44.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{
      "slide_number" : 45,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide45.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 46,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide46.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 47,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide47.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 48,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide48.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },  {
      "slide_number" : 49,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" src="../learn/lessons/P001-T03-D-V002/img/Slide49.PNG">
        <a href="/learn/P1Training4" class="btn btn-primary" style="top:65%;">Start next training →</a>        
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}
    
var hintsForCheckPonts = {


}
  
var hints_data =   `


    ` 
  

/// Add custom JS for lesson below here