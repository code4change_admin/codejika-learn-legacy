var lesson_data = 
{
    "defaultCode": // default code, if user has not already started coding   
`



`,
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 1",
    "save_lesson_id": "P1Training1", // This is id that will be used to store save code in Firebase
    "slug": "", // not currently in use
    "slides" : [ {
      "slide_number" : 1,      
      "action" : true,
      "checkpoint" : false,
      "js_function" : "console.log('I am a DB loaded function')",      
      "html_content" : 
      `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide1.PNG">
        <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 2,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide2.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 3,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide3.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 4,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide4.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 5,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide5.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 6,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide6.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 7,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide7.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 8,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide8.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 9,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide9.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 10,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide10.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 11,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide11.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 12,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide12.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 13,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide13.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 14,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide14.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 15,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide15.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 16,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 1,  
      "reg" : [ "<head(.*)>[\\s\\r\\n]*(.*)<\\/head>" ],
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>Challenge</h2>
        <span class="challange-step" style="color:black;"> Step 1</span>
        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle  square-icon" style="color:#808080"></i>
            <span class="fa fa-check "></span>
            Add this in the editor below:<br>
            <div class="html-code-box">
              &lt;head&gt;<br>&lt;/head&gt;
            </div>
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div> 		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 17,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 2,  
      "reg" : [ "<body(.*)>[\\s\\r\\n]*(.*)</body>" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
        <h2>
          Challenge
        </h2>
        <span class="challange-step" style="color:black"> Step 2</span>


        <ol class="actions">
          <li data-index="0">
            <i class="far fa-circle square-icon" style="color:#808080"></i>
            <span class="fa fa-check"></span>

            Add a <b>body section</b> below your head section.
          </li>
        </ol>
        <div class="">
          <div class="button-locked">
            <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
          </div>
          <div class="button-unlocked"> 
            <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
           </div>
        </div>
		<div class="challenge-tip"><span>Tip:</span>
          Check the "structure" slide.
        </div>		
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 18,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide18.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 19,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide19.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 20,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide20.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 21,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide21.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 22,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide22.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 23,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 3,  
      "reg" : [ "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>" ],    
      "html_content" : `
      <div class="">
      <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black"> Step 3</span>


          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon" style="color:#808080"></i>
              <span class="fa fa-check"></span>

              Type an opening &amp; closing <b>
                <span class="html-code">
                  &lt;h1&gt;
                </span></b> tag.
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>  
        <div class="challenge-tip"><span>Tip:</span>
          In the <span class="html-code">&lt;body&gt;</span> section.
        </div>
      </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 24,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 4,  
      "reg" : [ "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*([a-z]{4,})((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>" ],    
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>
          <span class="challange-step" style="color:black"> Step 4</span>


          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon" style="color:#808080"></i>
              <span class="fa fa-check"></span>

              Insert your <b>First</b> &amp; <b>Last</b> name between the <span class="html-code">&lt;h1&gt;</span> tags.
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>  

          <div class="challenge-tip"><span>Tip:</span>
            Check the example in the &lt;h1&gt; briefing.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 25,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide25.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 26,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide26.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 27,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide27.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 28,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide28.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 29,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide29.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 30,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide30.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 31,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide31.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 32,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 5,  
      "reg" : [ "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>" ],    
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon" style="color:#808080"></i>
              <span class="fa fa-check"></span>
              Write: <b>"Launching Soon..."</b> between <span class="html-code">&lt;h3&gt;</span> tags.
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>  

          <div class="challenge-tip"><span>Where:</span>
            Below <span class="html-code">&lt;/h1&gt;</span> tag.
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 33,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide33.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 34,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide34.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 35,
      "action" : true,
      "checkpoint" : true,
      "checkpoint_id" : 6,  
      "reg" : [ "(<p>|<p [^>]*>)((.|\n)*)\s*([1-9]|[0-9][0-9])((.|\n)*)\s*([a-z][a-z][a-z])((.|\n)*)\s*([0-9][0-9][0-9][0-9])(.|\n)*\s*<\/p>" ],    
      "html_content" : `
      <div class="">
        <div class="checkpoint">
          <h2>
            Challenge
          </h2>

          <ol class="actions">
            <li data-index="0">
              <i class="far fa-circle square-icon" style="color:#808080"></i>
              <span class="fa fa-check"></span>

              Add a <span class="html-code">&lt;p&gt;</span> with today's date<br>EXAMPLE:<br>
              <div class="html-code-box">
                &lt;p&gt;<br>&nbsp;&nbsp;10 October, 2019<br>&lt;/p&gt;
              </div>
            </li>
          </ol>
        <div class="">
        <div class="button-locked">
          <a class="btn btn-primary skip check swiper-next" style="">Skip this →</a>
        </div>
        <div class="button-unlocked"> 
          <a class="btn btn-primary success check swiper-next" style="">I did it <i class="icon-sentiment_satisfied"></i></a>
        </div>
        </div>  

          <div class="challenge-tip"><span>Where:</span>
            Below <span class="html-code">&lt;h3&gt;</span>
          </div>
        </div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {

      "slide_number" : 36,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide36.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    },{

      "slide_number" : 37,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide37.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 38,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide38.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 39,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide39.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 40,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide40.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 41,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide41.PNG">
        <div class="swiper-lazy-preloader"></div>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    }, {
      "slide_number" : 42,
      "html_content" : `
      <div class="">
        <img class="swiper-lazy" data-src="../learn/lessons/P001-T01-D-V003/img/Slide42.PNG">
         <a href="/learn/P1Training2" class="btn btn-primary" style="top:65%;">Start next training →</a>
      </div>
      `,
      "created_at" : "",      
      "updated_at" : ""
    } ]
}

var check_points = {
  16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
/*  16:"A closing tag has a forward slash <strong>/<strong>",
  17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
  23:"In the body section. Hint given as tip on bottom of the slide.",
  24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
  32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
  36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"*/
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

` 


/// Add custom JS for lesson below here