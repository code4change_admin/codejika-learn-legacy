var lesson_data = 
{
    'defaultCode': // default code, if user has not already started coding   
`<head>
  <style>
    h1 {
      font-size: 75px;
    }
    i {
      font-size: 25px;
    }
    header {
      background: linear-gradient(110deg, yellow 40%, pink 40%);
    }
    section {
      background: lightgrey;
    }
    footer {
      background: black;
      color: white;
    }
  </style>
</head>
<body>
  <header>
    <h1>My Name</h1>
    <section>
      <h3>Motivation:
        <br><br>
        <i>I want to learn to code because I love building new things.</i>
      </h3>
    </section>
    <h3>Launching Soon...</h3>
    <p>01 January 2000</p>
  </header>
  <footer>&copy 2020 My Name</footer>
</body>



` ,
    'kbLayout': '', // not currently in use
    'loadJS': 'https://cdnjs.cloudflare.com/ajax/libs/geopattern/1.2.3/js/geopattern.min.js"></script>', // not currently in use
    'prevLessonID': 'P1Training3', // Lesson ID of previous lesson where to load user's code
    'nextLessonSlug': '', // not currently in use
    'pageDesc': 'Learn how to build your first website with these easy intro lessons to coding.', 
    'pageKeywords': 'coding, code, learn to code, code website, my first website', 
    'pageTitle': 'CodeJIKA - Project 1, Training 4',
    'save_lesson_id': 'P1Training4', // This is id that will be used to store save code in Firebase
  'slides' : [ {
    'slide_number' : 1,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'intro',      
    'html_content' : `
      <div>    
      	<p class='slide-title h1 mt-5'>TRAINING 4</p>
      </div>      
      <div class=''>
        <p class='h1'>Let's<br>
        <strong class='fs-x2 aqua'>DO</strong><br>
        this!</p>
        <img class='mt-4 w-20 swiper-lazy' data-src='../img/emoji/72/grinning-face-with-star-eyes.png' alt=''>
      </div>      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 2,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'snapshot',    
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Snapshot</p>
  	  <p class='white fs75'>(These are your missions for today.)</p>
  	</div>      
	<div>       
		<ol class='h4'>
		  <li>Learn <span class='inline-code font-weight-bold'>&lt;div&gt;</span>.</li>
		  <li>Be "<strong>Font</strong>" savvy.</li>
		  <li>Add <span class='font-weight-bold'>borders</span> and padding.</li>
		</ol>
	</div>
	<div>
		<p class='slide-footer white'>How it will look:</p>
		<div class='btn btn-aqua next preview-output'>Preview</div>
	</div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  	
    'slide_number' : 3,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
  	<div>
  	  <p class='slide-header h2'>Mission</p>
  	</div>
    <div>
      <p class='h2'>Do the <span class='inline-code font-weight-bold'>&lt;div&gt;</span>.</p>
    <ol class='h4'>
      <li><strong>Understand</strong> it.</li>
      <li><strong>Wrap</strong> content in it.</li>
    </ol>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 3 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 	
    'slide_number' : 4,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING : &lt;div&gt;</p>
      </div>    
      <div>
        <p class='h3 p-3'>What’s a DIV?</p>
        <p class='h4 text-left w-75 p-1'><span class='inline-code'>&lt;<strong class='green'>div</strong>&gt;</span></p>
        <p class='h5 text-left w-75 p-1'>It’s like a <strong class='green'>magical  stretchy container</strong> and you can put any type of  object in it.
        <p class='h4 text-left w-75 p-2'><span class='inline-code'>&lt;<strong class='green'>div</strong>&gt;</span></p>
        <img class='swiper-lazy w-30' data-src='../learn/lessons/P001-T04-M-V001/img/div-suitcase.png' alt=''> 
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 5,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
  	    <p class='slide-header h6'>BRIEFING : &lt;div&gt;</p>
      </div>    
      <div>
        <p class='h3 pb-4'><span class='inline-code'>&lt;<strong class='green'>div</strong>&gt;</span> <span class='inline-code'>&lt;/<strong class='green'>div</strong>&gt;</span></p>
        <ol class='h5'>
          <li>A HTML <strong class='green'>section</strong>.</li>
          <li>There can be <strong class='green'>lots</strong> of <span class='inline-code'>&lt;div&gt;</span> s in a site.</li>
          <li>You can even put a <span class='inline-code'>&lt;div&gt;</span> <strong class='green'>inside another</strong> <span class='inline-code'>&lt;div&gt;</span>.</li>
        </ol>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 6,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'remember',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Remember</p>
    </div>
    <div>
      <p class='h2 pb-4'>What's a<br><span class='inline-code'>&lt;div&gt;</span>?</p>
      <p class='h2'>What can you  put in it?
</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 7,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>PRE-CHECK : &lt;div&gt;</p>
    </div> 
    <div>
      <p class='h5 pb-3 w-75'>Let’s wrap the content in <span class='inline-code'>&lt;header&gt;</span>  in a <span class='inline-code'>&lt;div&gt;</span>.</p>
      <p class='pb-1 green text-left'>PRE-CHECK:</p>
        <div class='html-code-box fade-box-top fade-box-bottom'>
        <span class='code-fade'>&lt;header&gt;<br></span>
        &nbsp;&nbsp;&lt;div&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;<span class='code-fade'>&lt;h1&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;My Name<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;/h1&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;h3&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Launching soon...<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;18th June, 2018<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;/p&gt;<br></span>
        &nbsp;&nbsp;&lt;/div&gt;<br>
        <span class='code-fade'>&lt;/header&gt;</span>
        </div>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 8,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 1,
    'reg' : [ '(<header>|<header [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/header>' ],
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 2</p>
      </div>
      <div>
        <div class='text-left pb-5'>
          <p class='blue text-uppercase text-left'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Insert a <span class='inline-code'>&lt;div&gt;<span> in <span class='inline-code'>&lt;header&gt;<span>.</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      <div class='slide-footer tips text-left'>
        <p class='pb-0'>Where:</p>
        <ol class=''>
          <li>Open it before &lt;h1&gt;</li>
          <li>Close it after the &lt;/p&gt; tags</li>
        </ol>
      </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 9,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>PRE-NOTICE : &lt;div&gt;</p>
    </div> 
    <div>
      <p class='h4 pb-3 w-75'>Now wrap the content in <span class='inline-code'>&lt;section&gt;</span> in a <span class='inline-code green'>&lt;div&gt;</span>.</p>
    </div>
    <div class='slide-footer tips'>
      <span class='red'>PSSST:</span> If you’re stuck, there's a "Checkpoint" after the next Challenge.
    </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 10,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 2,
    'reg' : [ '(<section>|<section [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/section>' ],
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 2</p>
      </div>
      <div>
        <div class='text-left pb-5'>
          <p class='blue text-uppercase text-left'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Insert a <span class='inline-code'>&lt;div&gt;<span> in <span class='inline-code'>&lt;section&gt;<span>.</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      <div class='slide-footer tips text-left'>
        <p class='pb-0'>Where:</p>
        <ol class=''>
          <li>Open the <span class='inline-code bold'>&lt;div&gt;</span> before your <span class='inline-code bold'>&lt;h3&gt;</span> opening tag</li>
          <li>Close it after the closing <span class='inline-code bold'>&lt;/h3&gt;</span> tag</li>
        </ol>
      </div>       
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 11,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
      <p class='fs75'>Your code should look something like this:</p>
    </div>
    <div>
      <p class='fs75 pb-0 text-left'>EXAMPLE:</p>
      <div class='html-code-box fade-box-top fade-box-bottom'>
        <span class='code-fade'>&lt;section&gt;<br> </span>
        &nbsp;&nbsp;&lt;div&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;<span class='code-fade'>&lt;h3&gt;<br>      
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOTIVATION:<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;br&gt;&lt;br&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Because...<br>  
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;/h3&gt;</span><br></span>
        &nbsp;&nbsp;&lt;div&gt;<br>
        <span class='code-fade'>&lt;/section&gt;
      </div> 
    </div>     
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 12,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h1 aqua'>GOOD JOB!</div>
      <img class='pt-5 swiper-lazy' data-src='../img/emoji/72/smiling-face-with-open-mouth.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 13,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'briefing',     
    'html_content' : `
    <div>
      <p class='slide-header h6'>PRE-NOTICE : &lt;div&gt;</p>
    </div> 
    <div>
      <p class='h4 pb-3 w-75'>Do the same in <span class='inline-code'>&lt;footer&gt;</span>.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 14,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 3,
    'reg' : [ '(<footer>|<footer [^>]*>)((.|\n)*)\s*(<div>|<div [^>]*>)((.|\n)*)\s*<\/div>((.|\n)*)\s*<\/footer>' ],
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 1</p>
      </div>
      <div>
        <div class='text-left pb-5'>
          <p class='blue text-uppercase text-left'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'> Wrap the content in <span class='inline-code'>&lt;footer&gt;</span> with a <span class='inline-code'>&lt;div&gt;</span></li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 15,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
      <p class='fs75'>Your code should look something like this:</p>
    </div>
    <div>
      <p class='fs75 pb-0 text-left'>EXAMPLE:</p>
      <div class='html-code-box fade-box-top fade-box-bottom'>
        <span class='code-fade'>&lt;footer&gt;<br> </span>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;<br>
        <span class='code-fade'>&nbsp;&nbsp;&nbsp;&nbsp;&amp;copy; 2021 My Name<br></span>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;<br>
        <span class='code-fade'>&lt;/footer&gt;
      </div> 
    </div>     
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 16,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h2 green pb-5 bold'>GREAT!</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/person-raising-both-hands-in-celebration-type-5.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 17,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
       <p class='slide-header h6'>BRIEFING : CSS Styling</p>
      </div>    
      <div>
        <p class='h1 pb-0 mb-1 pink'>CSS</p>
        <p class='fs75 pb-4'>It works like this:</p>
        <ol class='h5 w-75'>
          <li>The rules you write to the <span class='inline-code'>div { }</span>  selector in <span class='inline-code'>&lt;style&gt;</span></li>
          <li>are <span class='pink'>applied to all</span> the <span class='inline-code'>&lt;div&gt;</span>s in <span class='inline-code'>&lt;body&gt;</span>.</li>
        </ol>
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 18,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h2 green pb-5 bold'>HMMM...</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/face-with-one-eyebrow-raised.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 19,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='pb-1'>this styling...</p>
      <div class='html-code-box fade-box-top fade-box-bottom mb-3 fs75'>
        <span class='code-fade'>&lt;style&gt;</span><br>
        &nbsp;&nbsp;div {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;font-size: 75; <br>
        &nbsp;&nbsp;} <br>
        <span class='code-fade'>&lt;/style&gt;</span><br>
      </div>
      <p class='pb-1 pt-3 '>is applied to all the &lt;/div&gt;s</p>
      <div class='html-code-box fade-box-top fade-box-bottom mb-3 fs75'>
        <span class='code-fade'>&lt;body&gt;<br>
        &nbsp;&nbsp;&lt;header&gt;</span><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;<br>
        <span class='code-fade'>&nbsp;&nbsp;&lt;/header&gt;<br>
        &nbsp;&nbsp;&lt;section&gt;</span><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;<br>
        <span class='code-fade'>&nbsp;&nbsp;&lt;/section&gt;<br>
        &nbsp;&nbsp;&lt;footer&gt;</span><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;div&gt;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&lt;/div&gt;<br>
        <span class='code-fade'>&nbsp;&nbsp;&lt;/footer&gt;<br>
        &lt;/body&gt;<br>             
      </div>
    </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 20,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h2 green pb-5 bold'>OHHHH...</p>
      <img class='swiper-lazy w-20' data-src='../img/emoji/72/face-screaming-in-fear.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 21,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
      </div>    
      <div>
        <p class='h1 pb-0 mb-1 pink'>CSS</p>
        <p class='fs75 pb-4'>It works like this:</p>
        <p class='h5 pb-4'>If you have 100 pages <span class='pink bold'>linking to the  same CSS</span> styling…</p>
        <p class='h5 pb-4'>… all <span class='inline-code'>&lt;h3&gt;</span> tags  across all 100 pages <span class='pink bold'>will look the same</span>.</p>
        <img class='swiper-lazy' data-src='../img/emoji/72/thinking-face.png' alt=''> 
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 22,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
      </div>    
      <div>
        <p class='h5 pb-4 w-75'>And if you want to <span class='pink bold'>make</span> <span class='inline-code'>&lt;h3&gt;</span>s <span class='pink bold'>red</span> on  all the 100 pages …</p>
        <p class='h5 pb-4 w-75'>… you <span class='pink bold'>only</span> have to <span class='pink bold'>change</span> the CSS <span class='pink bold'>once</span> (in the central  CSS file.)</p>
        <img class='swiper-lazy' data-src='../img/emoji/72/shocked-face-with-exploding-head.png' alt=''> 
      </div>    
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {     
    'slide_number' : 23,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h4  pb-3 '>That's the</p>
      <p class='h2 aqua pb-3 bold'>POWER</p>
      <p class='h4  pb-3 '>of</p>
      <p class='h2 pink pb-3 bold'>CSS</p>
      <img class='swiper-lazy w-20 pr-2' data-src='../img/emoji/72/firecracker.png' alt=''> 
      <img class='swiper-lazy w-20 pr-2' data-src='../img/emoji/72/fire.png' alt=''> 
      <img class='swiper-lazy w-20' data-src='../img/emoji/72/firecracker.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {     
    'slide_number' : 24,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>PRE-NOTICE : &lt;div&gt;</p>
    </div>
    <div>
      <p class='h4 pb-5 w-75'>Now let’s <span class='blue bold'>style</span> these <span class='inline-code'>&lt;div&gt;</span>s  already.</p>
      <img class='swiper-lazy w-20' data-src='../img/emoji/72/fish-cake-with-swirl-design.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 25,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='h2 green bold'>READY?</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 26,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'><strong>Center</strong> and <strong>pad</strong>  everything in  the <span class='html-code'>&lt;div&gt;</span>s.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 3 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 27,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='h5 pb-4'>Styling the <span class='inline-code'>&lt;div&gt;</span></p>
      <p class='pb-3' >Use <strong>div { }</strong> to apply style to everything in  the <span class='inline-code'>&lt;div&gt;</span>s.</p>
      <p class='fs75 pt-4 text-left'>EXAMPLE:</p>
      <div class='html-code-box'>
        div {<br>
        &nbsp;&nbsp;text-align: center;<br>
        &nbsp;&nbsp;padding: 40px;<br>
        }
      </div>
    </div>
    <div class='slide-footer tips text-left'>
      <div><span class='red'>TIP:</span> Make sure your CSS  rule is in the <strong class='inline-code'>&lt;style&gt;</strong></div>
    </div>        
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 28,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 4,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 3</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a <span class='inline-code'>div { }</span> selector in the <span class='inline-code'>&lt;style&gt;</span> section</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box fade-box-top fade-box-bottom'>
         <span class='code-fade'>&nbsp;&nbsp;}</span><br>
         &nbsp;&nbsp;div { <br>
         &nbsp;&nbsp;}<br>
         <span class='code-fade'>&lt;/style&gt;</span>
        </div>

        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 29,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='h3 pb-4'><span class='inline-code'>text-align:</span></p>
      <p class='pb-3' >Tells the text where to "lean".</p>
      <p class='fs75 pt-3 text-left'>DEMO:</p>
      <div class='html-code-box mb-3 bg-yellow black'>
        <p class='text-left'>text-align: left;</p>
        <p class='text-center'>text-align: center;</p>
        <p class='text-right mb-0'>text-align: right;</p>
      </div>
      <p class='' >Left, right or center.</p>      
    </div>
      
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 30,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 5,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*text-align((.|\n)*)\s*:((.|\n)*)\s*center;((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ],
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 3</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Center align all the content in the <span class='inline-code'>&lt;div&gt;</span>s</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> Come on, you got this! ;)</div>
      </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 31,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='h3 pb-4'><span class='inline-code'>padding:</span></p>
      <p class='pb-3 w-75'>It's like having pillows around you when you  are inside a box.</p>
      <p class='fs75 pt-3 text-left'>DEMO:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'>
        <div class="mx-auto mt-2 mb-2" style='border: 1px solid black; width:fit-content'>
        <div style='background: repeating-linear-gradient( 45deg, rgba(0, 128, 0, 0.3), rgba(0, 128, 0, 0.3) 10px, rgba(0, 128, 0, 0.2) 10px, rgba(0, 128, 0, 0.2) 20px );   width: fit-content;    padding: 40px;'>
        <div class='bg-yellow p-1'>padding: 40px;</div>
        </div>
        </div>
      </div>    
    </div>
      
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {     
    'slide_number' : 32,   
    'action' : false,
    'checkpoint' : false,

    'html_content' : `
    <div>
      <p class='h4 pb-5 w-75'>Let's have a<br><strong>PILLOW FIGHT</strong>!!!
</p>
      <img class='swiper-lazy w-20' data-src='../img/emoji/72/smiling-face-with-open-mouth-and-tightly-closed-eyes.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 33,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 6,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*div((.|\n)*)\s*{((.|\n)*)\s*padding((.|\n)*)\s*:((.|\n)*)\s*40px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ],
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3 of 3</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add <strong>40px</strong> padding <i>(pillows)</i> to the <span class='inline-code'>&lt;div&gt;</span>s</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> Just try. Checkpoint is soon.</div>
      </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {   
    'slide_number' : 34,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
    </div>
    <div>
      <p class='pb-1 text-left w-75'>PREVIEW:</p>
      <p class='h3 pb-5 text-left w-75'>Is all the content (text) on the  website centered?</p>
      <p class='pt-3 text-left w-75'>If not, then something’s  wrong.</p>

    </div>   
            <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> Check your website.</div>
      </div>  
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 35,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
    </div>
    <div>
      <div class='html-code-box fade-box-top fade-box-bottom fs65'>
        <span class='code-fade'>&lt;style&gt;<br></span>
        &nbsp;&nbsp;h1 {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;font-size: 75px;<br>
        &nbsp;&nbsp;}<br>
        &nbsp;&nbsp;i {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;font-size: 25px;<br>
        &nbsp;&nbsp;}<br>
        &nbsp;&nbsp;header {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;background: linear-gradient(110deg, yellow 40%, pink 40%);<br>
        &nbsp;&nbsp;}<br>
        &nbsp;&nbsp;section {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;background: lightgrey;<br>
        &nbsp;&nbsp;}<br>
        &nbsp;&nbsp;footer {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;background: black;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;color: white;<br>
        &nbsp;&nbsp;}<br>
        &nbsp;&nbsp;div {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;text-align: center;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;padding: 40px;<br>
        &nbsp;&nbsp;}<br>
        <span class='code-fade'>&lt;/style&gt;</span>
      </div> 
    </div>   
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 36,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <p class='h3 pb-5 w-75'>You  completed  the mission.</p>
      <img class='swiper-lazy w-20' data-src='../img/emoji/72/flexed-biceps.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 37,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <p class='h3 pb-5 w-75'>Now let's style  the <span class='inline-code'>&lt;h3&gt;</span>s on the page.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 38,   
    'action' : false,
    'checkpoint' : false,
    'js_function' : '',
    'css_class' : 'mission',
    'html_content' : `
    <div>
      <p class='slide-header h2'>Mission</p>
    </div>
    <div>
      <p class='h3 w-75 mx-auto'>Super-charge your <span class='inline-code'>&lt;h3&gt;</span>s.</p>
      <p class='h1 dots'>...</p>
    </div>
    <div class='slide-footer'>
      <p class=''>Includes 3 challenges.</p>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 39,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 7,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add the h3 selector <span class='inline-code'>h3 { }</span>.</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> To &lt;style&gt; section.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 40,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <p class='h3 pb-4 w-75'>Let's start with adding a cool border. </p>
      <p class='fs75 pt-3 text-left'>CODE PREVIEW:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'>
        border: 2px solid white;
      </div>  
    </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 41,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='h3 pb-4'><span class='inline-code'>border:</span></p>
      <p class='pb-3 w-75'>Puts a border around  text or objects.</p>
      <p class='fs75 pt-3 text-left'>EXAMPLE 1:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'>
        <div class="mx-auto mt-2 mb-2" style='border: 2px solid black; width:fit-content'>
         <div class='p-2'>border: 2px solid black;</div>
        </div>
      </div>    
      <p class='fs75  text-left'>EXAMPLE 2:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'>
        <div class="mx-auto mt-2 mb-2" style='border: 4px dashed red; width:fit-content'>
         <div class='p-2'>border:  4px dashed red;</div>
        </div>
      </div>    
    </div>      
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {    
    'slide_number' : 42,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 8,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*border((.|\n)*)\s*:((.|\n)*)\s*((white((.|\n)*)\s*solid((.|\n)*)\s*2px;)|(solid((.|\n)*)\s*2px((.|\n)*)\s*white;)|(2px((.|\n)*)\s*solid((.|\n)*)\s*white;))((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 2 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a white solid 2px border to <span class='inline-code'>h3 { }</span>.</li>
          </ul>
        </div>
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> To &lt;style&gt; section.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {     
    'slide_number' : 43,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 9,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*font-size:((.|\n)*)\s*45px;((.|\n)*)\s*padding:((.|\n)*)\s*15px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 3 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Add a font-size of  45px and a padding of 15px to <span class='inline-code'>h3 { }</span>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box fade-box-top fade-box-bottom'>
          &nbsp;&nbsp;<span class='code-fade'>border: 2px solid white;</span> <br>
          &nbsp;&nbsp;font-size: 45px;<br>
          &nbsp;&nbsp;padding: 15px;<br>
          <span class='code-fade'>}</span>
        </div>        
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> Inside h3 { } in &lt;style&gt;.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 44,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='h3 pb-4'><span class='inline-code'>margin:</span></p>
      <p class='pb-3 w-75'>Puts a spacing around text or objects</p>
      <p class='fs75 pt-3 text-left'>EXAMPLE 1:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'> <div  >
        <div class="mx-auto mt-2 mb-2" style='background: repeating-linear-gradient( 45deg, rgba(205, 97, 2, 0.45), rgba(205, 97, 2, 0.45) 10px, rgba(205, 97, 2, 0.3) 10px, rgba(205, 97, 2, 0.3) 20px );  width:fit-content;    padding: 40px;'>
       
         <div class='p-2 bg-yellow text-left fs65' style='border: 1px solid grey; width:fit-content'><span class='code-fade'>border: 1px solid grey;</span><br>margin: 20px;</div>
        </div>
        </div>
      </div>    
      </div>  
    <div class='slide-footer tips text-left'>
      <div><span class='red'>TIP:</span> The difference between margins and padding is the spacing for margins is outside of any defined borders</div>
    </div>        
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 45,   
    'action' : false,
    'checkpoint' : false,    
    'css_class' : 'trick',
    'html_content' : `
    <div>
      <p class='slide-header h2'>TRICK</p>
    </div>    
    <div>
      <p class='h3 pb-4 w-75'>This is a trick to  center your box  or text. </p>
      <p class='fs75 pt-3 text-left'>CODE PREVIEW:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'>
        margin: auto;
      </div>  
    </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 46,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 10,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*margin((.|\n)*)\s*auto((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 4 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Center your <span class='inline-code'>h3 { }</span> by  using the <strong>margin</strong> trick.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box fade-box-top fade-box-bottom'>
          &nbsp;&nbsp;<span class='code-fade'>padding: 15px;</span><br>
          &nbsp;&nbsp;margin: auto; <br>
          <span class='code-fade'>}</span>
        </div>        
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      <div class='slide-footer tips text-left'>
        <div><span class='red'>TIP:</span> Inside h3 { } in &lt;style&gt;.</div>
      </div>           
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 47,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <p class='slide-header h6'>BRIEFING : CSS Styling</p>
    </div>
    <div>
      <p class='h3 pb-4'><span class='inline-code'>max-width:</span></p>
      <p class='pb-3 w-75'>It's the maximum  width your container will become.</p>
      <p class='fs75  text-left'>EXAMPLE 1:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'> 
        <div class="mx-auto mt-2 mb-2"   >
          <div class='p-2 bg-yellow fs65 text-center' style='background: repeating-linear-gradient( 45deg, rgb(196, 232, 248), rgb(196, 232, 248) 10px, rgb(164, 219, 243) 10px, rgb(164, 219, 243) 20px ); width: 200px;'>
            max-width: 200px;
            <div class='p-0 ' style=''>
              <div class="arrow arrow-left"></div>
              <div class="arrow-line"></div>
              <div class="arrow arrow-right"></div>
            </div>
          </div>
        </div>
      </div> 
      <p class='fs75  text-left'>EXAMPLE 2:</p>
      <div class='html-code-box mb-3 bg-yellow black text-center'> 
        <div class="mx-auto mt-2 mb-2"   >
          <div class='p-2 bg-yellow fs65 text-center' style='background: repeating-linear-gradient( 45deg, rgb(196, 232, 248), rgb(196, 232, 248) 10px, rgb(164, 219, 243) 10px, rgb(164, 219, 243) 20px ); width: 260px;'>
            max-width: 260px;
            <div class='p-0 ' style=''>
              <div class="arrow arrow-left"></div>
              <div class="arrow-line"></div>
              <div class="arrow arrow-right"></div>
            </div>
          </div>
        </div>
      </div>    
    </div>  
    <div class='slide-footer tips text-left'>
      <div><span class='red'>TIP:</span> This is important if your site will be viewed on different screen sizes. Like on phones or laptops.
</div>
    </div>        
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 48,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 11,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*h3((.|\n)*)\s*{((.|\n)*)\s*max-width((.|\n)*)\s*:((.|\n)*)\s*400px;((.|\n)*)\s*}((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 5 of 5</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Beautify your h3  border by giving it a <strong>max-width</strong> of <strong>400px</strong>.</li>
          </ul>
        </div>
        <p class='fs75 pb-0 text-left'>Like this:</p>
        <div class='html-code-box fade-box-top fade-box-bottom'>
          &nbsp;&nbsp;<span class='code-fade'>margin: auto;</span><br>
          &nbsp;&nbsp;max-width: 400px;<br>
          <span class='code-fade'>}</span>
        </div>        
        <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 49,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'checkpoint',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>CHECKPOINT</p>
      <p class='fs75 w-75'>Your h3 selector should look  something like the below</p>
    </div>
    <div>
      <p class='fs75 pb-0 text-left'>Like this:</p>
      <div class='html-code-box fade-box-top fade-box-bottom fs75'>
        <span class='code-fade'>&nbsp;&nbsp;}</span><br>
        &nbsp;&nbsp;h3 {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;border: 2px solid white;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;font-size: 45px;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;padding: 15px;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;margin: auto;<br>
        &nbsp;&nbsp;&nbsp;&nbsp;max-width: 400px;<br>
        &nbsp;&nbsp;}<br>
        <span class='code-fade'>&lt;/style&gt;</span>
      </div> 
    </div>     
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 50,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <div class='h3 pb-4'>One last thing…</div>
      <p class='h5 w-75 pb-3'>Give your <span class='inline-code'>&lt;h1&gt;</span> some class, by changing the font color.</p>
      <p class='h5 w-75 pb-3'>Do you like it more <span style='color:turquoise'>turquoise</span>?</p>
      <p class='h5 w-75 pb-5'>Or <span style='color:deeppink'>deeppink</span>?</p>
      <img class='swiper-lazy' data-src='../img/emoji/72/crown.png' alt=''>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 51,   
    'action' : false,
    'checkpoint' : false,
    'html_content' : `
    <div>
      <p class=' pb-4'>Like this:</p>
      <div class='html-code-box fade-box-top fade-box-bottom fs75'>
        <span class='code-fade'>&nbsp;&nbsp;h1 {<br>
        &nbsp;&nbsp;&nbsp;&nbsp;font-size: 75px;<br></span>
        &nbsp;&nbsp;&nbsp;&nbsp;color: deeppink;<br>
        <span class='code-fade'>}</span>
      </div> 
    </div> 
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 52,     
    'action' : true,
    'checkpoint' : true,
    'checkpoint_id' : 12,
    'reg' : [ '(<style>|<style [^>]*>)((.|\n)*)\s*h1((.|\n)*)\s*{((.|\n)*)\s*color((.|\n)*)\s*white;((.|\n)*)\s*}((.|\n)*)\s*i((.|\n)*)\s*{((.|\n)*)\s*font-size((.|\n)*)\s*25px((.|\n)*)\s*<\/style>' ], 
    'css_class' : 'challenge cp_yellow', 
    'html_content' : `
      <div>
        <h2 class='mb-0'>Challenge</h2>
        <p class='fs75'>STEP 1 of 1</p>
      </div>
      <div>
        <div class='text-left pb-3'>
          <p class='blue text-uppercase'>Code:</p>
          <ul class='list-none'>
            <li class='tasks'>Make your h1 text white.</li>
          </ul>
        </div>
          <div class='button-locked'>
          <a class='btn-action swiper-editor'>Let's get Coding <i class='icon-arrow-forward'></i></a> <br>
          <a class='swiper-next skip' style=''>Skip this step</a> 
        </div>
        <div class='button-unlocked'><a class='btn-success swiper-next'>I did it <i class='icon-sentiment-satisfied'></i></a>
        </div>
      </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {  
    'slide_number' : 53,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 pb-4'>CONGRATS!</div>
      <div style='background-color: #6aa2c7;border-radius:50%;margin: 40px auto;width: 200px;height: 200px;'>
        <div class='lesson-instructions '><img style='padding-top:20px;' class='w-75 swiper-lazy mb-3' data-src='../img/lessons/congrats_training_sml.png'></div>
      </div>
      <div class='h3 pb-4'>You've finished<br> Training 4</div>  
    </div>    
    `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 54,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 pb-0 green'>GUESS WHAT.</div>
      <div class='h3'>JUST HAPPENED ???</div>
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/thinking-face.png' alt=''>
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/flushed-face.png' alt=''>
      <img class='pt-5 w-20 swiper-lazy' data-src='../img/emoji/72/flushed-face.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 55,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3 w-75'>You just completed PROJECT 1.</div>
      <img class='pt-5 w-20 swiper-lazy' data-src='../img/emoji/72/person-raising-both-hands-in-celebration-type-5.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 56,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h5 pb-5'>That’s so amazing.</div>
      <div class='h3'>It’s time to  PAAARTYYYY!!!</div>
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/party-popper.png' alt=''> 
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/party-popper.png' alt=''> 
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/party-popper.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 57,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'play',    
    'html_content' : `
    <div>
      <p class='slide-header h2'>Play</p>
      <p class='slide-header fs75'>WANT TO HAVE SOME FUN?</p>
    </div>
    <div>
      <p class='h3 pb-4'>Play with your border style.</p>
      <p class='pb-1 w-75'>Try these borders:</p>
      <div class='html-code-box mb-3 bg-white black text-center'> 
        solid&nbsp;&nbsp;&nbsp;dotted<br>
        dashed&nbsp;&nbsp;&nbsp;double<br>
      </div> 
    </div>  
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, { 
    'slide_number' : 58,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
      <div>
      </div>    
      <div>
        <div class='h2'>Want to win some Amazing Prizes?</div>
        <img class='pt-1 pb-3 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/wrapped-present.png' alt=''><img class='pt-1 pb-3 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/wrapped-present.png' alt=''> 
        <div class='h5 mt-4'>Enter your code into our Code Challenge 2020 competition</div>
      </div>
      <div class='slide-footer tips' style='padding: .75em 60px;'>
        <span class='red'>Tip:</span> 
        If you are ready to submit your Project then swipe up.
      </div>   
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 59,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      </div>
    <div class='register-CodeChallenge'>
      <div class='h3'>Hmmm... doesn't look like you have an account?</div>
      <img class='pt-2 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/thinking-face.png' alt=''> 
      <div class='h5 pt-4 '>To enter the competition you need to first be registered and logged in</div>
      <a class='btn-action register-button'>Register Now</a>
    </div>
    <div class='enter-CodeChallenge d-none'>
      <div class='h4'>Don't forget that the best website design takes the prize.</div>
      <img class='pt-1 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/first-place-medal.png' alt=''>       
      <div class='h5 pt-4'>Done creating your own awesome website?</div>
      <a class='btn-success submit-CodeChallenge mt-3'>Yes, Let's Do This</a>
      <div class='btn-success bg-silver swiper-editor mt-3'>I'll keep coding</div>
    </div>
    <div class='slide-footer tips' style='padding: .75em 60px;'>
      <span class='red'>Tip:</span> 
      Competition entries are open from 04-31 Oct, 2020
    </div>     
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 60,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h3'>Tell your  friends and  celebrate  together.</div>
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/man-dancing-type-4.png' alt=''> 
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/balloon.png' alt=''> 
      <img class='pt-5 pl-2 w-20 swiper-lazy' data-src='../img/emoji/72/dancer-type-6.png' alt=''> 
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 61,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h4 w-75'>See you in PROJECT 2.</div>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  }, {
    'slide_number' : 62,   
    'action' : false,
    'checkpoint' : false,
    'css_class' : 'briefing',    
    'html_content' : `
    <div>
      <div class='h0  pb-4 yellow'>BYE.</div>
      <div class='h3'>See you soon.
</div>
    </div>
      `,
    'created_at' : '',  
    'updated_at' : ''
  } ]
}
    
var hintsForCheckPonts = {

}
  
var hints_data =   `


    ` 
 
/// Add custom JS for lesson below here


function previewLessonModal() {
  $.galleryDialog({
    modalName: 'preview-lesson-modal',
    htmlContent: `
    <iframe  srcdoc="
<head>
  <style>
    h1 {
      font-size: 75px;
      color: deeppink;
    }
    i {
      font-size: 25px;
    }
    header {
      background: linear-gradient(110deg, yellow 40%, pink 40%);
    }
    section {
      background: lightgrey;
    }
    footer {
      background: black;
      color: white;
    }
    div {
      text-align: center;
      padding: 40px;
    }
    h3 {
      border: 2px solid white;
      font-size: 45px;
      padding: 15px;
      margin: auto;
      max-width: 400px;
    }
  </style>
</head>

<body>
  <header>
    <div>
      <h1>My Name</h1>
      <section>
        <div>
          <h3>Motivation:
            <br><br>
            <i>I want to learn to code because I love building new things.</i>
          </h3>
        </div>
      </section>
      <h3>Launching Soon...</h3>
      <p>01 January 2000</p>
    </div>
  </header>
  <footer>
    <div>
      &copy 2021 My Name
    </div>
  </footer>
</body>

  "></iframe>
    `
  })
}



/// Add custom JS for lesson below here

function onlessonLoaded() {

  $('.submit-CodeChallenge').click(function() {
    console.log('submitCodeChallengeModal');
    submitCodeChallengeModal('submit-lesson-url');
  }) 
}

