var lesson_data = 
{
    "kbLayout": "", // not currently in use
    "loadJS": "", // not currently in use
    "prevLessonID": "", // Lesson ID of previous lesson where to load user's code
    "nextLessonSlug": "", // not currently in use
    "pageDesc": "Learn how to build your first website with these easy intro lessons to coding.", 
    "pageKeywords": "coding, code, learn to code, code website, my first website", 
    "pageTitle": "CodeJIKA - Project 1, Training 1",
    "total_slides": 43, // set how many slides in total
    "save_lesson_id": "P1Training1", // This is id that will be used to store save code in Firebase
    "slides": [ 
    ], // not currently in use
    "slug": "", // not currently in use
    "defaultCode": // default code, if user has not already started coding   
`



` 
}
var check_points = {
  16:"(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>",
  17: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*<\/body>",
  23: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  24: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*<\/body>",
  32: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*Soon...((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  33: "(<head>|<head [^>]*>)((.|\n)*)\s*<\/head>((.|\n)*)\s*(<body>|<body [^>]*>)((.|\n)*)\s*(<h1>|<h1 [^>]*>)((.|\n)*)\s*<\/h1>((.|\n)*)\s*(<h3>|<h3 [^>]*>)((.|\n)*)\s*<\/h3>((.|\n)*)\s*<\/body>",
  36:"(<p>|<p [^>]*>)((.|\n)*)\s*(2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030)((.|\n)*)\s*<\/p>"
}

var hintsForCheckPonts = {
/*  16:"A closing tag has a forward slash <strong>/<strong>",
  17:"Remember to open <span class='html-code'> &ldquo;&lt;&gt;&rdquo;</span> and close <span class='html-code'>&ldquo;&lt;/&gt;&rdquo;</span> your tag. Refer to the previous challenge(step 1) on how to open and close a tag.",
  23:"In the body section. Hint given as tip on bottom of the slide.",
  24:"Example: <strong><span class='html-code'>&lt;h1&gt;</span>John Doe <span class='html-code'>&lt/h1&gt</span></strong>",
  32:"Type your code below the closing <strong><span class='html-code'>&lt/h1&gt;</span></strong> tag.",
  36:"Just below the <strong><span class='html-code'>&lt;/h3&gt;</span></strong> tag"*/
}
  
var hints_data =   `

  <p style="margin-bottom:0px;">Slide: 12</p>
  <pre style="margin-bottom:10px; color:#ccc;">&lt;h1&gt;Nomzamo Mbatha&lt;/h1&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 14</p>
  <pre style="margin-bottom:10px;">&lt;p>Hi! I'm Nomzamo Mbatha, An Actress. Say Hello!&lt;/p&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 19</p>
  <pre style="margin-bottom:10px;">&lt;input type="email"&gt;</pre>
  <p style="margin-bottom:0px;">Slide: 23</p>	  
  <pre style="margin-bottom:10px;">&lt;input type="submit" &gt;</pre>
  <p style="margin-bottom:0px;">Slide: 27</p>
  <pre>placeholder="Your email"</pre>

` 

  
var slides_data = ` 

  <div class="tab-pane tab-pane-slide active" id="slide1" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide1.PNG">
    <a class="btn btn-primary next" style="top:65%;">Start Slideshow →</a>
  </div>
  <div class="tab-pane tab-pane-slide" id="slide2" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide2.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide3" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide3.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide4" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide4.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide5" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide5.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide6" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide6.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide7" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide7.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide8" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide8.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide9" role="tabpanel">
    <img src="/learn/lessons/P001-T01-D-V001/img/Slide9.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide10" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide10.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide11" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide11.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide12" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide12.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide13" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide13.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide14" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide14.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide15" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide15.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide16" data-checkpoint_id="1" role="tabpanel">

    <div class="checkpoint">
      <h2>

        Challenge
      </h2>
      <span class="challange-step" style="color:black;"> Step 1</span>


      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle  square-icon" style="color:#808080"></i>
          <span class="fa fa-check "></span>
          Write this in the editor below:<br>
          <div class="html-code-box">
            &lt;head&gt;<br>&lt;/head&gt;
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>
    </div>

  </div>


  <div class="tab-pane tab-pane-slide" id="slide17" data-checkpoint_id="2" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black"> Step 2</span>


      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon" style="color:#808080"></i>
          <span class="fa fa-check"></span>

          Add a <b>body section</b> below your head section.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Check the “Structure” slide.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide18" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide18.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide19" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide19.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide20" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide20.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide21" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide21.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide22" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide22.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide23" data-checkpoint_id="3" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black"> Step 3</span>


      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon" style="color:#808080"></i>
          <span class="fa fa-check"></span>

          Type an opening &amp; closing <b>
            <span class="html-code">
              &lt;h1&gt;
            </span></b> tag.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        In the <span class="html-code">&lt;body&gt;</span> section.
      </div>
    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide24" data-checkpoint_id="4" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>
      <span class="challange-step" style="color:black"> Step 4</span>


      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon" style="color:#808080"></i>
          <span class="fa fa-check"></span>

          Insert your <b>First</b> &amp; <b>Last</b> name between the <span class="html-code">&lt;h1&gt;</span> tags.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Tip:</span>
        Check the example in the &lt;h1&gt; briefing.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide25" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide25.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide26" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide26.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide27" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide27.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide28" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide28.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide29" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide29.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide30" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide30.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide31" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide31.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide32" data-checkpoint_id="5" role="tabpanel">
    <div class="checkpoint">
      <h2>

        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon" style="color:#808080"></i>
          <span class="fa fa-check"></span>
          Write: <b>Launching Soon...</b> between <span class="html-code">&lt;h3&gt;</span> tags.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span>
        Below <span class="html-code">&lt;/h1&gt;</span> tag.
      </div>
    </div>
  </div>


  <div class="tab-pane tab-pane-slide" id="slide33" data-checkpoint_id="6" role="tabpanel">
    <div class="checkpoint">
      <h2>
        Challenge

      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon" style="color:#808080"></i>
          <span class="fa fa-check"></span>
          Write: <b>Launching Soon...</b> between <span class="html-code">&lt;h3&gt;</span> tags.
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span>
        Below <span class="html-code">&lt;/h1&gt;</span> tag.
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide34" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide34.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide35" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide35.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide36" data-checkpoint_id="7" role="tabpanel">

    <div class="checkpoint">
      <h2>
        Challenge
      </h2>

      <ol class="actions">
        <li data-index="0">
          <i class="far fa-circle square-icon" style="color:#808080"></i>
          <span class="fa fa-check"></span>

          Add a <span class="html-code">&lt;p&gt;</span> with today's date<br>EXAMPLE<br>
          <div class="html-code-box">
            &lt;p&gt;<br>&nbsp;&nbsp;10 October, 2019<br>&lt;/p&gt;
          </div>
        </li>
      </ol>
      <a class="btn btn-primary next check" style="toXp:65%;">Skip this →</a>

      <div class="challenge-tip"><span>Where:</span>
        Below <span class="html-code">&lt;h3&gt;</span>
      </div>
    </div>
  </div>

  <div class="tab-pane tab-pane-slide" id="slide37" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide37.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide38" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide38.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide39" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide39.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide40" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide40.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide41" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide41.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide42" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide42.PNG">
  </div>
  <div class="tab-pane tab-pane-slide" id="slide43" role="tabpanel">
    <img class="lozad" data-src="/learn/lessons/P001-T01-D-V001/img/Slide43.PNG">
    <a href="/learn/P1Training2" class="btn btn-primary" style="top:65%;">Start next training →</a>
  </div>

` 
/// Add custom JS for lesson below here