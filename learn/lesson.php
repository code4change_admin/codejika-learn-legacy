<?php
//Header('Vary: User-Agent, Accept');
$home_url = 'https://' . $_SERVER['SERVER_NAME'];
?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <script type="text/javascript">
    var start = Date.now();
  </script>
  
  <title>Code JIKA - Learn HTML, CSS and Javascript</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
  <meta id="description" name="description" content="CodeJIKA Learn">
  <meta id="keywords" name="keywords" content="coding, learn to code, codejika, africa, south africa">
  <link href='../libraries/bootstrap/css/bootstrap.min.css' rel='stylesheet' />
  <link rel="stylesheet" href="../libraries/swiper/css/swiper.min.css"/>

  <!--link rel="stylesheet" href="../css/theme/base16-dark.css"/-->  

  <!--link href="<?php echo $home_url; ?>/css/merged.min.css" rel="stylesheet" type="text/css" /-->
  <link rel="stylesheet" href="../css/jquery.modal.min.css" />
  <!--script src="../js/merged_single_cdns.js"></script-->

  <link href="../libraries/fontawesome/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="../libraries/swiper/css/swiper.min.css"/>
  <script defer src="../libraries/fontawesome/js/all.js"></script>
  <script src="../js/js.cookie.js"></script>

  <script type="text/javascript">
    //<![CDATA[

    function randTxt() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
    }

    var randText = randTxt();

    if (Cookies.get('devicetype') == 'desktop'){
      var device_type = "desktop";
      document.write('<link rel="stylesheet" type="text/css" href="../css/codemirror.min.css"\/>');  
       document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url; ?>/css/mobile-projects.css?" + randText + "'\/>");      
    } else {
      var device_type = "mobile";
    }    

    document.write("<link rel='stylesheet' type='text/css' href='<?php echo $home_url; ?>/css/learn.css?" + randText + "'\/>");
   
  </script>
  <style>

  </style>
</head>

<body class="is-lessons">
  <div class="swiper-container swiper-container-m">
    <div class="swiper-wrapper justifyX-content-end">

      <div class="swiper-slide" id="content_page">
        <div class="menu-button toggle-menu">
          <div class="bar"></div>
          <div class="bar"></div>
          <div class="bar"></div>
        </div>        
        <div id="header" class="embed-nav" style="">
          <a href="<?php echo $home_url; ?>/learn/" class="p-0 m-0"><img src="<?php echo $home_url; ?>/img/jika_logo.png" alt="logo" id="logo" style="" /></a>
          <ul id="lesson-navbar" class="lesson-navbar mobile-only" style="">
            <li id="menu-lesson" class="active"><a><span>SLIDES</span><i class="icon-sentiment_satisfied"></i></a></li>

            <li id="menu-code" class=""><a><span>EDITOR</span><i class="icon-code1"></i></a></li>
            <li id="menu-preview" class=""><a><span>BROWSER</span><i class="icon-personal_video"></i></a></li>
          </ul>
          <ul id="" class="lesson-navbar desktop-only" style="">
            <li id="menu-lesson" class="">
              <a class="nav-link" href="/learn/projects"><span>Projects</span><i class="icon-local_library"></i></a>
            </li>
            <li id="menu-code" class="">
              <a class="nav-link create-GalleryPage"  data-toggle="modal" data-target="#createGalleryPage"><span>Gallery</span><i class="icon-images icomoon"></i>
            </li>
          </ul>          
          <ul id="menu-navbar" class="hide" style="background-color: #009688;padding:0;position: absolute; right: 0; top: 0; border: 0;">
            <li id="menu-profile"><a><i class="icon-dehaze"></i></a></li>
          </ul>
        </div>        
 
 <script type="text/javascript">

 if (device_type == 'mobile'){
      document.write( ` 
        <div class="loading">
          <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
          </div>
        </div>       
        <div class="swiper-container swiper-container-h mobile-only">
          <div class="fade-background"></div>
          <div class="swiper-wrapper">
            <div class="swiper-slide" id="lesson-page">
              <div class="swiper-container swiper-container-v">
                <div class="swiper-wrapper ">
                </div>
                <div class="swiper-scrollbar swiper-scrollbar-v"></div>
              </div>
              <div id="help-icon" class="">
                <i class="d-none fas fa-question-circle fa-2x"></i>
                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="lightbulb-on" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-lightbulb-on fa-w-20 fa-2x"><path fill="currentColor" d="M112,192a24,24,0,0,0-24-24H24a24,24,0,0,0,0,48H88A24,24,0,0,0,112,192Zm-4.92,95.22-55.42,32a24,24,0,1,0,24,41.56l55.42-32a24,24,0,0,0-24-41.56Zm24-232-55.42-32a24,24,0,1,0-24,41.56l55.42,32a24,24,0,1,0,24-41.56ZM520.94,100a23.8,23.8,0,0,0,12-3.22l55.42-32a24,24,0,0,0-24-41.56l-55.42,32a24,24,0,0,0,12,44.78ZM616,168H552a24,24,0,0,0,0,48h64a24,24,0,0,0,0-48ZM588.34,319.23l-55.42-32a24,24,0,1,0-24,41.56l55.42,32a24,24,0,0,0,24-41.56ZM320,0C217.72,0,144,83,144,176a175,175,0,0,0,43.56,115.78c16.63,19,42.75,58.8,52.41,92.16V384h48v-.12a47.67,47.67,0,0,0-2.13-14.07C280.25,352,263,305.06,223.66,260.15A127.48,127.48,0,0,1,192.06,176C191.84,102.36,251.72,48,320,48a127.91,127.91,0,0,1,96.34,212.15c-39.09,44.61-56.4,91.47-62.09,109.46A56.78,56.78,0,0,0,352,383.92V384h48V384c9.69-33.37,35.78-73.18,52.41-92.15A175.93,175.93,0,0,0,320,0Zm0,80a96.11,96.11,0,0,0-96,96,16,16,0,0,0,32,0,64.08,64.08,0,0,1,64-64,16,16,0,0,0,0-32ZM240.06,459.19a16,16,0,0,0,2.69,8.84l24.5,36.83A16,16,0,0,0,280.56,512h78.85a16,16,0,0,0,13.34-7.14L397.25,468a16.2,16.2,0,0,0,2.69-8.84L400,416H240Z" class=""></path></svg>
              </div>
            </div>
            <div class="swiper-slide" id="editor-page">
<div class="intro1-place"></div>
              <!-- <textarea id='code-editor' name='editor' style="display: none;visibility:hidden;" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false">

              </textarea> -->
              <!-- <div id="editor" contenteditable="true" class="banana-cake" style="width: 100%; height:100%"></div> -->
              <!-- <div id="lineNo"></div> -->
              <textarea onchange="editorChange()" id="editor" style="width: 100%; height:100%; display:inherit" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"></textarea>
              <!-- onkeyup="getLineNumber(this, document.getElementById('lineNo'));" -->

              <div class="row bottom_keys">
                <button class="button_design" onclick="btclicked('<')">
                  < </button> <button class="button_design" onclick="btclicked('>')"> >
                </button>
                <button class="button_design" onclick='btclicked("double_quote")'> " </button>
                <button class="button_design" onclick='btclicked("single_quote")'> ' </button>
                <button class="button_design" onclick="btclicked('/')"> / </button>
                <button class="button_design" onclick="btclicked('=')"> = </button>
              </div>
            </div>
            <div class="swiper-slide" id="preview_page">
              <iframe class="preview-iframe" id='mobile-preview' name="preview"></iframe>

              <div class="swipe-overlay"></div>
              <div id="disallow-interact" class="icon-locked">
                <i class=" icon-uniE011 fs2"></i>
                <div>Swipe Mode</div>
              </div>
              <div id="allow-interact" class="icon-unlocked hide">
                <i class="icon-uniE010 fs2"></i>
                <div>Input Mode</div>
              </div>
            </div>
            <div class="swiper-pagination swiper-pagination-h"></div>
          </div>
        </div>
      `);
    } else {
      document.write( `
        <div class="flex-row d-flex flex-fill no-gutters flex-nowrap desktop-only" id="main_page">
          <div class='contXainer' style="padding:0;height: 100%;">
            <div class='flex-row d-flex no-gutters flex-nowrap overflow-hidden resizer-parent' style='height: 100%;'>
              <div class='flex-column d-flex content split-horizontal  col-lesson-code ' style=' '>
                <div class='no-gutter lesson-skills'>
                  <div class="tab-pane active" id="lesson_tab" role="tabpanel" style="" aria-expanded="true">
                    <div class="slides">
                      <div class="loading d-none">
                        <div class="spinner">
                          <div class="rect1"></div>
                          <div class="rect2"></div>
                          <div class="rect3"></div>
                          <div class="rect4"></div>
                          <div class="rect5"></div>
                        </div>
                      </div>
                      <div id="lesson-page" class="swiper-container swiper-container-d tab-pane">
                        <div class="swiper-wrapper">
                        </div>
                        <div class="swiper-pagination swiper-pagination-d"></div>
                      </div>
                    </div>
                    <div class="row pagination ">
                      <div class="col-sm-12 no-gutter">
                        <div class="progress">
                          <div id="lessonProgressBar" class="progress-bar progress-bar-success" role="progressbar" style="">
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row remove-all-margin">
                          <div class="col-sm-4 text-left no-gutter">
                            <div class="prev" href="#" aria-label="Previous Slide">
                              <span>< Previous</span>
                            </div>
                          </div>
                          <div class="col-sm-4 text-center no-gutter">
                            <div class="lessonProgressText"></div>
                          </div>
                          <div class="col-sm-4 text-right no-gutter">
                            <div class="next" href="#" aria-label="Next Slide">Start Slideshow
                            </div>
                          </div>
                        </div>
                      </div>
                      <div style="position: relative; width: 100%; z-index: 10;">
                        <div class="previous-overlay-btn-container swiper-button-prev prev">
                        </div>

                        <div class="next-overlay-btn-container swiper-button-next next">
                        </div>
                      </div>
                    </div>  
                    <div class="hint-popup-checked"></div>
                  </div>
                </div>
                
                <div class='no-gutter code-editor' style=''>
                  <!--<div class=""><div class=""></div>
                            <a onclick="close_hint()" class="close-hint-popup" style="display:none"><i class="fas fa-times-circle fa-lg"></i></a>
                          </div>-->
                  <textarea id='myeditor' name='myeditor' style="display: none;">

                  </textarea>
                </div>
              </div>
              <div class='flex-column d-flex content split-horizontal  col-preview ' style=''>
                <div class='' style='    height: 100%;'>
                  <div id="cloak" class="hint-popup">
                    <div class="hint-text"></div>
                    <a onclick="close_hint()" class="close-hint-popup"><i class="fas fa-times-circle fa-lg"></i></a>
                  </div>
                  <textarea id='myeditor' name='myeditor' style="display: none;">

                          </textarea>
                  <iframe class="preview-iframe" id='desktop-preview'>


                  </iframe>

                </div>
              </div>
            </div>
          </div>
        </div> 
      `);
    }
  </script>
        <div class="fade-background"></div>       
      </div>
      <div class="swiper-slide" id="profile_page">
        <div class="container pt-0">
          <i class="icon-account_circle fs5 d-none" style="display: block;"></i>
          <h2 class="profile_name mt-4 mb-3">Hello</h2>
          <div class="menu">
            <ul>
               <h4 class="profile_name text-center white mb-1 " >Menu</h4>             
              <li class=""><a class="divLink" href="<?php echo $home_url; ?>"></a><i class="icon-home fs2"></i> Homepage</li>
              <li class="projects-page"><a class="divLink" href="<?php echo $home_url; ?>/learn/projects"></a><i class="icon-local_library"></i> Explore Projects</li>
              <li class="reset-lesson" data-toggle="modal" data-target="#reset-lesson"><i class="icon-replay fs2"></i> Reset Current Lesson</li>
              <li class="create-GalleryPage" data-toggle="modal" data-target="#createGalleryPage"><i class="icon-images icomoon"></i> Code Gallery</li>
              <li  class="d-none"><i class="icon-personal_video"></i> View as Desktop</li>             
               <h4 class="profile_name text-center white mb-1 mt-3" >My Profile</h4>
              <li class="login-button pointer" style="width: 45%;display: inline-block;" data-dismiss="modal"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i> Login</li>
              <li class="register-button pointer" style="width: 53%;display: inline-block;" data-dismiss="modal"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i> Register</li>
              <li class="create-profilePage profile-button d-none" data-toggle="modal" data-target="#createProfilePage"><i class="icon-account_circle fs2"></i>Edit My Profile</li>
              <li class=""><i class="icon-face fs2"></i> Instructor <div class="option_selected select_robot" style="margin-left: 5px;">Robot</div>
                <div class="option_unselected select_lego">Lego</div>
              </li>
              <li class="logout-button pointer d-none" data-dismiss="modal"><i class="icon-sign-in fs2" style="font-family:icomoon !important;"></i> Logout</li> 
              <li id="share-link" class="hide"><i class="icon-assignment fs2"></i> Share on: <div class="option_unselected facXebook-share" data-js="facebook-share" style="margin-left: 5px;">FB</div>
                <div class="option_unselected twitXter-share" style="margin-left: 5px;" data-js="twitter-share">TW</div> <a id="whatsapp" href="">
                  <div class="option_unselected ">WA</div>
                </a>
              </li>
              <center><p id="switch-device"> View as <span class="mobile-only">Desktop</span><span class="desktop-only">Mobile</span></p></center>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </div>




  <div class="modalPlaceholder"></div>
 

  <div class="success-animation hide" style="">

    <span class="success-layers">
      <svg class="success-svg-icon" style="color: #fff;" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
        <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
      </svg><!-- <i class="fas fa-circle" style="color:#fff"></i> -->
      <svg class="success-svg-icon" style="color: #00A921;" aria-hidden="true" data-prefix="fas" data-icon="check-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
        <path fill="currentColor" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path>
      </svg><!-- <i class="fas fa-inverse fa-check-circle" style="color:#00A921"></i> -->
    </span>

    </span>
  </div>

<!--   Core JS Files   -->
<script src='../js/core/jquery.3.2.1.min.js' type='text/javascript'></script>
<script src='../libraries/bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
<script src='../libraries/popper/popper.min.js' type='text/javascript'></script>
<script src='../libraries/tippy/tippy-bundle.umd.min.js' type='text/javascript'></script>
<link rel="stylesheet" href="../libraries/tippy/themes/light.css"/>
<script src="../libraries/firebase/firebase-app.js"></script>
<script src="../libraries/firebase/firebase-database.js"></script>
<script src="../libraries/swiper/js/swiper.min.js"></script>
<script src='../js/jquery.modal.min.js' type='text/javascript'></script>
<script defer src="../libraries/fontawesome/js/all.js"></script>
<script src='../js/jquery.dateFormat.min.js' type='text/javascript'></script>


<?php
function get_all_get()
{
        $output = "?"; 
        $firstRun = true; 
        foreach($_GET as $key=>$val) { 
        if($key != $parameter) { 
            if(!$firstRun) { 
                $output .= "&"; 
            } else { 
                $firstRun = false; 
            } 
            $output .= $key."=".$val;
         } 
    } 

    return $output;
} 

/*
function dev_type()
{
  if ( getenv("DEVICE_TYPE") == "desktop" ) {
    return "desktop";
  } 
  else if ( getenv("DEVICE_TYPE") == "mobile" ) {
    return "mobile";
  } 
  else {
    return "unknown";
  }     
}  
*/
  
?>

<script type="text/javascript">

  var page_template = "lessons";
  var lessons_data_mobile = {
    "5-minute-website": "INTRO-5MIN-M-V007",
    "5-minute-website-new": "INTRO-5MIN-M-V007",
    "the-party": "INTRO-JS_PARTY-M-V001",
    "P1Training1": "P001-T01-M-V008",
    "P1Training2": "P001-T02-M-V001",
    "P1Training3": "P001-T03-M-V001",
    "P1Training4": "P001-T04-M-V001",
    "P2Training1": "P002-T01-M-V001",
    "P2Training2": "P002-T02-M-V001",
    "P2Training3": "P002-T03-M-V001",
    "P2Training4": "P002-T04-M-V001",
    "P2Training5": "P002-T05-M-V001",
    "P2Training6": "P002-T06-M-V001",
  };

  var lessons_data_desktop = {
    "5-minute-website": "INTRO-5MIN-D-V005",
    "the-party": "INTRO-JS_PARTY-M-V001",    
    "P1Training1": "P001-T01-D-V004",
    "P1Training2": "P001-T02-D-V002",
    "P1Training3": "P001-T03-D-V002",
    "P1Training4": "P001-T04-D-V002",
    "P2Training1": "P002-T01-D-V002",
    "P2Training2": "P002-T02-D-V002",
    "P2Training3": "P002-T03-D-V002",
    "P2Training4": "P002-T04-D-V002",
    "P2Training5": "P002-T05-D-V002",
    "P2Training6": "P002-T06-D-V002",
	"P3Training1": "P003-T01-D-V001",
  };

  var lessson_url = "<?php echo htmlspecialchars($_GET["lesson_id"]); ?>";
  //var lessson_url = "P1Training1";  // used to manually set for testing
  var set_user_id = "<?php echo htmlspecialchars($_GET["user_id"]); ?>";

  if (device_type == 'desktop'){
    var lesson_id = lessons_data_desktop[lessson_url] ? lessons_data_desktop[lessson_url] : "ComingSoon";

    document.write(`   
      <script src="../libraries/codemirror/lib/codemirror.js"><\/script>
      <script src='../js/split.min.js'><\/script>
      <script src="../libraries/codemirror/mode/javascript/javascript.js"><\/script>
      <script src="../libraries/codemirror/mode/xml/xml.js"><\/script>
      <script src="../libraries/codemirror/mode/css/css.js"><\/script>
      <script src="../libraries/codemirror/mode/htmlmixed/htmlmixed.js"><\/script>
      <script src="../libraries/codemirror/addon/edit/closetag.js"><\/script>
      <script src="../libraries/codemirror/addon/edit/matchbrackets.js"><\/script>
      <script src="../libraries/codemirror/addon/search/match-highlighter.js"><\/script>
      <script src="../libraries/codemirror/htmlhint.js"><\/script>
      <script src="../libraries/codemirror/csslint.js"><\/script>
      <script src="../libraries/codemirror/jshint.js"><\/script>
      <script src="../libraries/codemirror/addon/lint/lint.js"><\/script>
      <script src="../libraries/codemirror/addon/lint/html-lint.js"><\/script>
      <script src="../libraries/codemirror/addon/lint/css-lint.js"><\/script>
      <script src="../libraries/codemirror/addon/lint/javascript-lint.js"><\/script>
      <script src="../libraries/codemirror/markdown.js"><\/script>
      `);
  } else {
    var lesson_id = lessons_data_mobile[lessson_url] ? lessons_data_mobile[lessson_url] : "ComingSoon";
  }

  console.log("PHP GET/query string: <?php echo get_all_get(); ?>" );
  console.log("lesson_url: " + lessson_url);
  console.log("lesson_id: " + lesson_id);
  console.log("set_user_id: " + set_user_id);

  document.write("<link href='lessons/" + lesson_id + "/custom.css?" + randText + "' rel='stylesheet' />");
  document.write("<script type='text/javascript' src='lessons/" + lesson_id + "/lesson_data.js?" + randText + "'><\/script>");
  document.write("<script type='text/javascript' src='../js/common-learn.js?" + randText + "'><\/script>");
  if (Cookies.get('devicetype') == 'desktop'){
    document.write("<script type='text/javascript' src='../js/desktop-lesson.js?" + randText + "'><\/script>");
  } 
  document.write("<script type='text/javascript' src='../js/mobile-lesson.js?" + randText + "'><\/script>");



</script>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"OEZCv1zDGU20kU", domain:"codejika.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=OEZCv1zDGU20kU" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> 
</body>

</html>
